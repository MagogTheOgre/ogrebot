<?php
require_once __DIR__ . "/../base/bootstrap.php";
global $env;

// this script takes quite a long time to run
ini_set("max_execution_time", 20 * SECONDS_PER_MINUTE);

$POST = $env->get_request_args(true);
$project = $POST['project'] ?? null;
$start = $POST['start'] ?? null;

$errorMessage = "";
if (!preg_match("/^20\d{12}$/", $start)) {
    $errorMessage = "No start date specified";
} else if ($project !== "commons.wikimedia") {
    $errorMessage = "Invalid project";
} else {
    
    if (!file_exists(UPDATE_POLL_DIRECTORY)) {
        mkdir(UPDATE_POLL_DIRECTORY, 0775);
    }
    
    Local_Io::file_put_contents_ensure(UPDATE_POLL_DIRECTORY . "new-$start", "");
}
?><!DOCTYPE html>
<html>
<head></head>
<body>
<?php
if ($errorMessage) {
    ?> $errorMessage <?php 
} else {
	?><p>Your request has been submitted and will update in the next few minutes.</p><?php
}?>
</body>
</html>
