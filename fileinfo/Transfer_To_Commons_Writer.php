<?php
class Transfer_To_Commons_Writer {
	
	/**
	 *
	 * @var int
	 */
	const INCLUDE_AUTHOR_DATE = 1;
	
	/**
	 *
	 * @var int
	 */
	const INCLUDE_LICENSE = 2;
	
	/**
	 *
	 * @var int
	 */
	const INCLUDE_FIELDS = 4;

	public function __construct(
        private readonly Project_Data $project_data,
        private readonly string $text,
        private array $imageinfos) {
		global $logger;
		
		$logger->info(
			"__construct($project_data, string(" . strlen($text) . "), array[" .
				 count($imageinfos) . "])");
	}

    /**
     *
     * @throws ProjectNotFoundException
     */
	private function relink(string $text): string {
		global $MB_WS_RE_OPT;
		
		/* parse wikilinks */
		preg_match_all("/\[\[$MB_WS_RE_OPT:?$MB_WS_RE_OPT(.+?)$MB_WS_RE_OPT]]/u", $text, $matches,
			PREG_OFFSET_CAPTURE);
		
		for($i = count($matches[0]) - 1; $i >= 0; $i--) {
			$linktext = $matches[1][$i][0];
			
			// relink
			if (preg_match("/^(.+?)$MB_WS_RE_OPT\|$MB_WS_RE_OPT(.+?)$/u", $linktext, $match)) {
				$linktext = $this->project_data->formatPageLink($match[1], $match[2]);
			} else {
				$linktext = $this->project_data->formatPageLinkAuto($linktext);
			}
			
			// save results to string
			$text = substr($text, 0, $matches[0][$i][1]) . $linktext .
				 substr($text, +$matches[0][$i][1] + strlen($matches[0][$i][0]));
		}
		
		return $text;
	}

    /**
     *
     * @param string[] $field_names
     * @throws ProjectNotFoundException
     */
	private function information_template_field(Template $tmp, array $field_names): string {
		$value = null;
        foreach ($field_names as $fieldname) {
			$value = $tmp->information_style_fieldvalue($fieldname);
			if ($value !== null) {
				break;
			}
		}
		if ($value === null) {
			$value = "";
		}
		$value = Wiki_String_Utils::filter_templates($value);
		$value = $this->relink($value);
        return String_Utils::mb_trim($value);
	}

    /**
     *
     * @throws TemplateParseException|ProjectNotFoundException
     */
	public function write(int $options): string {
		global $MB_WS_RE_OPT, $logger, $validator;
		
		$validator->validate_arg($options, "int");
		
		$logger->info("write($options)");
		
		$author_date = !!($options & self::INCLUDE_AUTHOR_DATE);
		$license = !!($options & self::INCLUDE_LICENSE);
		$fields = !!($options & self::INCLUDE_FIELDS);

		$project = $this->project_data->getProject();
		$lang = $this->project_data->getSubproject();
		if ($author_date || $license || $fields) {
			$upload_history = end($this->imageinfos);
			$user = String_Utils::sanitize($upload_history['user']);
			$author_text_anchor = "{{user at project|$user|$project|$lang}}";
			$author_text = $author_date ? $author_text_anchor : "";
		} else {
			$upload_history = [];
			$user = "";
			$author_text = "";
		}
		
		$license_text = "";
		if ($license) {
			if (preg_match(
				"/\{\{\s*(?:[Gg](?:FDL|fdl)-[Ss]elf|[Gg]FDL-self-no-disclaimers)\s*(?:\|\s*[Mm]igration\s*=\s*([A-Za-z\-]+)\s*)?}}/",
				$this->text, $match)) {
				$license_text .= "\n{{GFDL-user-en-no-disclaimers|$user|migration=$match[1]}}";
			}
			if (preg_match(
				"/\{\{\s*[Gg]FDL-self-(?:en|with-disclaimers)\s*(?:\|\s*[Mm]igration\s*=\s*([A-Za-z\-]+)\s*)?}}/",
				$this->text, $match)) {
				$license_text .= "\n{{GFDL-user-en-with-disclaimers|$user|migration=$match[1]}}";
			}
			if (preg_match(
				"/\{\{\s*(?:[Pp]D-(?:[Ss]elf|SELF)|[Pp]d-self|[Pp]dself)\s*(?:\|\s*[Dd]ate\s*=\s*[^|}]+)?}}/",
				$this->text)) {
                if ($project === "wikipedia" && preg_match("/^(als|ar|bg|ca|cs|da|de|en|es|fi|fr|he|hi|hr|hu|it|ja|ka|lt|lv" . 
                    "|ml|nl|nn|no|pl|pt|ro|ru|simple|sk|sl|sv|th|uk|vls|zh)$/", $lang)) {
                    $license_text .= "\n{{PD-user-$lang|$user}}";
                } else {
                    $license_text .= "\n{{PD-user-w|$lang|$project|$user}}";
                }
			}
			if (preg_match("/\{\{\s*([Cc][Cc]-(?:by-[^|}]+|0|zero|sa))}}/", $this->text,
				$match)) {
				$license_text .= "\n{{" . "$match[1]}}";
			}
			if (preg_match(
				"/\{\{\s*([Gg]FDL(?:-(?:no|with)-disclaimers|-en)?|[Gg]fdl)\s*(?:\|\s*[Mm]igration\s*=\s*([A-Za-z\-]+)\s*)?}}/",
				$this->text, $match)) {
				$license_text .= "\n{{" . "$match[1]|migration=$match[2]}}";
			}
			if (preg_match("/\{\{\s*(?:[Pp]d-old|[Pp]D-old|[Pp]D-old-100)\s*}}/", $this->text)) {
				$license_text .= "\n{{PD-old-100}}";
			}
			if (preg_match("/\{\{\s*[Pp][Dd]-(?:[Oo]ld-70|UK)\s*(\|[\S\s]*?)?}}/", $this->text)) {
				$license_text .= "\n{{PD-old-70$match[1]}}";
			}
			if (preg_match("/\{\{\s*[Pp][Dd]-(?:US|us)\s*}}/", $this->text)) {
				$license_text .= "\n{{PD-US}}";
			}
			if (preg_match(
				"/\{\{\s*(pd-(?:us[^}]+?|old-(?!70|100)[^}]+?|(?!us|uk|old|self)[\S\s]+?))}}/i",
				$this->text, $match)) {
				$license_text .= "\n{{" . "$match[1]}}";
			}
			if (preg_match(
				"/\{\{\s*(?:[Nn]oRightsReserved|[Nn]orightsreserved|[Nn]o[ _]+rights[ _]+reserved)\s*(\|[\S\s]*?)?}}/",
				$this->text, $match)) {
				$license_text .= "\n{{No rights reserved$match[1]}}";
			}
			if (preg_match(
				"/\{\{\s*(?:[Cc]opyrighted[ _]+free[ _]+use|[Cc]opyrightedFreeUse)\s*(\|[\S\s]*?)?\s*}}/",
				$this->text, $match)) {
				$license_text .= "\n{{Copyrighted free use$match[1]}}";
			}
			if (preg_match(
				"/\{\{\s*[Mm]ultilicense[ _]+replacing[ _]+placeholder\s*(\|[\S\s]*?)?\s*}}/",
				$this->text, $match)) {
				$license_text .= "\n{{Multilicense replacing placeholder$match[1]}}";
			}
			if (preg_match(
				"/\{\{\s*[Mm]ultilicense[ _]+replacing[ _]+placeholder[ _]+new\s*(\|[\S\s]*?)?\s*}}/",
				$this->text, $match)) {
				$license_text .= "\n{{Multilicense[ _]+replacing[ _]+placeholder[ _]+new$match[1]}}";
			}
			if (preg_match(
				"/\{\{\s*(?:[Cc]opyrighted[ _]+free[ _]+use[ _]+provided[ _]+that|[Cc]opyrightedFreeUseProvidedThat)\s*(\|[\S\s]*?)?\s*}}/",
				$this->text, $match)) {
				$license_text .= "\n{{Copyrighted free use provided that$match[1]}}";
			}
			if (preg_match(
				"/\{\{\s*(?:[Aa]ttribution|[Cc]opyrightedFreeUseProvided)\s*(\|[\S\s]*?)?\s*}}/",
				$this->text, $match)) {
				$license_text .= "\n{{Copyrighted free use provided that$match[1]}}";
			}
			if (preg_match("/\{\{\s*[Cc]opyrightedFreeUse-Link\s*(\|[\S\s]*?)?\s*}}/",
				$this->text, $match)) {
				$license_text .= "\n{{CopyrightedFreeUse-Link$match[1]}}";
			}
			if (preg_match("/\{\{\s*[Ff]ree\s+(?:software\s+)?screenshot\s*(\|[\S\s]*?)?}}/",
				$this->text, $match)) {
				$license_text .= "\n{{Free screenshot$match[1]}}";
			}
			if (preg_match("/\{\{\s*[Ww]ikipedia(?:-|[ _]+)screenshot\s*(\|[\S\s]*?)?\s*}}/",
				$this->text, $match)) {
				$license_text .= "\n{{Wikipedia-screenshot$match[1]}}";
			}
			if (preg_match("/\{\{\s*[Ss]elf2?\s*(\|[^}]+)}}/", $this->text, $match)) {
				$license_text .= "\n{{self|author=$author_text_anchor$match[1]}}";
			}
			
			/* header */
			if ($license_text != "") {
				$license_text = "\n\n== {{int:license-header}} ==$license_text";
			}
		}

		if ($fields) {
			$tmp = new Template($this->text, "information");
			
			$description_field = $this->information_template_field($tmp, ["description", "beschreibung"]);
			$source_field = $this->information_template_field($tmp, ["source", "quelle"]);
			$date_field = $this->information_template_field($tmp, ["date", "datum"]);
			$author_field = $this->information_template_field($tmp, ["author", "urheber"]);
			$permission_field = $this->information_template_field($tmp, ["permission", "genehmigung"]);
			$other_versions_field = $this->information_template_field($tmp, ["other_versions", "andere versionen"]);
			$etc = $this->information_template_field($tmp, ["location", "anmerkungen"]);
			if ($etc) {
				$description_field = $description_field ? "$etc\n$description_field" : $etc;
			}
			
			// grab everything that isn't a header, category, or in a template
			$description_text = Wiki_String_Utils::filter_templates($this->text); // templates
			$description_text = preg_replace(
				"/\[\[" . $MB_WS_RE_OPT . "category$MB_WS_RE_OPT:[\s\S]*?]]/ui", "",
				$description_text); // categories 1
			$description_text = preg_replace(
				"/\{\{" . $MB_WS_RE_OPT . "defaultsort$MB_WS_RE_OPT:[\s\S]*?}}/ui", "",
				$description_text); // categories 2
			$description_text = preg_replace("/^=.*=$MB_WS_RE_OPT$/umi", "", $description_text); // headers
			$description_text = $this->relink($description_text);
			$description_text = String_Utils::mb_trim($description_text);
		} else {
            $source_field = null;
            $date_field = null;
            $author_field = null;
            $permission_field = null;
            $other_versions_field = null;
            $description_text = "";
            $description_field = "";
        }
		
		/* description */
		if ($description_text && $description_field) {
			$description_text = "$description_text\n$description_field";
		} else {
			$description_text = "$description_text$description_field";
		}
		if (stripos($description_text, "=") !== FALSE) {
			$description_text = "1=$description_text";
		}
		if ($description_text) {
			$description_text = "{{" . "$lang|$description_text}}";
		}
		
		/* source */
		if ($source_field) {
			$source_text = $source_field;
		} else {
			$source_text = "{{transferred from|$lang.$project}}";
		}
		
		/* date */
		if (!String_Utils::is_empty($date_field)) {
			$date_text = $date_field;
		} else if ($author_date) {
			$date_text = "{{original upload date|" . substr($upload_history['timestamp'], 0, 10) . "}}";
		} else {
			$date_text = "";
		}
		
		/* author */
		if (!String_Utils::is_empty($author_field)) {
			$author_text = $author_field;
		}
		
		/* permission */
		$permission_text = $permission_field;
		
		/* other_versions */
		$other_versions_text = $other_versions_field;

        return "== {{int:filedesc}} ==\n" . "{{Information\n" . "|Description=$description_text\n" .
             "|Source=$source_text\n" . "|Date=$date_text\n" . "|Author=$author_text\n" .
             "|Permission=$permission_text\n" . "|other_versions=$other_versions_text\n" .
             "}}$license_text";
	}
}