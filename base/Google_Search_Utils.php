<?php
class Google_Search_Utils {

    private string $search_url;

    /**
     * @var string[]
     */
    private array $supported_mimes;

	public function __construct() {
		[$this->search_url, $this->supported_mimes] = Environment::props("constants", ["google.image-search.url",
            "google.image-search.supported-mime"]);
	}

    public function get_url(string $image_url): ?string {
        return String_Utils::replace_named_variables($this->search_url , ["url" => urlencode($image_url)]);
    }

    public function is_mime_supported(string $mime) {
        return in_array($mime, $this->supported_mimes);
    }
}