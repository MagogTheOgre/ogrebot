<?php

/**
 *
 * @author magog
 *
 */
class Template_Iterator implements Iterator {

	private ?string $origText;

	private ?string $text;

	private int $index = 0;

    /**
     * @var Abstract_Template[]
     */
	private array $templates;

	private Template_Factory $template_factory;

	public function __construct(?string &$text, Template_Factory $template_factory = null) {
		if ($template_factory === null) {
			$template_factory = Default_Template_Factory::get_singleton();
		}
		$this->template_factory = $template_factory;
		$this->text = &$text;
		$this->reset();
		$this->rewind();
	}

	private function check(): void  {
		if ($this->origText !== $this->text) {
			$this->reset();
		}
	}

	private function reset(): void {
		$this->origText = $this->text;
		
		$this->templates = [];
		
		preg_match_all("/{{\s*([^\[\]{}|#\s][^\[\]{}|#]+?)\s*(?:\||}})/", $this->text ?? "",
			$matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
		
		foreach ($matches as $match) {
			$template = $this->template_factory->extract($this->text, $match[1][0], $match[0][1]);
			if ($template && strlen($template->__get("before")) == $match[0][1]) {
				$this->templates[] = $template;
			}
		}
	}

	public function key(): int {
		$this->check();
		return $this->index;
	}

	public function next(): void {
		$this->check();
		$this->index++;
	}

	public function current(): Abstract_Template {
		$this->check();
		return $this->templates[$this->index];
	}

	public function rewind(): void {
		$this->index = 0;
	}

	public function valid(): bool {
		return $this->index < count($this->templates);
	}
}