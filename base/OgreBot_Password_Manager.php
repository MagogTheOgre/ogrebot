<?php
/**
 * 
 * @author magog
 *
 */
class OgreBot_Password_Manager implements Password_Manager {
	
	/**
	 * 
	 * @var array
	 */
	private $secrets;
	
	public function __construct() {
		$this->secrets = Local_Io::load_property_file("secrets");
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Password_Manager::get_password()
	 * @throws ArrayIndexNotFoundException if the password can't be located
	 */
	public function get_password($configuration) {
		$username = str_replace(" ", "_", $configuration['username']);
		return Array_Utils::array_key_or_exception($this->secrets, "password_$username");
	}
}