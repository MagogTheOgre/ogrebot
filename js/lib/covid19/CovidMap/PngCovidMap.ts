import SvgCovidMap from "./SvgCovidMap";
import { sep } from "path";
import sharp from "sharp";
import Io from "../../io";
import MapBuilder from "../MapBuilder/MapBuilder";

export default class PngCovidMap extends SvgCovidMap {

    constructor(private readonly outputPngFileName: string, date: Date, mapBuilder: MapBuilder)  {
        super(`${Io.tempDir}/${+date}.svg`, date, mapBuilder);
    }
    //`${this.outputDir}/covid19-${dateFileName}-${this.mapBuilder.name}.svg`
    public async create() {
        const svg = await super.create();
        await sharp(svg, {
            density: 216
        }).resize(1665, 1056).toFile(this.outputPngFileName);
        console.log(`${svg.split(sep).pop()}: PNG complete`);
        return this.outputPngFileName;
    }
}
