/// <reference path="../../../js/node_modules/@types/angular/index.d.ts" />
/// <reference path="./modules/watchHash.ts" />
/// <reference path="./modules/wikiTitleToUrl.ts" />

{
    type StepSize = 20 | 50 | 100 | 200 | 500;
    interface CategoryFilesBlameScope extends angular.IScope {
        _start: number
        _stepSize: StepSize
        _typedGallery: string
        dates: string[]
        entries: string[]
        galleries: string[]
        loadData(): Promise<void>
        loading: boolean
        selectedDate: string
        selectedGallery: string
        stepSize: StepSize
        stepSizes: StepSize[]
        typedGallery: string
        readonly end: number
        readonly entriesSubset: string[]
        readonly mobileView: boolean
        readonly hasNext: boolean
        readonly hasPrevious: boolean
        readonly start: number
        readonly total: number
    }


    const MAX_MOBILE_WIDTH = 1024;
    const watchHash = new WatchHash();
    angular
        .module("app", ["ui.bootstrap"])
        .controller("ctrl", ["$scope", "$http","$timeout",
            async ($scope: CategoryFilesBlameScope, $http: angular.IHttpService, $timeout: angular.ITimeoutService) => {
                
                var lastSearchedDate: string | undefined;            
                var lastSearchedGallery: string | undefined;

                Object.assign($scope, {
                    dates: [],
                    galleries: [],
                    entries: [],
                    _start: 0,
                    stepSizes: [20, 50, 100, 200, 500],
                    _stepSize: 200,
                    _typedGallery: "",
                    next() {
                        $scope._start += $scope._stepSize;
                    },
                    previous() {
                        $scope._start = Math.max($scope._start  - $scope._stepSize, 0);
                    },
                    async loadData() {
                        const updateEntries = () => {
                            $scope._start = 0;
                            $scope.$digest();
                        };

                        const {selectedDate, selectedGallery} = $scope;

                        if (selectedDate && selectedGallery && (lastSearchedDate !== selectedDate || lastSearchedGallery !== selectedGallery)) {
                            $scope.loading = true;
                            lastSearchedDate = selectedDate;
                            $scope.entries = (await $http({
                                method: "get",
                                url: `category-files-blame-data.php?date=${lastSearchedDate}&gallery=${encodeURIComponent(selectedGallery)}`
                            })).data as string[];

                            $scope.loading = false;
                            updateEntries();
                        } else {
                            $scope.entries = [];
                            updateEntries();
                        }
                    }
                });
                Object.defineProperties($scope, {
                    end: {
                        get() {
                            return Math.min($scope._start + $scope._stepSize, $scope.total);
                        }
                    },
                    entriesSubset: {
                        get() {
                            return $scope.entries.slice($scope._start, $scope._start + $scope._stepSize);
                        }
                    },
                    hasNext: {
                        get() {
                            return $scope._start + $scope._stepSize < $scope.total;
                        }
                    },
                    hasPrevious: {
                        get() {
                            return $scope._start > 0;
                        }
                    },
                    mobileView: {
                        get() {
                            return "ontouchstart" in window && outerWidth <= MAX_MOBILE_WIDTH;
                        }
                    },
                    start: {
                        get() {
                            return Math.min($scope._start + 1, $scope.end);
                        }
                    },
                    stepSize: {
                        get() {
                            return $scope._stepSize;
                        },
                        set(stepSize: StepSize) {
                            $scope._stepSize = stepSize;
                        }
                    },
                    total: {
                        get() {
                            return $scope.entries.length;
                        }
                    },
                    typedGallery: {
                        get() {
                            return $scope._typedGallery;
                        },
                        set (gallery: string) {
                            $scope._typedGallery = gallery;
                            if ($scope.galleries.includes(gallery)) {
                                $scope.selectedGallery = gallery;
                            }
                        }
                    }
                });
                watchHash.addProxy($scope, {
                    date: "selectedDate",
                    gallery: "selectedGallery"
                }, () => $scope.loadData());
                $scope.typedGallery = $scope.selectedGallery;
                
                addEventListener("resize", () => void $scope.$apply(() => {}));

                if ($scope.selectedDate && $scope.selectedGallery) {
                    $scope.loadData();
                } else {
                    await $timeout();
                    $scope.selectedDate = $scope.dates[0];
                    $scope.$digest();
                }
            }
        ])
        .filter("escape", () => encodeWikiTitleToUrl);
}