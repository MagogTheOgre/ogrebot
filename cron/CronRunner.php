<?php

/**
 * 
 * @author magog
 *
 */
interface CronRunner
{

    public function getRunConfig(): CronConfig;

    public function setCronDate(CronDate $cronDate): void;

    public function setRunConfig(CronConfig $runConfig): void;

    /**
     *
     * @throws IllegalStateException
     */
    public function run(): void;
}