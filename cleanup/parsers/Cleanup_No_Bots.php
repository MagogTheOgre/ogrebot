<?php
/**
 * 
 * @author magog
 *
 */
class Cleanup_No_Bots implements Cleanup_Module {
	
	
	/**
	 *
	 * @var Template_Factory
	 */
	private $template_factory;
	
	/**
	 *
	 * @param Cleanup_Package $cleanup_package        	
	 */
	public function __construct(Cleanup_Package $cleanup_package) {
	    $this->template_factory = $cleanup_package->get_infobox_template_factory();
	}
	
	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see Cleanup_Module::cleanup()
	 */
	public function cleanup(Cleanup_Instance $ci) {
	    if ($ci->is_human()) {
	        //ok
	        return;
	    }
	    
	    $text = $ci->get_text();
	    $template_array = iterator_to_array(new Template_Iterator($text, $this->template_factory));
        array_walk($template_array, function (Abstract_Template $template) {
            $name = ucfirst($template->getname());
            if ($name === "Nobots") {
                if ($template->fieldisset("allow")) {
                    foreach (preg_split("/\s*\,\s*/", $template->fieldvalue("allow")) as $allow) {
                        if ($allow === "OgreBot 2" || $allow === "OgreBot_2" || strtolower($allow) === "all") {
                            // OK
                            return;
                        }
                    }
                }
                throw new Cleanup_Abort_Exception("Nobots template found.");
            }
            if ($name === "Bots") {
                if ($template->fieldvalue("allow") === "none") {
                    throw new Cleanup_Abort_Exception("Bots template found.");
                }
                if ($template->fieldisset("deny")) {
                    foreach (preg_split("/\s*\,\s*/", $template->fieldvalue("deny")) as $deny) {
                        if ($deny === "OgreBot 2" || $deny === "OgreBot_2" || strtolower($deny) === "all" || strtolower($deny) === "none") {
                            throw new Cleanup_Abort_Exception("Bots template found.");
                        }
                    }
                }
            }
        });
	}
	
	
}