import MapBuilder from "./MapBuilder";
import CovidUtils from "../CovidUtils";
import { mapValues } from "../../collectionUtils";
import { FinalCountyResult, getCountyResultsBuilder, CountyResultsBuilder } from "../Results/CountyResults";
import { CountyKey } from "../County/County";

export default abstract class AbstractMapBuilder implements MapBuilder {

    protected abstract readonly colorLevels: [number, string][];

    protected abstract getResults(builder: (type: CountyKey) => CountyResultsBuilder): FinalCountyResult;

    public async getColors(date: Date) {
        const colorBuilder = CovidUtils.getColor(this.colorLevels);
        const results = await this.getResults(getCountyResultsBuilder(date));
        return [mapValues(results, colorBuilder), this.defaultColor] as const;
    }

    protected get defaultColor() {
        return this.colorLevels[0][1];
    }

}