<?php
class Logger {
	
	/**
	 *
	 * @var LogFormatter
	 */
	private readonly LogFormatter $logFormatter;
	
	/**
	 * 
	 * @var Level[]
	 */
	private array $level_stack = [];

	private Level $debug_level = Level::OFF;
	
	/**
	 *
	 * @var resource[]
	 */
	private array $out = [];

	/**
	 * @throws IllegalStateException|XMLParserException
	 */
	public function __construct(string $filename) {
		$this->logFormatter = new DefaultLogFormatter();

		$data = OgreBotXmlParser::xmlFileToStruct($filename);

		// read XML file
		$errors = array();
		$outStreams = array();
		$defaultLevel = null;
		$thisLevel = null;

		if (defined("LOGGER_NAME")) {
			$thisScript = LOGGER_NAME;
		} else {
			$thisScript = preg_replace("/(?:[\S\s]*\\" . DIRECTORY_SEPARATOR . ")?(.+?)\.php$/", "$1",
				$_SERVER['PHP_SELF']);
		}

		// parse XML file
		$configs = $data['CONFIGS'][0]['elements']['CONFIG'] ??
			throw new IllegalStateException("Can't find configs element");

		foreach ($configs as $config) {
			$scriptName = $config['attributes']['SCRIPT'] ?? null;

			if ($scriptName === '*' || $scriptName === $thisScript) {
				$defaultLevel = $config['elements']['LEVEL'][0]['value'] ?? null;
				$outElements = $config['elements']['OUT'] ?? [];
				foreach ($outElements as $outElement) {
					$nextStream = $outElement['value'] ?? null;
					if ($nextStream !== null) {
						$outStreams[] = $nextStream;
					}
				}
			}
		}


		// read OUT variable
		if (count($outStreams) > 0) {
			foreach ($outStreams as $outStream) {
				$nextStream = Logger::outStringToStream($outStream);
				if ($nextStream === null) {
					$errors[] = "Cannot open file output stream for $outStream";
				} else {
					$this->out[] = $nextStream;
				}
			}
		} else {
			$errors[] = "OUT not specified; defaulting to standard out.";
			$this->out = [fopen('php://stdout', 'w')];
		}

		// read LEVEL variable
		$level = $thisLevel ?? $defaultLevel;
		if ($level == null) {
			$errors[] = "LEVEL not specified; defaulting to DEBUG.";
			$level = "DEBUG";
		}
		$this->debug_level = Level::parse($level);

		// errors and confirmation to logger
		foreach ($errors as $error) {
			$this->error($error);
		}

		$this->echoLevel();
		if ($this->isDebugEnabled()) {
			$streamNames = [];
			foreach ($this->out as $handle) {
				$metadata = stream_get_meta_data($handle);
				$streamNames[] = $metadata['uri'];
			}
			$this->trace(
				"Streams added: " . count($this->out) . ". {" . implode(", ", $streamNames) . "}");
		}
	}
	
	/**
	 * @return resource
	 */
	private static function outStringToStream(string $match) {

		if (strtoupper($match) == "STDERR") {
			return fopen('php://stderr', 'w');
		} else if (strtoupper($match) == "STDOUT") {
			return fopen('php://stdout', 'w');
		} else {
			if (defined("LOGGER_NAME")) {
				$script = LOGGER_NAME;
			} else {
				$script = $_SERVER['PHP_SELF'];
			}
			if ($script == '-') {
				$script = "DEFAULT";
			}
			$match = String_Utils::replace_named_variables_defaults($match,
                ["script" => preg_replace("/(?:.*\\" . DIRECTORY_SEPARATOR . ")?(.+?)\.php$/", "$1", $script),
                    "logdir" => LOG_DIRECTORY]);
			return fopen($match, 'a');
		}
	}

	public function doLog(mixed $message, Level $level, int $depth): void {
		if ($level->value <= $this->debug_level->value) {
			if (is_bool($message)) {
				$message = $message ? "TRUE" : "FALSE";
			} else if ($message instanceof Exception) {
				if ($message instanceof BaseException) {
					if ($message->isLogged()) {
						return;
					} else {
						$message->setLogged();
					}
				}
				$message = String_Utils::exceptionToString($message);
			} else if (!is_string($message)) {
				$message = print_r($message, true);
			}
			
			$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $depth)[$depth - 1];
			
			$file = String_Utils::get_short_filename($backtrace['file']);
			$line = $backtrace['line'];
			
			$outString = $this->logFormatter->doFormat($level->name, $file, $line, $message);
			
			$this->writeRaw($outString);
		}
	}

	private function writeRaw(string $message): void {
		foreach ($this->out as $stream) {
			fwrite($stream, $message);
		}
	}

	/**
	 *
	 * @param string $string        	
	 * @throws IllegalStateException
	 * @return void
	 */
	public function addOutputStreams($string) {
		$startCount = count($this->out);
		$stream = Logger::outStringToStream($string);
		
		if ($stream) {
			$this->out[] = $stream;
		} else {
			throw new IllegalStateException("Can't initialize stream: $string");
		}
		$this->trace("Added " . (count($this->out) - $startCount) . " streams");
	}

	public function all(mixed $message = "", int $depth = 0): void {
		$this->doLog($message, LEVEL::ALL, $depth + 2);
	}
	
	public function insane(mixed $message = "", int $depth = 0): void {
		$this->doLog($message, LEVEL::INSANE, $depth + 2);
	}
	
	public function trace(mixed $message = "", int $depth = 0): void {
		$this->doLog($message, LEVEL::TRACE, $depth + 2);
	}

	public function debug(mixed $message = "", int $depth = 0): void {
		$this->doLog($message, Level::DEBUG, $depth + 2);
	}

	public function debugWithArrays(mixed $message, array... $arrays): void {
		$this->doLog(sprintf($message, ...array_map([$this, "get_array_string"], $arrays)),
			Level::DEBUG, 2);
	}
	
	public function info(mixed $message = "", int $depth = 0): void {
		$this->doLog($message, Level::INFO, $depth + 2);
	}
	
	public function warn(mixed $message = "", $depth = 0): void {
		$this->doLog($message, Level::WARN, $depth + 2);
	}

	public function error(mixed $message = "", int $depth = 0): void {
		$this->doLog($message, Level::ERROR, $depth + 2);
	}
	public function fatal(mixed $message = "", int $depth = 0): void {
		$this->doLog($message, LEVEL::FATAL, $depth + 2);
	}

	public function log(Level $level, mixed $message = "", int $depth = 0): void {
		$this->doLog($message, $level, $depth + 2);
	}

	public function write(string $message, Level $level): void {
		if ($level->value <= $this->debug_level->value) {
			$this->writeRaw($message);
		}
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isInsaneEnabled() {
		return $this->debug_level->value >= Level::INSANE->value;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isTraceEnabled() {
		return $this->debug_level->value >= Level::TRACE->value;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isDebugEnabled() {
		return $this->debug_level->value >= Level::DEBUG->value;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isInfoEnabled() {
		return $this->debug_level->value >= Level::INFO->value;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isWarnEnabled() {
		return $this->debug_level->value >= Level::WARN->value;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isErrorEnabled() {
		return $this->debug_level->value >= Level::ERROR->value;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isFatalEnabled() {
		return $this->debug_level->value >= Level::FATAL->value;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isOff() {
		return $this->debug_level->value == Level::OFF->value;
	}

	public function getDebugLevel(): Level {
		return $this->debug_level;
	}

	public function setDebugLevel(Level $debug_level): void {
		$this->debug_level = $debug_level;
		$this->echoLevel();
	}


	public function pushLevel(Level $level): void {
		array_unshift($this->level_stack, $this->debug_level);
		$this->setDebugLevel($level);
	}
	

	public function popLevel(): void {
		$level = array_shift($this->level_stack);
		if ($level === null) {
			$this->warn("Can't pop level: stack already empty.");
		} else {
			$this->debug_level = $level;
		}
	}
	
	/**
	 *
	 * @return void
	 */
	private function echoLevel() {
		$this->trace("Logger class initialized at level {$this->debug_level->name}");
	}

	/**
	 * @param string[] $strings
	 * @param string $separator
	 * @return string
	 */
	public function get_array_string(array $strings, $separator = ",") {
		if ($this->isTraceEnabled()) {
			return "[" . join($separator, $strings) . "]";
		}
		return "array[" . count($strings) . "]";
	}
}

?>
