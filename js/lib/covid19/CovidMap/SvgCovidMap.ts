import CovidMap from "./CovidMap";
import MapBuilder from "../MapBuilder/MapBuilder";
import { getAndDelete } from "../../collectionUtils";
import Io from "../../io";
import CovidUtils from "../CovidUtils";
import DataLoader from "../DataLoader";
import CovidConstants from "../Constants";


const {getCountyString, toMdyString} = CovidUtils;
export default class SvgCovidMap implements CovidMap<string> {

    private readonly date: Date;

    constructor(private readonly outputFileName: string, date: Date, private readonly mapBuilder: MapBuilder) {
        this.date = new Date(date);
    }

    public async create() {
        
        console.log(`${toMdyString(this.date)}: creating SVG...`);

        const [[states, build], [colors, defaultColor]] = await Promise.all([
            DataLoader.getStates(), 
            this.mapBuilder.getColors(this.date)
        ]);
    
        states.forEach(svgCounty => {
            const {county, state} = svgCounty;
            const countyString = getCountyString(county, state);
            const parentString =  CovidConstants.COMBINED_COUNTIES_INV[countyString];
            svgCounty.fill = getAndDelete(colors, parentString || countyString) || defaultColor;  
        });
        await Io.writeFile(this.outputFileName, build());

        console.log(`${toMdyString(this.date)}: SVG complete`);
        return this.outputFileName;
    }
}
