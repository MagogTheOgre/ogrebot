import Io from "./io";

var warningTimer : NodeJS.Timeout;
const MAX_TIMEOUT = 0x7fffffff;

//dummy timeout to keep the Node process alive
export function startup(timeout = MAX_TIMEOUT) {
    warningTimer = warningTimer || setTimeout(() => {}, timeout);
}

export function shutdown() {
    Io.removeTempFiles();
    warningTimer && clearTimeout(warningTimer);
}

export class SleepPromise extends Promise<void> {
    private _timeout: number = 0;

    constructor(millis: number) {
        super(resolve => this._timeout = setTimeout(resolve, millis));
    }

    public cancel() {
        clearTimeout(this._timeout);
    }
}


export const sleep = (millis: number) => new SleepPromise(millis);
