<?php
class FacebookExifHandler extends NewUploadHandler {

	public function getTitleKey() {
		return "uploadhandler.facebookexif";
	}

	public function isDisplayUpload($upload, $uploader) {
	    $value = $upload[METADATA][SPECIAL_INSTRUCTIONS] ?? false;
	    return $value && is_array($value) && !!preg_match("/^FBMD[\da-f]+$/", $value[0]??"");
	}
}