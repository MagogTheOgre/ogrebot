class WatchHash {
    private ignoreHashChange = false;

    constructor() {
        const { hash, origin, pathname, search } = location;
        const searchString = search.substring(1);
        if (searchString && !hash) {
            location.replace(`${origin}/${pathname}#${searchString}`);
        }
    }

    public addProxy(target: Record<string, any>, keys: Record<string, string>, notify?: () => void) {
        const hashKeys = Object.keys(keys);
        const backingObject = Object.fromEntries(hashKeys.map(key => [key, ""]));

        this.get(backingObject, hashKeys);
        for (const [hashKey, objectKey] of Object.entries(keys)) {
            Object.defineProperty(target, objectKey, {
                get() {
                    return backingObject[hashKey];
                },
                set: (val) => {
                    backingObject[hashKey] = val;
                    this.setHashParameters(backingObject);
                }
            });
        }
        addEventListener("hashchange", () => {
            if (!this.ignoreHashChange) {
                this.get(backingObject, hashKeys);
                notify?.();
            }
        });
    }
    
    private setHashParameters(values: Record<string, any>) {
        this.ignoreHashChange = true;
        location.hash = Object.entries(values).flatMap(([key, val]) => {
            if (val) {
                return [`${encodeURIComponent(key)}=${encodeURIComponent(val)}`];
            }
            else {
                return [];
            }
        }).join("&");
        this.ignoreHashChange = false;
    }

    private get(object: Record<string, string>, keys: string[]) {
        const searchString = location.hash.substring(1);
        if (searchString) {
            searchString.split("&").forEach(param => {
                const [name, val = ""] = param.split("=");
                if (keys.includes(name)) {
                    object[decodeURIComponent(name)] = decodeURIComponent(val);
                }
            });
        }
    }
}
