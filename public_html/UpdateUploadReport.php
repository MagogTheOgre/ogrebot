<?php
require_once __DIR__ . "/../base/bootstrap.php";
global $env, $wiki_interface;

$POST = $env->get_request_args(true);
$start = "$POST[start]000000";
$unix_time = strtotime($start);
$status = "";
if (!preg_match("/^20\d{12}$/", $start)) {
    $status = "No start date specified or date format not recognized.";
} else if ($unix_time > strtotime("+20 minutes")) {
    $status = "Gallery cannot be in the future, and must have been live for 20 minutes.";
} else {
    if (!file_exists(UPDATE_POLL_DIRECTORY)) {
        mkdir(UPDATE_POLL_DIRECTORY, 0775);
    }
    Local_Io::file_put_contents_ensure(UPDATE_POLL_DIRECTORY . "notable-$start", "");
    
    $galleryPageName = UploadReportWriter::getGalleryPageName($start);
    $pageLink = String_Utils::wikilink($galleryPageName, "commons.wikimedia", true, false, "Return to the gallery");
    $historyLink = String_Utils::wikilink(
        $galleryPageName,
        "commons.wikimedia",
        true,
        false,
        "view history",
        null,
        false,
        "history");
}
?><!DOCTYPE html>
<html>
<head></head>
<body>
<?= $status ?>
<?php
if (@$pageLink) {
	?><p>Your request has been submitted and will update in the next few minutes.</p>
	<p><?= $pageLink ?> or <?= $historyLink ?>.</p><?php
}
?></body>
</html>
