<?php
/**
 * Low level class to replace string recursively with bars
 */
class Deep_String_Replacer implements String_Replacer
{

    /**
     * @var string[] | string
     */
    private $from;

    /**
     * @var string[] | string
     */
    private $to;

    /**
     * @var callable
     */
    private $replacer;

    /**
     * @var callable|callable-string
     */
    private $callback;

    /**
     * @param string[] | string $from
     * @param string[] | string $to
     * @param bool $keys
     * @param callable|string $callback
     */
    public function __construct($from, $to , bool $keys = false, $callback  = "str_replace" ) {
        $this->from = $from;
        $this->to = $to;
        $this->replacer = [$this, $keys ? "replace_keys_values" : "replace_values"];
        $this->callback = $callback;
    }

    /**
     * @param mixed $input
     * @param int $depth
     * @return mixed
     */
    function replace($input, int $depth = 0) {
        call_user_func_array($this->replacer, [&$input, "", $depth]);
        return $input;
    }

    private function replace_values(&$input, $_, int $depth){
        if (is_string($input)) {
            $input = $this->replace_internal($input);
        } else if (is_array($input) && $depth > 0) {
            array_walk($input, $this->replacer, $depth - 1);
        }
    }

    private function replace_keys_values(&$input, $_, int $depth) {
        $this->replace_values($input, $_, $depth);
        if (is_array($input) && $depth > 0) {
            foreach ($input as $key => $value) {
                unset($input[$key]);
                $input[$this->replace_internal($key)] = $value;
            }
        }
    }

    private function replace_internal(string $string): string {
        return call_user_func($this->callback, $this->from, $this->to, $string);
    }
}


