<?php

class Timer
{
    /**
     * @var float
     */
    private $start;

    /**
     * @throws IllegalStateException
     */
    private static function _autoload() {
        $int_size = PHP_INT_SIZE;
        if ($int_size < 8) {
            throw new IllegalStateException("Timer unsupported on platform with int size $int_size");
        }
    }

    public function __construct() {
        $this->start = $this->get_millis();
    }

    public function get_diff(): int {
        return $this->get_millis() - $this->start;
    }

    private function get_millis(): int {
        $now = microtime();
        $seconds = substr($now, ComputedConstants::MICROTIME_SECONDS_OFFSET);
        $millis = substr($now, 0, ComputedConstants::MICROTIME_MICRO_OFFSET);
        return ((int)$seconds) * 1000 + ((int)round($millis * 1000));
    }
}