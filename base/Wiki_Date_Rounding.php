<?php

enum Wiki_Date_Rounding {
    case EXCEPTION;
    case ROUND_DOWN;
    case ROUND_UP;
}
