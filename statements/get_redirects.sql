SELECT
    p.page_namespace,
    p.page_title,
    r.rd_title
FROM
    page p
INNER JOIN
    redirect r
ON
    p.page_id = r.rd_from
WHERE
    r.rd_namespace = ?
AND
    r.rd_title in (??)
