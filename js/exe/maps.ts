import { ElectionDiffTracker } from './../lib/maps/ElectionDiffTracker';

import {StatesBuilder} from "../lib/maps/Maps";
import { diffElectionResultColorer, standardElectionResultColorer } from '../lib/maps/ElectionResultsColorer';
import Io from '../lib/io';
import { UsElectionAtlasTracker } from '../lib/maps/UsElectionAtlasTracker';
import { AllSettledThreadPoolImpl } from '../lib/ThreadPool';
(async () => {
    const thisElection = new StatesBuilder().addElectionTrackers(
        new UsElectionAtlasTracker('/home/patrick/Desktop/Pres_Election_Data_2020.xlsx', "USA counties-v2")
    ).setElectionResultHandler(standardElectionResultColorer)
    const compareElection = new StatesBuilder().addElectionTrackers(new ElectionDiffTracker(
        new UsElectionAtlasTracker('/home/patrick/Desktop/Pres_Election_Data_2016.xlsx', "USA counties"),
        new UsElectionAtlasTracker('/home/patrick/Desktop/Pres_Election_Data_2020.xlsx', "USA counties")
    )).setElectionResultHandler(diffElectionResultColorer);
    
    await new AllSettledThreadPoolImpl().enqueueAll([thisElection, compareElection].map(
        builder => () => builder.build(`${Io.PROJECT_DIR}/../Desktop`)));
    console.log("Done");
})();
