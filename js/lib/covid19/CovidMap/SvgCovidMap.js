"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const collectionUtils_1 = require("../../collectionUtils");
const io_1 = __importDefault(require("../../io"));
const CovidUtils_1 = __importDefault(require("../CovidUtils"));
const DataLoader_1 = __importDefault(require("../DataLoader"));
const Constants_1 = __importDefault(require("../Constants"));
const { getCountyString, toMdyString } = CovidUtils_1.default;
class SvgCovidMap {
    outputFileName;
    mapBuilder;
    date;
    constructor(outputFileName, date, mapBuilder) {
        this.outputFileName = outputFileName;
        this.mapBuilder = mapBuilder;
        this.date = new Date(date);
    }
    async create() {
        console.log(`${toMdyString(this.date)}: creating SVG...`);
        const [[states, build], [colors, defaultColor]] = await Promise.all([
            DataLoader_1.default.getStates(),
            this.mapBuilder.getColors(this.date)
        ]);
        states.forEach(svgCounty => {
            const { county, state } = svgCounty;
            const countyString = getCountyString(county, state);
            const parentString = Constants_1.default.COMBINED_COUNTIES_INV[countyString];
            svgCounty.fill = collectionUtils_1.getAndDelete(colors, parentString || countyString) || defaultColor;
        });
        await io_1.default.writeFile(this.outputFileName, build());
        console.log(`${toMdyString(this.date)}: SVG complete`);
        return this.outputFileName;
    }
}
exports.default = SvgCovidMap;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3ZnQ292aWRNYXAuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJTdmdDb3ZpZE1hcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUVBLDJEQUFxRDtBQUNyRCxrREFBMEI7QUFDMUIsK0RBQXVDO0FBQ3ZDLCtEQUF1QztBQUN2Qyw2REFBMEM7QUFHMUMsTUFBTSxFQUFDLGVBQWUsRUFBRSxXQUFXLEVBQUMsR0FBRyxvQkFBVSxDQUFDO0FBQ2xELE1BQXFCLFdBQVc7SUFJQztJQUFxRDtJQUZqRSxJQUFJLENBQU87SUFFNUIsWUFBNkIsY0FBc0IsRUFBRSxJQUFVLEVBQW1CLFVBQXNCO1FBQTNFLG1CQUFjLEdBQWQsY0FBYyxDQUFRO1FBQStCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDcEcsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRU0sS0FBSyxDQUFDLE1BQU07UUFFZixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUUxRCxNQUFNLENBQUMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsWUFBWSxDQUFDLENBQUMsR0FBRyxNQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUM7WUFDaEUsb0JBQVUsQ0FBQyxTQUFTLEVBQUU7WUFDdEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUN2QyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQ3ZCLE1BQU0sRUFBQyxNQUFNLEVBQUUsS0FBSyxFQUFDLEdBQUcsU0FBUyxDQUFDO1lBQ2xDLE1BQU0sWUFBWSxHQUFHLGVBQWUsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDcEQsTUFBTSxZQUFZLEdBQUksbUJBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN6RSxTQUFTLENBQUMsSUFBSSxHQUFHLDhCQUFZLENBQUMsTUFBTSxFQUFFLFlBQVksSUFBSSxZQUFZLENBQUMsSUFBSSxZQUFZLENBQUM7UUFDeEYsQ0FBQyxDQUFDLENBQUM7UUFDSCxNQUFNLFlBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBRWpELE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3ZELE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvQixDQUFDO0NBQ0o7QUE1QkQsOEJBNEJDIn0=