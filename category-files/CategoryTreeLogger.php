<?php
interface CategoryTreeLogger {
	/**
	 * Called once for each category.
	 * @param string $gallery_name
	 * @param string[][] $pages
	 * @return void
	 */
	public function log(string $gallery_name, array $pages);
}