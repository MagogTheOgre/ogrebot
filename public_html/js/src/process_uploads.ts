/// <reference path="../../../js/node_modules/@types/angular/index.d.ts" />
/// <reference path="./modules/wikiTitleToUrl.ts" />

interface ProcessUploadsScope extends angular.IScope {
    logic: any
}

angular
    .module("app", [])
    .controller("process-uploads", [
        "$scope",
        ($scope: ProcessUploadsScope) => void Object.assign($scope, window["logic"])
    ])
    .filter("escapeTitle", () => encodeWikiTitleToUrl)
    .filter("escape", () => encodeURIComponent)
    .filter("trusted", ["$sce", ($sce: angular.ISCEService) => (url: string) => $sce.trustAsResourceUrl(url)]);
