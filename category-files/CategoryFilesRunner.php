<?php
class CategoryFilesRunner {
	public function __construct(private readonly Wiki $wiki) {
	}
	
	/**
	 *
	 * @param string[] $pageNames        	
	 * @return CategoryTree
	 */
	public function runFromPageNames(array $pageNames): CategoryTree {
		global $logger;
		
		if (count($pageNames) == 0) {
			$logger->warn("Empty \$pageNames array");
		}
		
		$categoryTree = new CategoryTree();
		$this->run($pageNames, $categoryTree, 0);
		return $categoryTree;
	}

	public function runFromFileUploadDates(Wiki_Date $start_time, Wiki_Date $end_time): CategoryTree {
		global $logger, $wiki_interface;
		
		$logger->info("runFromFileUploadDates $start_time $end_time");
		
		$all_uploads = $wiki_interface->get_recent_uploads_single_array($this->wiki, $start_time->get(),
			$end_time->get());
		// validate_arg_array($all_uploads, "array");
		if (count($all_uploads) == 0) {
			$logger->warn("Empty \$all_uploads array");
		}
		
		$fileNames = array_keys($all_uploads);
		
		return $this->runFromPageNames($fileNames);
	}
	
	/**
	 *
	 * @param string[] $pages
	 * @throws WikiDataException
	 * @return void
	 */
	private function run(array $pages, CategoryTree $categoryTree, int $recurse_level) {
		global $logger, $constants, $wiki_interface;
		
		$max_category_query = Array_Utils::array_key_or_exception($constants, 'maxcategoryquery');
		
		/* graceful termination on bug. We will never reach this level unless there is an error. */
		if ($recurse_level === 85) {
			$logger->error(
				"Unable to complete full run. The following categories are still around.");
			$logger->error($pages);
			return;
		}
		
		$pages = array_filter($pages, fn(string $page) => !$categoryTree->isPageStored($page));
		    
	    $count = count($pages);
	    if ($count === 0) {
	        return;
	    }
		/*
		 * place all pages into a giant query, so we can do it all at once.
		 * It will probably be more than $max_category_query, so break into multiple queries
		 */
		$nxt_query = array();
		for($pages_ptr = 0; $pages_ptr < $count; $pages_ptr += $max_category_query) {
			$logger->debug(
				"$pages_ptr $count $recurse_level." . ($pages_ptr / $max_category_query + 1) . " of " .
					 intval(($count - 1) / $max_category_query + 1));
			
			$page_str = implode("|", array_slice($pages, $pages_ptr, $max_category_query));
			$clcontinue = NULL;
			do {
				$vars = array('action' => 'query', 'titles' => $page_str, 'prop' => 'categories', 
					'cllimit' => 'max', 'redirects' => '');
				if ($clcontinue !== NULL) {
					$vars['clcontinue'] = $clcontinue;
				}
				
				$query = $wiki_interface->api_query($this->wiki, $vars, true);
				
				foreach ($query['query']['pages'] ?? [] as $pageid) {
					
					if (!array_key_exists('title', $pageid) || !is_string($pageid['title'])) {
						throw new WikiDataException(
							"Bad API values: " . print_r($vars, true) . "=>" . print_r($pageid, 
								true));
					}
					$page_title = $pageid['title'];
					
					/* uncategorized or missing pages have no categories */
					if (!array_key_exists("categories", $pageid) || !is_array($pageid["categories"])) {
						continue;
					}
					
					$cat_titles = array();
					foreach ($pageid["categories"] as $cat) {
						if (!array_key_exists("title", $cat)) {
							throw new WikiDataException("Bad API values: " . print_r($cat, true));
						}
						$cat_title = $cat['title'];
						
						if (!is_string($cat_title)) {
							throw new WikiDataException("Bad API value: " . print_r($cat_title, 
								true));
						}
						
						$cat_titles[] = $cat_title;
						
						if ($cat_title != $page_title && !$categoryTree->isPageStored($cat_title)) {
							$nxt_query[] = $cat_title;
						}
					}
					
					$categoryTree->addSuperCategories($page_title, $cat_titles);
				}
				$clcontinue = $query['query-continue']['clcontinue'] ?? null;
			} while ($clcontinue !== null);
		}
		
		$this->run(array_unique($nxt_query), $categoryTree, $recurse_level + 1);
	}
}
?>
