<?php

class License_Cache {

	/**
	 * Unix timestamp for when licenses are loaded
	 * @var int
	 */
	public int $load_time;

	/**
	 * @var string[]
	 */
	public array $license_regexes = [];

}
