<?php

/**
 *
 * @author magog
 *
 */
class Category_Files_Overflow_Exception extends BaseException {

    private int $existingSize;
    private int $maxSize;
    private int $totalSize;

	public function __construct(int $existingSize, int $maxSize, int $totalSize) {
		parent::__construct(Category_Files_Overflow_Exception::class, null, false);
        $this->existingSize = $existingSize;
        $this->maxSize = $maxSize;
        $this->totalSize = $totalSize;
	}

    public function getExistingSize(): int {
        return $this->existingSize;
    }

    public function getMaxSize(): int {
        return $this->maxSize;
    }

    public function getTotalSize(): int {
        return $this->totalSize;
    }
}