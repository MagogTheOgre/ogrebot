import { Party, PartyOrTie } from "./types";

export interface ElectionResult {

    party: PartyOrTie
    percent: number
    results: {[x in Party]: number}
    complete: boolean
}

export class ElectionResultImpl implements ElectionResult {
    constructor(public readonly results: ElectionResult["results"]) {

    }
    get party() {
        const {r, d} = this.results;
        return r > d ? "r" : d > r ? "d" : "t";
    }

    get percent() {
        const {party} = this;
        return party !== "t" ? this.results[party] : this.results.r;
    }

    get complete() {
        const {r, d} = this.results;
        return r !== 0 || d !== 0;
    }

}

export class ElectionIncompleteResult implements ElectionResult {

    get party() {
        return "t" as const;
    }

    get percent() {
        return 0;
    }

    get results(): ElectionResult["results"] {
        return {
            r: 0,
            d: 0
        };
    }
    
    get complete() {
        return false;
    }
    
}