function checkImageLoad (images: Iterable<HTMLImageElement>, onSuccess: (image: HTMLImageElement) => void, onError: (image: HTMLImageElement) => void) {
    for (const image of images) {
        if (image.complete) {
            setTimeout(() => (image.naturalHeight ? onSuccess : onError)(image));
        } else {
            image.addEventListener("load", () => onSuccess(image));
            image.addEventListener("error", () => onError(image));
        }
    }
}