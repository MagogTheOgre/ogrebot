<?php
/**
 * 
 * @author magog
 *
 */
class Remote_Database_Dao implements Database_Dao {
        
    /**
     * 
     * @var Remote_Call
     */
    private $remote_call;
    
    function __construct() {
        $this->remote_call = new Remote_Call();
    }
    
    /**
     *
     * {@inheritdoc}
     * @see Database_Dao::read_metadata()
     */
    public function read_metadata(string $environment, array $names, array $fields): array {
        return $this->remote_call->call(__FUNCTION__, func_get_args());
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see Database_Dao::get_pages_with_tags()
     */
    public function get_pages_with_tags(string $environment, array $tagIds, int $startTime,
        int $endTime): array {
        return $this->remote_call->call(__FUNCTION__, func_get_args(), 120);
    }

    /**
     * {@inheritDoc}
     * @see Database_Dao::get_pages_with_redirects_in_categories()
     */
    public function get_pages_with_redirects_in_categories(string $environment, array $categories,
                                                           int $limit): array {
        return $this->remote_call->call(__FUNCTION__, func_get_args(), 120);
    }

    /**
     * {@inheritDoc}
     * @see Database_Dao::get_redirects()
     */
    public function get_redirects(string $environment,int $namespace, array $titles,
                                  int $limit): array {
        return $this->remote_call->call(__FUNCTION__, func_get_args(), 120);
    }

    /**
     * {@inheritDoc}
     * @see Database_Dao::get_redirects_to()
     */
    public function get_redirects_to(string $environment, int $namespace, array $titles,
                                     int $limit): array {
        return $this->remote_call->call(__FUNCTION__, func_get_args(), 120);
    }

    public function get_visible_categories(string $environment, int $namespace, array $pageNames): array {
        return $this->remote_call->call(__FUNCTION__, func_get_args(), 120);
    }
}