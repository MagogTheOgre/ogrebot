
import xmljs from "xml2js";
import Io from "../io";
import fs from "fs";
import { ElectionTracker } from "./ElectionTracker";
import styleToObject from 'style-to-object';
import { ElectionResultColorer } from "./ElectionResultsColorer";
import { toArray } from "../collectionUtils";
import { styleToString } from "../stringUtils";
const builder = new xmljs.Builder();
const mapsDir = `${Io.PROJECT_DIR}/county-maps`;

class County {
    constructor(public readonly state: string, public readonly name: string, private path : any) {
    }

    public set fill(fill: string | null) {  
        if (fill) { 
            this.path.style = styleToString({
                ...styleToObject(this.path.style) || {},
                fill
            });
        }
    }

    //normalize common abbreviations
    public static normalize(state: string, county: string) {
        return ((state, county) => {
            switch (state) {
                case "LA":
                    switch (county) {
                        case "lasalle":
                            return "la salle";
                    }
                    break;
                case "MS":
                    switch (county) {
                        case "jeff davis":
                            return "jefferson davis";
                    }
                    break;
                case "NV":
                    switch (county) {
                        case "carson city_city":
                            return "carson city";
                    }
            }
            return county;
        })(state, county.toLowerCase()).replace(/ /g, "_").replace(/'/g, "");
    }
}

class Map {
    readonly counties: Record<string, Record<string, County>>;

    constructor(private xml: any) {
        this.counties = toArray(function*() {
            for (const g of xml.svg.g) {
                for (const {$} of g?.path || []) {
                    const {id} = <{[x: string] : string}>$;
                    const [, state, countyName] = id?.match(/^([A-Z]{2})_([A-Za-z_\.\-]+)$/) || [];
                    if (state && countyName) {
                        yield new County(state, countyName, $);
                    }
                }
            }
        }).reduce((previous, county ) => {
            (previous[county.state] || (previous[county.state] = {}))[County.normalize(county.state, county.name)] = county;
            return previous;
        }, <Map["counties"]>{});
    }

    async write(dest: string) {
        await fs.writeFileSync(dest, builder.buildObject(this.xml));
    }
}

export class StatesBuilder {
    private electionTrackers: ElectionTracker[] = [];
    private electionResultColorer: ElectionResultColorer | undefined;

    addElectionTrackers(...electionTrackers: ElectionTracker[]) {
        this.electionTrackers.push(...electionTrackers);
        return this;
    }

    setElectionResultHandler(electionResultColorer: ElectionResultColorer) {
        this.electionResultColorer = electionResultColorer;
        return this;
    }
    
    async build(dest: string): Promise<any> {
        var {electionResultColorer, electionTrackers} = this;
        if (!electionResultColorer || !electionTrackers[0]) {
            throw new Error("build() called before ready");
        }

        const promises = function*(){
            for (const file of Io.readDir(mapsDir)) {
                const path = `${mapsDir}/${file}`;
                const mapName = file.replace(/\.svg$/, "");
                const tracker = electionTrackers.find(tracker => tracker.isEligible(mapName));
                if (tracker) {
                    yield (async() => {
                        const text = fs.readFileSync(path).toString();
                        const xml = await xmljs.parseStringPromise(text);
                        const map = new Map(xml);

                        for (const [state, counties] of Object.entries(await tracker.getCountyTotals())) {
                            if (map.counties[state]) {

                                for (const [county, result] of Object.entries(counties)) {
                                    const normalizedCounty = County.normalize(state, county);
                                    const stateCounty = map.counties[state]?.[normalizedCounty];
                                    if (!stateCounty) {
                                        console.error(`County ${county}, ${state} not found in SVG`);
                                        continue;
                                    }
        
                                    delete map.counties[state][normalizedCounty];
                                    stateCounty.fill = result.complete ? electionResultColorer(result): null;
                                }
                                for (const county of Object.keys(map.counties[state])) {
                                    console.error(`County ${county}, ${state} not found in election results`);
                                }
                            }
                        }

                        map.write(`${dest}/${file}`);
                    })();
                }
            }
        }();

        return Promise.all([...promises]);
    }
}
