"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.awaitReady = void 0;
const awaitReady = () => (t, _property, propertyDescriptor) => {
    const originalFunction = propertyDescriptor.value;
    propertyDescriptor.value = async function () {
        await this.ready;
        return originalFunction.apply(this, arguments);
    };
};
exports.awaitReady = awaitReady;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXdhaXRSZWFkeS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImF3YWl0UmVhZHkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQU8sTUFBTSxVQUFVLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBcUMsQ0FBSSxFQUFFLFNBQWlCLEVBQUUsa0JBQXNDLEVBQUUsRUFBRTtJQUNwSSxNQUFNLGdCQUFnQixHQUFHLGtCQUFrQixDQUFDLEtBQUssQ0FBQztJQUNsRCxrQkFBa0IsQ0FBQyxLQUFLLEdBQUcsS0FBSztRQUM1QixNQUFNLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDakIsT0FBTyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQ25ELENBQUMsQ0FBQztBQUNOLENBQUMsQ0FBQztBQU5XLFFBQUEsVUFBVSxjQU1yQiJ9