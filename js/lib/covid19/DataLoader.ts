
import xmljs from "xml2js";
import XmlCounty from "./County/XmlCounty";
import { cachable } from "../decorators/cachable";
import Io from "../io";
import { csvToObject } from "../csvUtil";
import CovidUtils from "./CovidUtils";
import CovidConstants from "./Constants";
import {County} from "./County/County";

const GIT_DIR = `${Io.PROJECT_DIR}/../git`;

const text = Io.readFile(`${Io.PROJECT_DIR}/county-maps/USA counties.svg`);
export default class DataLoader {
    //private static globalStates = DataLoader.getStates().then(([states]) => states);

    @cachable()
    static async getPopulations() : Promise<Record<string, number>> {
        const fileContent = await Io.readFile(`${GIT_DIR}/co-est2019-alldata.csv`);
        const populations = csvToObject(fileContent).flatMap(({ COUNTY, STATE, STNAME, CTYNAME, POPESTIMATE2019 }) =>
            +COUNTY && CTYNAME && STNAME ? {
                fips: +STATE * 1000 + +COUNTY,
                county: CTYNAME,
                state: STNAME,
                population: +POPESTIMATE2019
            } : []
        );
    
        (function combineCounties() {
            for (const populationsI of populations) {
                const {county, state} = populationsI;
                const countyState = CovidUtils.getCountyString(county, state);
    
                const combinedCounties = CovidConstants.COMBINED_COUNTIES[countyState];
                if (combinedCounties) {
                    var updates = false;
                    for (var j = 0; j < populations.length; j++) {
                        const populationsJ = populations[j];
                        const { county, state } = populationsJ;
                        const countyState = CovidUtils.getCountyString(county, state);
                        if (combinedCounties.includes(countyState)) {
                            populations.splice(j, 1);
                            j--;
                            updates = true;
                            for (const index of ["population"] as const) {
                                populationsI[index] += populationsJ[index];
                            }
                        }
                    }
                    if (updates) {
                        combineCounties();
                        return;
                    }
                }
            }
        }());

        return Object.fromEntries(populations.map(({ county, state, population }) =>
            [CovidUtils.getCountyString(county, state), population] as const));
    }

    static async getStates() {
        const xml = await xmljs.parseStringPromise(await text);
        const build = () => new xmljs.Builder().buildObject(xml);
        const states = [...(function *getAllCounties(parent: any): Generator<XmlCounty> {
            for (const g of parent.g || []) {
                yield *getAllCounties(g);
            }
    
            for (const {$} of parent.path || []) {
                const {id} = <{[x: string] : string}>$;
                const [, stateName, countyName] = id && id.match(/^([A-Z]{2})_([A-Za-z_\.\-]+)$/) || [];
                if (stateName) {
                    yield new XmlCounty(stateName, countyName.replace(/ /g, "_"), $);
                }
            }
        })(xml.svg)];
    
        return [states, build] as const;
    }

    @cachable()
    static async getRawDataForDate(date: string) {
        const fileContent = Io.readFileSync(`${GIT_DIR}/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/${date}.csv`);
        const all = csvToObject(fileContent).flatMap(({ Country_Region, Admin2, Confirmed, Deaths, FIPS, Recovered, Active, Province_State }) =>
            Country_Region && (+FIPS % 1000) ? {
                state: Province_State,
                county: Admin2,
                confirmed: +Confirmed,
                deaths: +Deaths,
                // recovered: +Recovered,
                // active: +Active
            } : []
        );

        return <Record<string, County>>Object.fromEntries(all.map(({ county, state, confirmed, deaths }) => 
            [CovidUtils.getCountyString(county, state), {confirmed, deaths}] as const));
    }
}
