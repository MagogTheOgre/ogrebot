<?php

/**
 * @property-read string $year
 * @property-read string $month
 * @property-read string $date
 */
readonly class Wiki_Date {

    private DateTimeImmutable $datetime;

    /**
     * @throws Regex_Exception
     */
    public function __construct(DateTimeImmutable|Wiki_Date|int|string $input,
        Wiki_Date_Rounding $rounding = Wiki_Date_Rounding::EXCEPTION) {
        if ($input instanceof Wiki_Date) {
            $this->datetime = $input->datetime;
        } else if ($input instanceof DateTimeImmutable) {
            $this->datetime = $input;
        } else {
            if (preg_match("/^(\d{4})(\d{2})(\d{2})$/", $input, $date_match)) {
                [, $year, $month, $day] = $date_match;
                [$hour, $minute, $second] = match ($rounding) {
                    Wiki_Date_Rounding::ROUND_DOWN => [0, 0, 0],
                    Wiki_Date_Rounding::ROUND_UP => [23, 59, 59],
                    default => throw new Regex_Not_Matched_Exception("Eight digits not supported", $input)
                };
            } else {
                [, $year, $month, $day, $hour, $minute, $second] =
                    Regex_Util::match("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", $input);
            }
            $this->datetime = (new DateTimeImmutable())->setDate($year, $month, $day)
                ->setTime($hour, $minute, $second)
                ->setTimezone(new DateTimeZone("UTC"));
        }
    }

    public function add(DateInterval $interval): self {
        return new self($this->datetime->add($interval));
    }

    public function diff(self $other, bool $absolute = false): DateInterval {
        return $this->datetime->diff($other->datetime, $absolute);
    }

    public function format($format): string {
        return $this->datetime->format($format);
    }

    public function get(): int {
        return +$this->format("YmdHis");
    }

    public function __toString(): string {
        return "". $this->get();
    }

    public function __get(string $var) {
        return match ($var) {
            "year" => $this->format("Y"),
            "month" => $this->format("m"),
            "date" => $this->format("d"),
            "hour" => $this->format("H"),
            "minute" => $this->format("i"),
            "second" => $this->format("s"),
            default => null,
        };
    }

}
