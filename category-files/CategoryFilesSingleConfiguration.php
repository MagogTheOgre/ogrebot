<?php
class CategoryFilesSingleConfiguration {

    private const OMIT_REGEXP = "/^omit(?: (\d+))?$/";

    private function __construct(

        /**
         *
         * @var Category_Files_Category[]
         */
        private readonly array $categories,
        private string $galleryName,
        private readonly ?int $width,
        private readonly ?int $height,
        private readonly bool $subpage,
        private readonly ?int $daysPerGallery,
        /**
         *
         * @var string[]
         */
        private readonly array $ignoredSubcategories,
        private readonly bool $warning,
        private readonly bool $noIndex,
        private readonly ?string $mode,
        private readonly bool $show_filename,
        private readonly int $max_overflow,

        /**
         * @var Category_Files_Category[]
         */
        private readonly array $hard_omissions) {
    }

    /**
     *
     * @return Category_Files_Category[]
     */
    public function getCategories(): array {
        return $this->categories;
    }

    public function getGalleryName(): string {
        return $this->galleryName;
    }

    public function getWidth(): ?int {
        return $this->width;
    }

    public function getHeight(): ?int {
        return $this->height;
    }

    public function getSubpage(): bool {
        return $this->subpage;
    }

    public function getDaysPerGallery(): ?int {
        return $this->daysPerGallery;
    }

    /**
     * @return string[]
     */
    public function getIgnoredSubcategories(): array {
        return $this->ignoredSubcategories;
    }

    public function isWarning(): bool {
        return $this->warning;
    }

    public function isNoIndex(): bool {
        return $this->noIndex;
    }

    public function getMode(): ?string {
        return $this->mode;
    }

    public function isShowFilename(): bool {
        return $this->show_filename;
    }

    /**
     * @return Category_Files_Category[]
     */
    public function get_hard_omissions(): array {
        return $this->hard_omissions;
    }

    /**
     *
     * @param Wiki_Date $date
     * @param boolean $include_name
     * @return string[]
     */
    public function get_subpage_and_overflow(Wiki_Date $date, bool $include_name = true): array {
        $subpage = $include_name ? $this->galleryName : "";
        if ($this->subpage) {
            $subpage .= "/" . $this->rangeText($date);
        }
        $subpages = [$subpage];

        for ($i = 1; $i <= $this->max_overflow; $i++) {
            $subpages[] = "$subpage/Overflow $i";
        }
        return $subpages;
    }

    private function rangeText(Wiki_Date $date): string {
        global $logger;

        /* range text */
        $monthName = $date->format("F");
        $daysPerGallery = $this->daysPerGallery;
        $daysThisMonth = cal_days_in_month(CAL_GREGORIAN, $date->month, $date->year);
        $galleriesPerMonth = ceil($daysThisMonth / $daysPerGallery);

        $minDays = floor($daysThisMonth / $galleriesPerMonth);
        $maxDays = ceil($daysThisMonth / $galleriesPerMonth);

        $rangeEnd = 0;
        do {
            $rangeStart = $rangeEnd + 1;
            if (($daysThisMonth - $rangeEnd) % $maxDays == 0) {
                $rangeEnd += $maxDays;
            } else {
                $rangeEnd += $minDays;
            }
        } while ($rangeEnd < +$date->date);

        $rangeText = ($rangeStart == 1 && $rangeEnd == $daysThisMonth) ? "" : (($rangeStart ==
            $rangeEnd) ? " $rangeStart" : " $rangeStart-$rangeEnd");

        $rangeText = "$date->year $monthName$rangeText";
        $logger->debug("\$rangeText($daysPerGallery) => \"$rangeText\"");

        return $rangeText;
    }

    /**
     * @return Category_Files_Category[]
     */
    private static function get_category_files_categories(Abstract_Template $template, string $variable,
        ?int $default_depth): array {
        $base_regexp = "/^$variable(?: (\d+))?";
        $name_regexp = "$base_regexp$/";
        $depth_regexp =  "$base_regexp depth$/";

        $names = self::getArrayType($template, $name_regexp, "Category:");
        $depths = self::getArrayType($template, $depth_regexp);
        /** @var Category_Files_Category[] $categories */
        return Array_Utils::array_map_pass_key($names, fn($key, $name) => new Category_Files_Category(
            $name, $depths[$key] ?? $default_depth));
    }

    /**
     * @return string[]
     */
    private static function getArrayType(Abstract_Template $template, string $regexp, string $prepend = ""): array {
        /** @var string[] $fieldsForRegexp */
        $keys = array_keys($template->__get("fields"));
        $fieldsForRegexp = Array_Utils::map_array_function_keys($keys, function(string $fieldKey) use ($template, $regexp, $prepend): ?array {
            $match = Regex_Util::match($regexp, $fieldKey, 0);
            return $match ? [isset($match[1]) ? +$match[1] : 1, $prepend . trim($template->fieldvalue($fieldKey))] : null;
        });
        ksort($fieldsForRegexp);
        return array_values($fieldsForRegexp);
    }

    private static function getStringType(Abstract_Template $template, string $field): ?string {
        $value = $template->fieldvalue($field);
        return $value !== false ? trim($value) : null;
    }

    private static function getIntegerType(Abstract_Template $template, string $field): ?int {
        $string_value = self::getStringType($template, $field);
        return $string_value !== null && $string_value !== "" ? +$string_value : null;
    }

    private static function getBooleanType(Abstract_Template  $template, string $field): ?bool {
        $string_value = self::getStringType($template, $field);
        if ($string_value) {
            if (in_array(strtolower($string_value), ["0", "false"])) {
                return false;
            }
            return true;
        }
        return null;
    }

    /**
     *
     * @return CategoryFilesSingleConfiguration[]
     */
    private static function initFromText(Wiki $wiki, string $gallery_vars_txt): array {
        global $logger, $wiki_interface;

        /** @var CategoryFilesSingleConfiguration[] $affiliations */
        $affiliations = Array_Utils::map(new Template_Iterator($gallery_vars_txt),
            function (Abstract_Template $configuration): ?CategoryFilesSingleConfiguration {
                $categories = self::get_category_files_categories($configuration, "category", null);
                $gallery = self::getStringType($configuration, "page");
                $width = self::getIntegerType($configuration, "width");
                $height = self::getIntegerType($configuration, "height");
                $subpage = self::getBooleanType($configuration, "subpage") ?? true;
                $daysPerGallery = self::getIntegerType($configuration, "days per gallery") ?? 11;
                $ignored_subcategories = self::getArrayType($configuration, self::OMIT_REGEXP, "Category:");
                $warning = self::getBooleanType($configuration, "warning") ?? false;
                $noIndex = self::getBooleanType($configuration, "noindex") ?? false;
                $mode = self::getStringType($configuration, "mode");
                $showFilename = self::getBooleanType($configuration, "show filename") ?? false;
                $max_overflow = self::getIntegerType($configuration, "max overflow") ??
                    Category_Files_Outputter::get_overflow_max();
                $hard_omissions = self::get_category_files_categories($configuration, "hard omit", 0);
                if ($gallery && $categories) {
                    return new CategoryFilesSingleConfiguration($categories, $gallery, $width, $height, $subpage,
                        $daysPerGallery, $ignored_subcategories, $warning, $noIndex, $mode, $showFilename,
                        $max_overflow, $hard_omissions);
                }
                return null;
            });

        /** @var CategoryFilesSingleConfiguration[] $affiliations_by_name */
        $affiliations_by_name = Array_Utils::map_array_function_keys($affiliations, fn($aff) => [$aff->getGalleryname(), $aff]);

        //normalize page titles
        /** @var Page_Text_Response[] $responses */
        $responses = $wiki_interface->get_text($wiki, array_map(fn($aff) => $aff->getGalleryName(), $affiliations));
        foreach ($responses as $response) {
            $from = $response->normalized_from ?? $response->redirect_from ?? null;
            if ($from) {
                $to =  $response->redirect_to ?? $response->normalized_to ?? null;
                $logger->info("Normalized/redirected $from -> $to");
                $affiliations_by_name[$from]->galleryName = $to;
            }
        }

        $logger->info(count($affiliations) . " galleries found.");
        $logger->debug($affiliations);

        return $affiliations;
    }

    /**
     *
     * @return CategoryFilesSingleConfiguration[]
     */
    public static function initCategoryFilesConfigurationsFromProperties(Wiki $wiki, string $pageName): array {
        global $logger, $wiki_interface;

        $logger->info("Querying variables page: " . $wiki->get_base_url() . " " . $pageName);

        $gallery_vars_txt = $wiki_interface->get_text($wiki, $pageName)->text;

        return CategoryFilesSingleConfiguration::initFromText($wiki, $gallery_vars_txt);
    }

    /**
     *
     * @return CategoryFilesSingleConfiguration[]
     * @throws CantOpenFileException
     */
    public static function initCategoryFilesConfigurationsFromLocalFile(Wiki $wiki, string $filename): array {
        global $logger;

        $logger->info("Initiating configuration from local properties: $filename");

        $text = Local_Io::file_get_contents_ensure($filename);
        return CategoryFilesSingleConfiguration::initFromText($wiki, $text);
    }
}

?>
