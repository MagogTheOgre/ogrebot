function encodeWikiTitleToUrl(url: string) {
    return encodeURIComponent(url).replace(/%2F/g, "/").replace(/%3A/g, ":").replace(/%20/g, "_");
}