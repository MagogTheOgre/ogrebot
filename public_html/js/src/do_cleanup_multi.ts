/// <reference path="../../../js/node_modules/@types/angular/index.d.ts" />
/// <reference path="./modules/wikiTitleToUrl.ts" />

interface DoCleanupServiceResponse {
    complete: boolean
    count: number
    error: boolean
    lineNum: number
    lines: {
        file: string
        changed: boolean
    }[]
    startup: boolean
}

interface DoCleanupMultiScope extends angular.IScope {
    complete: boolean
    error?: boolean
    filesCount: number
    lines: DoCleanupServiceResponse["lines"],
    processError: boolean
    request_key: string
    runTime: string
    scrollBottom: boolean
    started: boolean

    changedTotal(): number
}

angular
    .module("app", [])
    .controller("progress", ["$scope", "$http", "$timeout",
        async ($scope: DoCleanupMultiScope, $http: angular.IHttpService, $timeout: angular.ITimeoutService) => {
            function prettyTime(time: number) {
                var seconds;
                var minutes;
                var hours;
                var asString = "";

                time = +(time / 1000).toFixed(0);
                seconds = time % 60;
                time = (time - seconds) / 60;
                minutes = time % 60;
                hours = (time - minutes) / 60;

                if (hours) {
                    asString = `${hours} hours, `;
                }
                if (hours || minutes) {
                    asString += `${minutes} minutes, `;
                }

                return `${asString}${seconds} seconds`;
            }

            const startTime = +new Date();

            $scope.lines = [];
            $scope.changedTotal = () => {
                return $scope.lines.filter(line => line.changed).length;
            };

            //wait for request_key to be defined from ng-init
            await $timeout();

            if ($scope.error) {
                return;
            }

            let nextLine = 0;
            let readDataTime;

            (function call() {
                ($http.post("do_cleanup_multi_json.php", {
                    line: nextLine,
                    request_key: $scope.request_key
                }) as angular.IHttpPromise<DoCleanupServiceResponse>).then(
                    ({data: {complete, count, error, lineNum, lines, startup}}) => {
                    let readData;

                    nextLine = lineNum;

                    if (startup) {
                        $scope.started = true;
                        readData = true;
                    }

                    if (count != null) {
                        $scope.filesCount = count;
                        readData = true;
                    }

                    if (lines && lines.length) {
                        $scope.lines = $scope.lines.concat(lines);

                        if ($scope.scrollBottom) {
                            //scroll to the bottom
                            setTimeout(() => void scrollTo(0, document.body.scrollHeight));
                        }
                        readData = true;
                    }

                    if (complete) {
                        $scope.complete = true;
                        $scope.runTime = prettyTime(+new Date() - startTime);
                        return;
                    }

                    if (error) {
                        $scope.processError = true;
                        return;
                    }

                    //sanity check that background process is still running
                    if (readData) {
                        readDataTime = null;
                    } else {
                        if (readDataTime) {
                            if (new Date().getTime() - readDataTime > 300000) {
                                $scope.processError = true;
                                return;
                            }
                        } else {
                            readDataTime = new Date().getTime();
                        }
                    }

                    $timeout(call, 5000);
                }, () => void ($scope.processError = true));
            })();
        }
    ])
    .filter("escape", () => encodeWikiTitleToUrl)
    .filter("round", () => (number, precision) => number.toFixed(+precision));