
export interface County {
    confirmed: number;
    deaths: number;
}

export type CountyKey = keyof County;