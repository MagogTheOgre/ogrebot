<?php 

require_once __DIR__ . "/../base/bootstrap.php";
global $env, $logger;

$POST = $env->get_request_args();
//$POST['image'] = "File:Hbg fixed.jpg";

$obj = Do_Cleanup_Logic::get_cleanup_data($POST);

header('content-type: application/json; charset=utf-8');

//site is stateless; nothing harmful to be learned.
header("Access-Control-Allow-Origin: *");

$json_encode = String_Utils::json_encode($obj);
$logger->debug($json_encode);	
echo $json_encode;


?>