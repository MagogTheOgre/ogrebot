<?php class Latitude_Longitude_Svg {
	
	/**
	 *
	 * @var Array<string, Latitude_Longitude_Svg>
	 */
	private static array $instances;

	private static self $primary_instance;

    private function __construct(
        public readonly string $name,
        public readonly float $east,
        public readonly float $west,
        public readonly float $north,
        public readonly float $south,
        public readonly string $text,
        public readonly float $viewbox_east,
        public readonly float $viewbox_west,
        public readonly float $viewbox_north,
        public readonly float $viewbox_south,
        public readonly string $human_readable,
        public readonly bool $primary,
        public readonly string $wikitext
    ) {
        global $logger;

        $logger->debug("Loaded $name.svg");
    }

    private static function _autoload(): void {
        $map_wikitext = Local_Io::file_get_contents_ensure(BASE_DIRECTORY . "/properties/map.wikitext");
        self::$instances = Array_Utils::map_array_function_keys(
            Array_Utils::array_key_or_exception(OgreBotXmlParser::xmlFileToStruct("maps.xml"), "MAPS", 0,
                "elements", "MAP"),
            function ($element) use ($map_wikitext) {
                $name = Array_Utils::array_key_or_exception($element, "attributes", "NAME");
                $text = Local_Io::file_get_contents_ensure(BASE_DIRECTORY . "/maps/$name.svg");
                $author = Array_Utils::array_key_or_exception($element, "elements", "AUTHOR", 0, "value");
                $license = Array_Utils::array_key_or_exception($element, "elements", "LICENSE", 0, "value");

                preg_match("/viewBox\s*=\s*\"(-?[\d.]+) (\g1) (\g1) (\g1)\"/i", $text, $match);

                if ($match) {
                    list($viewbox_west, $viewbox_south, $viewbox_east, $viewbox_north) = $match;
                } else {
                    $viewbox_west = 0;
                    $viewbox_south = 0;

                    preg_match("/[^\-\w]height\s*=\s*\"([\d.]+)\"/i", $text, $height_match);
                    preg_match("/[^\-\w]width\s*=\s*\"([\d.]+)\"/i", $text, $width_match);
                    if (!$height_match || !$width_match) {
                        throw new UnexpectedValueException(
                            "Can't determine viewbox size " . "for $name");
                    }

                    $viewbox_east = $width_match[1];
                    $viewbox_north = $height_match[1];
                }

                $wikitext = String_Utils::replace_named_variables($map_wikitext,
                    [
                        "author" => $author,
                        "date" => date("Y-m-d"),
                        "license" => $license,
                        "source" => "[[:File:" . str_replace("_", " ", $name) . ".svg|]]"
                    ]);

                $instance = new Latitude_Longitude_Svg(
                    name: Array_Utils::array_key_or_exception($element, "attributes", "NAME"),
                    east: Array_Utils::array_key_or_exception($element, "attributes", "EAST"),
                    west: Array_Utils::array_key_or_exception($element, "attributes", "WEST"),
                    north: Array_Utils::array_key_or_exception($element, "attributes", "NORTH"),
                    south: Array_Utils::array_key_or_exception($element, "attributes", "SOUTH"),
                    text: $text,
                    viewbox_east: $viewbox_east,
                    viewbox_west: $viewbox_west,
                    viewbox_north: $viewbox_north,
                    viewbox_south: $viewbox_south,
                    human_readable: Array_Utils::array_key_or_exception($element, "attributes", "HUMAN-READABLE"),
                    primary: Array_Utils::deep_array_key_exists($element, "attributes", "PRIMARY"),
                    wikitext: $wikitext,
                );

                return [$name, $instance];
            }, "FAIL");

        $primaries = array_filter(self::$instances, fn($svg) => $svg->primary);

        if (count($primaries) !== 1) {
            throw new UnexpectedValueException(
                "Multiple primaries found in maps.xml. There should be exactly one.");
        }
        self::$primary_instance = array_pop($primaries);
    }

    public static function get_instances(): array {
		return self::$instances;
	}

    public static function get_primary_instance(): self {
        return self::$primary_instance;
    }

	public function get_height(): float {
		return ($this->viewbox_north - $this->viewbox_south) / ($this->north - $this->south);
	}

	public function get_width(): float {
		return ($this->viewbox_east - $this->viewbox_west) / ($this->east - $this->west);
	}
	
	/**
	 * Get the recommended height in order to make the map fit perfectly with
	 * the primary map
	 */
	public function get_recommended_height(): float {
		return ($this->north - $this->south) * self::$primary_instance->get_height();
	}
	
	/**
	 * Get the recommended height in order to make the map fit perfectly with
	 * the primary map
	 */
	public function get_recommended_width(): float {
		return ($this->east - $this->west) * self::$primary_instance->get_width();
	}
}
