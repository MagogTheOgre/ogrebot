<?php
require_once __DIR__ . "/../base/bootstrap.php";

/**
 * @throws ArrayIndexNotFoundException
 * @throws XMLParserException
 * @throws IllegalStateException
 */
function cron_run() {
	global $logger, $environment;
	
	$logger->debug("cron_run");
	
	if ($logger->isDebugEnabled()){
		$logger->debug(php_uname());
		$logger->debug(phpversion());
	}

	$cron_date_factory = Cron_Date_Factory::get_handler();

    $args = Environment::get()->load_command_line_args();
    $init = Array_Utils::find_command_line_arg($args, "init") !== null;

    if ($init) {
        $logger->all("Initialization requested.");
        $force = Array_Utils::find_command_line_arg($args, "force") !== null;
        $cron_date_factory->init($force);
        return;
    }

	$times = $cron_date_factory->get_dates();
	
	$logger->debug("Times:");
	$logger->debug($times);

	//load cron properties
	$processList = OgreBotXmlParser::xmlFileToStruct("processes-$environment[environment].xml");
	$configs = Array_Utils::array_key_or_exception($processList, array('CONFIGS', 0, 'elements', 'CONFIG'));
	
	$runners = array();
	foreach ($times as $time) {
		foreach($configs as $config) {

			$hour = intval(Array_Utils::array_key_or_exception($config, array('elements', 'HOUR', 0, 'value')));
			
			if ($hour === intval($time->getHour())) {
				$directory = Array_Utils::array_key_or_null($config, 'elements', 'DIRECTORY', 0, 'value');
				$command   = Array_Utils::array_key_or_exception($config, 'elements', 'COMMAND', 0, 'value');
				$args  = Array_Utils::array_key_or_blank($config, 'elements', 'ARGS', 0, 'value');
                $timeout = intval(Array_Utils::array_key_or_exception($config, 'elements', 'TIMEOUT', 0,
                        'value')) * SECONDS_PER_MINUTE;
				$runOnce = Array_Utils::deep_array_key_exists($config, 'elements', 'RUNONCE');
				$post = Array_Utils::array_key_or_null($config, 'elements', 'POST', 0, 'value');
				
				
				$cronConfig = new CronConfig();
				$cronConfig->setDirectory($directory);
				$cronConfig->setCommand($command);
				$cronConfig->setArgs($args);
				$cronConfig->setTimeout($timeout);
				$cronConfig->setPost($post);
				
				
				//check for previous runs
				$add = true;
				if ($runOnce) {
					foreach ($runners as $runner) {
						if ($runner->getRunConfig() == $cronConfig) {
							$logger->debug("Found duplicate instance of run config: $command. Not running.");
							$add = false;
							break;
						}
					}
				}
				
				if ($add) {
					$runner = Environment::get()->get_cron_runner();
					$runner->setCronDate($time);
					$runner->setRunConfig($cronConfig);
					
					$logger->debug("Config match for ".$runner);
										
					$runners[] = $runner;
				}				
			}
		}
	}
	
	$cron_date_factory->finalize();
	
	$logger->debug(count($runners)." commands to be run.");
	foreach ($runners as $i => $runner) {		
		$logger->info("**** Starting runner $i ****");
		$runner->run();	
		$logger->info("**** End runner $i ****");
	}
	$logger->debug("Complete.");
}

try {
	cron_run();
} catch (Exception $e) {
	Remote_Io::ogrebotMail($e);
}
