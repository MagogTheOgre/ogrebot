function generateUniqueClass(prefix: string) {
    var now = Date.now();
    
    var uniqueNowClass: string;

    do {
        uniqueNowClass = `${prefix}-${now++}`;
    } while (document.querySelector(`.${uniqueNowClass}`));

    return uniqueNowClass;
}
