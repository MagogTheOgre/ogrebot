<?php
readonly class CleanupResponse {
    /**
     * @param ?string $changes indicates if the cleanup method has made any changes whatsoever to the text.
     * Has the following possible values: array
     *           * "major"
     *           * "minor"
     *           * "none"
     * @param ?string[] $warnings if there are warnings emitted by the cleanup method.
     * @param ?string $text the new text emitted by the cleanup method.
     * @param ?string $edittime Mediawiki timestamp for the most recent edit to this file.
     */
    public function __construct(
        public ?string $changes = null,
        public ?array $warnings = null,
        public ?string $text = null,
        public ?string $edittime = null,
        public ?string $summary = null,
        public ?string $error = null,
        public ?string $title = null
    ) {
    }
}

class Do_Cleanup_Logic {
	
	/**
	 *
     * @param array{image: string, text: string} $post_data
	 *        	Should have the following indices:
	 *        	'image' => the file name to clean up
	 *        	'text' => the text for the filename. If not defined,
	 *        	then the logic will fetch the text of the page.
	 */
	public static function get_cleanup_data(array $post_data): CleanupResponse {
		global $messages;

		$environment = Environment::get();
        $database_dao = $environment->get_database_dao();
		$logger = $environment->get_logger();
        $wiki_interface = $environment->get_wiki_interface();

		try {
			
			$logger->debug("get_cleanup_data");
			$logger->debug($post_data);
			
			if (!array_key_exists('image', $post_data)) {
				return new CleanupResponse(error: "No file name specified");
			} else {
                $co = $wiki_interface->new_wiki("OgreBotCommons");
				
				$title = preg_replace("/^File:/", "", $post_data['image']);
				$image = $wiki_interface->new_image($co, $title);
				if ($post_data['text'] ?? null) {
					$text = $post_data['text'];
                    $pageCategories = null;
				} else {
					$text = $wiki_interface->get_text($co, "File:$title")->text;
                    $pageCategories = $database_dao->get_visible_categories("commons.wikimedia", 6,
                        [$title])[$title];
				}
				
				$history = $wiki_interface->get_upload_history($image);
				if (count($history) > 0) {
					
					$cleanup_response = (new Cleanup_Base)->super_cleanup($text, $pageCategories, true);
					
					$cleanup_text = $cleanup_response->get_text();
					
					$warnings = $cleanup_response->get_formatted_warnings(false);
					
					if ($cleanup_response->get_significant_changes()) {
						$changes = "major";
					} else {
						$any_changes = str_replace("\r\n", "\n", $cleanup_text) !==
							 str_replace("\r\n", "\n", $text);
						$changes = $any_changes ? "minor" : "none";
					}
					
					$default_summary = Array_Utils::array_key_or_exception($messages, 'do_cleanup.editsummary');
					$summary = $post_data['summary'] ?? null;
					
					if ($changes !== "none") {
						if ($summary) {
							$summary = "$summary +$default_summary";
						} else {
							$summary = $default_summary;
						}
					}

                    return new CleanupResponse(
                        changes: $changes,
                        warnings: $warnings,
                        text: $cleanup_text,
                        edittime: date('YmdHis', time() - 5),
                        summary: $summary
                    );
				} else {
					$encodedTitle = String_Utils::wikilink($title, "commons.wikimedia");

                    return new CleanupResponse(error: "\$title not found!", title: $encodedTitle);
				}
			}
		} catch (Exception $e) {
			Remote_Io::ogrebotMail($e);
			return new CleanupResponse(error: "Unknown error. The bot owner has been notified.");
		}
	}
}