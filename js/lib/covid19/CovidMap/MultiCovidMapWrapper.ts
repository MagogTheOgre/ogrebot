import CovidMap from "./CovidMap";
import MapBuilder from "../MapBuilder/MapBuilder";
import { AllSettledThreadPoolImpl } from "../../ThreadPool";
import CovidUtils from "../CovidUtils";

const {dateRoundFloorCopy} = CovidUtils;
export default class MultiCovidMapWrapper implements CovidMap<string[]> {

    private maps: CovidMap<string>[] = [];

    constructor(outputDir: string, requestedDates: Date[], extension: string, mapBuilder: MapBuilder,
        namingScheme: (date: Date, count: number) => string, mapConstructor: {new(s: string, d: Date, mapBuilder: MapBuilder): 
            CovidMap<string>}) {

        requestedDates = requestedDates.map(d => dateRoundFloorCopy(d));
        const digits = String(((+requestedDates[requestedDates.length - 1] - +requestedDates[0]) 
            / (1000 * 60 * 60 * 24)).toFixed(0)).length;
        for (const [count, date] of Object.entries(requestedDates)) {
            this.maps.push(new mapConstructor(`${outputDir}/${namingScheme(date, +count).padStart(digits, "0")}.${
                extension}`, date, mapBuilder));
        }
    }


    public create() {
        return new AllSettledThreadPoolImpl().enqueueAll(this.maps.map(map => () => map.create()));
    }

}