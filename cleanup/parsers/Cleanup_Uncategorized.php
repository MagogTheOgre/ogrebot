<?php
/**
 * @author magog
 *
 */
class Cleanup_Uncategorized implements Cleanup_Module {

	/**
	 *
	 * @var string[]
	 */
	private $categories;

	public function __construct(Cleanup_Package $cleanup_package) {
		$this->categories = $cleanup_package->get_constants()["categories_ignore_for_uncategorized"];
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see Cleanup_Module::cleanup()
	 */
	public function cleanup(Cleanup_Instance $ci) {
	    global $logger;

	    $categories  = $ci->get_categories();
	    if ($categories !== null) {
            if (array_diff($categories, $this->categories)) {
                $logger->debug("Visible categories present. Ignoring.");
            } else {
                $logger->debug("Visible categories NOT present. Adding unc.");
                $ci->add_uncategorized();
            }
        } else {
            $logger->debug("Categories not fetched. Ignoring flow.");
        }
	}
}