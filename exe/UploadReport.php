<?php
require_once __DIR__ . "/../base/bootstrap.php";
global $env, $wiki_interface;

try {
	$argv = $env->load_command_line_args();
	$update = Array_Utils::find_command_line_arg($argv, "update") !== null;
	$start = $argv[1] ?? null;
	$end = $argv[2] ?? null;
	if (!$end) {
		$end = substr($start, 0, 8) . "235959";
	}
	
	$co = $wiki_interface->new_wiki("OgreBotCommons");
	$upload_report_writer = new UploadReportWriter($co, +$start, +$end);
	$upload_report_writer->loadAndWrite($update);
} catch (Exception $e) {
	Remote_Io::ogrebotMail($e);
	throw $e;
}

?>
