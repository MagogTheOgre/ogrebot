import { ElectionResult, ElectionIncompleteResult } from './ElectionResult';
import { ElectionTracker } from './ElectionTracker';

class ElectionDiffResult implements ElectionResult {

    constructor(private diff: number) {

    }

    get party() {
        return this.diff > 0 ? "r" : this.diff < 0 ? "d" : "t";
    }

    get percent() {
        return Math.abs(this.diff);
    }

    get results(): ElectionResult["results"] {
        throw new Error("Unsupported method");
    }
    
    get complete() {
        return true;
    }
}

export class ElectionDiffTracker implements ElectionTracker {

    
    constructor(private readonly first: ElectionTracker, private readonly second: ElectionTracker) {
        
    }

    isEligible(stateName: string) {
        return this.first.isEligible(stateName) && this.second.isEligible(stateName);
    }
    async getCountyTotals() {
        const [first, second] = await Promise.all([this.first.getCountyTotals(), this.second.getCountyTotals()]);
        
        for (const [state, counties] of Object.entries(first)) {
            const secondCounties = second[state];
            if (!secondCounties) {
                console.warn(`Second doesn't have ${state}`);
                delete first[state];
                continue;
            }

            for (const [county, first] of Object.entries(counties)) {
                const second = secondCounties[county];
                if (!second) {
                    delete counties[county];
                    console.warn(`Second doesn't have ${county}, ${state}`);
                    continue;
                }
                const {results: {r: firstR, d: firstD}, complete: firstComplete} = first;
                const {results: {r: secondR, d: secondD}, complete: secondComplete} = second;

                if (!firstComplete || !secondComplete) {
                    counties[county] = new ElectionIncompleteResult();
                } else {
                    counties[county] = new ElectionDiffResult(secondR  + firstD - firstR - secondD);
                }
            }
        }
        return first;
    }
    
}