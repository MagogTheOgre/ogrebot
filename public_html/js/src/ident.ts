/// <reference path="../../../js/node_modules/@types/angular/index.d.ts" />
/// <reference path="../../../js/node_modules/@types/angular-cookies/index.d.ts" />

interface IdentScope extends angular.IScope {
    ajax: boolean,
    cookie: string | null | undefined,
    username: string | null | undefined
}

const IDENT_COOKIE_NAME = "magog-ident";
var watch: boolean;

if (location.pathname.endsWith("/complete.php")) {
    history.pushState("", "", "start.php");
}

angular
    .module("app", ["ngCookies"])
    .controller("ctrl", ["$scope", "$http", "$log", "$interval", "$cookies",
        ($scope: IdentScope, $http: angular.IHttpService, $log: angular.ILogService,
             $interval: angular.IIntervalService, $cookies: angular.cookies.ICookiesService) => {
            function getCookie() {
                return $cookies.get(IDENT_COOKIE_NAME);
            }

            function postWithWatch(state) {
                if (watch) {
                    parent.postMessage({ state: state, cookie: $scope.cookie }, location.origin);
                }
            }

            angular.extend($scope, {
                async authorize() {
                    $scope.ajax = true;
                    
                    const {redirect} = (await $http({ 
                        method: "get",
                        url: "authorize.php" 
                    }) as angular.IHttpResponse<{
                        redirect: string
                    }>).data;

                    $scope.ajax = false;
                    if (redirect) {
                        top.location.href = redirect;
                    }
                },
                async logout() {
                    $scope.ajax = true;
                    const {success} = (await $http({ 
                        method: "get",
                        url: "logout.php" 
                    }) as angular.IHttpResponse<{
                        success : boolean
                    }>).data;

                    $scope.ajax = false;
                    if (success) {
                        $scope.username = $scope.cookie = null;
                    }
                },
                open() {
                    open(location.pathname, "_blank");
                }
            });

            $scope.cookie = getCookie();

            $interval(() => {
                let newCookie = getCookie();
                if (newCookie !== $scope.cookie) {
                    $scope.username = null;
                    $scope.cookie = newCookie;
                    postWithWatch("update");
                }
            }, 2000);

            addEventListener("message", event => {
                const {data, origin} = event;

                if (origin !== location.origin) {
                    $log.error(`Bad origin: ${origin} != ${location.origin}`);
                    return;
                }

                watch = data?.watch;
                if (watch) {
                    postWithWatch("initial");
                }
            });
        }
    ])
    .filter("escape", () => (url: string) =>
        encodeURIComponent(url).replace(/%2F/g, "/").replace(/%3A/g, ":").replace(/%20/g, "_"));
