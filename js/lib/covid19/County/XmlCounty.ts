

export default class XmlCounty {
    constructor(public readonly state: string, public readonly county: string, private path : {style?: string}) {
    }

    public set fill(color: string) {
        this.path.style = `fill: ${color}`;
    }
}