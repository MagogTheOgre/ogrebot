"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCountyResultsBuilder = void 0;
const CovidUtils_1 = __importDefault(require("../CovidUtils"));
const DataLoader_1 = __importDefault(require("../DataLoader"));
const collectionUtils_1 = require("../../collectionUtils");
class CountyResultsBuilderImpl {
    _date;
    _key;
    initialStream;
    pipes = [];
    constructor(_date, _key) {
        this._date = _date;
        this._key = _key;
        this.initialStream = this.getDate(0).then(rawData => collectionUtils_1.mapValues(rawData, county => [county, county[this._key]]));
    }
    getDate(offset) {
        //shallow copy to perform manipulations
        const date = new Date(this._date);
        const fileName = CovidUtils_1.default.toMdyString((date.setDate(date.getDate() - offset), date));
        return DataLoader_1.default.getRawDataForDate(fileName);
    }
    get(offset) {
        return this.getDate(offset).then(rawData => collectionUtils_1.mapValues(rawData, ({ [this._key]: result }) => result));
    }
    pipe(setup, mapper) {
        this.pipes.push((stream) => setup.then(initial => collectionUtils_1.mapValues(stream, ([county, total], countyName) => [county, mapper(total, initial, countyName)])));
        return this;
    }
    async build() {
        return collectionUtils_1.mapValues(await this.pipes.reduce(async (previous, pipe) => pipe(await previous), this.initialStream), ([, number]) => number);
    }
    diff(numberOfDays) {
        return this.pipe(this.get(numberOfDays), (total, previous, name) => total - (previous[name] || 0));
    }
    perCapita() {
        return this.pipe(DataLoader_1.default.getPopulations(), (total, populations, name) => total / (populations[name] || Infinity));
    }
}
const getCountyResultsBuilder = (date) => (county) => new CountyResultsBuilderImpl(date, county);
exports.getCountyResultsBuilder = getCountyResultsBuilder;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ291bnR5UmVzdWx0cy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNvdW50eVJlc3VsdHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQ0EsK0RBQXVDO0FBQ3ZDLCtEQUF1QztBQUN2QywyREFBa0Q7QUFlbEQsTUFBTSx3QkFBd0I7SUFJRztJQUE4QjtJQUhuRCxhQUFhLENBQXdCO0lBQzVCLEtBQUssR0FBd0QsRUFBRSxDQUFDO0lBRWpGLFlBQTZCLEtBQVcsRUFBbUIsSUFBZTtRQUE3QyxVQUFLLEdBQUwsS0FBSyxDQUFNO1FBQW1CLFNBQUksR0FBSixJQUFJLENBQVc7UUFDdEUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUNoRCwyQkFBUyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsRUFBRSxDQUFtQixDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3JGLENBQUM7SUFFTyxPQUFPLENBQUMsTUFBYztRQUMxQix1Q0FBdUM7UUFDdkMsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xDLE1BQU0sUUFBUSxHQUFHLG9CQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUN2RixPQUFPLG9CQUFVLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVPLEdBQUcsQ0FBQyxNQUFjO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQywyQkFBUyxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDdkcsQ0FBQztJQUVPLElBQUksQ0FBSSxLQUFpQixFQUFFLE1BQWlFO1FBQ2hHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBb0IsRUFBRSxFQUFFLENBQ3JDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQywyQkFBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxFQUFFLFVBQVUsRUFBRSxFQUFFLENBQ3JFLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUNwRCxDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLEtBQUssQ0FBQyxLQUFLO1FBQ2QsT0FBTywyQkFBUyxDQUFDLE1BQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLFFBQVEsQ0FBQyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsRUFDeEcsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFTSxJQUFJLENBQUMsWUFBb0I7UUFDNUIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsS0FBSyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdkcsQ0FBQztJQUVNLFNBQVM7UUFDWixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQVUsQ0FBQyxjQUFjLEVBQUUsRUFBRSxDQUFDLEtBQUssRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQztJQUN6SCxDQUFDO0NBQ0o7QUFFRCxNQUFNLHVCQUF1QixHQUFHLENBQUMsSUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDLE1BQWlCLEVBQXlCLEVBQUUsQ0FDekYsSUFBSSx3QkFBd0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFFRSwwREFBdUIifQ==