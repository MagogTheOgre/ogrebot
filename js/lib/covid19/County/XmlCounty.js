"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class XmlCounty {
    constructor(state, county, path) {
        this.state = state;
        this.county = county;
        this.path = path;
    }
    set fill(color) {
        this.path.style = `fill: ${color}`;
    }
}
exports.default = XmlCounty;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiWG1sQ291bnR5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiWG1sQ291bnR5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUEsTUFBcUIsU0FBUztJQUMxQixZQUE0QixLQUFhLEVBQWtCLE1BQWMsRUFBVSxJQUF1QjtRQUE5RSxVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQWtCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFtQjtJQUMxRyxDQUFDO0lBRUQsSUFBVyxJQUFJLENBQUMsS0FBYTtRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxTQUFTLEtBQUssRUFBRSxDQUFDO0lBQ3ZDLENBQUM7Q0FDSjtBQVBELDRCQU9DIn0=