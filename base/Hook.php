<?php
class Hook {
	
	/**
	 * 
	 * @var callable[]
	 */
	private array $callbacks = array();

	private bool $is_run = false;

	private function run(callable $callback): void {
		global $logger;
        $logger?->trace("Running callback.".print_r($callback, true));
		$callback();
	}

	public function add(callable $callback): void {
		if ($this->is_run) {
			$this->run($callback);
		} else {
			$this->callbacks[] = $callback;
		}
	}
	
	/**
	 * @return void
	 */
	public function trigger(): void {
		global $logger;

        $logger?->trace("Running " . count($this->callbacks) . " callbacks.");
        foreach ($this->callbacks as $callback) {
            $this->run($callback);
        }
		
		$this->is_run = true;
	}
}