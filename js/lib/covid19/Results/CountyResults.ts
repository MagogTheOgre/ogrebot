import { County, CountyKey } from "../County/County";
import CovidUtils from "../CovidUtils";
import DataLoader from "../DataLoader";
import { mapValues } from "../../collectionUtils";

type CountyStream = Record<string, [County, number]>;

type FinalCountyResult = Promise<Record<string, number>>;

interface CountyResultsBuilder {

    diff(numberOfDays: number): this
    
    perCapita(): this

    build() : FinalCountyResult
}

class CountyResultsBuilderImpl implements CountyResultsBuilder {
    private initialStream: Promise<CountyStream>;
    private readonly pipes: ((stream: CountyStream) => Promise<CountyStream>)[] = [];
    
    constructor(private readonly _date: Date, private readonly _key: CountyKey) {
        this.initialStream = this.getDate(0).then(rawData => 
            mapValues(rawData, county => <[County, number]>[county, county[this._key]]));
    }

    private getDate(offset: number) {
        //shallow copy to perform manipulations
        const date = new Date(this._date);
        const fileName = CovidUtils.toMdyString((date.setDate(date.getDate() - offset), date));
        return DataLoader.getRawDataForDate(fileName);
    }

    private get(offset: number) {
        return this.getDate(offset).then(rawData => mapValues(rawData, ({[this._key]: result}) => result));
    }

    private pipe<T>(setup: Promise<T>, mapper: (total: number, initial: T, countyName: string) => number) {
        this.pipes.push((stream: CountyStream) =>
            setup.then(initial => mapValues(stream, ([county, total], countyName) =>
               [county, mapper(total, initial, countyName)]))
        );
        return this;
    }

    public async build() : FinalCountyResult {
        return mapValues(await this.pipes.reduce(async (previous, pipe) => pipe(await previous), this.initialStream),
            ([, number]) => number);
    }

    public diff(numberOfDays: number) {
        return this.pipe(this.get(numberOfDays), (total, previous, name) => total - (previous[name] || 0));
    }

    public perCapita() {
        return this.pipe(DataLoader.getPopulations(), (total, populations, name) => total / (populations[name] || Infinity));
    }
}

const getCountyResultsBuilder = (date: Date) => (county: CountyKey) : CountyResultsBuilder => 
    new CountyResultsBuilderImpl(date, county);

export {CountyResultsBuilder, FinalCountyResult, getCountyResultsBuilder};