declare namespace JQueryUI {
    interface DatepickerTimeOptions extends DatepickerOptions {
        timeFormat?: string
    }
}

interface JQuery {

    datetimepicker(options: JQueryUI.DatepickerTimeOptions): JQuery;
}