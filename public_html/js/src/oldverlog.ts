/// <reference path="../../../js/node_modules/@types/jquery/index.d.ts" />
/// <reference path="../../../js/node_modules/@types/jqueryui/index.d.ts" />

/**
 * @license
 * Datepicker icon is licensed as BSD by Wikibooks user Tbernhard,
 * https://commons.wikimedia.org/wiki/File:Calendar_-_2.png
 */

const VIEW_BY_DATE_SELECTOR = ".view-by-date";
const viewByDateFrom = $("[name='from']", VIEW_BY_DATE_SELECTOR);
const viewByDateTo = $("[name='to']", VIEW_BY_DATE_SELECTOR);

const datepickerElements = viewByDateFrom.add(viewByDateTo)

const datepickerFormat = "yy-mm-dd";

function updateDatepickerMinMax() {
    const from = viewByDateFrom.datepicker("getDate");
    const to = viewByDateTo.datepicker("getDate");
    const startDate = $.datepicker.parseDate(datepickerFormat, $("#start-date").val() as string);
    const endDate = $.datepicker.parseDate(datepickerFormat, $("#end-date").val() as string);

    viewByDateFrom.datepicker("option", { minDate: startDate, maxDate: to || endDate });
    viewByDateTo.datepicker("option", { minDate: from || startDate, maxDate: endDate });
}

const viewByDate = $(VIEW_BY_DATE_SELECTOR).dialog({
    autoOpen: false,
    close() {
        datepickerElements.datepicker("destroy");
    },
    modal: true,
    width: 300
}) as JQuery<HTMLButtonElement> | undefined;

$(".view-by-date-button").on("click", () => {
    viewByDate.dialog("open");
    datepickerElements.trigger("blur").datepicker({
        showOn: "button",
        buttonImage: "images/calendar.png",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: datepickerFormat,
        onClose: updateDatepickerMinMax
    });
    updateDatepickerMinMax();
});

datepickerElements.on("change", updateDatepickerMinMax);

$("#view-by-date-cancel").on("click", () => void viewByDate.dialog("close"));

$("#view-by-date-form")
    .on("submit", () => {
        const matches = datepickerElements.is((_, element) => !!($(element).val() as string).match(/\d{4}-\d{2}-\d{2}/));
        return matches || (alert("Please enter a date in the form yyyy-mm-dd"), false);
    })
    .on("reset", () => setTimeout(updateDatepickerMinMax));