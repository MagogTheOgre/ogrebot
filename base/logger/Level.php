<?php
enum Level: int {
    case ALL = -1000;
    case OFF = -1;
    case FATAL = 0;
    case ERROR = 1;
    case WARN = 2;
    case INFO = 3;
    case DEBUG = 4;
    case TRACE = 5;
    case INSANE = 6;
	
	/**
	 * @throws InvalidArgumentException
	 */
	public static function parse(string $level): Level {
        foreach (self::cases() as $haystack) {
            if (strtoupper($level) === strtoupper($haystack->name)) {
                return $haystack;
            }
        }
        throw new InvalidArgumentException("Unrecognized level: $level");
    }
}