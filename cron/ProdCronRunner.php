<?php

class ProdCronRunner extends AbstractCronRunner {

    public function getExecCommand() {
        $directory = $this->runConfig->getDirectory();
        $command = $this->runConfig->getCommand();
        $args = $this->runConfig->getArgs();
        $timeout = $this->runConfig->getTimeout();
        $timestamp = ComputedConstants::NOW;
        $pid = getmypid();

        return "toolforge-jobs run $command-$timestamp-$pid --command " .
            " \"php7.4 /data/project/magog/$directory/$command.php $args\"" .
            " --image tf-php74 --no-filelog --mem 4G";
    }
}