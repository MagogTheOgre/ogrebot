<?php
class Wiki_Date_Iterator implements Iterator {

    private Wiki_Date $start;

    private Wiki_Date $current;

    private Wiki_Date $end;

    private DateInterval $diff;

    private int $key = 0;

    private DateInterval $secondOffset;

    /**
     * @throws Regex_Exception
     */
    public function __construct(
        DateTimeImmutable|Wiki_Date|int|string $startInclusive,
        DateTimeImmutable|Wiki_Date|int|string $endInclusive,
        DateInterval|int $diff = 1
    ) {
        [$this->start, $this->end] = array_map(fn ($date) => new Wiki_Date($date), [$startInclusive, $endInclusive]);
        $this->current = $this->start;
        $this->diff = is_numeric($diff) ? new DateInterval("P1D") : $diff;
        $this->secondOffset = new DateInterval("PT1S");
        $this->secondOffset->invert = ($this->diff->invert - 1) * -1;
    }

    public function next() : void {
        $this->key++;
        $this->current = $this->peek();
    }

    public function valid() : bool {
        return $this->current->diff($this->end)->invert === $this->diff->invert;
    }

    /**
     *
     * {@inheritDoc}
     * @see Iterator::current()
     * @return [Wiki_Date, Wiki_Date]
     */
    public function current(): array {
        return [
            $this->current,
            $this->peekWithEnd()
        ];
    }

    public function rewind(): void {
        $this->key = 0;
        $this->current = $this->start;
    }

    public function key(): int {
        return $this->key;
    }

    private function peek() : Wiki_Date {
        return $this->current->add($this->diff);
    }

    private function peekWithEnd(): Wiki_Date {
        $peek = $this->peek()->add($this->secondOffset);
        return $peek->diff($this->end)->invert === $this->diff->invert ? $peek : $this->end;
    }

}
