<?php
/**
 * 
 * @author magog
 *
 */
class FileStatement extends Statement {
    
    /**
     * 
     * @param string $name
     * @param mysqli $connection
     * @throws CantOpenFileException
     */
    function __construct(string $name, mysqli $connection) {
        parent::__construct(Local_Io::file_get_contents_ensure(
            BASE_DIRECTORY . "/statements/$name.sql"), $connection);
    }    
}