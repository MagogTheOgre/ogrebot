import fs from "fs";

const LOG_LEVELS = ["trace", "debug", "info", "warn", "error"] as const;

type LogLevel = typeof LOG_LEVELS[number];

const processMatch = process.argv[1].match(/\/([^\/]+?)\.js/);
if (!processMatch) {
    throw new Error("Can't determine process name");
}


console.log(processMatch[1]);
const logLevelString = <LogLevel>(
    JSON.parse(fs.readFileSync(`${__dirname}/../config.json`).toString())["log-level"][processMatch[1]].toLowerCase());

const myLevelLevel :number = LOG_LEVELS.indexOf(logLevelString);

if (myLevelLevel === undefined) {
    throw new Error(`Unrecognized log level: ${logLevelString}`);
}
interface Logger {
    readonly traceEnabled: boolean;
    readonly debugEnabled: boolean;
    readonly infoEnabled: boolean;
    readonly warnEanbled: boolean;
    readonly errorEnabled: boolean;
    trace(...args: any[]) : void;
    debug(...args: any[]) : void;
    info(...args: any[]) : void;
    warn(...args: any[]) : void;
    error(...args: any[]) : void;
}

const logger = <Logger>Object.fromEntries(function*(){
    for (const [level, entry] of Object.entries(LOG_LEVELS)) {
        const enabled =  myLevelLevel <= +level;
        yield [`${entry}Enabled`, enabled];
        yield [entry, enabled ? console[(entry !== "trace" ? entry : "debug")] : function () { }];
    }
}());

logger.debug(`Logger loaded with level ${logLevelString}`);

export default logger;