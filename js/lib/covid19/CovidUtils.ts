import CovidConstants from "./Constants";

export default class CovidUtils {
    static dateRoundFloorCopy(date: Date, offset: number = 0) {
        const newDate = new Date(date);
        newDate.setUTCDate(newDate.getUTCDate() + offset);
        (["setUTCHours", "setUTCMinutes", "setUTCSeconds", "setUTCMilliseconds"] as const).forEach(method => void newDate[method](0));
        return newDate;
    }
    static toDmyString(date: Date) {
        return `${String(date.getUTCFullYear()).padStart(4, "0")}-${String(date.getUTCMonth() + 1).padStart(2, "0")}-${
            String(date.getUTCDate()).padStart(2, "0")}`;
    }
    static toMdyString(date: Date) {
        return `${String(date.getUTCMonth() + 1).padStart(2, "0")}-${String(date.getUTCDate()).padStart(2, "0")
            }-${String(date.getUTCFullYear()).padStart(4, "0")}`;
    }
    static getCountyString(county: string, state: string) {
        return `${county.replace(/_/g, " ").replace(/ (County|Parish)$/, "").
            replace(/'/g, "")}_${CovidConstants.STATE_ABBREVIATIONS[state] || state}`;
    }
    static getColor(colors: [number, string][]) {
        return (colorPerCapita: number) => {
            for (const [min, color] of colors) {
                if (min >= colorPerCapita) {
                    return color;
                }
            }
            return colors[colors.length - 1][1];
        };
    }
}