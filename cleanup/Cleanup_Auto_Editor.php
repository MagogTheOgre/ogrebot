<?php
class Cleanup_Auto_Editor {
	
	/**
	 *
	 * @var Cleanup_Base
	 */
	private $cleanup_base;
	
	/**
	 *
	 * @var Page
	 */
	private $noref_pg;
	
	/**
	 *
	 * @var Page
	 */
	private $nootrs_pg;
	
	/**
	 *
	 * @var Page
	 */
	private $del_pg;
	
	/**
	 *
	 * @var Wiki
	 */
	private $co2;
	
	/**
	 *
	 * @var string
	 */
	private $edit_summary_all_files_desc;
	
	/**
	 *
	 * @param Wiki $co2        	
	 * @param string $edit_summary_all_files_desc        	
	 */
	public function __construct(Wiki $co2, $edit_summary_all_files_desc) {
		global $logger, $wiki_interface;
		
		$logger->debug("Starting up...");
		$this->cleanup_base = new Cleanup_Base();
		
		$this->noref_pg = $wiki_interface->new_page($co2, 
			"User:OgreBot/Pages moved to Commons with missing information");
		$this->nootrs_pg = $wiki_interface->new_page($co2, 
			"User:OgreBot/Pages moved to Commons with missing OTRS templates");
		$this->del_pg = $wiki_interface->new_page($co2, 
			"User:OgreBot/Pages moved to Commons with possible licensing issues");
		$this->co2 = $co2;
		$this->edit_summary_all_files_desc = $edit_summary_all_files_desc;
		
		$logger->debug("Startup finished");
	}
	
	public function process(string $title, string $text, ?array $categories) {
		global $logger, $wiki_interface;
		
		$made_change = false;
		do {
			$thistime = time();
			$logger->debug("Autoprocessing $title");
			$cleanup_response = $this->cleanup_base->super_cleanup($text, $categories, false);
			$newtext = $cleanup_response->get_text();
			
			$time_to_process = time() - $thistime;
			
			if ($time_to_process > 5) {
				$logger->error("$title took $time_to_process seconds to process.");
			}
			$editsummary = "";
			if ($cleanup_response->get_significant_changes()) {
				$editsummary = "clean up";
			}
			
			if ($editsummary !== "") {
				$pg = $wiki_interface->new_page($this->co2, $title);
				if ($text !== $wiki_interface->get_page_text($pg, true)) {
					// edit conflict?
					$text = $wiki_interface->get_page_text($pg, false);
					$logger->debug("(EDIT CONFLICT)");
					continue;
				}
				$revisions = $pg->history(5000);
				array_shift($revisions);
				
				$logger->info("Editing $title");
				$wiki_interface->edit_suppress_exceptions($pg, $newtext, 
					"(BOT): $editsummary of " . $this->edit_summary_all_files_desc, true, true, 
					false, false, false);
				$made_change = true;
			}
			$warnings = $cleanup_response->get_warnings();
			
			if ((Array_Utils::array_remove($warnings, Cleanup_Shared::OTRS_REMOVED))) {
				$logger->info("($title - logging no OTRS transclusion... ");
				$newlink = "\n*[[:$title]]";
				$text = $wiki_interface->get_page_text($this->nootrs_pg);
				if (strpos($text, $newlink) === false) {
					$wiki_interface->edit_suppress_exceptions($this->nootrs_pg, "\n*[[:$title]]", 
						"(BOT): adding [[$title]]", true, true, false, "ap");
				} else {
					$logger->info("already logged, skipped)");
				}
			}
			if ((Array_Utils::array_remove($warnings, Cleanup_Shared::LOCAL_DELETION))) {
				$logger->info("($title - logging possibly deleteable transclusion... ");
				
				$newlink = "\n*[[:$title]]";
				$text = $wiki_interface->get_page_text($this->del_pg);
				if (strpos($text, $newlink) === false) {
					$wiki_interface->edit_suppress_exceptions($this->del_pg, "\n*[[:$title]]", 
						"(BOT): adding [[$title]]", true, true, false, "ap");
				} else {
					$logger->info("already logged, skipped)");
				}
			}
			if ($warnings) {
				$logger->info("($title - logging no cite replacement error)... ");
				
				$message = "\n*[[:$title]] - " .
					 implode("; ", $cleanup_response->get_formatted_warnings(true));
				$text = $wiki_interface->get_page_text($this->noref_pg);
				if (strpos($text, $message) === false) {
					$wiki_interface->edit_suppress_exceptions($this->noref_pg, $message, 
						"(BOT): adding [[$title]]", true, true, false, "ap");
				} else {
					$logger->info("already logged, skipped)");
				}
			}
		} while (0);
		
		return $made_change;
	}
}