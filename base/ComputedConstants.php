<?php

define("COMPUTED_CATEGORY_FILES_OUTPUT_PATH", BASE_DIRECTORY . "/" .
    Environment::prop("constants", "category_files.output_path") . "/");
define("COMPUTED_DEFAULT_JSON_ENCODE_OPTIONS", 
    Environment::prop("environment", "jsonencode.options"));
define("DATABASE_MAX_QUERY", Environment::prop("databases", "max-query"));
define("SSL_VERIFY_PEER", Environment::prop("environment", "ssl.verify"));
define("ComputedConstants_now", time());
define("COMPUTED_FILE_IMPORT_TAGS", 
    array_map(function(string $string) {
        return +$string;
    },
    Environment::prop("constants", "file_importer_tags")));

define("MICROTIME_MICRO_OFFSET", strpos(microtime(), " "));

class ComputedConstants {
    const DATABASE_MAX_QUERY = DATABASE_MAX_QUERY;
    const CATEGORY_FILES_OUTPUT_PATH = COMPUTED_CATEGORY_FILES_OUTPUT_PATH;
    const DEFAULT_JSON_ENCODE_OPTIONS = COMPUTED_DEFAULT_JSON_ENCODE_OPTIONS;
    CONST SSL_VERIFY_PEER = SSL_VERIFY_PEER;

    const MICROTIME_MICRO_OFFSET = MICROTIME_MICRO_OFFSET;

    const MICROTIME_SECONDS_OFFSET = MICROTIME_MICRO_OFFSET + 1;
    
    /**
     * 
     * @var integer
     */
    const NOW = ComputedConstants_now;
    
    /**
     * 
     * @var int[]
     */
    const FILE_IMPORT_TAGS = COMPUTED_FILE_IMPORT_TAGS;
}