"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ElectionDiffTracker = void 0;
const ElectionResult_1 = require("./ElectionResult");
class ElectionDiffResult {
    diff;
    constructor(diff) {
        this.diff = diff;
    }
    get party() {
        return this.diff > 0 ? "r" : this.diff < 0 ? "d" : "t";
    }
    get percent() {
        return Math.abs(this.diff);
    }
    get results() {
        throw new Error("Unsupported method");
    }
    get complete() {
        return true;
    }
}
class ElectionDiffTracker {
    first;
    second;
    constructor(first, second) {
        this.first = first;
        this.second = second;
    }
    isEligible(stateName) {
        return this.first.isEligible(stateName) && this.second.isEligible(stateName);
    }
    async getCountyTotals() {
        const [first, second] = await Promise.all([this.first.getCountyTotals(), this.second.getCountyTotals()]);
        for (const [state, counties] of Object.entries(first)) {
            const secondCounties = second[state];
            if (!secondCounties) {
                console.warn(`Second doesn't have ${state}`);
                delete first[state];
                continue;
            }
            for (const [county, first] of Object.entries(counties)) {
                const second = secondCounties[county];
                if (!second) {
                    delete counties[county];
                    console.warn(`Second doesn't have ${county}, ${state}`);
                    continue;
                }
                const { results: { r: firstR, d: firstD }, complete: firstComplete } = first;
                const { results: { r: secondR, d: secondD }, complete: secondComplete } = second;
                if (!firstComplete || !secondComplete) {
                    counties[county] = new ElectionResult_1.ElectionIncompleteResult();
                }
                else {
                    counties[county] = new ElectionDiffResult(secondR + firstD - firstR - secondD);
                }
            }
        }
        return first;
    }
}
exports.ElectionDiffTracker = ElectionDiffTracker;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRWxlY3Rpb25EaWZmVHJhY2tlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkVsZWN0aW9uRGlmZlRyYWNrZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEscURBQTRFO0FBRzVFLE1BQU0sa0JBQWtCO0lBRUE7SUFBcEIsWUFBb0IsSUFBWTtRQUFaLFNBQUksR0FBSixJQUFJLENBQVE7SUFFaEMsQ0FBQztJQUVELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBQzNELENBQUM7SUFFRCxJQUFJLE9BQU87UUFDUCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxJQUFJLE9BQU87UUFDUCxNQUFNLElBQUksS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELElBQUksUUFBUTtRQUNSLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Q0FDSjtBQUVELE1BQWEsbUJBQW1CO0lBR0M7SUFBeUM7SUFBdEUsWUFBNkIsS0FBc0IsRUFBbUIsTUFBdUI7UUFBaEUsVUFBSyxHQUFMLEtBQUssQ0FBaUI7UUFBbUIsV0FBTSxHQUFOLE1BQU0sQ0FBaUI7SUFFN0YsQ0FBQztJQUVELFVBQVUsQ0FBQyxTQUFpQjtRQUN4QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ2pGLENBQUM7SUFDRCxLQUFLLENBQUMsZUFBZTtRQUNqQixNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxHQUFHLE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFekcsS0FBSyxNQUFNLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDbkQsTUFBTSxjQUFjLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ2pCLE9BQU8sQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEtBQUssRUFBRSxDQUFDLENBQUM7Z0JBQzdDLE9BQU8sS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNwQixTQUFTO2FBQ1o7WUFFRCxLQUFLLE1BQU0sQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEQsTUFBTSxNQUFNLEdBQUcsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN0QyxJQUFJLENBQUMsTUFBTSxFQUFFO29CQUNULE9BQU8sUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUN4QixPQUFPLENBQUMsSUFBSSxDQUFDLHVCQUF1QixNQUFNLEtBQUssS0FBSyxFQUFFLENBQUMsQ0FBQztvQkFDeEQsU0FBUztpQkFDWjtnQkFDRCxNQUFNLEVBQUMsT0FBTyxFQUFFLEVBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFDLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBQyxHQUFHLEtBQUssQ0FBQztnQkFDekUsTUFBTSxFQUFDLE9BQU8sRUFBRSxFQUFDLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBQyxFQUFFLFFBQVEsRUFBRSxjQUFjLEVBQUMsR0FBRyxNQUFNLENBQUM7Z0JBRTdFLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxjQUFjLEVBQUU7b0JBQ25DLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLHlDQUF3QixFQUFFLENBQUM7aUJBQ3JEO3FCQUFNO29CQUNILFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLGtCQUFrQixDQUFDLE9BQU8sR0FBSSxNQUFNLEdBQUcsTUFBTSxHQUFHLE9BQU8sQ0FBQyxDQUFDO2lCQUNuRjthQUNKO1NBQ0o7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0NBRUo7QUF6Q0Qsa0RBeUNDIn0=