"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatesBuilder = void 0;
const xml2js_1 = __importDefault(require("xml2js"));
const io_1 = __importDefault(require("../io"));
const fs_1 = __importDefault(require("fs"));
const style_to_object_1 = __importDefault(require("style-to-object"));
const collectionUtils_1 = require("../collectionUtils");
const stringUtils_1 = require("../stringUtils");
const builder = new xml2js_1.default.Builder();
const mapsDir = `${io_1.default.PROJECT_DIR}/county-maps`;
class County {
    state;
    name;
    path;
    constructor(state, name, path) {
        this.state = state;
        this.name = name;
        this.path = path;
    }
    set fill(fill) {
        if (fill) {
            this.path.style = stringUtils_1.styleToString({
                ...style_to_object_1.default(this.path.style) || {},
                fill
            });
        }
    }
    //normalize common abbreviations
    static normalize(state, county) {
        return ((state, county) => {
            switch (state) {
                case "LA":
                    switch (county) {
                        case "lasalle":
                            return "la salle";
                    }
                    break;
                case "MS":
                    switch (county) {
                        case "jeff davis":
                            return "jefferson davis";
                    }
                    break;
                case "NV":
                    switch (county) {
                        case "carson city_city":
                            return "carson city";
                    }
            }
            return county;
        })(state, county.toLowerCase()).replace(/ /g, "_").replace(/'/g, "");
    }
}
class Map {
    xml;
    counties;
    constructor(xml) {
        this.xml = xml;
        this.counties = collectionUtils_1.toArray(function* () {
            for (const g of xml.svg.g) {
                for (const { $ } of g?.path || []) {
                    const { id } = $;
                    const [, state, countyName] = id?.match(/^([A-Z]{2})_([A-Za-z_\.\-]+)$/) || [];
                    if (state && countyName) {
                        yield new County(state, countyName, $);
                    }
                }
            }
        }).reduce((previous, county) => {
            (previous[county.state] || (previous[county.state] = {}))[County.normalize(county.state, county.name)] = county;
            return previous;
        }, {});
    }
    async write(dest) {
        await fs_1.default.writeFileSync(dest, builder.buildObject(this.xml));
    }
}
class StatesBuilder {
    electionTrackers = [];
    electionResultColorer;
    addElectionTrackers(...electionTrackers) {
        this.electionTrackers.push(...electionTrackers);
        return this;
    }
    setElectionResultHandler(electionResultColorer) {
        this.electionResultColorer = electionResultColorer;
        return this;
    }
    async build(dest) {
        var { electionResultColorer, electionTrackers } = this;
        if (!electionResultColorer || !electionTrackers[0]) {
            throw new Error("build() called before ready");
        }
        const promises = function* () {
            for (const file of io_1.default.readDir(mapsDir)) {
                const path = `${mapsDir}/${file}`;
                const mapName = file.replace(/\.svg$/, "");
                const tracker = electionTrackers.find(tracker => tracker.isEligible(mapName));
                if (tracker) {
                    yield (async () => {
                        const text = fs_1.default.readFileSync(path).toString();
                        const xml = await xml2js_1.default.parseStringPromise(text);
                        const map = new Map(xml);
                        for (const [state, counties] of Object.entries(await tracker.getCountyTotals())) {
                            if (map.counties[state]) {
                                for (const [county, result] of Object.entries(counties)) {
                                    const normalizedCounty = County.normalize(state, county);
                                    const stateCounty = map.counties[state]?.[normalizedCounty];
                                    if (!stateCounty) {
                                        console.error(`County ${county}, ${state} not found in SVG`);
                                        continue;
                                    }
                                    delete map.counties[state][normalizedCounty];
                                    stateCounty.fill = result.complete ? electionResultColorer(result) : null;
                                }
                                for (const county of Object.keys(map.counties[state])) {
                                    console.error(`County ${county}, ${state} not found in election results`);
                                }
                            }
                        }
                        map.write(`${dest}/${file}`);
                    })();
                }
            }
        }();
        return Promise.all([...promises]);
    }
}
exports.StatesBuilder = StatesBuilder;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWFwcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIk1hcHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQ0Esb0RBQTJCO0FBQzNCLCtDQUF1QjtBQUN2Qiw0Q0FBb0I7QUFFcEIsc0VBQTRDO0FBRTVDLHdEQUE2QztBQUM3QyxnREFBK0M7QUFDL0MsTUFBTSxPQUFPLEdBQUcsSUFBSSxnQkFBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO0FBQ3BDLE1BQU0sT0FBTyxHQUFHLEdBQUcsWUFBRSxDQUFDLFdBQVcsY0FBYyxDQUFDO0FBRWhELE1BQU0sTUFBTTtJQUNvQjtJQUErQjtJQUFzQjtJQUFqRixZQUE0QixLQUFhLEVBQWtCLElBQVksRUFBVSxJQUFVO1FBQS9ELFVBQUssR0FBTCxLQUFLLENBQVE7UUFBa0IsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07SUFDM0YsQ0FBQztJQUVELElBQVcsSUFBSSxDQUFDLElBQW1CO1FBQy9CLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsMkJBQWEsQ0FBQztnQkFDNUIsR0FBRyx5QkFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRTtnQkFDdkMsSUFBSTthQUNQLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUVELGdDQUFnQztJQUN6QixNQUFNLENBQUMsU0FBUyxDQUFDLEtBQWEsRUFBRSxNQUFjO1FBQ2pELE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUN0QixRQUFRLEtBQUssRUFBRTtnQkFDWCxLQUFLLElBQUk7b0JBQ0wsUUFBUSxNQUFNLEVBQUU7d0JBQ1osS0FBSyxTQUFTOzRCQUNWLE9BQU8sVUFBVSxDQUFDO3FCQUN6QjtvQkFDRCxNQUFNO2dCQUNWLEtBQUssSUFBSTtvQkFDTCxRQUFRLE1BQU0sRUFBRTt3QkFDWixLQUFLLFlBQVk7NEJBQ2IsT0FBTyxpQkFBaUIsQ0FBQztxQkFDaEM7b0JBQ0QsTUFBTTtnQkFDVixLQUFLLElBQUk7b0JBQ0wsUUFBUSxNQUFNLEVBQUU7d0JBQ1osS0FBSyxrQkFBa0I7NEJBQ25CLE9BQU8sYUFBYSxDQUFDO3FCQUM1QjthQUNSO1lBQ0QsT0FBTyxNQUFNLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN6RSxDQUFDO0NBQ0o7QUFFRCxNQUFNLEdBQUc7SUFHZTtJQUZYLFFBQVEsQ0FBeUM7SUFFMUQsWUFBb0IsR0FBUTtRQUFSLFFBQUcsR0FBSCxHQUFHLENBQUs7UUFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyx5QkFBTyxDQUFDLFFBQVEsQ0FBQztZQUM3QixLQUFLLE1BQU0sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUN2QixLQUFLLE1BQU0sRUFBQyxDQUFDLEVBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLEVBQUUsRUFBRTtvQkFDN0IsTUFBTSxFQUFDLEVBQUUsRUFBQyxHQUEyQixDQUFDLENBQUM7b0JBQ3ZDLE1BQU0sQ0FBQyxFQUFFLEtBQUssRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxDQUFDLCtCQUErQixDQUFDLElBQUksRUFBRSxDQUFDO29CQUMvRSxJQUFJLEtBQUssSUFBSSxVQUFVLEVBQUU7d0JBQ3JCLE1BQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFLFVBQVUsRUFBRSxDQUFDLENBQUMsQ0FBQztxQkFDMUM7aUJBQ0o7YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUcsRUFBRTtZQUM1QixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQztZQUNoSCxPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDLEVBQW1CLEVBQUUsQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFRCxLQUFLLENBQUMsS0FBSyxDQUFDLElBQVk7UUFDcEIsTUFBTSxZQUFFLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Q0FDSjtBQUVELE1BQWEsYUFBYTtJQUNkLGdCQUFnQixHQUFzQixFQUFFLENBQUM7SUFDekMscUJBQXFCLENBQW9DO0lBRWpFLG1CQUFtQixDQUFDLEdBQUcsZ0JBQW1DO1FBQ3RELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2hELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxxQkFBNEM7UUFDakUsSUFBSSxDQUFDLHFCQUFxQixHQUFHLHFCQUFxQixDQUFDO1FBQ25ELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxLQUFLLENBQUMsS0FBSyxDQUFDLElBQVk7UUFDcEIsSUFBSSxFQUFDLHFCQUFxQixFQUFFLGdCQUFnQixFQUFDLEdBQUcsSUFBSSxDQUFDO1FBQ3JELElBQUksQ0FBQyxxQkFBcUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ2hELE1BQU0sSUFBSSxLQUFLLENBQUMsNkJBQTZCLENBQUMsQ0FBQztTQUNsRDtRQUVELE1BQU0sUUFBUSxHQUFHLFFBQVEsQ0FBQztZQUN0QixLQUFLLE1BQU0sSUFBSSxJQUFJLFlBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3BDLE1BQU0sSUFBSSxHQUFHLEdBQUcsT0FBTyxJQUFJLElBQUksRUFBRSxDQUFDO2dCQUNsQyxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDM0MsTUFBTSxPQUFPLEdBQUcsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUM5RSxJQUFJLE9BQU8sRUFBRTtvQkFDVCxNQUFNLENBQUMsS0FBSyxJQUFHLEVBQUU7d0JBQ2IsTUFBTSxJQUFJLEdBQUcsWUFBRSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt3QkFDOUMsTUFBTSxHQUFHLEdBQUcsTUFBTSxnQkFBSyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNqRCxNQUFNLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFFekIsS0FBSyxNQUFNLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxPQUFPLENBQUMsZUFBZSxFQUFFLENBQUMsRUFBRTs0QkFDN0UsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dDQUVyQixLQUFLLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTtvQ0FDckQsTUFBTSxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztvQ0FDekQsTUFBTSxXQUFXLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLENBQUM7b0NBQzVELElBQUksQ0FBQyxXQUFXLEVBQUU7d0NBQ2QsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLE1BQU0sS0FBSyxLQUFLLG1CQUFtQixDQUFDLENBQUM7d0NBQzdELFNBQVM7cUNBQ1o7b0NBRUQsT0FBTyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7b0NBQzdDLFdBQVcsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLENBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQztpQ0FDNUU7Z0NBQ0QsS0FBSyxNQUFNLE1BQU0sSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtvQ0FDbkQsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLE1BQU0sS0FBSyxLQUFLLGdDQUFnQyxDQUFDLENBQUM7aUNBQzdFOzZCQUNKO3lCQUNKO3dCQUVELEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQztvQkFDakMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDUjthQUNKO1FBQ0wsQ0FBQyxFQUFFLENBQUM7UUFFSixPQUFPLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQztDQUNKO0FBM0RELHNDQTJEQyJ9