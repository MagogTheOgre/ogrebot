"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AbstractMediawiki = void 0;
const Mediawiki_1 = require("./Mediawiki");
const ThreadPool_1 = require("../ThreadPool");
const cachable_1 = require("../decorators/cachable");
const Logger_1 = __importDefault(require("../Logger"));
const promiseUtils_1 = require("../promiseUtils");
const io_1 = __importDefault(require("../io"));
const node_fetch_1 = __importDefault(require("node-fetch"));
const fetch = require('fetch-cookie/node-fetch')(node_fetch_1.default);
class AbstractMediawiki {
    api;
    _ready;
    threadPool;
    throttle;
    constructor({ username = Mediawiki_1.MediawikiUsername.OGREBOT_2, api = Mediawiki_1.MediawikiApi.COMMONS, threadPoolSize = 20, throttle = 0 } = {}) {
        this.api = api;
        this.threadPool = new ThreadPool_1.ThreadPoolImpl(threadPoolSize);
        this.throttle = throttle;
        this._ready = (async () => {
            const password = io_1.default.getProperty("secrets", `password_${username}`);
            const logintoken = await this.fetchToken("login");
            const { clientlogin: { status } } = await this.post({
                action: "clientlogin",
                username,
                password,
                loginreturnurl: "http://localhost",
                logintoken
            }, false);
            if (status !== "PASS") {
                throw new Error(`Response not as expected: ${username}, ${api}`);
            }
        })();
    }
    get ready() {
        return this._ready;
    }
    getNowString() {
        function zeroPad(value) {
            return String(value).substr(-2);
        }
        var date = new Date();
        return `${date.getFullYear()}${zeroPad(date.getMonth() - 1)}${zeroPad(date.getDate())}${zeroPad(date.getHours())}${zeroPad(date.getMinutes())}${zeroPad(date.getSeconds())}`;
    }
    fetchToken(type) {
        return this.query({
            meta: "tokens",
            type: type
        }, ["query", "tokens", `${type}token`]);
    }
    async query(parameters, subIndices = []) {
        var result = await this.post({
            ...parameters,
            action: "query"
        }, false);
        for (const index of subIndices) {
            result = result[index];
        }
        return result;
    }
    post(parameters, update) {
        const body = "format=json" + this.encodeFromEntries(Object.entries(parameters), "&", "&");
        return this.fetchAndRead({
            method: 'POST',
            body: body,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "User-Agent": "OgreBot, NodeJS framework"
            }
        }, update);
    }
    fetchAndRead(options = {}, throttled, retries = 3) {
        return new Promise((resolve, reject) => {
            const self = this;
            this.threadPool.enqueue(async function doQuery() {
                Logger_1.default.debug(`query: ${JSON.stringify(options)}`);
                await (throttled && promiseUtils_1.sleep(self.throttle));
                const response = await fetch(self.api, options);
                const responseText = await response.text();
                Logger_1.default.debug(`response: ${responseText.substring(0, 300)}`);
                try {
                    const responseTextJson = JSON.parse(responseText);
                    const { warnings, error } = responseTextJson;
                    if (warnings) {
                        Logger_1.default.warn("Response had warnings. ", warnings);
                    }
                    if (error) {
                        Logger_1.default.warn("Response had errors. ", error);
                    }
                    resolve(responseTextJson);
                }
                catch (e) {
                    if (--retries < 1) {
                        Logger_1.default.error("Error, can't parse response text.", responseText, e);
                        reject(e);
                    }
                    else {
                        await doQuery();
                    }
                }
            });
        });
    }
    encodeFromEntries(entries, sep, prefix) {
        return (entries.length ? prefix : "") + entries.map(([key, val]) => `${encodeURIComponent(key)}=${encodeURIComponent(val)}`).join("&");
    }
}
__decorate([
    cachable_1.cachable()
], AbstractMediawiki.prototype, "fetchToken", null);
exports.AbstractMediawiki = AbstractMediawiki;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWJzdHJhY3RNZWRpYXdpa2kuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJBYnN0cmFjdE1lZGlhd2lraS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSwyQ0FBOEQ7QUFDOUQsOENBQTJEO0FBQzNELHFEQUFrRDtBQUNsRCx1REFBK0I7QUFDL0Isa0RBQXdDO0FBQ3hDLCtDQUF1QjtBQUN2Qiw0REFBbUM7QUFFbkMsTUFBTSxLQUFLLEdBQUcsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUMsb0JBQVMsQ0FBQyxDQUFBO0FBRTNELE1BQXNCLGlCQUFpQjtJQUUzQixHQUFHLENBQWU7SUFDbEIsTUFBTSxDQUFnQjtJQUN0QixVQUFVLENBQWE7SUFDdkIsUUFBUSxDQUFTO0lBRXpCLFlBQVksRUFBRSxRQUFRLEdBQUcsNkJBQWlCLENBQUMsU0FBUyxFQUFFLEdBQUcsR0FBRyx3QkFBWSxDQUFDLE9BQU8sRUFBRSxjQUFjLEdBQUcsRUFBRSxFQUFFLFFBQVEsR0FBRyxDQUFDLEVBQUMsR0FBRyxFQUFFO1FBQ3JILElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2YsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLDJCQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFFekIsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEtBQUssSUFBSSxFQUFFO1lBQ3RCLE1BQU0sUUFBUSxHQUFHLFlBQUUsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLFlBQVksUUFBUSxFQUFFLENBQUMsQ0FBQztZQUNuRSxNQUFNLFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDbEQsTUFBTSxFQUFDLFdBQVcsRUFBRSxFQUFDLE1BQU0sRUFBQyxFQUFDLEdBQUcsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUM1QyxNQUFNLEVBQUUsYUFBYTtnQkFDckIsUUFBUTtnQkFDUixRQUFRO2dCQUNSLGNBQWMsRUFBRSxrQkFBa0I7Z0JBQ2xDLFVBQVU7YUFDYixFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ1YsSUFBSSxNQUFNLEtBQUssTUFBTSxFQUFFO2dCQUNuQixNQUFNLElBQUksS0FBSyxDQUFDLDZCQUE2QixRQUFRLEtBQUssR0FBRyxFQUFFLENBQUMsQ0FBQzthQUNwRTtRQUNMLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDVCxDQUFDO0lBRUQsSUFBSSxLQUFLO1FBQ0wsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFHUyxZQUFZO1FBQ2xCLFNBQVMsT0FBTyxDQUFDLEtBQWE7WUFDMUIsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEMsQ0FBQztRQUNELElBQUksSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFFdEIsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsRUFBRSxDQUFBO0lBQ2hMLENBQUM7SUFHUyxVQUFVLENBQUMsSUFBWTtRQUM3QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDZCxJQUFJLEVBQUUsUUFBUTtZQUNkLElBQUksRUFBRSxJQUFJO1NBQ2IsRUFBRSxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsR0FBRyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVTLEtBQUssQ0FBQyxLQUFLLENBQUMsVUFBa0IsRUFBRSxhQUF1QixFQUFFO1FBRS9ELElBQUksTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQztZQUN6QixHQUFJLFVBQVU7WUFDZCxNQUFNLEVBQUUsT0FBTztTQUNsQixFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRVYsS0FBSyxNQUFNLEtBQUssSUFBSSxVQUFVLEVBQUU7WUFDNUIsTUFBTSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMxQjtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFUyxJQUFJLENBQUMsVUFBa0IsRUFBRSxNQUFlO1FBQzlDLE1BQU0sSUFBSSxHQUFHLGFBQWEsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFMUYsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ3JCLE1BQU0sRUFBRSxNQUFNO1lBQ2QsSUFBSSxFQUFFLElBQUk7WUFDVixPQUFPLEVBQUU7Z0JBQ0wsY0FBYyxFQUFFLG1DQUFtQztnQkFDbkQsWUFBWSxFQUFFLDJCQUEyQjthQUM1QztTQUNKLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDZixDQUFDO0lBRVMsWUFBWSxDQUFDLFVBQXVCLEVBQUUsRUFBRSxTQUFrQixFQUFFLE9BQU8sR0FBRyxDQUFDO1FBQzdFLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFbkMsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEtBQUssVUFBVSxPQUFPO2dCQUMxQyxnQkFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNsRCxNQUFNLENBQUMsU0FBUyxJQUFJLG9CQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLE1BQU0sUUFBUSxHQUFHLE1BQU0sS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ2hELE1BQU0sWUFBWSxHQUFHLE1BQU0sUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMzQyxnQkFBTSxDQUFDLEtBQUssQ0FBQyxhQUFhLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDNUQsSUFBSTtvQkFDQSxNQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBRWxELE1BQU0sRUFBQyxRQUFRLEVBQUUsS0FBSyxFQUFDLEdBQUcsZ0JBQWdCLENBQUM7b0JBQzNDLElBQUksUUFBUSxFQUFFO3dCQUNWLGdCQUFNLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLFFBQVEsQ0FBQyxDQUFDO3FCQUNwRDtvQkFDRCxJQUFJLEtBQUssRUFBRTt3QkFDUCxnQkFBTSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxLQUFLLENBQUMsQ0FBQztxQkFDL0M7b0JBRUQsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUM7aUJBQzdCO2dCQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNSLElBQUksRUFBRSxPQUFPLEdBQUcsQ0FBQyxFQUFFO3dCQUNmLGdCQUFNLENBQUMsS0FBSyxDQUFDLG1DQUFtQyxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUMsQ0FBQzt3QkFDbkUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNiO3lCQUFNO3dCQUNILE1BQU0sT0FBTyxFQUFFLENBQUM7cUJBQ25CO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFFUyxpQkFBaUIsQ0FBQyxPQUFjLEVBQUUsR0FBVyxFQUFFLE1BQWM7UUFDbkUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxHQUFHLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxJQUFJLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDM0ksQ0FBQztDQUNKO0FBeEVHO0lBREMsbUJBQVEsRUFBRTttREFNVjtBQWhETCw4Q0FtSEMifQ==