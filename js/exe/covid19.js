"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const io_1 = __importDefault(require("../lib/io"));
const promiseUtils_1 = require("../lib/promiseUtils");
const MultiCovidMapWrapper_1 = __importDefault(require("../lib/covid19/CovidMap/MultiCovidMapWrapper"));
const PngCovidMap_1 = __importDefault(require("../lib/covid19/CovidMap/PngCovidMap"));
const AbstractMapBuilder_1 = __importDefault(require("../lib/covid19/MapBuilder/AbstractMapBuilder"));
const CovidUtils_1 = __importDefault(require("../lib/covid19/CovidUtils"));
const collectionUtils_1 = require("../lib/collectionUtils");
const minimist_1 = __importDefault(require("minimist"));
//keep thread alive until done
promiseUtils_1.startup();
const MIN_START_DATE = toUtcDate("2020-03-23");
const MAX_END_DATE = new Date();
MAX_END_DATE.setUTCHours(0);
MAX_END_DATE.setUTCMinutes(0);
MAX_END_DATE.setUTCSeconds(0);
MAX_END_DATE.setUTCMilliseconds(0);
function toUtcDate(string) {
    const [, year, month, day] = string.match(/^(\d{4})\-(\d{1,2})\-(\d{1,2})$/) || [];
    if (!year) {
        throw new Error(`Illegal date argument: ${string}`);
    }
    return new Date(Date.UTC(+year, +month - 1, +day));
}
function getArgs() {
    const { days, multiplier, type, outputDir, perCapita, start, end } = minimist_1.default(process.argv, {
        boolean: "perCapita",
        default: {
            days: 7,
            multiplier: .01,
            outputDir: "../Desktop/maps/$days",
            perCapita: true,
            type: "confirmed",
            start: "",
            end: ""
        }
    });
    if (typeof days !== "number") {
        throw new Error(`Illegal days argument: ${days}`);
    }
    if (typeof multiplier !== "number") {
        throw new Error(`Illegal multiplier argument: ${multiplier}`);
    }
    if (typeof outputDir !== "string") {
        throw new Error(`Illegal outputDir argument: ${outputDir}`);
    }
    if (typeof perCapita !== "boolean") {
        throw new Error(`Illegal perCapita argument: ${perCapita}`);
    }
    if (typeof type !== "string" || type !== "confirmed" && type !== "deaths") {
        throw new Error(`Illegal type argument: ${type}`);
    }
    if (typeof start !== "string") {
        throw new Error(`Illegal start argument: ${start}`);
    }
    if (typeof end !== "string") {
        throw new Error(`Illegal end argument: ${end}`);
    }
    const startDate = start ? toUtcDate(start) : MIN_START_DATE;
    const endDate = end ? toUtcDate(end) : MAX_END_DATE;
    const response = {
        days,
        multiplier,
        outputDir: io_1.default.path(outputDir.replace(/\$days/g, `${days}`)),
        perCapita,
        type,
        startDate,
        endDate
    };
    console.log(JSON.stringify(response));
    return response;
}
(async () => {
    try {
        const { days, multiplier, outputDir, perCapita, type, startDate, endDate } = getArgs();
        if (!io_1.default.existsSync(outputDir)) {
            io_1.default.mkdirSync(outputDir);
        }
        const requestedDates = collectionUtils_1.toArray(function* () {
            for (const date = new Date(startDate); date <= endDate; date.setDate(date.getDate() + days)) {
                yield new Date(date);
            }
        });
        const mapBuilder = new class extends AbstractMapBuilder_1.default {
            constructor() {
                super(...arguments);
                this.colorLevels = [
                    [0, "#fcbba1"],
                    //[.0125, "#fcbba1"],
                    [.01, "#fc9272"],
                    [.0316, "#fb6a4a"],
                    [.1, "#de2d26"],
                    [.316, "#a50f15"],
                    [1, "#000"]
                ].map(([lowerLimit, color]) => [lowerLimit * multiplier, color]);
            }
            getResults(builder) {
                const countyResultsBuilder = builder(type);
                countyResultsBuilder.diff(days);
                if (perCapita) {
                    countyResultsBuilder.perCapita();
                }
                return countyResultsBuilder.build();
            }
        }();
        await new MultiCovidMapWrapper_1.default(outputDir, requestedDates, "png", mapBuilder, CovidUtils_1.default.toDmyString, PngCovidMap_1.default).create();
        console.log("done");
        // await new AnimatedPngCovidMap(`${outputDir}/covid19.apng`, startDate, endDate, mapBuilder, 1, 1).create();
    }
    finally {
        promiseUtils_1.shutdown();
    }
})();
// console.log(counties.sort((a, b) => b.confirmedPerCapita - a.confirmedPerCapita).
//     map(({ county, relativeConfirmedPerCapita }) => `$county: ${relativeConfirmedPerCapita * 100}% relative confirmed per capita`).join("\n"));
// console.log(counties.sort((a, b) => b.deathsPerCapita - a.deathsPerCapita).
//     flatMap(({ county, relativeDeathsPerCapita }) => relativeDeathsPerCapita > 0 ? [`$county: ${relativeDeathsPerCapita * 100}% relative deaths per capita`] : []).join("\n"));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY292aWQxOS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNvdmlkMTkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxtREFBMkI7QUFDM0Isc0RBQXdEO0FBQ3hELHdHQUFnRjtBQUNoRixzRkFBOEQ7QUFDOUQsc0dBQThFO0FBQzlFLDJFQUFtRDtBQUNuRCw0REFBaUQ7QUFDakQsd0RBQWdDO0FBRWhDLDhCQUE4QjtBQUM5QixzQkFBTyxFQUFFLENBQUM7QUFFVixNQUFNLGNBQWMsR0FBRyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDL0MsTUFBTSxZQUFZLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztBQUNoQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQzVCLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDOUIsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUM5QixZQUFZLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFFbkMsU0FBUyxTQUFTLENBQUMsTUFBYztJQUM3QixNQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsaUNBQWlDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFFbkYsSUFBSSxDQUFDLElBQUksRUFBRTtRQUNQLE1BQU0sSUFBSSxLQUFLLENBQUMsMEJBQTBCLE1BQU0sRUFBRSxDQUFDLENBQUM7S0FDdkQ7SUFFRCxPQUFPLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUN2RCxDQUFDO0FBRUQsU0FBUyxPQUFPO0lBQ1osTUFBTSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBQyxHQUFHLGtCQUFRLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRTtRQUN2RixPQUFPLEVBQUUsV0FBVztRQUNwQixPQUFPLEVBQUU7WUFDTCxJQUFJLEVBQUUsQ0FBQztZQUNQLFVBQVUsRUFBRSxHQUFHO1lBQ2YsU0FBUyxFQUFFLHVCQUF1QjtZQUNsQyxTQUFTLEVBQUUsSUFBSTtZQUNmLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxFQUFFO1lBQ1QsR0FBRyxFQUFFLEVBQUU7U0FDVjtLQUNKLENBQUMsQ0FBQztJQUVILElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxFQUFFO1FBQzFCLE1BQU0sSUFBSSxLQUFLLENBQUMsMEJBQTBCLElBQUksRUFBRSxDQUFDLENBQUM7S0FDckQ7SUFDRCxJQUFJLE9BQU8sVUFBVSxLQUFLLFFBQVEsRUFBRTtRQUNoQyxNQUFNLElBQUksS0FBSyxDQUFDLGdDQUFnQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO0tBQ2pFO0lBQ0QsSUFBSSxPQUFPLFNBQVMsS0FBSyxRQUFRLEVBQUU7UUFDL0IsTUFBTSxJQUFJLEtBQUssQ0FBQywrQkFBK0IsU0FBUyxFQUFFLENBQUMsQ0FBQztLQUMvRDtJQUNELElBQUksT0FBTyxTQUFTLEtBQUssU0FBUyxFQUFFO1FBQ2hDLE1BQU0sSUFBSSxLQUFLLENBQUMsK0JBQStCLFNBQVMsRUFBRSxDQUFDLENBQUM7S0FDL0Q7SUFDRCxJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsSUFBSSxJQUFJLEtBQUssV0FBVyxJQUFJLElBQUksS0FBSyxRQUFRLEVBQUU7UUFDdkUsTUFBTSxJQUFJLEtBQUssQ0FBQywwQkFBMEIsSUFBSSxFQUFFLENBQUMsQ0FBQztLQUNyRDtJQUNELElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1FBQzNCLE1BQU0sSUFBSSxLQUFLLENBQUMsMkJBQTJCLEtBQUssRUFBRSxDQUFDLENBQUM7S0FDdkQ7SUFDRCxJQUFJLE9BQU8sR0FBRyxLQUFLLFFBQVEsRUFBRTtRQUN6QixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixHQUFHLEVBQUUsQ0FBQyxDQUFDO0tBQ25EO0lBRUQsTUFBTSxTQUFTLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQztJQUU1RCxNQUFNLE9BQU8sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDO0lBRXBELE1BQU0sUUFBUSxHQUFHO1FBQ2IsSUFBSTtRQUNKLFVBQVU7UUFDVixTQUFTLEVBQUUsWUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxHQUFHLElBQUksRUFBRSxDQUFDLENBQUM7UUFDM0QsU0FBUztRQUNULElBQUk7UUFDSixTQUFTO1FBQ1QsT0FBTztLQUNELENBQUM7SUFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUN0QyxPQUFPLFFBQVEsQ0FBQztBQUNwQixDQUFDO0FBQ0QsQ0FBQyxLQUFLLElBQUksRUFBRTtJQUNSLElBQUk7UUFDQSxNQUFNLEVBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFDLEdBQUcsT0FBTyxFQUFFLENBQUM7UUFDckYsSUFBSSxDQUFDLFlBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDM0IsWUFBRSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUMzQjtRQUNELE1BQU0sY0FBYyxHQUFHLHlCQUFPLENBQUMsUUFBUSxDQUFDO1lBQ3BDLEtBQUssTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsSUFBSSxJQUFJLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRTtnQkFDekYsTUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN4QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxVQUFVLEdBQUcsSUFBSSxLQUFNLFNBQVEsNEJBQWtCO1lBQWhDOztnQkFDQSxnQkFBVyxHQUF1QztvQkFDakUsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDO29CQUNkLHFCQUFxQjtvQkFDckIsQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDO29CQUNoQixDQUFDLEtBQUssRUFBRSxTQUFTLENBQUM7b0JBQ2xCLENBQUMsRUFBRSxFQUFFLFNBQVMsQ0FBQztvQkFDZixDQUFDLElBQUksRUFBRSxTQUFTLENBQUM7b0JBQ2pCLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQztpQkFDSixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLFVBQVUsR0FBRyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztZQVMvRSxDQUFDO1lBUmEsVUFBVSxDQUFDLE9BQXdEO2dCQUN6RSxNQUFNLG9CQUFvQixHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDM0Msb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNoQyxJQUFJLFNBQVMsRUFBRTtvQkFDWCxvQkFBb0IsQ0FBQyxTQUFTLEVBQUUsQ0FBQztpQkFDcEM7Z0JBQ0QsT0FBTyxvQkFBb0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN4QyxDQUFDO1NBQ0osRUFBRSxDQUFDO1FBQ0osTUFBTSxJQUFJLDhCQUFvQixDQUFDLFNBQVMsRUFBRSxjQUFjLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxvQkFBVSxDQUFDLFdBQVcsRUFBRSxxQkFBVyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDM0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwQiw2R0FBNkc7S0FDaEg7WUFBUztRQUNOLHVCQUFRLEVBQUUsQ0FBQztLQUNkO0FBQ0wsQ0FBQyxDQUFDLEVBQUUsQ0FBQTtBQUVKLG9GQUFvRjtBQUNwRixrSkFBa0o7QUFDbEosOEVBQThFO0FBQzlFLGtMQUFrTCJ9