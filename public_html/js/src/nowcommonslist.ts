
/// <reference path="../../../js/node_modules/@types/jquery/index.d.ts" />
/// <reference path="../../../js/node_modules/@types/jqueryui/index.d.ts" />
/// <reference path="./modules/MDialog.ts" />
/// <reference path="./modules/checkImageLoad.ts" />
/// <reference path="./modules/setIntervalImmediate.ts" />
/// <reference path="./modules/topbar.ts" />
/// <reference path="./modules/XDomain.ts" />
/// <reference path="./types/JQuery.isReady.d.ts" />
(async function() {
    "use strict";

    const AUTO_DELETE = "#auto-delete";
    const AUTO_MARK = "#auto-mark";
    const AUTO_DELETE_REMOVE_DELETED = `${AUTO_DELETE}-remove-deleted`;
    const AUTO_DELETE_NC = `${AUTO_DELETE}-nc`;
    const AUTO_DELETE_OK = `${AUTO_DELETE}-ok`;
    const AUTO_DELETE_TEST = `${AUTO_DELETE}-test`;
    const AUTO_MARK_OK = `${AUTO_MARK}-ok`;
    const AUTO_MARK_TEST = `${AUTO_MARK}-test`;
    const AUTO_MARK_TEXT = `${AUTO_MARK}-text`;
    const AUTO_MARK_TEXT_OMIT = `${AUTO_MARK}-text-omit`;
    const DELETE_CLASS = ".delete";
    const MARK_AUTO = ".mark-auto";
    const NC_ACTION = "ncAction";
    const NAME = "name";
    const NC_ROW = ".nc-row";
    const REASON = "reason";
    const SELECTED = "selected";
    const SELECTED_CLASS = `.${SELECTED}`;
    const DATA_IMAGE_WATCH = "data-img-watch";
    const DATA_IMAGE_WATCH_SELECTOR = `[${DATA_IMAGE_WATCH}]`;

    var xDomainPromise = XDomain.promise();
    setIntervalImmediate(() => {
        $("#ready-count").html("" + roughPercent($("input.file").length / +$("#files-count").val()));
        return !$.isReady;
    });

    await $.ready;

    checkImageLoad(
        document.querySelectorAll(DATA_IMAGE_WATCH_SELECTOR),
        image => image.removeAttribute(DATA_IMAGE_WATCH),
        image => topBar({
            css: { background: "#990000" },
            delay: 2000,
            message: `Failed to load ${image.getAttribute("src").replace(/^.+\//, "")}`
        }));

    function removeRow(name: string) {
        if (autoHideCheckbox.is(":checked")) {
            $("a.delete").each((_, link) => {
                const $link = $(link);
                if ($link.data(NAME) === name) {
                    $link.closest(NC_ROW).prev().addBack().remove();
                    const count = +autoMarkCount.attr("max") - 1;
                    [autoDeleteCount, autoMarkCount, autoMarkStart].forEach(field => {
                        field.attr("max", count).val(Math.min(+field.val(), count));
                    });
                }
            });
            setupUploaderDropdown();
        }
    }

    function roughPercent(decimal: number): number {
        return Math.floor(decimal * 100);
    }

    function parseRegex(string: string): RegExp {
        const lastSlash = string.lastIndexOf("/");

        if (string.indexOf("\n") < 0 && string.match(/^\/.*?\/[mi]*$/)) {
            try {
                return new RegExp(string.substring(1, lastSlash), string.substring(lastSlash + 1));
            } catch {}
        }
    }

    interface AbstractMatch {
        match(nowommonsRow: NowCommonsRow): boolean
    }

    class UserMatch implements AbstractMatch {
        constructor(private readonly user: string) {}

        match(nowcommonsrow: NowCommonsRow) {
            return nowcommonsrow.uploaders.includes(this.user);
        }
    }

    class CallbackMatch implements AbstractMatch {
        match: AbstractMatch["match"]

        constructor(callback: AbstractMatch["match"]) {
            this.match = callback;
        }
    }

    abstract class TextMatch implements AbstractMatch {        
        match({texts}: NowCommonsRow) {
            return this._filter(texts.some(this.__matchOne.bind(this)));
        }

        setFilter(filter) {
            this._filter = filter;
        }

        __matchOne(haystack: string) {
            return this._matchOne(haystack);
        }

        abstract _matchOne(haystack: string): boolean;

        private _filter(result: boolean) {
            return result;
        }
    }

    class MatchRegex extends TextMatch {

        constructor(private readonly regex: RegExp) {
            super();
        }

        _matchOne(haystack: string) {
            return !!haystack.match(this.regex);
        }
    }

    class MatchString extends TextMatch {
        
        constructor(private readonly needle: string) {
            super();
        }
        
        _matchOne(haystack: string) {
            return haystack.toUpperCase().indexOf(this.needle) > -1;
        }
    }
    class NowCommonsRow {
        public readonly texts: string[];
        public readonly deleteLink: JQuery<HTMLElement>;
       
        constructor(private readonly rows: JQuery<HTMLElement>) {
            this.texts = $(".text", rows).get()
                .map(textElement => $(textElement).text());
            this.deleteLink = $(DELETE_CLASS, rows);
        }

        get deleteReason() {
            return this.deleteLink.data(REASON) as string;
        }

        get localName() {
            return this.deleteLink.data(NAME) as string;
        }

        get nowCommonsLink() {
            return $(".nowcommons", this.rows);
        }
        
        get selected() {
            return this.rows.is(SELECTED_CLASS);
        }

        get hasWarnings() {
            return !!$(`.mark-problematic,.mark-keeplocal,.error-warning,${DATA_IMAGE_WATCH_SELECTOR}`, this.rows)[0];
        }

        get hasLinks() {
            return !!$(".linkback", this.rows)[0];
        }

        get needsTransfer() {
            return !!$(".oldver", this.rows)[0];
        }

        get hasTalk() {
            return !!$(".talk", this.rows)[0];
        }

        get uploaders() {
            return $(".user", this.rows).get().map(link => $(link).text());
        }

        toggleSelect(toggle: boolean) {
            this.rows.toggleClass(SELECTED, toggle);
        }
    }

    interface NowCommonsAction {
        run(): void;
        test(): JQuery<NowCommonsRow>
    }

    abstract class AbstractNowCommonsAction implements NowCommonsAction {

        run() {
            const rows = this.test().get().slice(this.min(), this.max());

            if (this.confirm(rows.length)) {
                this._run(rows);
            }

            this.postRun();
        }
        protected confirm(_count: number)  {
            return true;
        }

        
        protected abstract _run(rows: NowCommonsRow[]);
        protected abstract postRun(): void
        abstract test(): JQuery<NowCommonsRow>

        min() {
            return 0;
        }

        max() {
            return Number.MAX_VALUE;
        }

        static getNowCommonsRows(filter) {
            return $(NC_ROW).map(function() {
                const row = new NowCommonsRow($(this).prev(NC_ROW).addBack());
                if (filter(row)) {
                    return row;
                }
            });
        }
    }

    class NowCommonsProxyAction implements NowCommonsAction {
        constructor(public action: NowCommonsAction) {}

        run() {
            return this.action.run();
        }

        test() {
            return this.action.test();
        }
    }
    
    abstract class NowCommonsMarkedAction extends AbstractNowCommonsAction {
        
        confirm(count: number) {
            return confirm(`Delete ${count} files?`);
        }

        _run(rows) {
            this._markedRun(rows);
        }
        
        test() {
            return AbstractNowCommonsAction.getNowCommonsRows(({selected}) => selected);
        }

        max() {
            return +autoDeleteCount.val();
        }

        postRun() {
            autoDeleteDialog.close();
        }

        protected abstract _markedRun(rows: NowCommonsRow[])
    }

    class NowCommonsSearchAction extends AbstractNowCommonsAction {

        min() {
            return +autoMarkStart.val();
        }

        max() {
            return +autoMarkCount.val();
        }

        _run(rows: NowCommonsRow[]) {
            rows.forEach(row => void row.toggleSelect(true));
        }

        test() {
            const searchVal = ($(AUTO_MARK_TEXT).val() as string).replace(/\n/g, "").trim();
            const searchRegex = parseRegex(searchVal);
            const omitVal = ($(AUTO_MARK_TEXT_OMIT).val() as string).replace(/\n/g, "").trim();
            const omitRegex = parseRegex(omitVal);
            const autoUserVal = (autoUser.val() as string).trim();
            const filters = [] as AbstractMatch[];
            var omitFilter: AbstractMatch;

            if (!searchVal && !autoUserVal) {
                throw "Please enter a value into the text box.";
            }

            if (autoUserVal) {
                filters.push(new UserMatch(autoUserVal));
            }
            if (searchVal) {
                filters.push(
                    searchRegex
                        ? new MatchRegex(searchRegex)
                        : new MatchString(searchVal.toUpperCase())
                );
            }
            if (omitVal) {
                omitFilter = omitRegex
                    ? new MatchRegex(omitRegex)
                    : new MatchString(omitVal.toUpperCase());
                (omitFilter as MatchRegex | MatchString).setFilter(val => !val);
                filters.push(omitFilter);
            }

            if (!$("#include-warnings:checked")[0]) {
                filters.push(new CallbackMatch(({hasWarnings}) => !hasWarnings));
            }

            if (!$("#include-diff-name:checked")[0]) {
                filters.push(new CallbackMatch(({hasLinks}) => !hasLinks));
            }

            if (!$("#include-multiple:checked")[0]) {
                filters.push(new CallbackMatch(({needsTransfer}) => !needsTransfer));
            }

            if (!$("#include-talk:checked")[0]) {
                filters.push(new CallbackMatch(({hasTalk}) => !hasTalk));
            }

            return AbstractNowCommonsAction.getNowCommonsRows(
                (nowCommonsRow: NowCommonsRow) => filters.every((filter: AbstractMatch) =>
                     filter.match(nowCommonsRow)));
        }

        postRun() {
            markDialog.close();
        }
    }

    abstract class NowCommonsAutoOpenAction extends NowCommonsMarkedAction {

        protected _markedRun(rows: NowCommonsRow[]) {
            rows.forEach(row => {
                open(this._getAutoOpenLink(row).attr("href"), "_blank");
                row.toggleSelect(false);
                removeRow(row.localName);
            });
        }

        protected abstract _getAutoOpenLink(row: NowCommonsRow): JQuery<HTMLElement>
    }

    class NowCommonsDeleteAjaxAction extends NowCommonsMarkedAction {
        protected _markedRun(rows: NowCommonsRow[]) {
            const ajaxDeleteObject = {};
            rows.forEach(({deleteLink}) => {
                ajaxDeleteObject[deleteLink.data(NAME)] = deleteLink.data(REASON);
            });
            ajaxCountElement.text(+ajaxCountElement.text() + rows.length);
            xDomain.postMessage(ajaxDeleteObject);
        }
    }

    class NowCommonsDeletePopupAction extends NowCommonsAutoOpenAction {
        protected _getAutoOpenLink({deleteLink}: NowCommonsRow) {
            return deleteLink;
        }
    }

    class NowCommonsNowCommonsAction extends NowCommonsAutoOpenAction {

        confirm(count: number) {
            return confirm(`Delete ${count} files?`);
        }

        protected _getAutoOpenLink({nowCommonsLink}: NowCommonsRow) {
            return nowCommonsLink;
        }
    }

    const markedAction = new NowCommonsProxyAction(new NowCommonsDeletePopupAction());

    addEventListener("beforeunload", (e) => {
        const selectedLength = $(SELECTED_CLASS).length / 2;
        if (selectedLength) {
            e.returnValue = `You have ${selectedLength} selections outstanding`;
        }
    });

    function setupUploaderDropdown() {
        let previousVal = autoUser.val();
        autoUser.html("").append("<option/>")
            .append(
                [...new Set(AbstractNowCommonsAction.getNowCommonsRows(() => true).get()
                    .flatMap(({uploaders}) => uploaders))
                ].sort().map(uploader => $("<option/>").val(uploader).text(uploader)[0])
            );
        autoUser.val(previousVal);
    }

    const closeButtons = $(".close-buttons");
    const autoUser = $("#auto-user");
    const autoHideCheckbox = $(AUTO_DELETE_REMOVE_DELETED);
    const nowCommonsPopupOption = $(AUTO_DELETE_NC);
    const ajaxCountElement = $(".ajax-count");
    const autoMarkStart = $("#auto-mark-start");
    const autoMarkCount = $("#auto-mark-count");
    const autoDeleteCount = $("#auto-delete-count");

    setupUploaderDropdown();
    $("#doc-load-per,.ready-count,.bottom-buttons").toggle();

    $(MARK_AUTO).on("click keypress", ({target, which}) => {
        //check it was a click or pressing the enter key
        if ([1, 13].includes(which)) {
            $(target).closest(NC_ROW).toggleClass(SELECTED);
        }
    });

    $("#clear-marks").on("click", () => void $(SELECTED_CLASS).removeClass(SELECTED));

    $(`${AUTO_MARK_TEXT},${AUTO_MARK_TEXT_OMIT}`).on("keydown paste drop focus", ({target}) => {
        setTimeout(() => {
            const jqTextarea = $(target);
            jqTextarea.toggleClass("regex", !!parseRegex(jqTextarea.val() as string));
        }, 1);
    });

    nowCommonsPopupOption.on("change", () => {
        markedAction.action = nowCommonsPopupOption.is(":checked")
            ? new NowCommonsNowCommonsAction()
            : new NowCommonsDeletePopupAction();
    });

    $(`${AUTO_MARK_OK},${AUTO_MARK_TEST}`).data(NC_ACTION, new NowCommonsSearchAction());
    $(`${AUTO_DELETE_OK},${AUTO_DELETE_TEST}`).data(NC_ACTION, markedAction);

    $(`${AUTO_DELETE_OK},${AUTO_MARK_OK}`).on("click", ({target}) => {
        try {
            $(target).data(NC_ACTION).run();
        } catch (error) {
            alert(error);
        }
    });

    $(`${AUTO_MARK_TEST},${AUTO_DELETE_TEST}`).on("click", ({target}) => {
        try {
            $("#count").html($(target).data(NC_ACTION).test().length);
            testDialog.open();
        } catch (error) {
            alert(error);
        }
    });

    closeButtons.on("click", () => {
        $(".bottom-buttons-buttons > *").animate({ width: "toggle" }, 350);
        closeButtons.toggleClass("glyphicon-menu-left");
    });

    const testDialog = new MDialog("#auto-files-found");
    testDialog.addCloseButton("#auto-delete-found-ok");

    const autoDeleteDialog = new MDialog(AUTO_DELETE, {
        open() {
            const {length} = markedAction.test();
            autoDeleteCount.val(length).attr("max", length);
        }
    });
    autoDeleteDialog.addOpenButton("#auto-delete-open");
    autoDeleteDialog.addCloseButton("#auto-delete-cancel");


    const markDialog = new MDialog(AUTO_MARK);
    markDialog.addOpenButton(`${AUTO_MARK}-open`);
    markDialog.addCloseButton(`${AUTO_MARK}-cancel`);

    try {
        var xDomain = await xDomainPromise;
        ajaxCountElement.text(0);
        topBar({ css: { background: "#000099" }, message: "Ajax delete detected" });
        markedAction.action = new NowCommonsDeleteAjaxAction();

        //wait until document ready in case it hasn't loaded
        nowCommonsPopupOption.closest(".table-row").hide();

        xDomain.addListener(data => {
            ajaxCountElement.text(+ajaxCountElement.text() - 1);
            if (data.status === "success") {
                topBar({
                    css: { background: "#009999" },
                    delay: 2000,
                    message: `${data.page}: deleted`
                });
                removeRow(data.page);
            } else {
                topBar(`${data.page}: ${data.message}`);
            }
        });
    } catch (e) {
        topBar({ css: { background: "#990000" }, message: "Ajax detection failed" });
    }
})();
