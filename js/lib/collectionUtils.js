"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.invert = exports.toArray = exports.mapValues = exports.mapEntries = exports.getAndDelete = exports.arrayFindAllMap = exports.arrayFindAll = exports.arrayFind = exports.arrayFindCallback = exports.arrayFindOrDie = exports.computeIfAbsent = void 0;
function computeIfAbsent(store, key, valueFactory) {
    var value = store.get(key);
    if (value === undefined) {
        value = valueFactory();
        store.set(key, value);
    }
    return value;
}
exports.computeIfAbsent = computeIfAbsent;
function arrayFindOrDie(array, predicate, thisArg) {
    return array[arrayFindCallback(array, predicate)];
}
exports.arrayFindOrDie = arrayFindOrDie;
function arrayFindCallback(array, predicate, thisArg) {
    const index = array.findIndex(predicate);
    if (index < 0) {
        throw new Error(`Predicate failed on array: ${JSON.stringify(array)}`);
    }
    return index;
}
exports.arrayFindCallback = arrayFindCallback;
function arrayFind(array, value) {
    return arrayFindCallback(array, e => e === value);
}
exports.arrayFind = arrayFind;
function arrayFindAll(array, ...values) {
    return values.map(value => arrayFind(array, value));
}
exports.arrayFindAll = arrayFindAll;
/**
 *
 * @param array
 * @param values a sparsely populated array
 */
function arrayFindAllMap(array, values) {
    const obj = {};
    for (const [key, dataKey] of Object.entries(values)) {
        obj[arrayFind(array, dataKey)] = key;
    }
    return obj;
}
exports.arrayFindAllMap = arrayFindAllMap;
function getAndDelete(object, key) {
    const value = object[key];
    delete object[key];
    return value;
}
exports.getAndDelete = getAndDelete;
function mapEntries(object, mapper) {
    return Object.fromEntries(Object.entries(object).flatMap(args => mapper(...args)));
}
exports.mapEntries = mapEntries;
function mapValues(object, mapper) {
    return mapEntries(object, (key, value) => {
        const newValue = mapper(value, key);
        return newValue != null ? [[key, newValue]] : [];
    });
}
exports.mapValues = mapValues;
function toArray(generator) {
    return [...generator()];
}
exports.toArray = toArray;
function invert(object) {
    return Object.fromEntries(Object.entries(object).map(([key, val]) => [val, key]));
}
exports.invert = invert;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvblV0aWxzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29sbGVjdGlvblV0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUtBLFNBQWdCLGVBQWUsQ0FBTyxLQUFrQixFQUFFLEdBQU0sRUFBRSxZQUFxQjtJQUNuRixJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNCLElBQUksS0FBSyxLQUFLLFNBQVMsRUFBRTtRQUNyQixLQUFLLEdBQUcsWUFBWSxFQUFFLENBQUM7UUFDdkIsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7S0FDekI7SUFDRCxPQUFPLEtBQUssQ0FBQztBQUNqQixDQUFDO0FBUEQsMENBT0M7QUFFRCxTQUFnQixjQUFjLENBQUksS0FBVSxFQUFFLFNBQXlELEVBQUUsT0FBYTtJQUNsSCxPQUFPLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQztBQUN0RCxDQUFDO0FBRkQsd0NBRUM7QUFFRCxTQUFnQixpQkFBaUIsQ0FBSSxLQUFVLEVBQUUsU0FBeUQsRUFBRSxPQUFhO0lBQ3JILE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDekMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO1FBQ1gsTUFBTSxJQUFJLEtBQUssQ0FBQyw4QkFBOEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDMUU7SUFDRCxPQUFPLEtBQUssQ0FBQztBQUNqQixDQUFDO0FBTkQsOENBTUM7QUFFRCxTQUFnQixTQUFTLENBQUksS0FBVSxFQUFFLEtBQVE7SUFDN0MsT0FBTyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUM7QUFDdEQsQ0FBQztBQUZELDhCQUVDO0FBRUQsU0FBZ0IsWUFBWSxDQUFJLEtBQVUsRUFBRSxHQUFHLE1BQVc7SUFDdEQsT0FBTyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO0FBQ3hELENBQUM7QUFGRCxvQ0FFQztBQUVEOzs7O0dBSUc7QUFDSCxTQUFnQixlQUFlLENBQXNCLEtBQVUsRUFBRSxNQUFxQjtJQUNsRixNQUFNLEdBQUcsR0FBNEIsRUFBRSxDQUFDO0lBQ3hDLEtBQUssTUFBTSxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1FBQ2pELEdBQUcsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO0tBQ3hDO0lBQ0QsT0FBMkIsR0FBRyxDQUFDO0FBQ25DLENBQUM7QUFORCwwQ0FNQztBQUVELFNBQWdCLFlBQVksQ0FBNkIsTUFBcUIsRUFBRSxHQUFNO0lBQ2xGLE1BQU0sS0FBSyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMxQixPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNuQixPQUFPLEtBQUssQ0FBQztBQUNqQixDQUFDO0FBSkQsb0NBSUM7QUFDRCxTQUFnQixVQUFVLENBQU8sTUFBNEIsRUFBRSxNQUFnRDtJQUMzRyxPQUFPLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDdkYsQ0FBQztBQUZELGdDQUVDO0FBQ0QsU0FBZ0IsU0FBUyxDQUFPLE1BQTRCLEVBQUUsTUFBZ0Q7SUFDMUcsT0FBTyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxFQUFFO1FBQ3JDLE1BQU0sUUFBUSxHQUFHLE1BQU0sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDcEMsT0FBTyxRQUFRLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUEsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUNwRCxDQUFDLENBQUMsQ0FBQztBQUNQLENBQUM7QUFMRCw4QkFLQztBQUNELFNBQWdCLE9BQU8sQ0FBSSxTQUE2QjtJQUNwRCxPQUFPLENBQUMsR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFBO0FBQzNCLENBQUM7QUFGRCwwQkFFQztBQUNELFNBQWdCLE1BQU0sQ0FBQyxNQUE4QjtJQUNqRCxPQUFPLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFVLENBQUMsQ0FBQyxDQUFDO0FBQy9GLENBQUM7QUFGRCx3QkFFQyJ9