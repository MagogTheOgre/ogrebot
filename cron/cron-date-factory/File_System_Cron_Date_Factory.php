<?php

/**
 * 
 * @author magog
 *
 */
class File_System_Cron_Date_Factory extends Cron_Date_Factory {
	
	/**
	 * 
	 * @var string
	 */
	const PROCESS_FILE_DIRECTORY = BASE_DIRECTORY;
	
	/**
	 * 
	 * @var string
	 */
	const PROCESS_FILE_NAME = "process.txt";

	/**
	 * 
	 * @var int Unix timestamp
	 */
	private $now;
	


    public function __construct() {
        $this->now = time();
    }
	/**
	 * 
	 * @return string
	 */
	private static function get_process_file_name() {
		return self::PROCESS_FILE_DIRECTORY . "/" . self::PROCESS_FILE_NAME;
	}

    /**
     * (non-PHPdoc)
     *
     * @throws IllegalStateException
     * @see Cron_Date_Factory::get_range()
     */
	protected function get_range(): array {
		global $logger;
		
		// get next run time.
		$processRunFileName = self::get_process_file_name();
		
		if (!filesize($processRunFileName)) {
            throw new IllegalStateException("$processRunFileName doesn't exist.");
        }

        $processRunFileContents = file_get_contents($processRunFileName);
        $logger->debug("Process file content: $processRunFileContents");
        $previousTime = intval($processRunFileContents);

        $logger->debug("Previous run time: $previousTime: " . date('Y-m-d H:i:s', $previousTime));

        if ($previousTime <= 0) {
            throw new IllegalStateException("Illegal previous run time: $processRunFileContents");
        }

		return [$previousTime + $this->get_interval() * SECONDS_PER_HOUR, $this->now];
	}
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see Cron_Date_Factory::finalize()
	 */
	public function finalize() {
		// all done parsing; update process file.
		Local_Io::file_put_contents_ensure(self::get_process_file_name(), $this->now);
	}

    /**
     * @throws IllegalStateException
     */
    public function init(bool $force): void {
        global $logger;
        if (file_exists(self::get_process_file_name())) {
            if ($force) {
                $previous_timestamp = file_get_contents(self::get_process_file_name());
                $logger->all("Force requested. Previous content: $previous_timestamp");
            } else {
                throw new IllegalStateException("File already exists: " . self::get_process_file_name());
            }
        }
        $this->finalize();
    }


}