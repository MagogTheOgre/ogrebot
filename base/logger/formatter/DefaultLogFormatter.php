<?php
class DefaultLogFormatter implements LogFormatter {

	/**
	 * @return string
	 */
	private function formatTime(): string {
		return date('Ymd H:i:s', time());
	}

	public function doFormat(string $level, string $file, int $line, string $message): string {
		$time = $this->formatTime();
		return "$level $time $file:$line: $message\n";
	}
}
