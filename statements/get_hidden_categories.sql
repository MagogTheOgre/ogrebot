SELECT
    p.page_title,
    cl.cl_to
FROM
    page p
INNER JOIN
    categorylinks cl
ON
    p.page_id = cl.cl_from
INNER JOIN
    page c
ON
    cl.cl_to = c.page_title
INNER JOIN
    page_props pp
ON
    c.page_id = pp.pp_page
WHERE
    pp.pp_propname = 'hiddencat'
AND
    c.page_namespace = 14
AND
    p.page_namespace = ?
AND
    p.page_title in (??)