<?php

/**
 *
 * @author magog
 */
readonly class Category_Files_Category
{
    public function __construct(
        public string $category,
        public ?int $max_depth = null
    ) {}

}