/// <reference path="../../../js/node_modules/@types/jquery/index.d.ts" />
/// <reference path="../../../js/node_modules/@types/jqueryui/index.d.ts" />
/// <reference path="./types/JQuery.dateTimePicker.d.ts" />


const src = $("#src");
const initialSrcVal = src.attr("value");
const testSearch = $("#test-search");
const form = $("form");
const identCookie = $("#ident-cookie");
const checkboxClicked = () => {
    okButton.attr("disabled", $("#checkbox-disclaimers").is(":checked") ? null : "disabled");
};

let okButton: JQuery<HTMLButtonElement>;

(document.querySelector("#ident-frame") as HTMLIFrameElement).addEventListener("load", ({target}) => {
    (target as HTMLIFrameElement).contentWindow.postMessage({ watch: true }, location.origin);

    addEventListener("message", ({data, origin}) => {
        if (origin !== location.origin) {
            return;
        }

        const cookie = data.cookie as string;

        $(".submit_button").toggle(!!cookie);
        $("#verify-text").toggle(!cookie);
        identCookie.val(cookie);
    });
});

$("#start,#end").datetimepicker({ dateFormat: "yy/mm/dd", timeFormat: "HH:mm:ss" });

$("[name='type']").on("change", ({target}) => {
    var typeText = $("#type-text");
    var uploaderOptions = $(".uploader-option");
    var val = $(target).val() as string;
    var wrapper = $("#subcategories-wrapper");
    var wrapperParent = wrapper.parent();

    wrapper
        .add(testSearch)
        .add(uploaderOptions)
        .hide();
    wrapperParent.css("height", "initial");
    src.val("");
    typeText.text(val.substr(0, 1).toUpperCase() + val.substr(1));
    if (val === "category") {
        wrapper.show();
        src.val(initialSrcVal);
    } else if (val === "search") {
        wrapperParent.css("height", `${wrapperParent.height()}px`);
        testSearch.show();
    } else {
        uploaderOptions.show();
    }
}).trigger("change");

testSearch.on("click", () => {
    open("//commons.wikimedia.org/w/index.php?title=Special:Search&fulltext=" +
            `Search&ns6=1&search=${encodeURIComponent(src.val() as string)}`, "_blank");
});

//validation
form.on("submit", event => {
    //verify logged in
    if (!identCookie.val()) {
        return false;
    }

    src.val($.trim(src.val() as string));
    if ($("[name='type']").val() === "category" && !(src.val() as string).match(/^category:\S/i)) {
        $("<div/>")
            .html("Please input a valid category")
            .dialog({
                modal: true,
                width: 400,
                buttons: {
                    OK() {
                        $(this).dialog("close");
                    }
                }
            });
        event.stopImmediatePropagation();
        return false;
    }
});

form.on("submit", function disclaimers() {
    $("#dialog-disclaimers").dialog({
        modal: true,
        width: 600,
        buttons: {
            OK() {
                $(this).dialog("close");
                form.off("submit", disclaimers).trigger("submit");
            },
            Cancel() {
                $(this).dialog("close");
            }
        },
        open() {
            //FIX ME: don't do this by index...
            okButton = $("button:eq(1)", $(this).parent());
            checkboxClicked();
        }
    });

    return false;
});
$("#checkbox-disclaimers").on("change", checkboxClicked);