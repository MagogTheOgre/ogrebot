<?php

class Http_Cache_Reader {

	
	const CACHE_FILE_NAME = "files.properties";
	
	/**
	 * 
	 * @var int
	 */
	private static $default_cache_time = DEFAULT_URL_CACHE_TIME;
	
	/**
	 * 
	 * @var int
	 */
	private $cache_time;
	
	/**
	 * 
	 * @var string[][]
	 */
	private static $cache_table;
	
	/**
	 * @var boolean
	 */
	private static $purged_cache = false;
	
	/**
	 * 
	 * @var File_Downloader
	 */
	private $file_downloader;
	
	private static function _autoload() {
	    $cache_table_text = @file_get_contents(CACHE_DIRECTORY . self::CACHE_FILE_NAME) ?: "{}";
	    
	    self::$cache_table = json_decode($cache_table_text, true) ?: [];
	}
	
	/**
	 * 
	 * @param int $default_cache_time
	 * @return void
	 */
	public static function set_default_cache_time(int $default_cache_time) {
		self::validate_cache_time($default_cache_time);
		self::$default_cache_time = $default_cache_time;
	}
	
	/**
	 * 
	 * @param int $cache_time
	 * @return void
	 * @throws IllegalArgumentException
	 */
	public static function validate_cache_time(int $cache_time) {
		if ($cache_time < 0 || $cache_time > DEFAULT_URL_CACHE_TIME) {
			throw new IllegalArgumentException("Cache time cannot be less than 0 or ".
					" greater than ". DEFAULT_URL_CACHE_TIME);
		}
	}
	
	/**
	 * 
	 * @param int $http_cache_time
	 * @param array $curl_opts
	 */
	public function __construct(int $cache_time = null, File_Downloader $file_downloader = null) {
	    $this->set_cache_time($cache_time ?: self::$default_cache_time);
	    $this->file_downloader = $file_downloader ?: new Download_Util();
	}
	
	/**
	 * 
	 * @return int
	 */
	public function get_cache_time() {
		return $this->cache_time;
	}
	
	/**
	 * 
	 * @param int $cache_time
	 * @return void
	 */
	public function set_cache_time(int $cache_time) {
		self::validate_cache_time($cache_time);
		$this->cache_time = $cache_time;
	}

	/**
	 *
	 * @param int $cache_time
	 * @return void
	 */
	public function set_max_cache_time(int $cache_time) {
		self::validate_cache_time($cache_time);
		if ($this->cache_time > $cache_time) {
			$this->cache_time = $cache_time;
		}
	}
	
	/**
	 * Currently only supports URLs with <=255 characters (excluding http://)
	 * 
	 * @param string $url
	 * @return string the http content
	 * @throws IllegalArgumentException not an http:// url, or the url >=256 characters
	 * @throws CantOpenFileException can't open the cached file
	 * @throws AssertionFailureException $url is not a string
	 * @throws CURLError can't download the file
	 */
	public function get_and_store_url(string $url): string  {
		global $environment, $logger;
		
		$now = time();
		$cache_file_data = self::$cache_table[$url] ?? null;
        if ($cache_file_data) {
            $filename = $cache_file_data['filename'];
            $time = $cache_file_data['time'];
            if ($filename !== null && file_exists(CACHE_DIRECTORY . $filename) && $time >= $now - $this->cache_time) {
                $logger->debug("... found in cache");
                return Local_Io::file_get_contents_ensure(CACHE_DIRECTORY . $filename);
            }
            
            if (filename && file_exists(CACHE_DIRECTORY . $filename)) {
                unset(self::$cache_table[$url]);
                unlink(CACHE_DIRECTORY . $filename);
            }
        }
		
		
		do {
			$filename = (string)rand();
		} while (file_exists(CACHE_DIRECTORY . $filename));
		
		$this->file_downloader->download($url, CACHE_DIRECTORY . $filename);
		
        self::$cache_table[$url] = [
            'filename' => $filename,
            'time' => $now
        ];
		self::refresh_cache();
		
		return Local_Io::file_get_contents_ensure(CACHE_DIRECTORY . $filename);
	}
	
	public static function refresh_cache() {
	    global $logger;
	    
	    $logger->debug("Saving cache: " . count(self::$cache_table) . " entries.");
	    
	    $text = json_encode(self::$cache_table, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
	    Local_Io::file_put_contents_ensure(CACHE_DIRECTORY . self::CACHE_FILE_NAME, $text);
	}
	
	public function add_path(string $url, string $path): void {
	    throw new IllegalStateException("Unsupported operation.");	    
	}
}
