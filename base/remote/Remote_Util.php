<?php

/**
 * 
 * @author patrick
 *
 */
class Remote_Util {
	
    const DEFAULT_OPTS = [
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_USERAGENT => OGREBOT_USERAGENT, 
        CURLOPT_SSL_VERIFYPEER => ComputedConstants::SSL_VERIFY_PEER
    ];
	const REQUIRED_OPTS = [CURLOPT_RETURNTRANSFER => 1];
		
	/**
	 * 
	 * @var array
	 */
	private $opts;
	
	/**
	 * 
	 * @param array $options 
	 */
	public function __construct(array $options = []) {
	    $this->opts = array_replace([], self::DEFAULT_OPTS, $options, self::REQUIRED_OPTS);
	}
	
	/**
	 * 
	 * @param string $url
	 * @throws CURLError
	 * @return Remote_Response
	 */
	public function execute(string $url): Remote_Response {
		global $logger;
	
		$logger->debug("Fetching $url");
		
		$ch = curl_init();
		try {
		    curl_setopt_array($ch, $this->opts);
		    curl_setopt($ch, CURLOPT_URL, $url);
		    
    		$response = new Remote_Response();
    		$response->response_text = curl_exec($ch);
    		$response->response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    		$response->time = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
    		$response->size = strlen($response->response_text);
    		$response->mime = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
    
    		if ($response->response_code < 200 || $response->response_code > 203) {
                throw new CURLError($response->response_code, $response->response_text);
    		}
		} finally {
		    curl_close($ch);
		}
	
		return $response;
	}
}