import path from "path";
import xlsx from 'node-xlsx';
import { arrayFindOrDie } from '../collectionUtils';
import Io from '../io';
import { ElectionTracker } from './ElectionTracker';
import CovidConstants from "../covid19/Constants";
import {castNumber, castString} from "../../lib/typeUtil";
import { UnPromisify } from "./types";
import { ElectionResultImpl } from "./ElectionResult";

export class UsElectionAtlasTracker implements ElectionTracker {

    
    constructor(private readonly dataFile: string, private readonly svgFile: string) {
        
    }

    isEligible(stateName: string) {
        return stateName === this.svgFile;
    }
    async getCountyTotals() {
        const xlsContent = await Io.readFileFromZip(`${this.dataFile}.zip`, path.parse(this.dataFile).base);
        const {data} = arrayFindOrDie(xlsx.parse(xlsContent), ({name}) => name === "County"); 

        const countyCitySplit = +arrayFindOrDie(Object.entries(data[0]), ([, name]) => name ==="LSAD_TRANS")[0];
        const dIndex = +arrayFindOrDie(Object.entries(data[0]), ([, candidate]) => candidate === "Clinton" || candidate === "Biden")[0];
        const rIndex = +arrayFindOrDie(Object.entries(data[0]), ([, candidate]) => candidate === "Trump")[0];

        const stateCounties : UnPromisify<ReturnType<ElectionTracker["getCountyTotals"]>> = {};
        
        for (const row of data) {
            const county = castString(row[0]);
            const state = castString(row[1]);
            const cityAppend = castString(row[countyCitySplit]) === "City" ? "_city" : "";
            if (CovidConstants.STATE_ABBREVIATIONS_INV[state]) {
                (stateCounties[state] || (stateCounties[state] = {}))[county + cityAppend] =
                    new ElectionResultImpl({
                        r: castNumber(row[rIndex]),
                        d: castNumber(row[dIndex])
                    });
            }
        }
        return stateCounties;
    }
    
}