<?php
class Category_Files_Log_Entry {

	/**
	 * 
	 * @return string[]
	 */
	public static function get_all_dates() {
	    $dates = Array_Utils::array_map_filter(Local_Io::get_all_files_in_directory(ComputedConstants::CATEGORY_FILES_OUTPUT_PATH,
            false), function (string $name) {
            $date = Regex_Util::match("/^(\d+)\.json$/", $name, 0)[1] ?? null;
            if ($date) {
                return $date;
            }
            return null;
        });
	    sort($dates);
	    return $dates;
	}
	
	/**
	 * 
	 * @return string[]
	 */
	public static function get_all_gallery_names() {
	    $master_files = Array_Utils::flat_map(self::get_all_dates(),
	        fn (string $date) => Local_Io::json_decode(ComputedConstants::
                    CATEGORY_FILES_OUTPUT_PATH . "$date.json"));
        $master_files = array_unique($master_files);
        sort($master_files, SORT_FLAG_CASE | SORT_STRING);
		
		return $master_files;
	}

	public static function get_json_file(int $date, string $gallery): ?string {
        $index = array_flip(Local_Io::json_decode(
            ComputedConstants::CATEGORY_FILES_OUTPUT_PATH . "$date.json", []))[$gallery] ?? null;
        return $index !== null ? ComputedConstants::CATEGORY_FILES_OUTPUT_PATH . "$date.$index.json" : null;
	}
}