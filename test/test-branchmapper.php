<?php
require_once __DIR__ . "/../base/bootstrap.php";

global $classloader;
$classloader->get_all_class_names_of_type( Latitude_Longitude_Svg_Creator::class);

function test_usa_counties_latitude_longitude(): void {
    $svg_creator = Direct_Latitude_Longitude_Svg_Creator::load_from_request_args(
        ["latlong" =>
            "34.9408N, 78.9261W
             34.3903N, 77.682W"
        ]
    );
    $svg = Latitude_Longitude_Svg::get_instances()["USA_location_map_-_counties"];
    $text = $svg_creator->run($svg);
    Assertions::notEmpty($text);
}

/**
 * Imagick requirements
 */
function test_svg_format(): void {
    foreach (Latitude_Longitude_Svg::get_instances() as $svg) {
        Assertions::startsWith(
            haystack: $svg->text,
            needle: '<?xml version="1.0" encoding="UTF-8" standalone="no"?>',
            context: $svg->name,
        );
    }
}

/**
 * @type callable[] $tests
 */
$tests = ["test_usa_counties_latitude_longitude", "test_svg_format"];

foreach ($tests as $test) {
    $test();
}
