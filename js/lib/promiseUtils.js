"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sleep = exports.SleepPromise = exports.shutdown = exports.startup = void 0;
const io_1 = __importDefault(require("./io"));
var warningTimer;
const MAX_TIMEOUT = 0x7fffffff;
//dummy timeout to keep the Node process alive
function startup(timeout = MAX_TIMEOUT) {
    warningTimer = warningTimer || setTimeout(() => { }, timeout);
}
exports.startup = startup;
function shutdown() {
    io_1.default.removeTempFiles();
    warningTimer && clearTimeout(warningTimer);
}
exports.shutdown = shutdown;
class SleepPromise {
    _timeout = 0;
    _promise;
    constructor(millis) {
        this._promise = new Promise(resolve => {
            this._timeout = setTimeout(resolve, millis);
        });
    }
    get promise() {
        return this._promise;
    }
    cancel() {
        clearTimeout(this._timeout);
    }
}
exports.SleepPromise = SleepPromise;
const sleep = (millis) => new SleepPromise(millis).promise;
exports.sleep = sleep;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvbWlzZVV0aWxzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicHJvbWlzZVV0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLDhDQUFzQjtBQUV0QixJQUFJLFlBQTZCLENBQUM7QUFDbEMsTUFBTSxXQUFXLEdBQUcsVUFBVSxDQUFDO0FBRS9CLDhDQUE4QztBQUM5QyxTQUFnQixPQUFPLENBQUMsT0FBTyxHQUFHLFdBQVc7SUFDekMsWUFBWSxHQUFHLFlBQVksSUFBSSxVQUFVLENBQUMsR0FBRyxFQUFFLEdBQUUsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0FBQ2pFLENBQUM7QUFGRCwwQkFFQztBQUVELFNBQWdCLFFBQVE7SUFDcEIsWUFBRSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ3JCLFlBQVksSUFBSSxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDL0MsQ0FBQztBQUhELDRCQUdDO0FBRUQsTUFBYSxZQUFZO0lBQ2IsUUFBUSxHQUFXLENBQUMsQ0FBQztJQUNyQixRQUFRLENBQWdCO0lBRWhDLFlBQVksTUFBYztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksT0FBTyxDQUFPLE9BQU8sQ0FBQyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUMvQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxJQUFXLE9BQU87UUFDZCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQUVNLE1BQU07UUFDVCxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7Q0FDSjtBQWpCRCxvQ0FpQkM7QUFHTSxNQUFNLEtBQUssR0FBRyxDQUFDLE1BQWMsRUFBRSxFQUFFLENBQUMsSUFBSSxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDO0FBQTdELFFBQUEsS0FBSyxTQUF3RCJ9