import csvParse from 'csv-parse/lib/sync';


export function csvToObject<T extends string>(fileContent: string) {
    const [fields, ...results] = <string[][]>csvParse(fileContent);
    return results.map(resultArray => <{ [x in T]: string }><{ [x in string]: string }>Object.fromEntries(resultArray.map((value, index) =>
        [fields[index], value])));
}