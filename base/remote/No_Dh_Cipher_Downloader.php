<?php

/**
 * Class to bypass OpenSSL shortcomings for some sites
 * @author magog
 *
 */
class No_Dh_Cipher_Downloader extends Wget_File_Downloader {
    
    /**
     *
     * @param array $opts
     */
    public function __construct(array $opts = []) {
        parent::__construct(array_replace([], $opts, [
            "cipher" => "DEFAULT:!DH"
        ]));
    }
    
}

