<?php
require_once "constants.php";
require_once "Classloader.php";

require_once __DIR__ . "/../REL1_0/Init.php";

$env = Environment::get();

/** @var string[] $argv */
/** @var array $constants */
/** @var Logger $logger */
/** @var String_Utils $string_utils */
/** @var Validation $validator */
/** @var Wiki_Interface $wiki_interface */