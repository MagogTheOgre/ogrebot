<?php
require_once __DIR__ . "/../base/bootstrap.php";
global $string_utils;

// get all gallery names
$all_galleries = Category_Files_Log_Entry::get_all_gallery_names();

$dates = array_reverse(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1-$2-$3", 
    Category_Files_Log_Entry::get_all_dates()));

$http_io = new Http_Io();
$http_io->ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<?php 
	$http_io->transcludeScriptRemote(['bootstrap.css.url', 'bootstrap.css.theme.url']);
	$http_io->transcludeScript("category-files-blame", "css");
	?>
	<title>Category Files Blamer</title>
</head>
<body data-ng-app="app" data-ng-controller="ctrl"
      data-ng-init='galleries=<?= $string_utils->encode_json_for_html($all_galleries) ?>;
			 dates=<?= $string_utils->encode_json_for_html($dates) ?>'>
	<form ng-cloak>
		<div class="_table">
			<div class="table-row">
				<div class="table-cell">
					<label for="date">Date</label>&nbsp;
				</div>
				<div class="table-cell">
					<select class="form-control" data-ng-model="selectedDate" id='date' data-ng-change="loadData()">
						<option data-ng-repeat="date in dates" value="{{date}}">{{date}}</option>
					</select>
				</div>
			</div>
			<div class="table-row">
				<div class="table-cell">
					<label for="gallery">Gallery</label>&nbsp;
				</div>
				<div class="table-cell">
                    <select class="form-control" data-ng-model="selectedGallery" ata-ng-attr-id="gallery"
                            data-ng-show="mobileView">
                        <option value="">&lt;-- Select --&gt;</option>
                        <option data-ng-repeat="gallery in galleries" value="{{gallery}}">{{gallery}}</option>
                    </select>
                    <input type="text" placeholder="&lt;-- Select --&gt;" class="form-control" autocomplete="off"
                           style="width: 536px" data-ng-attr-id="gallery" data-ng-show="!mobileView"
                           data-uib-typeahead="gallery for gallery in galleries | filter:$viewValue"
                           data-typeahead-show-hint="true" data-typeahead-min-length="0" data-ng-model="typedGallery"
                           data-typeahead-is-open="true" data-typeahead-auto-close="disabled" />
				</div>
			</div>
		</div>
        {{start}}–{{end}} of {{total}}
        (<button class="btn btn-link" style="padding: 0 6px" data-ng-repeat-start="size in stepSizes"
                data-ng-click="stepSize = size" data-ng-disabled="stepSize === size">{{size}}</button><span
                data-ng-repeat-end data-ng-if="!$last">|</span>)
        <button aria-label="previous" data-ng-disabled="!hasPrevious" style="transform: rotate(180deg);"
                data-ng-click="previous()" class="disablable-button">&#10148;</button>
        <button aria-label="next" data-ng-disabled="!hasNext" data-ng-click="next()"
                class="disablable-button">&#10148;</button>
	</form>
	
	<div data-ng-show="loading">Please wait one moment while the data is loading...</div>
	<div data-ng-show="!loading && !total && selectedGallery">No entries found which match your query.</div>
	<div data-ng-cloak class="mainTable" data-ng-repeat="entry in entriesSubset" data-ng-show="!loading">
		<span data-ng-repeat="leaf in entry">
			<span data-ng-show="!$first">=&gt;</span>
			<a target="_blank" href="https://commons.wikimedia.org/wiki/{{leaf | escape}}">{{leaf}}</a>
		</span>
	</div>
    <script type="text/ng-template" id="uib/template/typeahead/typeahead-popup.html">
        <ul class="dropdown-menu" data-ng-show="!mobileView && isOpen() && !moveInProgress" role="listbox"
            aria-hidden="{{!isOpen()}}" data-ng-style="{top: position.top() + 'px', left: position().left+'px'}"
            style="overflow-y: scroll; max-height: 300px">
            <li class="uib-typeahead-match" data-ng-repeat="match in matches track by $index"
                data-ng-class="{active: isActive($index)}" data-ng-mouseenter="selectActive($index)"
                data-ng-click="selectMatch($index, $event)" role="option" id="{{::match.id}}">
                <div data-uib-typeahead-match data-index="$index" data-match="match" data-query="query"
                     data-template-url="templateUrl"></div>
            </li>
        </ul>
    </script>
    <script type="text/ng-template" id="uib/template/typeahead/typeahead-match.html">
        <a href tabindex="-1" data-ng-bind-html="match.label | uibTypeaheadHighlight:query"
           data-ng-attr-title="{{match.label}}"></a>
    </script>
    <?php
    $http_io->transcludeScriptRemote(['angular.js.url', "angular.ui.bootstrap.js.url"]);
    $http_io->transcludeScript("category-files-blame", "js");
    ?>
</body>
</html>