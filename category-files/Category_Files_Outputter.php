<?php
class Category_Files_Outputter {

    private static int $gallery_max;

    private static int $overflow_max;

    private static int $overflow_max_files;

    private static string $log_link;

    private static int $page_max;

    /**
     *
     * @var CategoryFilesSingleConfiguration[]
     */
    private array $configurations;

    private CategoryFilesRunner $runner;

    private static function _autoload(): void {
        list(self::$gallery_max, self::$overflow_max,
            self::$page_max, self::$log_link) = Environment::props("constants",
            ['category_files.limit.gallery',
                "category_files.limit.overflow", 'category_files.limit.page', 'category_files.log.url']);

        self::$overflow_max_files = self::$page_max * self::$overflow_max;
    }

    /**
     * @throws CantOpenFileException
     * @throws IllegalArgumentException
     * @throws ProjectNotFoundException
     */
    public function __construct(
        private readonly Project_Data $project_data,
        ?string $file_override,
        /**
         * @var class-string<CategoryTreeLogger>
         */
        private readonly string $category_tree_logger_class
    ) {
        $this->runner = new CategoryFilesRunner($project_data->getWiki());

        if ($file_override) {
            $this->configurations = CategoryFilesSingleConfiguration::initCategoryFilesConfigurationsFromLocalFile(
                $project_data->getWiki(), $file_override);
        } else {
            $this->configurations = CategoryFilesSingleConfiguration::initCategoryFilesConfigurationsFromProperties(
                $project_data->getWiki(), Environment::prop('constants', 'category_files.gallery_definition'));
        }
    }

    public function run(Wiki_Date $start, Wiki_Date $end): void {
        global $constants, $logger, $wiki_interface;

        $date_format = "== {{date|$start->year|$start->month|$start->date}} ==\n";
        $logger->debug("\$dateformat: $date_format");

        $existing_texts = $wiki_interface->get_text($this->project_data->getWiki(),
            Array_Utils::flat_map($this->configurations, fn(CategoryFilesSingleConfiguration $config) =>
                $config->get_subpage_and_overflow($end)));


        $categoryTree = $this->runner->runFromFileUploadDates($start, $end);
        // $categoryTree = $configRunner->runFromPageNames(array("File:Cargolux B748F LX-VCG.jpg"));
        $categoryTree->flush();

        $logger->info("Organizing");
        $file_lists = [];
        $configs = [];
        $i = 0;

        /**
         * @var CategoryTreeLogger $category_tree_logger
         */
        $category_tree_logger = new $this->category_tree_logger_class($start);

        foreach ($this->configurations as $config) {
            $logger->debug("**************************");
            $logger->debug(++$i . " of " . count($this->configurations) . ": " . $config->getGalleryName());
            $logger->debug("**************************");
            $logger->debug("");

            $hash = spl_object_hash($config);
            $configs[$hash] = $config;

            foreach ($config->getCategories() as $category) {
                $nextFilesAndCategories = $categoryTree->pagesInTree($category,
                     $config->getIgnoredSubcategories(), $config->getGalleryName(), $category_tree_logger,
                    $config->get_hard_omissions());

                // remove everything but files
                $next = array_filter($nextFilesAndCategories, fn(string $nextFileOrCat) => strncmp($nextFileOrCat, "File:", 5) === 0);

                $file_lists[$hash] = array_merge($file_lists[$hash] ?? [], $next);
            }
        }

        $logger->info("Outputting to file pages");

        foreach ($file_lists as $hash => $files) {
            $files = array_unique($files);
            $config = $configs[$hash];
            $logger->debug("****************************************");
            $logger->debug($config->getGalleryName());

            $count = count($files);

            if ($count === 0) {
                $logger->info("Skipped");
                continue;
            }

            $logger->debug("$count files");
            $target = $config->getGalleryName();

            $subpages = $config->get_subpage_and_overflow($start);

            $existing_text_responses = array_map(fn (string $key): Page_Text_Response => $existing_texts[$key], $subpages);

            try {
                $all_write_data = $this->get_write_data($existing_text_responses, $files);
            } catch (Category_Files_Overflow_Exception $e) {
                $this->write_error($start, $target, "category_files.limit.gallery.text",
                    "category_files.limit.gallery.title",
                    ["max" => $e->getMaxSize(), "count" => $e->getTotalSize() + $e->getExistingSize()]);
                continue;
            }

            $page_and_write_data = [];

            $gallery_count = 0;
            foreach ($all_write_data as $i => $write_data) {
                $text = "";
                foreach ($write_data as $gallery_files) {
                    if ($gallery_files) {

                        $text .= "\n\n== {{date|$start->year|$start->month|$start->date}}";
                        if ($gallery_count++) {
                            $text .= " ($gallery_count)";
                        }
                        $text .= " ==\n<gallery";
                        if ($config->isShowFilename()) {
                            $text .= " showfilename=\"true\"";
                        }
                        if ($config->getMode() !== null) {
                            $text .= " mode=\"" . $config->getMode() . "\"";
                        }
                        if ($config->getWidth() !== null) {
                            $text .= " widths=\"" . $config->getWidth() . "px\"";
                        }
                        if ($config->getHeight() !== null) {
                            $text .= " heights=\"" . $config->getHeight() . "px\"";
                        }
                        $text .= ">\n" . join("\n", $gallery_files) . "\n</gallery>";
                    }
                }
                if ($text) {                    
                    $range_pagename = $config->get_subpage_and_overflow($start, false)[$i];
                    if ($i > 0 && !$config->getSubpage()) {
                        $this->write_error($start, $target, "category_files.limit.subpage.text",
                            "category_files.limit.subpage.title",
                            ["max" => self::$page_max]);
                        continue;
                    }
                    $page_and_write_data[$range_pagename] = $text;
                }
            }

            
            if ($config->getSubpage()) {
                /* output new line to list page if it's not already present */
                $logger->debug("Subpage edit TRUE");

                $list_text = "";
                $current_list_text = $wiki_interface->get_text($this->project_data->getWiki(), $target)->text;
                foreach ($page_and_write_data as $page => $text) {

                    $page_wikitext = "\n*[[$page]]";
                    $logger->debug("\$range_page_wikitext: $page");

                    if (strpos($current_list_text, $page_wikitext) === false) {
                        $logger->debug("Range not found in gallery; adding it.");
                        $list_text .= $page_wikitext;
                    }
                }
                if ($list_text) {
                    $edit_summary = substr(String_Utils::replace_named_variables(
                        Environment::prop("messages", "category_files.gallery.new"),
                        ["galleries" =>
                            join(", ",
                                Array_Utils::array_map_pass_key($page_and_write_data, fn($key) => "[[$key]]")
                            )
                        ]
                    ), 0, 255);

                    try {
                        $listpg = $wiki_interface->new_page($this->project_data->getWiki(), $target);
                        $wiki_interface->edit($listpg, $list_text, $edit_summary,
                            EDIT_APPEND);
                    } catch (Exception $e) {
                        Remote_Io::ogrebotMail($e);
                    }
                }
            } else {
                $logger->debug("Subpage edit FALSE");
            }

            foreach ($page_and_write_data as $subpage => $text) {
                $pg = $wiki_interface->new_page($this->project_data->getWiki(),
                    "$target$subpage");

                if (!$pg->get_exists()) {
                    $text = "__TOC__\n$text";
                    if ($config->isNoIndex()) {
                        $text = "__NOINDEX__\n$text";
                    }
                    if ($config->isWarning()) {
                        $text = "{{" . $constants["category_files.warning_template"] . "}}\n$text";
                    }
                }

                $firstCategory = $config->getCategories()[0]->category;
                $catText = count($config->getCategories()) == 1 ? "[[$firstCategory]]" : "multiple categories";
                $summary = "BOT: updating gallery for files in $catText";
                $wiki_interface->set_curl_timeout(300); // the page takes a long time for Mediawiki to render...
                try {
                    $wiki_interface->edit($pg, $text, $summary, EDIT_APPEND);
                } catch (Exception $e) {
                    Remote_Io::ogrebotMail($e);
                }
                $wiki_interface->reset_curl_timeout();
            }
        }

    }

    /**
     *
     * @param Page_Text_Response[] $existing_pages
     * @param string[] $files
     * @return string[][][]
     * @throws Category_Files_Overflow_Exception
     */
    private function get_write_data(array $existing_pages, array $files): array
    {
        $total_size = count($files);
        $split_files = [];
        foreach ($existing_pages as $existing_text_response) {
            $existing_length = $this->getExistingSize($existing_text_response);
            $next_split = [];
            for ($j = 0; $j < self::$page_max; $j += self::$gallery_max) {
                $max = max([min([self::$page_max - $j - $existing_length, self::$gallery_max]), 0]);
                $next = array_splice($files, 0, $max);
                $next_split[] = $next;
                if (!$files) {
                    break;
                }
            }
            $split_files[] = $next_split;
            if (!$files) {
                return $split_files;
            }
        }

        $existing_size = array_reduce($existing_pages, fn($ax, $dx) =>
            $ax + $this->getExistingSize($dx), 0);
        throw new Category_Files_Overflow_Exception(
            $existing_size,
            count($existing_pages) * self::$page_max,
            $total_size
        );
    }

    private function getExistingSize(Page_Text_Response $page_text_response) : int {
        return ($page_text_response->exists ? substr_count($page_text_response->text, "\nFile:") : 0);
    }

    /**
     * @param Wiki_Date $wiki_date
     * @param string $target
     * @param string $message_key
     * @param string $summary_key
     * @param string[] $variables
     */
    private function write_error(Wiki_Date $wiki_date, string $target, string $message_key, string $summary_key,
                                 array $variables): void {
        global $logger, $wiki_interface;
        try {
            $date_string = "$wiki_date->year-$wiki_date->month-$wiki_date->date";
            $gallery = rawurlencode($target);
            $log_link = self::$log_link . "?date=$date_string&gallery=$gallery";

            $error_summary = String_Utils::replace_named_variables(Environment::prop("messages", $summary_key),
                ["gallery" => $target]);

            $error_text = String_Utils::replace_named_variables(Environment::prop("messages", $message_key),
                array_replace($variables, ["gallery" => $target, "log_link" => $log_link]));

            $logger->warn($error_text);


            $talk_page = $wiki_interface->new_page($this->project_data->getWiki(),
                $this->project_data->get_talk_page_name(Project_Data::get_base_page_name($target)));
            $wiki_interface->edit($talk_page, $error_text, $error_summary, EDIT_APPEND);
        } catch (Exception $e) {
            Remote_Io::ogrebotMail($e);
        }
    }
    
    public static function get_overflow_max(): int {
        return self::$overflow_max;
    }

}

?>
