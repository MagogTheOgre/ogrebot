<?php

class LocalCronRunner extends AbstractCronRunner {

	public function getExecCommand() {
		$directory = $this->runConfig->getDirectory();
		$command   = $this->runConfig->getCommand();
		$args      = $this->runConfig->getArgs();
		$timeout   = $this->runConfig->getTimeout();
		$base_directory = BASE_DIRECTORY;

		return "timeout $timeout php $base_directory/$directory/$command.php $args";
	}
}