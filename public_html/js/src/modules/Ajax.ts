type ResponseKey = "arrayBuffer" | "blob" | "formData" | "json" | "text";
type ReturnedPromise<T extends ResponseKey> = ReturnType<Awaited<Response[T]>>;
type RequestInput<T extends ResponseKey | null | undefined> = RequestInit & {
    //required
    url: string

    //optional, default to "json"
    responseType?: T extends undefined | null ? "json" : T

}
type AjaxInput<T extends ResponseKey> = Omit<RequestInput<T>, "method">;

interface AbstractInput<T> {
    data: T
}
type FormType = HTMLFormElement | ConstructorParameters<typeof URLSearchParams>[0];
type FormInput = AbstractInput<FormType>
type JsonInput = AbstractInput<any>

interface GetFormInput<T extends ResponseKey> extends AjaxInput<T>, Partial<FormInput> {
    method?: "get" | null | undefined
}

interface PostFormInput<T extends ResponseKey> extends AjaxInput<T>, FormInput {
    method: "post"
}

interface PostJsonInput<T extends ResponseKey> extends AjaxInput<T>, JsonInput {
    method: "json"
}

type AnyPostInput<T extends ResponseKey> = PostFormInput<T> | PostJsonInput<T>
type AjaxAnyInput<T extends ResponseKey> = GetFormInput<T> | AnyPostInput<T>
class Ajax<T extends ResponseKey> implements PromiseLike<ReturnedPromise<T>> {

    then: Promise<ReturnedPromise<T>>["then"];

    constructor(input: AjaxAnyInput<T>) {
        const promise = new Promise(async (resolve, reject) => {
            try {
                const responseType = (input.responseType || "json") as T;
                const [requestInfo, requestInit] = Ajax[({
                    "get": "getFormSerializer",
                    "post": "postFormSerializer",
                    "json": "postJsonSerializer"
                } as const)[input.method || "get"]](input);
                const response = await fetch(requestInfo, requestInit);
                resolve(await response[responseType]());
            } catch (e) {
                reject(e);
            }
        });
        this.then = promise.then.bind(promise);
    }

    private static formToUrlSearchParams(data: FormType) {
        if (data instanceof HTMLFormElement) {
            data = <[string, string][]>[...new FormData(data)].filter(([, value]) =>
                 typeof value === "string");
        }
        return new URLSearchParams(data)
    }
    
    private static getFormSerializer(input: AjaxAnyInput<ResponseKey>) {
        const { data, url, ...requestInput } = input as GetFormInput<ResponseKey>;
        const [baseUrl] = url.split("?");
        const combinedSearchParams = new URLSearchParams([
            ...new URL(url).searchParams,
            ...this.formToUrlSearchParams(data)
        ]).toString();
        requestInput.method = "get";
        return [baseUrl + (combinedSearchParams ? `?${combinedSearchParams}` : ""),
            requestInput] as const;
    }

    private static postSerializer<I extends AnyPostInput<ResponseKey>>(
        contentType: string, bodyBuilder: (data: I["data"]) => BodyInit,
        input: AjaxInput<ResponseKey>) {
        const { data, headers, url, ...requestInput } = input as I;
        const newHeaders = new Headers(headers);
        newHeaders.set("Content-Type", contentType);
        return [url, Object.assign(requestInput, {
            headers: newHeaders,
            method: "post",
            body: bodyBuilder(data as I["data"])
        })] as const;
    }

    private static postFormSerializer(input: AjaxInput<ResponseKey>) {
        return this.postSerializer("application/x-www-form-urlencoded", 
            this.formToUrlSearchParams, input)
    }
    
    private static postJsonSerializer(input: AjaxInput<ResponseKey>) {
        return this.postSerializer("application/json", JSON.stringify, input)
    }
}