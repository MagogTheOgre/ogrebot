<?php

/**
 * Currently only configured for Commons
 * @author magog
 *
 */
class Category_License_Reader {
	
	const GALLERY_PAGE = "User:OgreBot/License categories";
	const PROJECT_DATA = "commons.wikimedia";
	
	/**
	 * 
	 * @var Category_License_Reader
	 */
	private static $singleton;
	
	
	/**
	 * 
	 * @var string[]
	 */
	private $categories;
	
	/**
	 * 
	 * @var string[]
	 */
	private $regexes;
	
	/**
	 * 
	 */
	private function __construct() {
		global $wiki_interface;
		
		$text_response = $wiki_interface->get_text((new Project_Data(self::PROJECT_DATA))->getWiki(), 
			self::GALLERY_PAGE);
		$this->categories = Array_Utils::map_array_function_keys(String_Utils::read_configuration_page_lines($text_response->text),
            function ($line) {
                preg_match("/^\[\[\s*\:?\s*category\s*\:\s*(.+?)(?:\s*\|\s*(.+?))?\s*\]\]/i", $line,
                    $match);

                if (!$match) {
                    Remote_Io::ogrebotMail(self::class . ": Unrecognized line: $line");
                    return;
                }

                return [$match[1], isset($match[2]) ? $match[2] : $match[1]];
            }, "IGNORE", "NOTEQUAL", true);
		
		$raw_licenses = Local_Io::load_property_file("licenses");
		$this->regexes = Array_Utils::map_array_function_keys($raw_licenses["regexes"],
            function ($value) {
                $lastBar = mb_strrpos($value, "|");

                $category = String_Utils::mb_trim("/^" . substr($value, 0, $lastBar) . "$/u");
                $replacement = String_Utils::mb_trim(substr($value, $lastBar + 1));

                return [$category, $replacement];
            }, "FAIL");
	}
	
	/**
	 * 
	 * @param string[] $categories
	 * @return string[]
	 */
	public function get_license_categories(array $categories) {
		return Array_Utils::array_map_filter($categories, function ($category) {
            if ($readable = $this->categories[$category] ?? null) {
                return $readable;
            }

            foreach ($this->regexes as $regex => $replacement) {
                if (preg_match($regex, $category)) {
                    return preg_replace($regex, $replacement, $category);
                }
            }
        });
	}
	
	/**
	 * 
	 * @return Category_License_Reader
	 */
	public static function get_singleton() {
		if (self::$singleton === null) {
			self::$singleton = new self();
		}
		return self::$singleton;
	}
	
	
}