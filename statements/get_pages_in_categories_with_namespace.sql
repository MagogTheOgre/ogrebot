SELECT
 	p.page_namespace,
 	p.page_title,
    cl.cl_to
FROM
    categorylinks cl
INNER JOIN
	page p
ON 
	p.page_id = cl.cl_from
WHERE
	cl.cl_to in (??)