
export default interface MapBuilder {

    getColors(date: Date) : Promise<readonly [Record<string, string>, string]>;
}
