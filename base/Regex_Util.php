<?php

/**
 * 
 * @author magog
 *
 */
class Regex_Util {
    
	/**
	 * @return string[]
	 * @throws Regex_Exception
	 */
    public static function match(string $pattern, string $subject, int $min = 1,
                                 int $flags = 0, int $offset = 0): array {
        $result = preg_match($pattern, $subject, $matches, $flags, $offset);
		if ($result === false) {
			throw new Invalid_Regex_Exception($pattern);
		}
		if (count($matches) < $min) {
		    throw new Regex_Not_Matched_Exception($pattern, $subject);
		}
		return $matches;
	}


    /**
     *
     * @param string $pattern
     * @param string $subject
     * @param int $min
     * @param int $flags
     * @param int $offset
     * @return string[][]
     * @throws Regex_Exception
     */
	public static function match_all($pattern, $subject, int $min = 1, $flags = PREG_SET_ORDER, $offset = 0) {
		$result = preg_match_all($pattern, $subject, $matches, $flags, $offset);
		if ($result === false) {
			throw new Invalid_Regex_Exception($pattern);
		}
		if ($result < $min) {
			throw new Regex_Not_Matched_Exception($pattern, $subject);
		}
		return $matches;
	}
}