<?php
require_once __DIR__ . "/../base/bootstrap.php";
/* migrate category-files logs */
$regexp = "/^(\d{8}\.\d+)\.log$/";

$files = Local_Io::get_all_files_in_directory(ComputedConstants::CATEGORY_FILES_OUTPUT_PATH);

$logger = Environment::get()->get_logger();
foreach ($files as $file) {
    preg_match("/^([\d.]+)\.log$/", $file, $match);
    if ($match) {
        $logger->info("Converting $file...");
        $json = String_Utils::json_encode(unserialize(Local_Io::file_get_contents_ensure(
            ComputedConstants::CATEGORY_FILES_OUTPUT_PATH . $file)));
        Local_Io::file_put_contents_ensure(ComputedConstants::CATEGORY_FILES_OUTPUT_PATH . "$match[1].json",
            $json);
        unlink(ComputedConstants::CATEGORY_FILES_OUTPUT_PATH . $file);
    }
}

$logger->info("Done.");
?>
