/// <reference path="../../../../js/node_modules/@types/jquery/index.d.ts" />
/// <reference path="../../../../js/node_modules/@types/jqueryui/index.d.ts" />

class MDialog {
    private dialog: JQuery<HTMLElement>;
    
    constructor(jq: string | JQuery<any> | HTMLElement, options: JQueryUI.DialogOptions = {}) {
        this.dialog = $(jq).dialog(
            {autoOpen: false, modal: true, width: 600 , ...options}
        );
    }

    addOpenButton(jq: string | JQuery<any> | HTMLElement) {
        $(jq).on("click", () => this.open());
    }

    addCloseButton(jq: string | JQuery<any> | HTMLElement) {
        $(jq).on("click", () => this.close());
    }

    open() {
        this.dialog.dialog("open");
    }

    close() {
        this.dialog.dialog("close");
    }
};