"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cachable = exports.mapBasedCacheFactory = void 0;
const collectionUtils_1 = require("../collectionUtils");
class MapBasedCache {
    map = {};
    get(args) {
        return this.map[this.serialize(args)];
    }
    set(args, value) {
        return this.map[this.serialize(args)] = value;
    }
    serialize(args) {
        return JSON.stringify(args);
    }
}
const mapBasedCacheFactory = () => new MapBasedCache();
exports.mapBasedCacheFactory = mapBasedCacheFactory;
const cachable = (cacheFactory = exports.mapBasedCacheFactory) => {
    return (wrapper, property, propertyDescriptor) => {
        const callable = propertyDescriptor.value;
        //attached to this prototype and function, but should be unique for new object
        const allCaches = new Map();
        propertyDescriptor.value = function () {
            const cache = collectionUtils_1.computeIfAbsent(allCaches, this, () => cacheFactory(wrapper, property));
            return collectionUtils_1.computeIfAbsent(cache, arguments, () => callable.apply(this, arguments));
        };
    };
};
exports.cachable = cachable;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FjaGFibGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjYWNoYWJsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSx3REFBNEQ7QUFJNUQsTUFBTSxhQUFhO0lBQ1AsR0FBRyxHQUF3QixFQUFFLENBQUM7SUFFdEMsR0FBRyxDQUFDLElBQWdCO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELEdBQUcsQ0FBQyxJQUFnQixFQUFFLEtBQVU7UUFDNUIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7SUFDbEQsQ0FBQztJQUVTLFNBQVMsQ0FBQyxJQUFnQjtRQUNoQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDaEMsQ0FBQztDQUNKO0FBRU0sTUFBTSxvQkFBb0IsR0FBRyxHQUFHLEVBQUUsQ0FBQyxJQUFJLGFBQWEsRUFBRSxDQUFDO0FBQWpELFFBQUEsb0JBQW9CLHdCQUE2QjtBQUV2RCxNQUFNLFFBQVEsR0FBRyxDQUFJLGVBQXFDLDRCQUFvQixFQUFFLEVBQUU7SUFDckYsT0FBTyxDQUFDLE9BQVUsRUFBRSxRQUFnQixFQUFFLGtCQUFzQyxFQUFFLEVBQUU7UUFDNUUsTUFBTSxRQUFRLEdBQUcsa0JBQWtCLENBQUMsS0FBSyxDQUFDO1FBRTFDLDhFQUE4RTtRQUM5RSxNQUFNLFNBQVMsR0FBRyxJQUFJLEdBQUcsRUFBK0IsQ0FBQztRQUN6RCxrQkFBa0IsQ0FBQyxLQUFLLEdBQUc7WUFDdkIsTUFBTSxLQUFLLEdBQUcsaUNBQWUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN0RixPQUFPLGlDQUFlLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3BGLENBQUMsQ0FBQztJQUNOLENBQUMsQ0FBQztBQUNOLENBQUMsQ0FBQTtBQVhZLFFBQUEsUUFBUSxZQVdwQiJ9