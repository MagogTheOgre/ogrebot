"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ElectionDiffTracker_1 = require("./../lib/maps/ElectionDiffTracker");
const Maps_1 = require("../lib/maps/Maps");
const ElectionResultsColorer_1 = require("../lib/maps/ElectionResultsColorer");
const io_1 = __importDefault(require("../lib/io"));
const UsElectionAtlasTracker_1 = require("../lib/maps/UsElectionAtlasTracker");
const ThreadPool_1 = require("../lib/ThreadPool");
(async () => {
    const thisElection = new Maps_1.StatesBuilder().addElectionTrackers(new UsElectionAtlasTracker_1.UsElectionAtlasTracker('/home/patrick/Desktop/Pres_Election_Data_2020.xlsx', "USA counties-v2")).setElectionResultHandler(ElectionResultsColorer_1.standardElectionResultColorer);
    const compareElection = new Maps_1.StatesBuilder().addElectionTrackers(new ElectionDiffTracker_1.ElectionDiffTracker(new UsElectionAtlasTracker_1.UsElectionAtlasTracker('/home/patrick/Desktop/Pres_Election_Data_2016.xlsx', "USA counties"), new UsElectionAtlasTracker_1.UsElectionAtlasTracker('/home/patrick/Desktop/Pres_Election_Data_2020.xlsx', "USA counties"))).setElectionResultHandler(ElectionResultsColorer_1.diffElectionResultColorer);
    await new ThreadPool_1.AllSettledThreadPoolImpl().enqueueAll([thisElection, compareElection].map(builder => () => builder.build(`${io_1.default.PROJECT_DIR}/../Desktop`)));
    console.log("Done");
})();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1hcHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSwyRUFBd0U7QUFFeEUsMkNBQStDO0FBQy9DLCtFQUE4RztBQUM5RyxtREFBMkI7QUFDM0IsK0VBQTRFO0FBQzVFLGtEQUE2RDtBQUM3RCxDQUFDLEtBQUssSUFBSSxFQUFFO0lBQ1IsTUFBTSxZQUFZLEdBQUcsSUFBSSxvQkFBYSxFQUFFLENBQUMsbUJBQW1CLENBQ3hELElBQUksK0NBQXNCLENBQUMsb0RBQW9ELEVBQUUsaUJBQWlCLENBQUMsQ0FDdEcsQ0FBQyx3QkFBd0IsQ0FBQyxzREFBNkIsQ0FBQyxDQUFBO0lBQ3pELE1BQU0sZUFBZSxHQUFHLElBQUksb0JBQWEsRUFBRSxDQUFDLG1CQUFtQixDQUFDLElBQUkseUNBQW1CLENBQ25GLElBQUksK0NBQXNCLENBQUMsb0RBQW9ELEVBQUUsY0FBYyxDQUFDLEVBQ2hHLElBQUksK0NBQXNCLENBQUMsb0RBQW9ELEVBQUUsY0FBYyxDQUFDLENBQ25HLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQyxrREFBeUIsQ0FBQyxDQUFDO0lBRXZELE1BQU0sSUFBSSxxQ0FBd0IsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFlBQVksRUFBRSxlQUFlLENBQUMsQ0FBQyxHQUFHLENBQy9FLE9BQU8sQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQUUsQ0FBQyxXQUFXLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNyRSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3hCLENBQUMsQ0FBQyxFQUFFLENBQUMifQ==