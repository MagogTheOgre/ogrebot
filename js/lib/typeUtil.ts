export function castString(t: any, def = ""): string {
    return typeof t === "string" ? t : def;
}
export function castNumber(t: any, def = 0): number {
    return typeof t === "string" || typeof t === "number" ? +t || 0: def;
}