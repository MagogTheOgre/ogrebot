import { ElectionResult } from "./ElectionResult";

export interface ElectionTracker {
    isEligible(mapName: string): boolean
    getCountyTotals(): Promise<{[x in string]: {[x in string] : ElectionResult}}>;
}