<?php
class Assertions {
    /**
     * @param string|string[] $haystack
     */
    static function startsWith(string|array $haystack, string $needle, string $context = null): void {
        if (is_array($haystack)) {
            foreach ($haystack as $value) {
                self::startsWith($value, $needle, $context);
            }
            return;
        }
        $haystackTruncated = substr($haystack, 0, strlen($needle));
        if (strlen ($haystack) > strlen($needle)) {
            $haystackTruncated = "$haystackTruncated...";
        }

        if ($context) {
            $context = "($context): ";
        }

        self::that(
            state: str_starts_with($haystackTruncated, $needle),
            message: "$context$haystackTruncated... did not start with $needle"
        );
    }

    static function notEmpty(string|array|null $variable, string $message = null): void {
        self::that(
            state: $variable !== null && $variable !== [] && $variable !== "",
            message: $message ?: "Expected non-empty: " . print_r($variable, true)
        );
    }

    static function that(bool $state, string $message): void {
        if (!$state) {
            throw new AssertionFailureException($message);
        }
    }
}
