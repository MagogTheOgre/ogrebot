<?php

/**
 * 
 * @author magog
 *
 */
class Remote_Response {
	
	/**
	 * For a download, the handle to the file that was downloaded
	 * @var resource|null
	 */
	public $filename;

	/**
	 * The mime type of the response
	 * @var string
	 */
	public $mime;
	
	/**
	 * 
	 * @var int
	 */
	public $response_code;
	
	/**
	 * 
	 * @var string
	 */
	public $response_text;
	
	/**
	 * 
	 * @var int
	 */
	public $size;
	
	/**
	 * The amount of time that a response took
	 * @var int
	 */
	public $time;
}