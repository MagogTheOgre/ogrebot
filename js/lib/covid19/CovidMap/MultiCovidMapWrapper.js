"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ThreadPool_1 = require("../../ThreadPool");
const CovidUtils_1 = __importDefault(require("../CovidUtils"));
const { dateRoundFloorCopy } = CovidUtils_1.default;
class MultiCovidMapWrapper {
    maps = [];
    constructor(outputDir, requestedDates, extension, mapBuilder, namingScheme, mapConstructor) {
        requestedDates = requestedDates.map(d => dateRoundFloorCopy(d));
        const digits = String(((+requestedDates[requestedDates.length - 1] - +requestedDates[0])
            / (1000 * 60 * 60 * 24)).toFixed(0)).length;
        for (const [count, date] of Object.entries(requestedDates)) {
            this.maps.push(new mapConstructor(`${outputDir}/${namingScheme(date, +count).padStart(digits, "0")}.${extension}`, date, mapBuilder));
        }
    }
    create() {
        return new ThreadPool_1.AllSettledThreadPoolImpl().enqueueAll(this.maps.map(map => () => map.create()));
    }
}
exports.default = MultiCovidMapWrapper;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTXVsdGlDb3ZpZE1hcFdyYXBwZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJNdWx0aUNvdmlkTWFwV3JhcHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUVBLGlEQUE0RDtBQUM1RCwrREFBdUM7QUFFdkMsTUFBTSxFQUFDLGtCQUFrQixFQUFDLEdBQUcsb0JBQVUsQ0FBQztBQUN4QyxNQUFxQixvQkFBb0I7SUFFN0IsSUFBSSxHQUF1QixFQUFFLENBQUM7SUFFdEMsWUFBWSxTQUFpQixFQUFFLGNBQXNCLEVBQUUsU0FBaUIsRUFBRSxVQUFzQixFQUM1RixZQUFtRCxFQUFFLGNBQ2hDO1FBRXJCLGNBQWMsR0FBRyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoRSxNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7Y0FDbEYsQ0FBQyxJQUFJLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUNoRCxLQUFLLE1BQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRTtZQUN4RCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLGNBQWMsQ0FBQyxHQUFHLFNBQVMsSUFBSSxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsSUFDOUYsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUM7U0FDdkM7SUFDTCxDQUFDO0lBR00sTUFBTTtRQUNULE9BQU8sSUFBSSxxQ0FBd0IsRUFBRSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDL0YsQ0FBQztDQUVKO0FBdEJELHVDQXNCQyJ9