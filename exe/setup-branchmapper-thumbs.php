<?php
/**
 * Warning:
 *
 * Must have both php-imagick and ImageMagick installed on host machine for this to work.
 * The latter is needed for SVG conversions.
 *
 * Also, this program takes a *very* long time to run.
 *
 * sudo apt install php8.2-imagick imagemagick
 */
require_once __DIR__. "/../base/bootstrap.php";

global $env, $logger;

$argv = $env->load_command_line_args(true);
$map = Array_Utils::find_command_line_arg($argv, "map");
$svgs = Latitude_Longitude_Svg::get_instances();
//$svgs = [Latitude_Longitude_Svg::get_instances()["USA_Wyoming_location_map"]];
if ($map) {
	$maps = preg_split("/\s*,\s*/", $map);
    $svgs = array_filter($svgs, fn($svg) => in_array($svg->name, $maps));
}

$logger->info("Setting up " . count($svgs) . " maps");

$png_creator = (new Png_Creator_Factory())->get_png_creator();
foreach ($svgs as $svg) {

    $logger->info("Converting $svg->name...");

    $png_blob = $png_creator->get_thumb($svg->text, 250);

    Local_Io::file_put_contents_ensure(
        BASE_DIRECTORY . "/public_html/images/$svg->name-thumb.png", $png_blob);

}
