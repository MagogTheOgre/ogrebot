"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const CovidUtils_1 = __importDefault(require("../CovidUtils"));
const collectionUtils_1 = require("../../collectionUtils");
const CountyResults_1 = require("../Results/CountyResults");
class AbstractMapBuilder {
    async getColors(date) {
        const colorBuilder = CovidUtils_1.default.getColor(this.colorLevels);
        const results = await this.getResults(CountyResults_1.getCountyResultsBuilder(date));
        return [collectionUtils_1.mapValues(results, colorBuilder), this.defaultColor];
    }
    get defaultColor() {
        return this.colorLevels[0][1];
    }
}
exports.default = AbstractMapBuilder;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWJzdHJhY3RNYXBCdWlsZGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiQWJzdHJhY3RNYXBCdWlsZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0EsK0RBQXVDO0FBQ3ZDLDJEQUFrRDtBQUNsRCw0REFBNEc7QUFHNUcsTUFBOEIsa0JBQWtCO0lBTXJDLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBVTtRQUM3QixNQUFNLFlBQVksR0FBRyxvQkFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDM0QsTUFBTSxPQUFPLEdBQUcsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLHVDQUF1QixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDckUsT0FBTyxDQUFDLDJCQUFTLENBQUMsT0FBTyxFQUFFLFlBQVksQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQVUsQ0FBQztJQUMxRSxDQUFDO0lBRUQsSUFBYyxZQUFZO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNsQyxDQUFDO0NBRUo7QUFoQkQscUNBZ0JDIn0=