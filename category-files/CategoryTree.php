<?php
class CategoryTree {
	
	/**
	 *
	 * @var string[][]
	 */
	private array $affiliations;

    public function __construct() {
        $this->affiliations = Local_Io::deserialize(CATEGORY_TREE_CACHE_FILE, [], function (string $str) {
            [$affiliations, $time] = unserialize($str);
            return time() - $time < SECONDS_PER_HOUR * 6 ?
                array_filter($affiliations, fn($page) => !String_Utils::str_starts_with($page, "File:"), ARRAY_FILTER_USE_KEY) :
                [];
        });
    }
    
    public function flush() {
		file_put_contents(CATEGORY_TREE_CACHE_FILE, serialize([$this->affiliations, time()]));
    }
	
	/**
	 *
	 * @param string $page        	
	 * @param string[] $supercategories        	
	 * @return void
	 */
	public function addSuperCategories($page, $supercategories) {		
		$this->affiliations[$page] = $supercategories;
	}
	
	/**
	 * Queries whether a page has had its categories added (not
	 * whether the category is a subcategory of another category
	 * 
	 * @param string $page        	
	 * @return bool
	 */
	public function isPageStored($page) {
		return isset($this->affiliations[$page]);
	}
	
	/**
	 *
	 * @param Category_Files_Category $category        	
	 * @param string[] $ignore_cats
	 * @param string $gallery_name
	 * @param CategoryTreeLogger $logback
	 * @param Category_Files_Category[] $omissions
	 * @return string[]
	 *@throws WikiDataException
	 */
	public function pagesInTree(Category_Files_Category $category, array $ignore_cats, string $gallery_name,
		CategoryTreeLogger $logback, array $omissions): array {

		$noop_logger =  new NoopCategoryTreeLogger();
		$omission_categories = Array_Utils::unique_key_flat_map($omissions,
			fn ($omission) => $this->doGetPagesInTree($omission, [], $gallery_name, $noop_logger));

		return array_keys($this->doGetPagesInTree($category, array_flip($ignore_cats) + $omission_categories,
			$gallery_name, $logback));
	}

	/**
	 *
	 * @param Category_Files_Category $category
	 * @param array<string, mixed> $ignore_cats
	 * @param string $gallery_name
	 * @param CategoryTreeLogger $logback
	 * @return array<string, string>
	 *@throws WikiDataException
	 */
	private function doGetPagesInTree(Category_Files_Category $category, array $ignore_cats, string $gallery_name,
		CategoryTreeLogger $logback): array {

		$logger = Environment::get()->get_logger();

		$hit_cats = array($category->category => [$category->category]);
		$recurse_level = 0;

		do {
			if ($recurse_level++ > $category->max_depth && $category->max_depth !== null) {
				$logger->debug("Stopping at depth $category->max_depth.");
				break;
			}

			if ($recurse_level > 200) {
				throw new WikiDataException("Recurse level exceed maximum.");
			}

			$pos_hit = false;
			foreach ($this->affiliations as $cat => $supercats) {
				if (!isset($hit_cats[$cat]) && !isset($ignore_cats[$cat])) {
					foreach ($supercats as $supercat) {
						if (isset($hit_cats[$supercat])) {
							$hit_cats[$cat] = [$cat, ...$hit_cats[$supercat]];
							$pos_hit = true;
						}
					}
				}
			}
		} while ($pos_hit);

		$logback->log($gallery_name, $hit_cats);

		return $hit_cats;
	}
}

?>
