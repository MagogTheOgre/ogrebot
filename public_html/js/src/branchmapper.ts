/// <reference path="../../../js/node_modules/@types/jquery/index.d.ts" />
/// <reference path="./modules/Ajax.ts" />
/// <reference path="./modules/saveAs.ts" />
/// <reference path="./modules/sortByKey.ts" />
/// <reference path="./modules/Spinner.ts" />
/// <reference path="./modules/TooltipsFactory.ts" />
/// <reference path="./types/JQuery.dateTimePicker.d.ts" />

interface SavedState {
    type: string
    inputs: Record<string, string>
}
const requestKey = <string>$("#request_key").val();

const panels = $(".panel");
const next = $("#next");
const previous = $("#previous");
const submit = $("#submit");
const typeCheckbox = $("[name='type']");
const startForm = <JQuery<HTMLFormElement>>$("#start-form");
const phaseInput = $("#phase");
const returnUserStorageName = "magog.branchmapper.returnuser";
const newUserInstructions = $(".newuser-instructions");
const saveState = $("#save-state");
const allMapLinksDiv = $(".all-map-links");
const allMapLinksToggle = $("#all-map-links-toggle");
const saveStateStorageName = "magog.branchmapper.savestate";
const bodyWrapper = $(".body-wrapper");
const { noop } = $;
var finalDownloadListener = noop as () => void | Promise<void>;

const spinnerFactory = new SpinnerFactory();
const tooltipsFactory = new TooltipsFactory();

function getPhase() {
    return +phaseInput.val();
}

function setPhase(i: number) {
    phaseInput.val(i);
}

function updatePhase(delta: number) {
    const phase = getPhase() + delta;
    const phaseLast = phase === panels.length - 1;
    const checkedVal = typeCheckbox.filter(":checked").val() as string | undefined;
    const element = checkedVal && $(`#branch-${checkedVal}`);

    setPhase(phase);
    panels.hide().eq(phase).show();
    previous.add(saveState).toggle(phase !== 0);
    next.toggle(!phaseLast);
    submit.toggle(phaseLast);

    if (element) {
        $(".branch-div").hide();
        element.show().closest(".panel");
    }
}

function showStartupHelpDialog() {
    newUserInstructions.dialog({
        modal: true,
        width: 800,
        buttons: {
            OK() {
                $(this).dialog("close");
            }
        },
        close() {
            localStorage.setItem(returnUserStorageName, "" + Date.now());
        }
    });
}

function getSavedStateFromText(text) {
    var states = (text ? JSON.parse(text) : {}) as Record<string, SavedState>;

    if (!$.isPlainObject(states)) {
        throw "Deserialization failed!";
    }

    return sortByKey(states);
}

function getSavedState() {
    return getSavedStateFromText(localStorage.getItem(saveStateStorageName));
}

function setSavedState(state: Record<string, SavedState>) {
    localStorage.setItem(saveStateStorageName, JSON.stringify(state));
}

startForm.on("submit", () => {
    //hitting enter key on another phase
    if (!submit.is(":visible")) {
        return false;
    }

    spinnerFactory.create(bodyWrapper);
    $(":submit").attr("disabled", "disabled");
});

updatePhase(0);

previous.on("click", () => updatePhase(-1));

next
    .on("click", () => updatePhase(1))
    .attr("disabled", typeCheckbox.filter(":checked")[0] ? null : "disabled");

typeCheckbox.one("change", $.fn.attr.bind(next, "disabled", null));

$(".download-button").on("click", function({target}) {
    const button = $(target) as JQuery<HTMLElement>; 
    const row = button.closest("[data-map-name]");
    const map = row.data("mapName");
    const svgOptions = $(".svg-options");

    finalDownloadListener = async () => {
        var thisSpinner = spinnerFactory.create(button
            .closest("tr")
            .children(".map,.selection,.resolution"));

        try {
            saveAs(await new Ajax({
                url: "download.php",
                method: "post",
                data: {
                    request_key: requestKey,
                    map: map,
                    color: <string>$("#option-color").val(),
                    radius: <string>$("#option-radius").val()
                },
                responseType: "blob"
            }), map);
        } catch (e) {
            console.error(e);
            alert(`Unable to download ${row.data("mapHumanReadable")}. ` +
                "Please reload the page and try again.");
        } finally {
            spinnerFactory.remove(thisSpinner);
        }
    };

    svgOptions.dialog({
        close() {
            finalDownloadListener = noop;
        },
        modal: true,
        width: 600,
        height: 400
    });
    $("#recommended-dimensions").html(
        `${row.data("mapRecommendedWidth")}` + `&nbsp;×&nbsp;${row.data("mapRecommendedHeight")}`
    );
});
$(".resolution :checkbox").on("click", ({target}) => {
    var $target = $(target);
    $target.siblings(":text").toggle(!$target.is(":checked"));
});
$(".show-wikitext").on("click", async e => {
    //cancel anchor click
    e.preventDefault();
    await $(e.target)
        .closest(".wikitext")
        .find(".wikitext-toggle")
        .each((_, element) => {
            var $element = $(element);
            $element.slideToggle($element.height());
        })
        .promise();
    spinnerFactory.redraw();
});
$("#verify-input").on("click", async () => {
    spinnerFactory.create(bodyWrapper);
    try {
        const { message, error } = (await new Ajax({
            url: "check.php",
            method: "post",
            data: startForm[0]
        })) || {};
        if (message) {
            $("#verify-text").val(message);
        } else {
            alert(error || "Connection problem or server error.");
        }
    } catch (e) {
        alert("Connection problem");
    } finally {
        spinnerFactory.remove(bodyWrapper);
    }
});
$("#final-download").on("click", () => void finalDownloadListener()); //has this user been asked within the last 30 days?
if ((localStorage.getItem(returnUserStorageName) || 0) < Date.now() - 1000 * 60 * 60 * 24 * 30) {
    showStartupHelpDialog();
}
$("#click-help").on("click", ({target}) => {
    showStartupHelpDialog(); //cancel sticky popup
    setTimeout(() => tooltipsFactory.close($(target)), 1);
});
$("#load-state").on("click", () => {
    var div = $("<div/>");
    var table = $("<table class='load-session'/>").clone().show();
    const state = getSavedState();
    $.each(state, (name, {type, inputs}) => {
        const loadButton = $("<input type='button' value='Load'  class='btn btn-primary' />").on("click", () => {
            typeCheckbox.filter(`[value='${type}']`).prop("checked", true); //TODO use Object.entries
            $.each(inputs, (key, value) => void $(`textarea[name='${key}']`).val(value));
            setPhase(1);
            updatePhase(0);
            div.dialog("close");
        });
        const removeButton = $("<input type='button' value='Remove' class='btn btn-warning' />").on("click", () => {
            removeButton.closest("tr").remove();
            delete state[name];
            setSavedState(state);
        });
        table.append($("<tr/>")
                .append($("<td/>").append(loadButton))
                .append($("<td/>").append(removeButton))
                .append($("<td/>").text(name))
        );
    });
    div.append(table).dialog({
        modal: true,
        width: 800,
        title: "Select a session",
        buttons: {
            Cancel() {
                $(this).dialog("close");
            }
        }
    });
});
saveState.on("click", () => {
    const prefix = "Saved ";
    var type = typeCheckbox.filter(":checked").val() as string;
    var inputs = $(`#branch-${type} textarea`);
    var state = getSavedState();
    var defaultName = 0;
    /**
     * get name
     */
    while (state[prefix + ++defaultName]) {}
    do {
        var name = prompt("Name of the saved state: ", `${prefix}${defaultName}`); //cancelled
        if (name === null) {
            return;
        }
        if (state[name]) {
            if (confirm("The name already exists. Overwrite?")) {
                delete state[name];
            }
        }
    } while (state[name]);
    const saveStateObject = { type, inputs: {} } as SavedState;
    inputs.each((_, input) => {
        var $input = $(input);
        saveStateObject.inputs[$input.attr("name") as string] = $input.val() as string;
    });
    state[name] = saveStateObject;
    setSavedState(state);
});

document.querySelector("#import")?.addEventListener("change", ({target: importElement}) => {
    const fileReader = new FileReader();
    fileReader.addEventListener("load", ({target: {result}}) => {
        try {
            var importCount = 0;
            const state = getSavedState();
            for (const [key, newState] of Object.entries(getSavedStateFromText(result))) {
                if (!state[key] || confirm(`Overwrite ${key}?`)) {
                    importCount++;
                    state[key] = newState;
                }
            }
            setSavedState(state);
            alert(`${importCount} entries loaded.`);
            (importElement as HTMLInputElement).value = "";
        } catch (e) {
            alert("Er, that didn't seem to work.");
        }
    });
    fileReader.readAsText((importElement as HTMLInputElement).files[0]);
});
document.querySelector("#export")?.addEventListener("click", () => {
    const blob = new Blob([localStorage.getItem("magog.branchmapper.savestate")], {
        type: "text/plain;charset=utf-8"
    });
    const objectUrl = URL.createObjectURL(blob);
    
    saveAs(
        new Blob([localStorage.getItem(saveStateStorageName)], {
            type: "text/plain;charset=utf-8"
        }),
        "branchmapper-data.txt"
    );
});
allMapLinksToggle.on("click", async e => {
    //cancel click
    e.preventDefault();
    await allMapLinksDiv.slideToggle(50).promise();
    allMapLinksToggle.text(allMapLinksDiv.is(":hidden") ? "+" : "-");
});
