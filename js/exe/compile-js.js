"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importStar(require("fs"));
const io_1 = __importDefault(require("../lib/io"));
const google_closure_compiler_1 = __importDefault(require("google-closure-compiler"));
const promiseUtils_1 = require("../lib/promiseUtils");
const ThreadPool_1 = require("../lib/ThreadPool");
const util_1 = require("util");
const { compiler: ClosureCompiler } = google_closure_compiler_1.default;
const { PROJECT_DIR } = io_1.default;
const JS_DIR = `${PROJECT_DIR}/public_html/js`;
const SRC_DIR = `${JS_DIR}/src`;
const DEST_DIR = `${JS_DIR}/bin`;
const MODULES_DIR = `${SRC_DIR}/modules`;
const FILES_TO_MINIFY = process.argv.slice(2);
(0, promiseUtils_1.startup)();
(async function () {
    const tsFiles = FILES_TO_MINIFY.length > 0 ?
        FILES_TO_MINIFY.map(file => `${file}.ts`) :
        fs_1.default.readdirSync(SRC_DIR).filter(file => file.endsWith(".ts"));
    const modules = fs_1.default.readdirSync(MODULES_DIR).flatMap(file => file.endsWith(".ts") ? [`${MODULES_DIR}/${file}`] : []).join(" ");
    try {
        //multithreaded Closure makes Windows freeze
        const threadPool = new ThreadPool_1.ThreadPoolImpl();
        await threadPool.enqueueAll(function* () {
            for (const tsFile of tsFiles) {
                yield async () => {
                    console.log(`Transpiling ${tsFile}...`);
                    const { tempFile } = io_1.default;
                    const { stdout } = await io_1.default.runCommandLowPriority(`tsc ${tsFile} ${modules} -t ES2021 --inlineSourceMap --outfile ${tempFile}`, SRC_DIR);
                    if (stdout) {
                        console.log(tsFile, stdout);
                    }
                    const baseFile = tsFile.replace(/\.ts$/, "");
                    const mapFile = `${baseFile}.js.map`;
                    const minFileName = `${baseFile}.min.js`;
                    console.log(`Minifying to ${minFileName}...`);
                    const outputFile = `${DEST_DIR}/${minFileName}`;
                    await new Promise((resolve, reject) => {
                        new ClosureCompiler({
                            js: tempFile,
                            compilation_level: 'SIMPLE',
                            //warning_level: 'VERBOSE',
                            language_in: 'ECMASCRIPT_NEXT',
                            language_out: 'ECMASCRIPT_2021',
                            js_output_file: outputFile,
                            isolation_mode: "IIFE",
                            create_source_map: `${DEST_DIR}/${mapFile}`,
                            source_map_location_mapping: `${JS_DIR}|..`
                        }).run((code, stdOutData, stdErrData) => {
                            if (stdOutData) {
                                console.log(tsFile, stdOutData);
                            }
                            if (stdErrData) {
                                console.error(tsFile, stdErrData);
                            }
                            if (code) {
                                return reject();
                            }
                            (0, util_1.promisify)(fs_1.appendFile)(outputFile, `${"\n"}//# sourceMappingURL=${mapFile}`).then(resolve, reject);
                        });
                    });
                    console.log(`Done ${tsFile}...`);
                };
            }
        });
        console.log("All files successfully compiled");
    }
    catch (e) {
        console.error(JSON.stringify(e, null, 4));
        console.error("All files NOT successfully compiled");
    }
    finally {
        (0, promiseUtils_1.shutdown)();
    }
}());
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcGlsZS1qcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNvbXBpbGUtanMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQTRDO0FBQzVDLG1EQUEyQjtBQUMzQixzRkFBK0M7QUFDL0Msc0RBQXdEO0FBQ3hELGtEQUFtRDtBQUNuRCwrQkFBaUM7QUFFakMsTUFBTSxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUUsR0FBRyxpQ0FBUSxDQUFDO0FBRS9DLE1BQU0sRUFBRSxXQUFXLEVBQUUsR0FBRyxZQUFFLENBQUM7QUFDM0IsTUFBTSxNQUFNLEdBQUcsR0FBRyxXQUFXLGlCQUFpQixDQUFDO0FBQy9DLE1BQU0sT0FBTyxHQUFHLEdBQUcsTUFBTSxNQUFNLENBQUM7QUFDaEMsTUFBTSxRQUFRLEdBQUcsR0FBRyxNQUFNLE1BQU0sQ0FBQztBQUNqQyxNQUFNLFdBQVcsR0FBRyxHQUFHLE9BQU8sVUFBVSxDQUFDO0FBQ3pDLE1BQU0sZUFBZSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBRTlDLElBQUEsc0JBQU8sR0FBRSxDQUFDO0FBQ1YsQ0FBQyxLQUFLO0lBQ0YsTUFBTSxPQUFPLEdBQUcsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4QyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDM0MsWUFBRSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFFakUsTUFBTSxPQUFPLEdBQUksWUFBRSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLFdBQVcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFFdEUsSUFBSTtRQUNBLDRDQUE0QztRQUM1QyxNQUFNLFVBQVUsR0FBRyxJQUFJLDJCQUFjLEVBQUUsQ0FBQztRQUN4QyxNQUFNLFVBQVUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ2pDLEtBQUssTUFBTSxNQUFNLElBQUksT0FBTyxFQUFFO2dCQUMxQixNQUFNLEtBQUssSUFBSSxFQUFFO29CQUViLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxNQUFNLEtBQUssQ0FBQyxDQUFDO29CQUN4QyxNQUFNLEVBQUUsUUFBUSxFQUFFLEdBQUcsWUFBRSxDQUFDO29CQUd4QixNQUFNLEVBQUUsTUFBTSxFQUFFLEdBQUcsTUFBTSxZQUFFLENBQUMscUJBQXFCLENBQzdDLE9BQU8sTUFBTSxJQUFJLE9BQU8sMENBQTBDLFFBQVEsRUFBRSxFQUM1RSxPQUFPLENBQUMsQ0FBQztvQkFFYixJQUFJLE1BQU0sRUFBRTt3QkFDUixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztxQkFDL0I7b0JBRUQsTUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQzdDLE1BQU0sT0FBTyxHQUFHLEdBQUcsUUFBUSxTQUFTLENBQUM7b0JBQ3JDLE1BQU0sV0FBVyxHQUFHLEdBQUcsUUFBUSxTQUFTLENBQUM7b0JBRXpDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLFdBQVcsS0FBSyxDQUFDLENBQUM7b0JBQzlDLE1BQU0sVUFBVSxHQUFHLEdBQUcsUUFBUSxJQUFJLFdBQVcsRUFBRSxDQUFDO29CQUNoRCxNQUFNLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO3dCQUNsQyxJQUFJLGVBQWUsQ0FBQzs0QkFDaEIsRUFBRSxFQUFFLFFBQVE7NEJBQ1osaUJBQWlCLEVBQUUsUUFBUTs0QkFDM0IsMkJBQTJCOzRCQUMzQixXQUFXLEVBQUUsaUJBQWlCOzRCQUM5QixZQUFZLEVBQUUsaUJBQWlCOzRCQUMvQixjQUFjLEVBQUUsVUFBVTs0QkFDMUIsY0FBYyxFQUFFLE1BQU07NEJBQ3RCLGlCQUFpQixFQUFFLEdBQUcsUUFBUSxJQUFJLE9BQU8sRUFBRTs0QkFDM0MsMkJBQTJCLEVBQUUsR0FBRyxNQUFNLEtBQUs7eUJBQzlDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxFQUFFOzRCQUNwQyxJQUFJLFVBQVUsRUFBRTtnQ0FDWixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQzs2QkFDbkM7NEJBQ0QsSUFBSSxVQUFVLEVBQUU7Z0NBQ1osT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUE7NkJBQ3BDOzRCQUNELElBQUksSUFBSSxFQUFFO2dDQUNOLE9BQU8sTUFBTSxFQUFFLENBQUM7NkJBQ25COzRCQUNELElBQUEsZ0JBQVMsRUFBQyxlQUFVLENBQUMsQ0FBQyxVQUFVLEVBQUUsR0FBRyxJQUFJLHdCQUF3QixPQUFPLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBQ3RHLENBQUMsQ0FBQyxDQUFDO29CQUNQLENBQUMsQ0FBQyxDQUFDO29CQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxNQUFNLEtBQUssQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUM7YUFDTDtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO0tBQ2xEO0lBQUMsT0FBTyxDQUFDLEVBQUU7UUFDUixPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzFDLE9BQU8sQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQztLQUN4RDtZQUFTO1FBQ04sSUFBQSx1QkFBUSxHQUFFLENBQUM7S0FDZDtBQUNMLENBQUMsRUFBRSxDQUFDLENBQUMifQ==