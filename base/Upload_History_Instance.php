<?php
class Upload_History_Instance {

	/**
	 *
	 * @var string
	 */
	public $comment;
	
	/**
	 * 
	 * @var string
	 */
	public $hash;
	
	/**
	 * 
	 * @var int|null
	 */
	public $height;

	/**
	 *
	 * @var number|null
	 */
	public $revert;

	/**
	 * 
	 * @var int
	 */
	public $size;
	
	/**
	 * Human-readable timestamp
	 *
	 * @var string
	 */
	public $timestamp;

	/**
	 *
	 * @var bool
	 */
	public $unchanged;

	public ?string $url;

    public ?string $google_search_url;

	/**
	 *
	 * @var string
	 */
	public $user;
	
	/**
	 * 
	 * @var int|null
	 */
	public $width;
	
	/**
	 *
	 * @param string[][] $image_info
	 * @return  Upload_History_Instance[]
	 */
	public static function read_from_wiki_image_info($image_infos) {
		$instances = array();
	
		$previous_sha = "Invalid SHA string";
		$image_infos = array_reverse($image_infos, false);

        $mime = $image_infos ? end($image_infos)["mime"] ?? null : null;
		foreach ($image_infos as $i => $image_info) {
			$instance = new Upload_History_Instance();
				
			$instance->comment = $image_info["comment"] ?? null;
			$instance->user = $image_info["user"] ?? null;
			$instance->timestamp = $image_info["timestamp"] ?? null;
			$instance->url = $image_info["url"] ?? null;
			$instance->size = $image_info["size"] ?? null;
			$instance->height = $image_info["height"] ?? null;
			$instance->width = $image_info["width"] ?? null;
			$instance->hash = $image_info["sha1"] ?? null;
            $instance->google_search_url = Environment::get()->get_wiki_interface()->get_google_search_url(
                $image_info, $mime);
				
			$instance->revert = false;
			if ($i > 0 && $instance->hash !== null && ($image_infos[$i - 1]['sha1'] ?? null) === $instance->hash) {
				$instance->unchanged = true;
			} else {
				$instance->unchanged = false;
				for ($j = 0; $j < $i; $j++) {
					if ($instance->hash !== null && ($image_infos[$j]['sha1'] ?? null) === $instance->hash) {
						$instance->revert = $j;
						break;
					}
				}
			}
				
			$instances[] = $instance;
		}
		return $instances;
	}
}