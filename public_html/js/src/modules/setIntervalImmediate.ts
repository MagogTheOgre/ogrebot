function setIntervalImmediate(callback: () => any, intervalLength = 0) {
    function test() {
        if (callback() === false) {
            clearInterval(clear);
        }
    }
    const clear = setInterval(test, intervalLength);
    test();
}