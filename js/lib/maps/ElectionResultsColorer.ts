import { ElectionResult } from "./ElectionResult";

const allColorsUsed = new Set<string>();

export type ElectionResultColorer = (party: ElectionResult) => string;

const electionColors = {
    d: {
        0: "#d3e7ff",
        1: "#d3e7ff",
        2: "#d3e7ff",
        3: "#d3e7ff",
        4: "#b9d7ff",
        5: "#86b6f2",
        6: "#4389e3",
        7: "#1666cb",
        8: "#0645b4",
        9: "#0645b4",
        10: "#0645b4"
    } as const,
    r: {
        0: "#ffccd0",
        1: "#ffccd0",
        2: "#ffccd0",
        3: "#ffccd0",
        4: "#f2b3be",
        5: "#e27f90",
        6: "#cc2f4a",
        7: "#d40000",
        8: "#aa0000",
        9: "#800000",
        10: "#800000"
    } as const,
    t: {
        0: "#888",
        1: "#888",
        2: "#888",
        3: "#888",
        4: "#888",
        5: "#888",
        6: "#888",
        7: "#888",
        8: "#888",
        9: "#888",
        10: "#888"
    } as const,
    i: {
        0: "",
        1: "",
        2: "",
        3: "",
        4: "",
        5: "",
        6: "",
        7: "",
        8: "",
        9: "",
        10: ""
    } as const
} as const;

export const standardElectionResultColorer = ({percent, party}: ElectionResult) => {
    const roundedPercent = <0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10>Math.floor(percent * 10);
    const color = electionColors[party][roundedPercent];
    
    if (!allColorsUsed.has(`${party}${roundedPercent}`)) {
        console.log(`${party}${roundedPercent}0: ${color}`);
        allColorsUsed.add(`${party}${roundedPercent}`);
    }
    return color;
}

export const diffElectionResultColorer = ({percent, party, complete}: ElectionResult) => {
    if (complete) {
        if (party === "d") {
            if (percent >= .3) {
                return "#00a1c8";
            }
            if (percent >= .25) {
                return "#00bdec";
            }
            if (percent >= .2) {
                return "#09ceff";
            }
            if (percent >= .15) {
                return "#4bdbff";
            }
            if (percent >= .1) {
                return "#77e3ff";
            }
            if (percent >= .05) {
                return "#AAEEFF";
            }
            if (percent > .02) {
                return "#D5F6FF";
            }
        } else {
            if (percent >= .3) {
                return "#D40000";
            }
            if (percent >= .25) {
                return "#FF0000";
            }
            if (percent >= .2) {
                return "#FF2A2A";
            }
            if (percent >= .15) {
                return "#FF5555";
            }
            if (percent >= .1) {
                return "#FF8080";
            }
            if (percent >= .05) {
                return "#FFAAAA";
            }
            if (percent > .02) {
                return "#FFD5D5";
            }
        }
        return "#eee";
    }
    return "#bbbbbb";
}