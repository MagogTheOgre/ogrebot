export default interface CovidMap<T extends string | string[]> {
    create() : Promise<T>
}
