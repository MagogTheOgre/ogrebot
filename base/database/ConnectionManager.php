<?php


class ConnectionManager {

    /**
     * @param string $environment
     * @return mysqli
     * @throws IllegalArgumentException
     * @throws SQLException
     */
    static function getConnection(string $environment): mysqli {
        $logger = Environment::get()->get_logger();

        list ($connection_string, $db_string) = Environment::prop("databases", $environment);

        list("user" => $user, "password" => $password) = parse_ini_file(BASE_DIRECTORY .
            "/replica.my.cnf");

        $connection = new mysqli($connection_string, $user, $password, $db_string);
        if ($connection -> connect_errno) {
            throw new SQLException("Failed to connect to MySQL: $connection->connect_error"
                . " $connection->connect_errno");
        }
        $logger->debug("Connection opened");
        return $connection;
    }

    /**
     * @param mysqli[]|mysqli_stmt[]|mysqli_result[]|Statement[]|null[] $closeables
     */
    static function close(...$closeables): void {
        foreach ($closeables as $closeable) {
            if ($closeable && $closeable->close() === false) {
                $logger = Environment::get()->get_logger();
                $logger->warn("Connection NOT successfully closed");
            }
        }
    }
}