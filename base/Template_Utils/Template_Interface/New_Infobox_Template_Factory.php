<?php
class New_Infobox_Template_Factory extends Default_Template_Factory {

    /**
     * @var int[]
     */
    private $names;

    private const INFOBOX_TEMPLATE_CATEGORIES = [
        "Book templates",
        "Data ingestion layout templates",
        "Glaspalast (München) templates",
        "Infobox templates",
        "Infobox templates: based on Artwork template",
        "Infobox templates: based on Book template",
        "Infobox templates: based on Information template",
        "Infobox templates: based on Photograph template",
        "Infobox templates: based on Musical work template"
    ];

    /**
     * New_Infobox_Template_Factory constructor.
     * @throws BaseException
     */
	public function __construct() {
	    $environment = Environment::get();

        $database_dao = $environment->get_database_dao();
	    $logger = $environment->get_logger();

        $templates = $database_dao->get_pages_with_redirects_in_categories(
            "commons.wikimedia", self::INFOBOX_TEMPLATE_CATEGORIES, 5000);
        $names = Array_Utils::flat_map($templates, function(array $row) {
            list ($namespace, $page, $redirects) = $row;
            if ($namespace === TEMPLATE_NAMESPACE) {
                return array_merge(
                    Array_Utils::array_map_filter($redirects, function(array $row) {
                        list ($namespace, $page) = $row;
                        if ($namespace === TEMPLATE_NAMESPACE) {
                            return $page;
                        }
                    }), [$page]);
            }
        });
        sort($names);
        $this->names = array_fill_keys($names, 1);
        if ($logger->isDebugEnabled()) {

            $logger->debug("\$names = ". print_r($names, true));
        }
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see Template_Factory::extract()
	 */
	public function extract($text, $name, $offset=0) {
		$template = Template::extract($text, $name, $offset);
		return $template && isset($this->names[String_Utils::ucfirst_utf8(String_Utils::mb_trim(
		    $template->getname()))]) ? $template : false;
	}
}