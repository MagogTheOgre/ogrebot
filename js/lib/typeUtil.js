"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.castNumber = exports.castString = void 0;
function castString(t, def = "") {
    return typeof t === "string" ? t : def;
}
exports.castString = castString;
function castNumber(t, def = 0) {
    return typeof t === "string" || typeof t === "number" ? +t || 0 : def;
}
exports.castNumber = castNumber;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZVV0aWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0eXBlVXRpbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxTQUFnQixVQUFVLENBQUMsQ0FBTSxFQUFFLEdBQUcsR0FBRyxFQUFFO0lBQ3ZDLE9BQU8sT0FBTyxDQUFDLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztBQUMzQyxDQUFDO0FBRkQsZ0NBRUM7QUFDRCxTQUFnQixVQUFVLENBQUMsQ0FBTSxFQUFFLEdBQUcsR0FBRyxDQUFDO0lBQ3RDLE9BQU8sT0FBTyxDQUFDLEtBQUssUUFBUSxJQUFJLE9BQU8sQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFBLENBQUMsQ0FBQyxHQUFHLENBQUM7QUFDekUsQ0FBQztBQUZELGdDQUVDIn0=