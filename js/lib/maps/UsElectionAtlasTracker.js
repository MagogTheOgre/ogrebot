"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsElectionAtlasTracker = void 0;
const path_1 = __importDefault(require("path"));
const node_xlsx_1 = __importDefault(require("node-xlsx"));
const collectionUtils_1 = require("../collectionUtils");
const io_1 = __importDefault(require("../io"));
const Constants_1 = __importDefault(require("../covid19/Constants"));
const typeUtil_1 = require("../../lib/typeUtil");
const ElectionResult_1 = require("./ElectionResult");
class UsElectionAtlasTracker {
    dataFile;
    svgFile;
    constructor(dataFile, svgFile) {
        this.dataFile = dataFile;
        this.svgFile = svgFile;
    }
    isEligible(stateName) {
        return stateName === this.svgFile;
    }
    async getCountyTotals() {
        const xlsContent = await io_1.default.readFileFromZip(`${this.dataFile}.zip`, path_1.default.parse(this.dataFile).base);
        const { data } = collectionUtils_1.arrayFindOrDie(node_xlsx_1.default.parse(xlsContent), ({ name }) => name === "County");
        const countyCitySplit = +collectionUtils_1.arrayFindOrDie(Object.entries(data[0]), ([, name]) => name === "LSAD_TRANS")[0];
        const dIndex = +collectionUtils_1.arrayFindOrDie(Object.entries(data[0]), ([, candidate]) => candidate === "Clinton" || candidate === "Biden")[0];
        const rIndex = +collectionUtils_1.arrayFindOrDie(Object.entries(data[0]), ([, candidate]) => candidate === "Trump")[0];
        const stateCounties = {};
        for (const row of data) {
            const county = typeUtil_1.castString(row[0]);
            const state = typeUtil_1.castString(row[1]);
            const cityAppend = typeUtil_1.castString(row[countyCitySplit]) === "City" ? "_city" : "";
            if (Constants_1.default.STATE_ABBREVIATIONS_INV[state]) {
                (stateCounties[state] || (stateCounties[state] = {}))[county + cityAppend] =
                    new ElectionResult_1.ElectionResultImpl({
                        r: typeUtil_1.castNumber(row[rIndex]),
                        d: typeUtil_1.castNumber(row[dIndex])
                    });
            }
        }
        return stateCounties;
    }
}
exports.UsElectionAtlasTracker = UsElectionAtlasTracker;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXNFbGVjdGlvbkF0bGFzVHJhY2tlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlVzRWxlY3Rpb25BdGxhc1RyYWNrZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsZ0RBQXdCO0FBQ3hCLDBEQUE2QjtBQUM3Qix3REFBb0Q7QUFDcEQsK0NBQXVCO0FBRXZCLHFFQUFrRDtBQUNsRCxpREFBMEQ7QUFFMUQscURBQXNEO0FBRXRELE1BQWEsc0JBQXNCO0lBR0Y7SUFBbUM7SUFBaEUsWUFBNkIsUUFBZ0IsRUFBbUIsT0FBZTtRQUFsRCxhQUFRLEdBQVIsUUFBUSxDQUFRO1FBQW1CLFlBQU8sR0FBUCxPQUFPLENBQVE7SUFFL0UsQ0FBQztJQUVELFVBQVUsQ0FBQyxTQUFpQjtRQUN4QixPQUFPLFNBQVMsS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3RDLENBQUM7SUFDRCxLQUFLLENBQUMsZUFBZTtRQUNqQixNQUFNLFVBQVUsR0FBRyxNQUFNLFlBQUUsQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxNQUFNLEVBQUUsY0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEcsTUFBTSxFQUFDLElBQUksRUFBQyxHQUFHLGdDQUFjLENBQUMsbUJBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxFQUFDLElBQUksRUFBQyxFQUFFLEVBQUUsQ0FBQyxJQUFJLEtBQUssUUFBUSxDQUFDLENBQUM7UUFFckYsTUFBTSxlQUFlLEdBQUcsQ0FBQyxnQ0FBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLElBQUksS0FBSSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4RyxNQUFNLE1BQU0sR0FBRyxDQUFDLGdDQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxTQUFTLENBQUMsRUFBRSxFQUFFLENBQUMsU0FBUyxLQUFLLFNBQVMsSUFBSSxTQUFTLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEksTUFBTSxNQUFNLEdBQUcsQ0FBQyxnQ0FBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLEVBQUUsRUFBRSxDQUFDLFNBQVMsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVyRyxNQUFNLGFBQWEsR0FBaUUsRUFBRSxDQUFDO1FBRXZGLEtBQUssTUFBTSxHQUFHLElBQUksSUFBSSxFQUFFO1lBQ3BCLE1BQU0sTUFBTSxHQUFHLHFCQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEMsTUFBTSxLQUFLLEdBQUcscUJBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQyxNQUFNLFVBQVUsR0FBRyxxQkFBVSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDOUUsSUFBSSxtQkFBYyxDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUMvQyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7b0JBQ3RFLElBQUksbUNBQWtCLENBQUM7d0JBQ25CLENBQUMsRUFBRSxxQkFBVSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDMUIsQ0FBQyxFQUFFLHFCQUFVLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUM3QixDQUFDLENBQUM7YUFDVjtTQUNKO1FBQ0QsT0FBTyxhQUFhLENBQUM7SUFDekIsQ0FBQztDQUVKO0FBbkNELHdEQW1DQyJ9