/// <reference path="../../../../js/node_modules/@types/jquery/index.d.ts" />

/*******************
 * spinner
*******************/
class Spinner {
    private _div: JQuery<HTMLElement>;
    constructor(public readonly jq: JQuery<HTMLElement>, options: any) {
        const webRoot = $("#web-root").val() || "." as string;

        this._div = $("<div class='spinner-div'/>")
            .hide()
            .append("<div/>")
            .append($(`<img src='${webRoot}/images/ajax-loader.gif'/>`))
            .appendTo("body")
            .show(options);
        this.redraw();
    }

    redraw() {
        var coords = this.jq.get().map(elem => {
            const $elem = $(elem);
            const {top, left} = $elem.offset() || { top: 0, left: 0 };
            const bottom = top + $elem.outerHeight(true);
            const right = left + $elem.outerWidth();
            return {top, left, bottom, right};
        });
        const [top, left, bottom, right] = [["top"], ["left"], ["bottom", 1], ["right", 1]].map(
            ([index, max]) => Math[max ? "max" : "min"](...coords.map(coord => coord[index])));
        this._div.css({
            width: `${right - left}px`,
            height: `${bottom - top}px`,
            top: `${top}px`,
            left: `${left}px`
        });
    }

    async remove(options: any) {
        await this._div.hide(options).promise();
        this._div.remove();
    }
}

class SpinnerFactory {
    private store = [] as Spinner[];
    
    create(jq: JQuery<HTMLElement>, options?: any) {
        this.store.push(new Spinner(jq, options));
        return jq;
    }

    redraw(filterJq: string | JQuery<any> | HTMLElement | undefined = undefined) {
        this.get(filterJq).forEach(spinner => {
            spinner.redraw();
        });
    }

    remove(filterJq : string | JQuery<any> | HTMLElement | undefined = undefined, options: any = undefined) {
        this.get(filterJq).forEach(spinner => {
            this.store = this.store.filter(removeSpinner => spinner !== removeSpinner);
            spinner.remove(options);
        });
    }
    
    get(filterJq: string | JQuery<any> | HTMLElement | undefined = undefined ) {
        var all = this.store;
        if (filterJq) {
            all = all.filter(
                spinner => !spinner.jq.not(filterJq)[0] && !$(filterJq).not(spinner.jq)[0]);
        }
        return all;
    }
}