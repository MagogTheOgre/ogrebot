/// <reference path="modules/projectMultibox.ts" />
/// <reference path="modules/documentFunctions.ts" />

projectMultibox();

const fields = querySelectorAll<HTMLInputElement>("#authordate,#license,#fields");
const checkedSet = new Set<HTMLInputElement>();
const information = querySelector("#information") as HTMLInputElement;
information.addEventListener("click", () => {
    const { checked } = information;
    fields.forEach(checked ? field => {
        field.checked = checkedSet.has(field);
    } : field => {
        checkedSet[field.checked ? "add" : "delete" as const](field);
        field.checked = false;
    });
    fields.forEach(field => field.disabled = !checked);
})
