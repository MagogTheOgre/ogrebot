<?php

class RollingCategoryTreeLogger implements CategoryTreeLogger {

    private const MAX_ENTRIES = 5000;

	/**
	 *
	 * @var Wiki_Date
	 */
	private Wiki_Date $date;

	/**
	 *
	 * @var string[]
	 */
	private array $galleries;

    /**
     *
     * @var true[]
     */
    private array $has_written_gallery = [];

    public function __construct(Wiki_Date $date) {
        $this->date = $date;

        $gallery_page = ComputedConstants::CATEGORY_FILES_OUTPUT_PATH .
            $this->date->format("Ymd").".json";

        $this->galleries = Local_Io::json_decode($gallery_page, []);
    }

    /**
     * (non-PHPdoc)
     * @param string[][] $pages
     * @see CategoryTreeLogger::log()
     */
	public function log(string $gallery_name, array $pages) {
        global $logger;

        $logger->debug("RollingCategoryTreeLogger::log(string[". count($pages) . "])");

        /** @type string[][] $new_pages */
        $new_pages = [];
        foreach ($pages as $page) {
            if (String_Utils::str_starts_with($page[0] ?? "", "File:")) {
                $new_pages[] = $page;

                //Truncate log size. We definitely don't need more than MAX_ENTRIES. This solution gets around slow loading
                // & OOM exceptions (see https://commons.wikimedia.org/w/index.php?oldid=705301993#OgreBot_down)
                if (count($new_pages) > self::MAX_ENTRIES) {
                    break;
                }
            }
        }
        if (!$new_pages) {
            return;
        }

        $index = array_search($gallery_name, $this->galleries);
        $date = $this->date->format("Ymd");

        $serialized = String_Utils::json_encode(array_values($new_pages));

        if (isset($this->has_written_gallery[$gallery_name])) {
            //gallery with multiple categories
            if ($index === false) {
                //unreachable
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new IllegalStateException("has_written_gallery[$gallery_name] is true but not in galleries...");
            }

            //manually update the JSON. This is a hack but it gets around OOM exceptions which show up
            // while serializing and deserializing. See https://commons.wikimedia.org/w/index.php?oldid=705301993#OgreBot_down.
            /** @noinspection PhpUnhandledExceptionInspection */
            $handle = Local_io::fopen_ensure(ComputedConstants::CATEGORY_FILES_OUTPUT_PATH . "$date.$index.json",
                "r+");
            fseek($handle, -1, SEEK_END);
            fwrite($handle, ",");
            fwrite($handle, substr($serialized, 1));
        } else {
            if ($index === false) {
                $logger->debug("New log entry for $date => $gallery_name");
                $index = count($this->galleries);
                $this->galleries[]= $gallery_name;
            }
            /** @noinspection PhpUnhandledExceptionInspection */
            $handle = Local_io::fopen_ensure(ComputedConstants::CATEGORY_FILES_OUTPUT_PATH . "$date.$index.json",
                "w");
            fwrite($handle, $serialized);
        }
        fclose($handle);

        $this->has_written_gallery[$gallery_name] = true;

        //update after every write in case of program failure
        $galleries_serialized = String_Utils::json_encode($this->galleries);
        Local_Io::file_put_contents_ensure(ComputedConstants::CATEGORY_FILES_OUTPUT_PATH . "$date.json", $galleries_serialized);
	}
}