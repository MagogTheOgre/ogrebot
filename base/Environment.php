<?php

/**
 * 
 * @author magog
 *
 */
class Environment {
	
	private static Environment $instance;

	/**
	 * 
	 * @var string[]
	 */
	private array $argv;
	
	/**
	 *
	 * @var array[]
	 */
	private array $properties;
	
	/**
	 *
	 * @var string[]
	 */
	private array $error_types;
	
	private readonly Logger $logger;

	/**
	 *
	 * @var string[]
	 */
	private array $properties_hooks;

	/**
	 *
	 * @var callable[]
	 */
	private array $variables_hooks;

	private readonly Validation $validator;

	private readonly Wiki_Interface $wiki_interface;

	private readonly String_Utils $string_utils;

	private readonly Remote_Io $remote_io;

	private readonly Database_Dao $database_dao;

    private readonly Hook_Register $hook_register;

	public function get_logger(): Logger {
		return $this->logger;
	}

	public function get_validator(): Validation {
		return $this->validator;
	}

	public function get_wiki_interface(): Wiki_Interface {
		return $this->wiki_interface;
	}

	public function get_remote_io(): Remote_Io {
		return $this->remote_io;
	}

	public function get_string_utils(): String_Utils {
		return $this->string_utils;
	}
	
	public function get_cron_runner(): CronRunner {
        return match ($this->get_prop("environment", "environment")) {
            'local' => new LocalCronRunner(),
            'prod' => new ProdCronRunner(),
            default => throw new InvalidArgumentException("Can't determine environment! $this[environment]"),
        };
	}

	public function get_database_dao(): Database_Dao {
	    return $this->database_dao;
    }

    /**
     *
     * @throws IllegalStateException|CantOpenFileException
     */
	private function __construct() {
        $this->hook_register = new Hook_Register();
		$environment_name = trim(Local_Io::file_get_contents_ensure(BASE_DIRECTORY .
            "/properties/environment"));
		
		$this->properties_hooks = ["environment" => "environment.$environment_name",
				"constants" => "constants", "messages" => "messages"];
        $this->variables_hooks = [
            "validator" => fn() => new Validation(),
            "logger" => fn() => new Logger("logger-" . $this->get_prop("environment", "environment") .".xml"),
            "argv" => [$this, "parse_command_line_options"],
            "string_utils" => fn() => new String_Utils(),
            "remote_io" => fn() => new Remote_Io(),
            "database_dao" => fn() => new ($this->get_prop("environment", "database_dao"))(),
            "wiki_interface" => fn() => new Wiki_Interface()
        ];
		
		$this->hook_register->add("argv", fn() => $this->logger->trace("Environment: $environment_name"));
	}

    /**
     * @throws IllegalStateException|ParseException
     */
    public static function _autoload(): Void {
        self::$instance = new self();
        self::$instance->setup();
    }


    /**
     * @throws IllegalStateException|ParseException
     */
	private function setup(): void {
		$this->setup_hooks();

		$this->hook_register->trigger("start");
		$this->register_properties_file($this->properties_hooks);
		array_walk($this->properties_hooks, function ($filename, $variable) {
			if ($filename !== $variable) {
				$this->properties[$variable] = &$this->properties[$filename];
				unset($this->properties[$filename]);
			}
			$GLOBALS[$variable] = &$this->properties[$variable]; // backwards compatibility
			$this->hook_register->trigger($variable);
		});
		array_walk($this->variables_hooks, function (callable $callable, $variable) {
			$this->$variable = $callable();
			$GLOBALS[$variable] = &$this->$variable; // backwards compatibility
			$this->hook_register->trigger($variable);
		});
		$this->hook_register->trigger("end");
	}

	public static function get(): self {
		return self::$instance;
	}

	public function error_handler(int $errno, string $string, string $file = null, int $line = null): void {
		
		// ignore undefined errors suppressed with @ sign
		$error_reporting = error_reporting();
		if (!$error_reporting) {
			return;
		}
		
		$email = false;
		$levelMsg = $this->error_types[$errno];
        $level = null;
		switch ($errno) {
			case E_ERROR :
			case E_RECOVERABLE_ERROR :
				$email = true;
				break;
			case E_DEPRECATED :
			case E_USER_DEPRECATED :
				$level = Level::DEBUG;
				break;
			case E_WARNING :
			case E_NOTICE :
				$level = Level::WARN;
				break;
			default :
				$level = Level::ERROR;
		}
		
		$error_message = "$levelMsg: $string";
		if ($file) {
			$error_message .= " in $file";
			
			if ($line) {
				$error_message .= " on line $line";
			}
		}
		
		if ($email) {
			Remote_Io::ogrebotMail($error_message);
		} else {
			if (@$this->logger && $this->logger->isTraceEnabled()) {
				$error_message .= Array_Utils::get_backtrace_string(Array_Utils::get_backtrace(true, 2));
			}
            $level && Local_Io::logger_or_stderr($level, $error_message);
		}
	}
	
	private function shutdown_handler(): void {
		$error = error_get_last();
		
		if ($error) {
            $this->error_handler($error["type"], $error["message"], $error["file"] ?? null, $error["line"] ?? null);
		} else {
			Local_Io::logger_or_stderr(Level::TRACE, "Script completed normally.");
		}
	}

    /**
     * @throws IllegalStateException
     */
    private function setup_hooks(): void {
		//must execute these before everything else...
		$this->hook_register->add("start", function() {
			mb_internal_encoding("UTF-8");
			
			$this->error_types = array_flip(Array_Utils::prune_array_to_keys(get_defined_constants(), ["E_ERROR", "E_WARNING",
                "E_PARSE", "E_NOTICE", "E_CORE_ERROR", "E_CORE_WARNING", "E_COMPILE_ERROR",
                "E_COMPILE_WARNING", "E_USER_ERROR", "E_USER_WARNING", "E_USER_NOTICE", "E_STRICT",
                "E_RECOVERABLE_ERROR", "E_DEPRECATED", "E_USER_DEPRECATED"]));
		});
		
		$this->hook_register->add("environment", fn() =>
			$this->properties["environment"]['live'] = !!$this->properties["environment"]['live']);
		$this->hook_register->add("constants", fn() =>
			$this->properties["constants"]["illegal_pagename_re"] = 
				"/" . preg_quote($this->properties["constants"]["illegal_pagename_chars"], "/") . "/");
		
		$this->hook_register->add("end", function() {

			set_error_handler([$this, "error_handler"]);
			register_shutdown_function([$this, "shutdown_handler"]);

			ini_set('memory_limit', '1536M');
			
			$backtrack_limit = ini_get("pcre.backtrack_limit");
			if ($backtrack_limit < 100000000) {
				ini_set("pcre.backtrack_limit", "100000000");
				$this->logger->trace("Backtrack limit increased from $backtrack_limit to 10000000");
			} else {
				$this->logger->trace("Backtrack limit at $backtrack_limit (not increased)");
			}
			
			$this->wiki_interface->set_live_edits(!!$this->get_prop("environment", "live_edits"));

			$this->load_wikimedia_site_regexes();
			
			Peachy::set_password_manager(new OgreBot_Password_Manager());
		});
	}

    /**
     *
     * @param string[] $filenames
     * @throws ParseException
     */
    public function register_properties_file(array $filenames): void {
        array_walk($filenames, fn($filename) => $this->properties[$filename] = Local_Io::load_property_file($filename));
	}
		
	/**
	 * @param bool $force_post
	 * @return string[]
	 */
	public function get_request_args($force_post = false) {
		
		$args = $force_post ? $_POST : $_REQUEST;
		if ($this->logger->isDebugEnabled()) {
			if ($args) {
				if ($this->logger->isTraceEnabled()) {
					$this->logger->trace($args);
				}
			} else {
				$this->logger->debug("Empty request args.");
			}
		}
		return $args;
	}
	
	/**
	 *
	 * @param bool $shift Whether to shift the first value off the array. DEFAULT: false
	 * @return string[]
	 */
	public function load_command_line_args(bool $shift = false): array {
		return $shift && $this->argv ? array_slice($this->argv, 1) : $this->argv;
	}
	
	/**
	 * TODO move this to where it's needed
	 * @return void
	 */
	private function load_wikimedia_site_regexes() {		
		// load dynamic regex constants
		$regexes = String_Utils::preg_quote_all($this->get_prop("constants", 'wikimedia_sites'));
		$this->properties["constants"]['wikimedia_sites_regex'] = "/^https?:\/\/(.+\.)?(" . 
			implode('|', $regexes) . ")$/i";
	}

    /**
     *
     * @return string[]
     * @throws IllegalArgumentException
     * @throws IllegalStateException
     */
    private function parse_command_line_options(): array {
        global $argv;

        if (!isset($argv)) {
            return [];
        }

        $local_argv = $argv;

        // Check for command line args overriding logger level and output location
        $levelText = Array_Utils::find_command_line_arg($local_argv, "LEVEL");
        if ($levelText !== null) {
            try {
                $level = Level::parse($levelText);
                $this->logger->setDebugLevel($level);
            } catch (InvalidArgumentException $e) {
                $this->logger->error(
                    "Unrecognized level in command line argument:" . " $levelText. Ignoring.");
            }
        }

        $debug_std_out = Array_Utils::find_command_line_arg($local_argv, "DEBUGSTDOUT") !== null;
        if ($debug_std_out) {
            try {
                $this->logger->addOutputStreams("STDOUT");
            } catch (Exception $e) {
                $this->logger->error($e);
            }
        }

        $debug_std_err = Array_Utils::find_command_line_arg($local_argv, "DEBUGSTDERR") !== null;
        if ($debug_std_err) {
            try {
                $this->logger->addOutputStreams("STDERR");
            } catch (Exception $e) {
                $this->logger->error($e);
            }
        }

        $no_live = Array_Utils::find_command_line_arg($local_argv, "NOLIVE") !== null |
            Array_Utils::find_command_line_arg($local_argv, "NO-LIVE") !== null;

        if ($no_live) {
            $this->logger->info("Live edits deactivated (NOLIVE)");
            $this->hook_register->add("end", fn ()  => $this->wiki_interface->set_live_edits(false));
        }

        $cache = Array_Utils::find_command_line_arg($local_argv, "CACHE");
        if ($cache) {
            if (preg_match("/^\s*(\d+)\s*(seconds|minutes|hours|days|months|years)\s*$/i", $cache,
                $matches)) {

                $multiplier = 1;
                switch (strtolower($matches[2])) {
                    case 'years' :
                        $multiplier *= 12;
                        //fallthrough
                    case 'months' :
                        $multiplier *= 30;
                        //fallthrough
                    case 'days' :
                        $multiplier *= 24;
                        //fallthrough
                    case 'hours' :
                        $multiplier *= 60;
                        //fallthrough
                    case 'minutes' :
                        $multiplier *= 60;
                }
                try {
                    Http_Cache_Reader::set_default_cache_time($matches[1] * $multiplier);
                } catch (IllegalArgumentException $e) {
                    $this->logger->error(
                        "Illegal time. Must be less than " . DEFAULT_URL_CACHE_TIME . " seconds");
                }
            } else {
                throw new IllegalArgumentException(
                    "Unrecognized cache time. Proper format: " .
                    "([int time])(seconds|minutes|hours|days|months|years)");
            }
        }

        $mem = Array_Utils::find_command_line_arg($local_argv, "MEM");
        if ($mem) {
            if (preg_match("/^\d+(?:\.\d*)?[GMK]?$/i", $mem)) {
                $this->logger->info("Overriding memory limit to $mem");
                ini_set('memory_limit', $mem);
            } else {
                throw new IllegalArgumentException(
                    "Unrecognized memory limit. Proper format: " . "/^\d+(?:\.\d*)?(?i:G|M|K)?$/i");
            }
        }

        return $local_argv;
    }


    /**
     *
     * @param string|string[] $properties
     * @return mixed
     * @throws IllegalArgumentException|ParseException
     */
    public function get_prop(array|string $properties): mixed {
        if (!is_array($properties)) {
            $properties = func_get_args();
        }

        $next = &$this->properties;

        $first_properties_file = current($properties);
        $this->properties[$first_properties_file] ?? $this->register_properties_file([$first_properties_file]);
        foreach ($properties as $property) {
            $next = &$next[$property];
            if ($next === null) {
                throw new IllegalArgumentException("Constant not found: " . print_r($properties, true));
            }
        }
        return $next;
    }

    /**
     * retrieve multiple properties
     * @param string $base
     * @param string[] $all_properties
     * @return array
     * @throws IllegalArgumentException
     * @throws ParseException
     */
	public static function props(string $base, array $all_properties): array {
		return array_map(
			function ($properties) use ($base) {
				if (is_array($properties)) {
					array_unshift($properties, $base);
				} else {
					$properties = [$base, $properties];
				}
				return self::$instance->get_prop($properties);
			}, $all_properties);
	}
	
	/**
	 * Shorthand for get_prop() 
	 * @param string|string[] $properties
	 * @return mixed
	 * @throws IllegalArgumentException|ParseException
     */
	public static function prop(array|string $properties) {
		return self::$instance->get_prop(is_array($properties) ? $properties : func_get_args());
	}
}