<?php
require_once __DIR__ . "/../base/bootstrap.php";
global $logger;

$cron_runner = Environment::get()->get_cron_runner();
$interval = +Environment::prop("environment", "poll.interval");

$logger->info("Polling at $interval seconds.");
while(true) {
    foreach (file_exists(UPDATE_POLL_DIRECTORY) ? Local_Io::
        get_all_files_in_directory(UPDATE_POLL_DIRECTORY, false) : [] as $file) {
        @list(, $type, $start) = Regex_Util::match("/^(notable|new)\-(\d{14})$/", $file, 0);
        if ($type) {
            $cronConfig = new CronConfig();
            $cronConfig->setDirectory("exe");
            if ($type === "notable") {
                $cronConfig->setCommand("UploadReport");
                $cronConfig->setArgs("$start --update");
                $cronConfig->setTimeout(120 * SECONDS_PER_MINUTE);
            } else {
                $cronConfig->setCommand("newuploads");
                $cronConfig->setArgs("--start=$start --new=0");
                $cronConfig->setTimeout(30 * SECONDS_PER_MINUTE);
            }
            //this script takes quite a long time to run
            $cron_runner->setRunConfig($cronConfig);
            $cron_runner->run();
        } else {
            $logger->warn("Unreadable file format: $file.");
        }
        unlink(UPDATE_POLL_DIRECTORY . $file);
    }
    
    sleep($interval);
}
?>
