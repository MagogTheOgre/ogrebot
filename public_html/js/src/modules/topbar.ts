
type TopBarOptions = {
    css?: Record<string, any>,
    message: string,
    delay?: number
};

function topBar(options: string | TopBarOptions) {
    function setHeight(height, div) {
        $(div).css("top", `${height * 26}px`);
    }

    const {
        message, 
        css = {}, 
        delay = 5000
    } = typeof options !== "object" ? {message: options} : options;

    var existing = $(".topbar:last");
    var div = $("<div />", { class: "topbar", text: message }).css(css);

    setTimeout(() => {
        div.remove();

        $(".topbar").each(setHeight);
    }, delay);

    if (existing[0]) {
        existing.after(div);
    } else {
        $("body").prepend(div);
    }

    setHeight($(".topbar").length - 1, div);
}