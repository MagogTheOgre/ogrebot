import Io from '../lib/io';
import { startup, shutdown } from '../lib/promiseUtils';
import MultiCovidMapWrapper from '../lib/covid19/CovidMap/MultiCovidMapWrapper';
import PngCovidMap from '../lib/covid19/CovidMap/PngCovidMap';
import AbstractMapBuilder from '../lib/covid19/MapBuilder/AbstractMapBuilder';
import CovidUtils from '../lib/covid19/CovidUtils';
import { toArray } from '../lib/collectionUtils';
import minimist from "minimist";

//keep thread alive until done
startup();

const MIN_START_DATE = toUtcDate("2020-03-23");
const MAX_END_DATE = new Date();
MAX_END_DATE.setUTCHours(0);
MAX_END_DATE.setUTCMinutes(0);
MAX_END_DATE.setUTCSeconds(0);
MAX_END_DATE.setUTCMilliseconds(0);

function toUtcDate(string: string) {
    const [, year, month, day] = string.match(/^(\d{4})\-(\d{1,2})\-(\d{1,2})$/) || [];

    if (!year) {
        throw new Error(`Illegal date argument: ${string}`);
    }

    return new Date(Date.UTC(+year, +month - 1, +day));
}

function getArgs() {
    const { days, multiplier, type, outputDir, perCapita, start, end} = minimist(process.argv, {
        boolean: "perCapita",
        default: {
            days: 7,
            multiplier: .01,
            outputDir: "../Desktop/maps/$days",
            perCapita: true,
            type: "confirmed",
            start: "",
            end: ""
        }
    });

    if (typeof days !== "number") {
        throw new Error(`Illegal days argument: ${days}`);
    }
    if (typeof multiplier !== "number") {
        throw new Error(`Illegal multiplier argument: ${multiplier}`);
    }
    if (typeof outputDir !== "string") {
        throw new Error(`Illegal outputDir argument: ${outputDir}`);
    }
    if (typeof perCapita !== "boolean") {
        throw new Error(`Illegal perCapita argument: ${perCapita}`);
    }
    if (typeof type !== "string" || type !== "confirmed" && type !== "deaths") {
        throw new Error(`Illegal type argument: ${type}`);
    }
    if (typeof start !== "string") {
        throw new Error(`Illegal start argument: ${start}`);
    }
    if (typeof end !== "string") {
        throw new Error(`Illegal end argument: ${end}`);
    }

    const startDate = start ? toUtcDate(start) : MIN_START_DATE;

    const endDate = end ? toUtcDate(end) : MAX_END_DATE;

    const response = { 
        days, 
        multiplier,
        outputDir: Io.path(outputDir.replace(/\$days/g, `${days}`)), 
        perCapita,
        type,
        startDate,
        endDate
    } as const;
    console.log(JSON.stringify(response));
    return response;
}
(async () => {
    try {
        const {days, multiplier, outputDir, perCapita, type, startDate, endDate} = getArgs();
        if (!Io.existsSync(outputDir)) {
            Io.mkdirSync(outputDir);
        }
        const requestedDates = toArray(function*() {
            for (const date = new Date(startDate); date <= endDate; date.setDate(date.getDate() + days)) {
                yield new Date(date);
            }
        });

        const mapBuilder = new class extends AbstractMapBuilder {
            protected readonly colorLevels: AbstractMapBuilder["colorLevels"] = ([
                [0, "#fcbba1"],
                //[.0125, "#fcbba1"],
                [.01, "#fc9272"],
                [.0316, "#fb6a4a"],
                [.1, "#de2d26"],
                [.316, "#a50f15"],
                [1, "#000"]
            ] as const).map(([lowerLimit, color]) => [lowerLimit * multiplier, color]);
            protected getResults(builder: Parameters<AbstractMapBuilder["getResults"]>[0]) {
                const countyResultsBuilder = builder(type);                
                countyResultsBuilder.diff(days);
                if (perCapita) {
                    countyResultsBuilder.perCapita();
                }
                return countyResultsBuilder.build();
            }
        }();
        await new MultiCovidMapWrapper(outputDir, requestedDates, "png", mapBuilder, CovidUtils.toDmyString, PngCovidMap).create();
        console.log("done");
        // await new AnimatedPngCovidMap(`${outputDir}/covid19.apng`, startDate, endDate, mapBuilder, 1, 1).create();
    } finally {
        shutdown();
    }
})()

// console.log(counties.sort((a, b) => b.confirmedPerCapita - a.confirmedPerCapita).
//     map(({ county, relativeConfirmedPerCapita }) => `$county: ${relativeConfirmedPerCapita * 100}% relative confirmed per capita`).join("\n"));
// console.log(counties.sort((a, b) => b.deathsPerCapita - a.deathsPerCapita).
//     flatMap(({ county, relativeDeathsPerCapita }) => relativeDeathsPerCapita > 0 ? [`$county: ${relativeDeathsPerCapita * 100}% relative deaths per capita`] : []).join("\n"));


