<?php
/**
 * 
 * @author magog
 *
 */
class Remote_Call {
    const CLASS_NAME = "class";
    const FUNCTION = "function";
    const PARAMETERS = "params";
    const JSON = "json";
    
    /**
     * 
     * @var string
     */
    private $class;
    
    /**
     * 
     * @var string
     */
    private $endpoint;
    
    /**
     * 
     * @var int
     */
    private $timeout;
    
    function __construct(string $class = Local_Database_Dao::class) {
        global $validator;
        
        $this->class = $class;
        $this->endpoint = "remote.php";
        $this->timeout = 30000;
        
        //sanity check
        $validator->validate_args_condition($class, "remote enabled", 
            count($class::REMOTE_ENABLED ?? []) > 0);
    }
    
    function call(string $function, array $parameters, int $timeout = null) {
        $service_call = new Service_Call($this->endpoint, true);
        $service_call->add_post_params([
            self::JSON => String_Utils::json_encode([
                self::CLASS_NAME => $this->class,
                self::FUNCTION => $function,
                self::PARAMETERS => $parameters
            ])
        ]);
        
        $serialized = $service_call->call($timeout ?: $this->timeout);
        // Remote has decided to start prepending random whitespace
        $serialized = ltrim($serialized);
        $response = unserialize($serialized);
        if (!($response instanceof Webservice_Response)) {
            throw new RemoteException("Deserialization failed. String was: $serialized");
        }
        if ($response->exception !== null) {
            throw new RemoteException("Remote exception", $response->exception);
        }
        
        return $response->response;
    }
    
    static function run(): void {
        try {
            list($class, $function, $parameters) = self::read_request();
            
            $return = (new $class())->$function(...$parameters);
            
            echo serialize(new Webservice_Response($return));   
        } catch (Throwable $e) {
            Remote_Io::ogrebotMail($e);
            echo serialize(new Webservice_Response(null, $e));
        }
    }
    
    private static function read_request(): array {
        global $validator;
        
        list(self::JSON => $json) = Service_Call::read_service_call();
        
        list(
            self::CLASS_NAME => $class,
            self::FUNCTION => $function,
            self::PARAMETERS => $parameters
        ) = json_decode($json, true);
        
        $parameters = $parameters ?: [];
        $validator->validate_arg($class, "string");
        $validator->validate_arg($function, "string");
        $validator->validate_arg($parameters, "array");
        
        $valid_functions = @$class::REMOTE_ENABLED;
        
        $validator->validate_args_condition($class, "valid remote class",
            is_array($valid_functions));
        
        $validator->validate_args_condition($function, "valid remote funciton",
            in_array($function, $valid_functions));
        
        return [$class, $function, $parameters];
    }
}