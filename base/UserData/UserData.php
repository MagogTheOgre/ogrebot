<?php
class UserData {
    
    const MAX_CACHE_TIME = SECONDS_PER_DAY;
    
	const MARSHALL_FILE_NAME = "userrights2.dat";
	
	/**
	 *
	 * @var UserData[][]
	 */
	private static $userData = null;
	
	/**
	 * username without the prefix
	 * 
	 * @var string
	 */
	private $username;
	
	/**
	 *
	 * @var int
	 */
	private $editCount;
	
	/**
	 *
	 * @var string[]
	 */
	private $rights;
	
	/**
	 *
	 * @var number
	 */
	private $registration;
	
	/**
	 *
	 * @var int
	 */
	private $talkpageWarnings;
	
	/**
	 * when did we last refresh this data?
	 * 
	 * @var number
	 */
	private $timestamp;
	
	private static function get_newest_cache() {
	    global $logger;
	    
	    $newest_timestamp = 0;
	    $file_name = null;
	    $regexp = "/^" . preg_quote(self::MARSHALL_FILE_NAME, "/") ."\-(\d+)-\d+$/";
	    foreach (Local_Io::get_all_files_in_directory(TMP_DIRECTORY_SLASH) as $file) {
            $timestamp = Regex_Util::match($regexp, $file, 0)[1] ?? null;
	        
	        if ($timestamp) {
	            // prune old records
	            if (+$timestamp + self::MAX_CACHE_TIME < ComputedConstants::NOW) {
	                unlink(TMP_DIRECTORY_SLASH . $file);
	                $logger->debug("Deleting old file $file");
	            } else if (+$timestamp > $newest_timestamp){
	                $newest_timestamp = +$timestamp;
	                $file_name = $file;
	            }
	        }
	    }
	    return $file_name ? unserialize(file_get_contents(TMP_DIRECTORY_SLASH . $file_name)) : [];
	}

	private static function _autoload() {
	    
	    global $logger;	    
	    $logger->debug("_autoload");
	   
	    self::$userData = self::get_newest_cache();
	   
	    $logger->insane("unmarshalled user data:");
	    $logger->insane(self::$userData);
	    
        // prune old records
	    foreach (self::$userData as &$userData) {
	        $countStart = count(self::$userData);
            foreach ($userData as $index => $data) {
                // corrupt or stale data
                if (! ($data instanceof UserData) || $data->timestamp + self::MAX_CACHE_TIME 
                    < ComputedConstants::NOW) {
                    unset($userData[$index]);
                }
            }        $countEnd = count(self::$userData);
            
            $pruned = $countStart - $countEnd;
            $logger->debug( "$countStart users found; $pruned users pruned; $countEnd users remaining.");
        }
    }
	
	public function __construct() {
	}
	
	/**
	 *
	 * @return string
	 */
	public function getUsername() {
		return $this->username;
	}
	
	/**
	 *
	 * @return number
	 */
	public function getEditCount() {
		return $this->editCount;
	}
	
	/**
	 *
	 * @return string[]
	 */
	public function getRights() {
		return $this->rights;
	}
	
	/**
	 *
	 * @return int
	 */
	public function getTalkpageWarnings() {
		return $this->talkpageWarnings;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isAdmin() {
		return in_array("sysop", $this->rights);
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isAutoPatrolled() {
		return in_array("autopatrolled", $this->rights) || $this->isAdmin();
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isBot() {
		return in_array("bot", $this->rights);
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isConfirmed() {
		return in_array("autoconfirmed", $this->rights) || in_array("confirmed", $this->rights);
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isFilemover() {
		return in_array("filemover", $this->rights) || $this->isAdmin();
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isImageReviewer() {
		return in_array("Image-reviewer", $this->rights) || $this->isAdmin();
	}
	
	/**
	 *
	 * @param Wiki $wiki        	
	 * @param string[] $usernames        	
	 * @param int $timeout
	 *        	timeout in seconds
	 * @return UserData[]
	 */
	public static function getUserData(Wiki $wiki, array $usernames, $timeout) {
		global $logger, $validator, $wiki_interface;
		
		$validator->validate_arg($wiki, "Wiki");
		$validator->validate_arg($timeout, "int");
		$validator->validate_arg_array($usernames, "string-numeric");
		
		// initialize array; store by username
		$newDataQuery = [];
		$userData = self::$userData[$wiki->get_base_url()] ?? (self::$userData[$wiki->get_base_url()] = []);
		foreach ($usernames as $name) {
		    if (!isset($userData[$name]) || $userData[$name]->timestamp + $timeout < 
			    ComputedConstants::NOW) {
				unset($userData[$name]);
				$newDataQuery[] = $name;
			}
		}
		
		if ($newDataQuery) {
			/**
			 * Get userpage data *
			 */
			
			/**
			 * Get user rights, registration date, and set query timestamp *
			 */
			$rights = $wiki_interface->query_generic($wiki, 'ususers', $newDataQuery, 
				['usprop' => 'blockinfo|groups|registration|editcount']);
			
			foreach ($rights as $right) {
				$user = $right['name'];
				$userData[$user] = new UserData();
				$userData[$user]->username = $user;
				$userData[$user]->rights = $right['groups'];
				$userData[$user]->registration = $right['registration'];
				$userData[$user]->timestamp = ComputedConstants::NOW;
				$userData[$user]->editCount = intval($right['editcount']);
			}
			
			/**
			 * Get talk page templates for warning data *
			 */
			$usernames_with_talk_prefix = preg_replace("/^/", "User talk:", $newDataQuery);
			
			$redirects = []; //for PDT
			$talkpages = $wiki_interface->new_query_pages($wiki, $usernames_with_talk_prefix, 
				'templates|revisions', [], $redirects);
			
			$talkpageWarnings = UserTalkPageWarnings::getWarningTemplates();
			foreach ($usernames_with_talk_prefix as $pageName) {
				
				$userName = substr($pageName, 10);
				
				// follow redirects
				$redirect = $redirects[$pageName] ?? null;
				if ($redirect !== null) {
					$logger->trace("Redirect: $pageName -> $redirect.");
					
					if (isset($redirects[$redirect])) {
						$logger->warn("$pageName is a double redirect.");
						continue;
					}
					$pageName = $redirect;
				}
				
				try {
					$content = Array_Utils::_array_key_or_exception($talkpages, [$pageName]);
				} catch (ArrayIndexNotFoundException $e) {
					$logger->warn("Array key not found: $pageName.");
					continue;
				}
				
				// deleted/non-existent user talk page
				if (isset($content['missing'])) {
					$logger->warn("$pageName unexpectedly not found.");
					continue;
				}
				
				$logger->insane($userName);
				if (isset($userData[$userName])) {
					$logger->insane($content);
					$userTalkWarnings = 0;

					foreach ($content['templates'] ?? [] as $template) {
						$userTalkWarnings |= $talkpageWarnings[$template["title"]] ?? null;
					}
					$userData[$userName]->talkpageWarnings = $userTalkWarnings;
					$validator->validate_arg($userData[$userName]->talkpageWarnings, "int");
				} else {
					$logger->error("$userName not in userData???");
				}
			}
			self::$userData[$wiki->get_base_url()] = $userData;
			self::marshallUserData();
		}
		
		return array_intersect_key($userData, array_fill_keys($usernames, true));
	}
	
	/**
	 *
	 * @param int $timeout        	
	 * @return void
	 */
	private static function unmarshallUserData($timeout) {
		global $logger;
		$filename = TMP_DIRECTORY_SLASH . self::MARSHALL_FILE_NAME;
		
		$logger->debug("unmarshallUserData");
		
		$userData = &self::$userData;
		
		$file_contents = file_exists($filename) ? file_get_contents($filename) : false;
		if ($file_contents !== false) {
			self::$userData = unserialize($file_contents);
		} else {
			self::$userData = false;
		}
		$logger->insane("unmarshalled user data:");
		$logger->insane(self::$userData);
		
		if (self::$userData === false) {
			self::$userData = array();
			$logger->warn("User data not found; starting from scratch");
		} else {
			$countStart = count(self::$userData);
			
			// prune old records
			$time = time();
			foreach ($userData as $index => $data) {
				$unset = !($data instanceof UserData) /* corrupt data */
					|| $data->timestamp + $timeout < $time; /* stale data */
				
				if ($unset) {
					unset(self::$userData[$index]);
				}
			}
			$countEnd = count(self::$userData);
			
			$logger->debug(
				"$countStart users found. " . ($countStart - $countEnd) .
					 " users pruned; $countEnd users remaining.");
		}
	}
	
	/**
	 *
	 * @return void
	 */
	private static function marshallUserData() {
	    global $logger;
	    
	    $filename = sprintf("%s%s-%s-%s", TMP_DIRECTORY_SLASH, self::MARSHALL_FILE_NAME,
	        ComputedConstants::NOW, getmypid());
	    
	    try {
	        Local_Io::file_put_contents_ensure($filename, serialize(self::$userData), LOCK_EX);
	    } catch (FileIOException $e) {
	        $logger->error($e);
	    }
	}
}
