import concat  from 'concat-stream';
import yauzl from 'yauzl-promise';
import fs from "fs";
import { matchAll } from "./stringUtils";
import { EOL } from "os";
import { cachable } from "./decorators/cachable";
import { promisify } from 'util';
import { exec} from "child_process";
import { normalize } from "canonical-path";


export default class Io {

    public static readonly readDir = fs.readdirSync.bind(fs);

    public static readonly EOL = EOL;

    public static readonly PROJECT_DIR  = normalize(`${__dirname}/../..`);

    private static TMP_DIR? :string;

    private static tempIndex = 0;

    /**
     * Get a path relative to the project directory
     * @param path 
     */
    public static path(path: string) {
        return normalize(`${this.PROJECT_DIR}/${path}`)
    }

    @cachable()
    private static getProperties(file: string) {
        const thisProperties = new Map<string, string>();
        const contents = fs.readFileSync(`${this.PROJECT_DIR}/properties/${file}.properties`, {encoding: "utf8"});
        for (const [, key, val] of matchAll(/^\s*(.+?)\s*\=\s*"?(.+)"\s*?$/gm, contents)) {
            thisProperties.set(key, val);
        }
        return thisProperties;

    }

    public static runCommandLowPriority(command: string, cwd?: string) {
        return promisify(exec)(process.platform === "win32" ? `start /low /b ${command}` 
        : `nice ${command}`, {cwd});
    }

    public static getProperty(file: string, property: string) {
        return this.getProperties(file).get(property);
    }

    public static writeFile(filename: string, data: string | NodeJS.ArrayBufferView, options: fs.WriteFileOptions = {}) {
        return new Promise<string>((resolve, reject) => {
            fs.writeFile(filename, data, options, function(err: NodeJS.ErrnoException | null) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data.toString());
                }
            });
        });
    }

    public static readFile(filename: string) {
        return new Promise<string>((resolve, reject) => {
            fs.readFile(filename, function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data.toString());
                }
            })
        });
    }
    public static readFileSync(filename: string) {
        return fs.readFileSync(filename, "utf8");
    }
    public static readFileSyncSuppress(filename: string) {
        try {
            return fs.readFileSync(filename, "utf8");
        } catch (e) {
            console.warn(e);
            return null;
        }
    }

    public static existsSync = fs.existsSync;

    public static mkdirSync = fs.mkdirSync;

    public static get tempDir() {
        if (!this.TMP_DIR) {
            const tempDir = this.path(`tmp/node-${process.pid}`);
            if (!this.existsSync(tempDir)) {
                this.mkdirSync(tempDir);
            }

            this.TMP_DIR = tempDir;
            console.log(`Temp directory : ${tempDir}`);
        }
        return this.TMP_DIR;
    }

    public static get tempFile() {
        return `${this.tempDir}/temp-${this.tempIndex++}`;
    }

    public static removeTempFiles() {
        if (this.TMP_DIR) {
            (fs.rmSync || promisify(fs.rmdir))(this.TMP_DIR, {
                recursive: true
            });
        }
    }

    public static exec = promisify(exec);

    public static async readFileFromZip(path: string, fileToRead: string) {
        const zipFile = await yauzl.open(path);
        try {
            while (true) {
                const entry = await zipFile.readEntry();
                if (entry.fileName === fileToRead) {

                    const stream = await entry.openReadStream();
                    try {
                        return await new Promise<Buffer>(async (resolve, reject) => {
                            stream.pipe(concat(resolve));
                            stream.on("error", reject);
                        });
                    } finally {
                        stream.destroy();
                    }
                }
            }
        } finally {
            await zipFile.close();
        }
    }
}