<?php
/**
 * 
 * @author magog
 *
 */
class Webservice_Response {
    
    /**
     * 
     * @var mixed
     */
    var $response;
    
    /**
     * 
     * @var Throwable | null
     */
    var $exception;
    
    /**
     * 
     * @param mixed $response
     * @param Throwable $exception
     */
    function __construct($response = null, Throwable $exception = null) {
        $this->response = $response;
        $this->exception = $exception;
    }
}

