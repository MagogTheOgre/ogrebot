"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const SvgCovidMap_1 = __importDefault(require("./SvgCovidMap"));
const path_1 = require("path");
const sharp_1 = __importDefault(require("sharp"));
const io_1 = __importDefault(require("../../io"));
class PngCovidMap extends SvgCovidMap_1.default {
    outputPngFileName;
    constructor(outputPngFileName, date, mapBuilder) {
        super(`${io_1.default.tempDir}/${+date}.svg`, date, mapBuilder);
        this.outputPngFileName = outputPngFileName;
    }
    //`${this.outputDir}/covid19-${dateFileName}-${this.mapBuilder.name}.svg`
    async create() {
        const svg = await super.create();
        await sharp_1.default(svg, {
            density: 216
        }).resize(1665, 1056).toFile(this.outputPngFileName);
        console.log(`${svg.split(path_1.sep).pop()}: PNG complete`);
        return this.outputPngFileName;
    }
}
exports.default = PngCovidMap;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUG5nQ292aWRNYXAuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJQbmdDb3ZpZE1hcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLGdFQUF3QztBQUN4QywrQkFBMkI7QUFDM0Isa0RBQTBCO0FBQzFCLGtEQUEwQjtBQUcxQixNQUFxQixXQUFZLFNBQVEscUJBQVc7SUFFbkI7SUFBN0IsWUFBNkIsaUJBQXlCLEVBQUUsSUFBVSxFQUFFLFVBQXNCO1FBQ3RGLEtBQUssQ0FBQyxHQUFHLFlBQUUsQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLE1BQU0sRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFEN0Isc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFRO0lBRXRELENBQUM7SUFDRCx5RUFBeUU7SUFDbEUsS0FBSyxDQUFDLE1BQU07UUFDZixNQUFNLEdBQUcsR0FBRyxNQUFNLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNqQyxNQUFNLGVBQUssQ0FBQyxHQUFHLEVBQUU7WUFDYixPQUFPLEVBQUUsR0FBRztTQUNmLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUNyRCxPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztJQUNsQyxDQUFDO0NBQ0o7QUFkRCw4QkFjQyJ9