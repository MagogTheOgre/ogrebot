import fs, { appendFile, exists } from "fs";
import io from "../lib/io";
import compiler from 'google-closure-compiler';
import { shutdown, startup } from "../lib/promiseUtils";
import { ThreadPoolImpl } from "../lib/ThreadPool";
import { promisify } from "util";

const { compiler: ClosureCompiler } = compiler;

const { PROJECT_DIR } = io;
const JS_DIR = `${PROJECT_DIR}/public_html/js`;
const SRC_DIR = `${JS_DIR}/src`;
const DEST_DIR = `${JS_DIR}/bin`;
const MODULES_DIR = `${SRC_DIR}/modules`;
const FILES_TO_MINIFY = process.argv.slice(2);

startup();
(async function () {
    const tsFiles = FILES_TO_MINIFY.length > 0 ?
        FILES_TO_MINIFY.map(file => `${file}.ts`) :
        fs.readdirSync(SRC_DIR).filter(file => file.endsWith(".ts"));
    
    const modules =  fs.readdirSync(MODULES_DIR).flatMap(file => 
        file.endsWith(".ts") ? [`${MODULES_DIR}/${file}`] : []).join(" ");

    try {
        //multithreaded Closure makes Windows freeze
        const threadPool = new ThreadPoolImpl();
        await threadPool.enqueueAll(function* () {
            for (const tsFile of tsFiles) {
                yield async () => {

                    console.log(`Transpiling ${tsFile}...`);
                    const { tempFile } = io;

                    
                    const { stdout } = await io.runCommandLowPriority(
                        `tsc ${tsFile} ${modules} -t ES2021 --inlineSourceMap --outfile ${tempFile}`, 
                        SRC_DIR);

                    if (stdout) {
                        console.log(tsFile, stdout);
                    }   

                    const baseFile = tsFile.replace(/\.ts$/, "");
                    const mapFile = `${baseFile}.js.map`;
                    const minFileName = `${baseFile}.min.js`;

                    console.log(`Minifying to ${minFileName}...`);
                    const outputFile = `${DEST_DIR}/${minFileName}`;
                    await new Promise((resolve, reject) => {
                        new ClosureCompiler({
                            js: tempFile,
                            compilation_level: 'SIMPLE',
                            //warning_level: 'VERBOSE',
                            language_in: 'ECMASCRIPT_NEXT',
                            language_out: 'ECMASCRIPT_2021',
                            js_output_file: outputFile,
                            isolation_mode: "IIFE",
                            create_source_map: `${DEST_DIR}/${mapFile}`,
                            source_map_location_mapping: `${JS_DIR}|..`
                        }).run((code, stdOutData, stdErrData) => {
                            if (stdOutData) {
                                console.log(tsFile, stdOutData);
                            }
                            if (stdErrData) {
                                console.error(tsFile, stdErrData)
                            }
                            if (code) {
                                return reject();
                            }
                            promisify(appendFile)(outputFile, `${"\n"}//# sourceMappingURL=${mapFile}`).then(resolve, reject);
                        });
                    });
                    console.log(`Done ${tsFile}...`);
                };
            }
        });
        console.log("All files successfully compiled");
    } catch (e) {
        console.error(JSON.stringify(e, null, 4));
        console.error("All files NOT successfully compiled");
    } finally {
        shutdown();
    }
}());
