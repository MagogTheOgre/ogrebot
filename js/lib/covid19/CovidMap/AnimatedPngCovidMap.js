"use strict";
// import CovidMap from "./CovidMap";
// import MapBuilder from "../MapBuilder/MapBuilder";
// import MultiCovidMapWrapper from "./MultiCovidMapWrapper";
// import Io from "../../../lib/io";
// import PngCovidMap from "./PngCovidMap";
// export default class AnimatedPngCovidMap implements CovidMap<string> {
//     private multiMap: CovidMap<string[]>;
//     constructor(private readonly outputFileName: string, startDate: Date, endDate: Date,
//          mapBuilder: MapBuilder, private readonly delayNumerator: number, private readonly delayDenominator: number)  {
//         this.multiMap = new MultiCovidMapWrapper(Io.tempDir, startDate, endDate, "png", mapBuilder, 
//             (_, count) => String(count), PngCovidMap);
//     }
//     public async create() {
//         const fileNames = await this.multiMap.create();
//         console.log("Executing apngasm");
//         const {stdout, stderr} = await Io.exec(`apngasm "${this.outputFileName}" "${fileNames[0]}" ${this.delayNumerator} ${this.delayDenominator}`);
//         stdout && console.log(stdout);
//         stderr && console.error(stderr);
//         return this.outputFileName;
//     }
// }
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQW5pbWF0ZWRQbmdDb3ZpZE1hcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkFuaW1hdGVkUG5nQ292aWRNYXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHFDQUFxQztBQUNyQyxxREFBcUQ7QUFDckQsNkRBQTZEO0FBQzdELG9DQUFvQztBQUNwQywyQ0FBMkM7QUFFM0MseUVBQXlFO0FBRXpFLDRDQUE0QztBQUU1QywyRkFBMkY7QUFDM0YsMEhBQTBIO0FBQzFILHVHQUF1RztBQUN2Ryx5REFBeUQ7QUFDekQsUUFBUTtBQUVSLDhCQUE4QjtBQUM5QiwwREFBMEQ7QUFDMUQsNENBQTRDO0FBQzVDLHdKQUF3SjtBQUN4Six5Q0FBeUM7QUFDekMsMkNBQTJDO0FBRTNDLHNDQUFzQztBQUN0QyxRQUFRO0FBQ1IsSUFBSSJ9