"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WikidataImpl = void 0;
const AbstractMediawiki_1 = require("./AbstractMediawiki");
const Mediawiki_1 = require("./Mediawiki");
const awaitReady_1 = require("../decorators/awaitReady");
const cachable_1 = require("../decorators/cachable");
class WikidataImpl extends AbstractMediawiki_1.AbstractMediawiki {
    constructor() {
        super({ username: Mediawiki_1.MediawikiUsername.OGREBOT, api: Mediawiki_1.MediawikiApi.WIKIDATA, threadPoolSize: 2, throttle: 0 });
    }
    async linktitles(tosite, totitle, fromsite, fromtitle) {
        const { success } = await this.post({
            action: "wblinktitles",
            bot: true,
            token: await this.fetchToken("csrf"),
            tosite, totitle, fromtitle, fromsite
        }, true);
        return !!success;
    }
    async createclaim(entity, property, value, snaktype = "value") {
        const { success } = await this.post({
            action: "wbcreateclaim",
            entity,
            property,
            snaktype,
            value: JSON.stringify(value),
            bot: true,
            token: await this.fetchToken("csrf")
        }, true);
        return !!success;
    }
    async getentities(site, titles, props) {
        const MAX_SIZE = 500;
        const allEntities = {};
        for (var i = 0; i < titles.length; i += MAX_SIZE) {
            const { entities, success } = await this.post({
                action: "wbgetentities",
                sites: site,
                titles: titles.slice(i, i + MAX_SIZE).join("|"),
                props: props.join("|"),
                bot: true
            }, false);
            if (!success) {
                throw new Error(`Unsuccessful response.`);
            }
            Object.assign(allEntities, entities);
        }
        return Object.values(allEntities).filter(entity => entity.missing === undefined);
    }
}
__decorate([
    awaitReady_1.awaitReady()
], WikidataImpl.prototype, "linktitles", null);
__decorate([
    awaitReady_1.awaitReady()
], WikidataImpl.prototype, "createclaim", null);
__decorate([
    cachable_1.cachable(),
    awaitReady_1.awaitReady()
], WikidataImpl.prototype, "getentities", null);
exports.WikidataImpl = WikidataImpl;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiV2lraWRhdGEuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJXaWtpZGF0YS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQSwyREFBd0Q7QUFDeEQsMkNBQThEO0FBQzlELHlEQUFzRDtBQUN0RCxxREFBa0Q7QUE4Q2xELE1BQWEsWUFBYSxTQUFRLHFDQUFpQjtJQUUvQztRQUNJLEtBQUssQ0FBQyxFQUFDLFFBQVEsRUFBRSw2QkFBaUIsQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFLHdCQUFZLENBQUMsUUFBUSxFQUFFLGNBQWMsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUE7SUFDNUcsQ0FBQztJQUdELEtBQUssQ0FBQyxVQUFVLENBQUMsTUFBYyxFQUFFLE9BQWUsRUFBRSxRQUFnQixFQUFFLFNBQWlCO1FBQ2pGLE1BQU0sRUFBQyxPQUFPLEVBQUMsR0FBRyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDOUIsTUFBTSxFQUFFLGNBQWM7WUFDdEIsR0FBRyxFQUFFLElBQUk7WUFDVCxLQUFLLEVBQUUsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQztZQUNwQyxNQUFNLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxRQUFRO1NBQ3ZDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDVCxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUM7SUFDckIsQ0FBQztJQUdELEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBYyxFQUFFLFFBQWdCLEVBQUUsS0FBVSxFQUFFLFdBQW9CLE9BQU87UUFDdkYsTUFBTSxFQUFDLE9BQU8sRUFBQyxHQUFHLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQztZQUM5QixNQUFNLEVBQUUsZUFBZTtZQUN2QixNQUFNO1lBQ04sUUFBUTtZQUNSLFFBQVE7WUFDUixLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7WUFDNUIsR0FBRyxFQUFFLElBQUk7WUFDVCxLQUFLLEVBQUUsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQztTQUN2QyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ1QsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDO0lBQ3JCLENBQUM7SUFJRCxLQUFLLENBQUMsV0FBVyxDQUFDLElBQVksRUFBRSxNQUFnQixFQUFFLEtBQWU7UUFDN0QsTUFBTSxRQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ3JCLE1BQU0sV0FBVyxHQUEyQixFQUFFLENBQUM7UUFDL0MsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLFFBQVEsRUFBRTtZQUM5QyxNQUFNLEVBQUMsUUFBUSxFQUFFLE9BQU8sRUFBQyxHQUFtQixNQUFNLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3hELE1BQU0sRUFBRSxlQUFlO2dCQUN2QixLQUFLLEVBQUUsSUFBSTtnQkFDWCxNQUFNLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7Z0JBQy9DLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztnQkFDdEIsR0FBRyxFQUFFLElBQUk7YUFDWixFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ1YsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDVixNQUFNLElBQUksS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7YUFDN0M7WUFDRCxNQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUN4QztRQUNELE9BQU8sTUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsT0FBTyxLQUFLLFNBQVMsQ0FBQyxDQUFDO0lBQ3JGLENBQUM7Q0FDSjtBQTVDRztJQURDLHVCQUFVLEVBQUU7OENBU1o7QUFHRDtJQURDLHVCQUFVLEVBQUU7K0NBWVo7QUFJRDtJQUZDLG1CQUFRLEVBQUU7SUFDVix1QkFBVSxFQUFFOytDQWtCWjtBQWxETCxvQ0FtREMifQ==