/// <reference path="documentFunctions.ts" />

async function projectMultibox() {
   
    function parse(project: string) {
        return project.match(/^([a-z-]+)\.([a-z-]+)$/)!!.slice(1);
    }

    function buildSubprojectOptions(currentValue: string) {
        subProjectElement.innerHTML = buildOptions(projects[projectElement.value], currentValue)
    }

    function buildOptions(keys: string[], currentValue? : string) {
        return keys
            .map(key => "<option" + (key === currentValue ? " selected>" : ">") + key + "</option>")
            .join("");
    }
    function buildSelectWithOptions(keys: string[], currentValue? : string) {
        const select = createElement("select");
        select.innerHTML = buildOptions(keys, currentValue);
        return select;
    }

    const select = querySelector("select[name='project']") as HTMLSelectElement;
    const cssClass = select.getAttribute("class") || "";
    const projects = {} as Record<string, string[]>;

    const [currentSubproject, currentProject] = parse(select.value);

    select.querySelectorAll("option").forEach(element => {
        const [subproject, project] = parse(element.innerHTML);

        (projects[project] ||= []).push(subproject);
    });

    const subProjectElement = createElement("select");
    const projectElement = buildSelectWithOptions(
        Object.keys(projects), currentProject);
    projectElement.addEventListener("change", () => buildSubprojectOptions(subProjectElement.value));
    buildSubprojectOptions(subProjectElement.value);
    buildSubprojectOptions(currentSubproject);

    const divElement = createElement("div");
    divElement.style.display = "table-row";
    divElement.append(projectElement, subProjectElement)
    const input = createElement("input")
    input.type = "hidden"
    input.name = "project"
    input.value = select.value;

    select.after(divElement, input)
    select.remove()

    for(const element of [projectElement, subProjectElement]) {
        const div = createElement("div");
        div.style.display = "table-cell";
        element.parentNode!!.insertBefore(div, element)
        div.append(element);
        element.classList.add(cssClass);
        element.addEventListener("change", () => input.value = subProjectElement.value + "." + projectElement.value);

    }
}