<?php
class Classloader {
	
	private const AUTOLOAD_FUNCTION_NAME = "_autoload";

    private const paths = ["base", "base/entity", "base/exception", "base/exception/instance", "base/database",
		"base/Lazy_Storage", "base/logger", "base/logger/formatter", "base/Page_Parser", 
		"base/Project_Data", "base/Page_Parser/unparsed-element-type", "base/remote", 
		"base/Template_Utils", "base/Template_Utils/Template_Interface", "base/UserData", 
		"BranchMapCreator", "BranchMapCreator/Factory", "BranchMapCreator/dto", 
		"BranchMapCreator/svg_creator", "category-files", "cleanup", 
		"cleanup/Cleanup_Template_Logic", "cleanup/instance", "cleanup/mass-tester", 
		"cleanup/package", "cleanup/parsers", "CommonsHelper", "CommonsHelper/debug", "cron", 
		"cron/cron-date-factory", "dao", "FCC_Database_Serializer", "fileinfo", "fileinfo/upload-history", 
		"generate-getters-setters", "identity", "mass-cleanup", "newuploads", "nowcommonslist", 
		"oldver", "Png_Svg_Service", "refresh-configs", "relink", "relink/types", 
		"UploadReport", "UploadReport/newuploadhandler", "util"];

	private const abstract_class_paths = [
		"Unparsed_Element_Type" => "base/Page_Parser/unparsed-element-type", 
		"Abstract_Cleanup" => "mass-cleanup", "Now_Commons_List" => "nowcommonslist/types", 
		"Refactor" => "refactor", "FCC_Database_Serializer" => "FCC_Database_Serializer", 
		"Latitude_Longitude_Svg_Creator" => "BranchMapCreator/svg_creator", 
		"Cleanup_Module" => "cleanup/parsers", 
		"Upload_History_Wiki_Text_Writer" => "fileinfo/upload-history"];
	
	/**
	 *
	 * @var string[]
	 */
	private array $files_by_path;

    public function __construct()  {
        $this->files_by_path = array_merge(...array_map(
            fn($path) => $this->php_files_in_directory(BASE_DIRECTORY . DIRECTORY_SEPARATOR . $path),
            self::paths
        ));
    }

	private function load_class(string $path, string $class) {
		require_once $path;
		if (method_exists($class, self::AUTOLOAD_FUNCTION_NAME)) {
			$method = (new ReflectionClass($class))->getMethod(self::AUTOLOAD_FUNCTION_NAME);
			if ($method->isStatic()) {
				$method->setAccessible(true);
				$method->invoke(null);
			}
		}
	}

	/**
	 *
	 * @param mixed $class_or_this
	 * @param String $method
	 * @return Closure
	 */
	public function get_closure($class_or_this, $method) {
		$static = is_string($class_or_this);

		$reflection_method = (new ReflectionClass(
			$static ? $class_or_this : get_class($class_or_this)))->getMethod($method);
		$reflection_method->setAccessible(true);

		return $reflection_method->getClosure($static ? null : $class_or_this);
	}

    private function get_calling_directory(): ?string {
        $backtrace_stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

        while ($next = next($backtrace_stack)) {
            $file = $next["file"] ?? null;
            if ($file) {
                return pathinfo($file, PATHINFO_DIRNAME);;
            }
        }
        return null;
    }

	/**
	 *
	 * @param class-string $class
	 */
	public function autoload(string $class) {
		$directory = self::get_calling_directory();
		if ($directory) {
			$path = "$directory/$class.php";
			if (file_exists($path)) {
				$this->load_class($path, $class);
				return true;
			}
		}

		if (isset($this->files_by_path[$class])) {
            $this->load_class($this->files_by_path[$class], $class);
			return true;
		}

		return false;
	}

	public function include_directory(string $path) {
		$files_in_directory = $this->php_files_in_directory($path);
		array_walk($files_in_directory, function ($file) {
			preg_match("/.+\/(.+?)\.php$/", $this->normalize($file), $class);
            $this->load_class($file, $class[1]);
		});
	}

	/**
	 *
	 * @return string[]
	 */
	private function php_files_in_directory(string $path): array {
        return array_reduce(scandir($path), function (array $files, string $filename) use ($path)  {
            if (preg_match("/(.+)\.php$/", $filename, $match)) {
                $full = $path . DIRECTORY_SEPARATOR . $filename;
                if (is_file($full)) {
                    $files[$match[1]] = $full;
                }
            }
            return $files;
        }, []);
	}

	/**
	 *
	 * @param class-string $class
	 * @param bool $allow_abstract DEFAULT false
	 * @return string[]
	 * @throws ArrayIndexNotFoundException if $class isn't registered
	 */
	public function get_all_class_names_of_type(string $class, bool $allow_abstract = false) {
		global $logger, $validator;

		$validator->validate_arg($class, "string");
		$validator->validate_arg($allow_abstract, "bool");

		$class_path = Array_Utils::array_key_or_exception(self::abstract_class_paths, $class);
		if ($class_path) {
			$this->include_directory(BASE_DIRECTORY . '/' . $class_path);
		}

		$classes = array();

		foreach (get_declared_classes() as $declared_class) {
			if (is_subclass_of($declared_class, $class)) {

				if (!$allow_abstract) {
					$reflection_class = new ReflectionClass($declared_class);
					if ($reflection_class->isAbstract()) {
						$logger->debug("Ignoring abstract $declared_class subclass of $class");
						continue;
					}
				}

				$logger->debug("Found $declared_class subclass of $class");
				$classes[] = $declared_class;
			}
		}

		return $classes;
	}

	/**
	 *
	 * @param class-string $class
	 * @param array|callable|null $args
	 * @return object[]
	 * @throws ArrayIndexNotFoundException if $class isn't registered
	 */
	public function get_all_instances_of_type(string $class, $args = null) {
		$callable = is_callable($args) || $args instanceof Closure ? $args : null;
		$class_names = $this->get_all_class_names_of_type($class, false);
		
		return Array_Utils::map_array_function_keys($class_names,
            function ($class_name) use ($callable, $args) {
                if ($callable) {
                    $next_args = $callable($class_name);
                } else if (is_array($args)) {
                    $next_args = $args;
                } else {
                    $next_args = [];
                }
                return [$class_name,
                    (new ReflectionClass($class_name))->newInstanceArgs($next_args)];
            });
	}
	
	/**
	 * 
	 * @param string $path
	 * @return string
	 */
	public function normalize($path) {
		return str_replace(DIRECTORY_SEPARATOR, "/", $path);
	}
}

$classloader = new Classloader();
spl_autoload_register([$classloader, 'autoload'], true, true);