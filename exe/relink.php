<?php
//--type=regex "--from=Kit right arm blacklowerthin.png" "--search=/_ra\d?\s*\=\s*_blacklowerthin\b/" --replacement="$0_2"
//--type=regex "--from=Kit right arm blacklowerthin.png" "--search=/\|\s*arm\s*\|\s*_blacklowerthin\b/" --replacement="$0_2"
require_once __DIR__ . "/../base/bootstrap.php";
global $env, $logger, $wiki_interface;

$argv = $env->load_command_line_args();
$type = Array_Utils::find_command_line_arg($argv, "type", false, true, "auto");
$errlog = Array_Utils::find_command_line_arg($argv, "errlog", false, true) !== null;
$test_conflicts = Array_Utils::find_command_line_arg($argv, "ignore-conflicts", false, true) === null;
$purge = Array_Utils::find_command_line_arg($argv, "purge", false, true) !== null;
$verify_mime = Array_Utils::find_command_line_arg($argv, "ignore_mime", false, true) === null;
$logger->debug("Starting up with relink type $type");

$en = $wiki_interface->new_wiki("OgreBot");
$co = $wiki_interface->new_wiki("OgreBotCommons");

switch ($type) {
	case "auto":
		$relink = new Auto_Relink($en, $co);
		$relink->set_write_warnings($errlog);
		
		$logger->info("Error logs are " . ($errlog ? "ON" : "OFF") . ".");
		break;
	case "pages":
		$relink = new Page_Relink($en, $co);
		$relink->set_pages_from_unparsed_command_line($argv);
		break;
	case "regex":
		$relink = new Regex_Relink($en, $co, $argv);
		break;
	default:
		throw new IllegalArgumentException("Unreconized type: $type");
}
$relink->set_verify_mime($verify_mime);
$relink->set_purge($purge);
$relink->set_test_conflicts($test_conflicts);
$relink->run();