"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const LOG_LEVELS = ["trace", "debug", "info", "warn", "error"];
const processMatch = process.argv[1].match(/\/([^\/]+?)\.js/);
if (!processMatch) {
    throw new Error("Can't determine process name");
}
console.log(processMatch[1]);
const logLevelString = (JSON.parse(fs_1.default.readFileSync(`${__dirname}/../config.json`).toString())["log-level"][processMatch[1]].toLowerCase());
const myLevelLevel = LOG_LEVELS.indexOf(logLevelString);
if (myLevelLevel === undefined) {
    throw new Error(`Unrecognized log level: ${logLevelString}`);
}
const logger = Object.fromEntries(function* () {
    for (const [level, entry] of Object.entries(LOG_LEVELS)) {
        const enabled = myLevelLevel <= +level;
        yield [`${entry}Enabled`, enabled];
        yield [entry, enabled ? console[(entry !== "trace" ? entry : "debug")] : function () { }];
    }
}());
logger.debug(`Logger loaded with level ${logLevelString}`);
exports.default = logger;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9nZ2VyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiTG9nZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsNENBQW9CO0FBRXBCLE1BQU0sVUFBVSxHQUFHLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBVSxDQUFDO0FBSXhFLE1BQU0sWUFBWSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7QUFDOUQsSUFBSSxDQUFDLFlBQVksRUFBRTtJQUNmLE1BQU0sSUFBSSxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQztDQUNuRDtBQUdELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDN0IsTUFBTSxjQUFjLEdBQWEsQ0FDN0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFFLENBQUMsWUFBWSxDQUFDLEdBQUcsU0FBUyxpQkFBaUIsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztBQUV2SCxNQUFNLFlBQVksR0FBVyxVQUFVLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBRWhFLElBQUksWUFBWSxLQUFLLFNBQVMsRUFBRTtJQUM1QixNQUFNLElBQUksS0FBSyxDQUFDLDJCQUEyQixjQUFjLEVBQUUsQ0FBQyxDQUFDO0NBQ2hFO0FBY0QsTUFBTSxNQUFNLEdBQVcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUM7SUFDL0MsS0FBSyxNQUFNLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUU7UUFDckQsTUFBTSxPQUFPLEdBQUksWUFBWSxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3hDLE1BQU0sQ0FBQyxHQUFHLEtBQUssU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ25DLE1BQU0sQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7S0FDN0Y7QUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBRUwsTUFBTSxDQUFDLEtBQUssQ0FBQyw0QkFBNEIsY0FBYyxFQUFFLENBQUMsQ0FBQztBQUUzRCxrQkFBZSxNQUFNLENBQUMifQ==