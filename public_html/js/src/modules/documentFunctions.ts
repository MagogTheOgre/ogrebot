function createElement<K extends keyof HTMLElementTagNameMap | string>(selector: K) {
    return document.createElement(selector) as K extends keyof HTMLElementTagNameMap ? HTMLElementTagNameMap[K] : HTMLElement;
}
function querySelector<E extends Element = Element>(selector: string) {
    return document.querySelector<E>(selector);
}
function querySelectorAll<E extends Element = Element>(selector: string) {
    return document.querySelectorAll<E>(selector);
}


function addOrRemoveListener(
    type: "addEventListener" | "removeEventListener",
    events: string,
    listener: EventListenerOrEventListenerObject,
    element: Element | Document) {
    events.split(" ").forEach(event => element[type](event, listener))
}

function addEventListeners(
    events: string,
    listener: EventListenerOrEventListenerObject,
    element: Element | Document = document) {
    addOrRemoveListener("addEventListener", events, listener, element)
}

function removeEventListeners(
    events: string,
    listener: EventListenerOrEventListenerObject,
    element: Element | Document = document) {
    addOrRemoveListener("removeEventListener", events, listener, element)
}
