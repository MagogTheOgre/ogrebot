"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediawikiImpl = void 0;
const awaitReady_1 = require("../decorators/awaitReady");
const AbstractMediawiki_1 = require("./AbstractMediawiki");
const cachable_1 = require("../decorators/cachable");
const Mediawiki_1 = require("./Mediawiki");
class MediawikiImpl extends AbstractMediawiki_1.AbstractMediawiki {
    async editAppend(title, summary, text) {
        await this.post({
            action: "edit",
            appendtext: text,
            title: title,
            summary: summary,
            starttimestamp: this.getNowString(),
            //watchlist: "nochange",
            token: await this.fetchToken("csrf")
        }, true);
    }
    async categoryMembersRecurse(category, depth) {
        if (depth > 0) {
            var members = [];
            const currentMembers = await this.categoryMembers(category);
            await Promise.all(currentMembers.map(async (prefixedMember) => {
                if (prefixedMember.startsWith(Mediawiki_1.CATEGORY_PREFIX)) {
                    const member = prefixedMember.substring(Mediawiki_1.CATEGORY_PREFIX.length);
                    const newMembers = await this.categoryMembersRecurse(member, depth - 1);
                    newMembers.forEach(newMember => newMember.unshift(prefixedMember));
                    members.push(...newMembers);
                }
                else {
                    members.push([prefixedMember]);
                }
            }));
            return members.sort(([a], [b]) => a.localeCompare(b));
        }
        else {
            return [[]];
        }
    }
    async categoryMembers(category) {
        const values = await this.query({
            generator: "categorymembers",
            gcmtitle: `${Mediawiki_1.CATEGORY_PREFIX}${category}`,
            prop: "info",
            gcmlimit: "max"
        }, ["query", "pages"]);
        return values ? Object.values(values).map(o => o.title) : [];
    }
}
__decorate([
    awaitReady_1.awaitReady()
], MediawikiImpl.prototype, "editAppend", null);
__decorate([
    cachable_1.cachable(),
    awaitReady_1.awaitReady()
], MediawikiImpl.prototype, "categoryMembersRecurse", null);
__decorate([
    cachable_1.cachable(),
    awaitReady_1.awaitReady()
], MediawikiImpl.prototype, "categoryMembers", null);
exports.MediawikiImpl = MediawikiImpl;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWVkaWF3aWtpSW1wbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIk1lZGlhd2lraUltcGwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEseURBQXNEO0FBQ3RELDJEQUF3RDtBQUN4RCxxREFBa0Q7QUFDbEQsMkNBQXlEO0FBRXpELE1BQWEsYUFBYyxTQUFRLHFDQUFpQjtJQUdoRCxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQWEsRUFBRSxPQUFlLEVBQUUsSUFBWTtRQUN6RCxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDWixNQUFNLEVBQUUsTUFBTTtZQUNkLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLEtBQUssRUFBRSxLQUFLO1lBQ1osT0FBTyxFQUFFLE9BQU87WUFDaEIsY0FBYyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDbkMsd0JBQXdCO1lBQ3hCLEtBQUssRUFBRSxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDO1NBQ3ZDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBSUQsS0FBSyxDQUFDLHNCQUFzQixDQUFDLFFBQWdCLEVBQUUsS0FBYTtRQUN4RCxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUU7WUFDWCxJQUFJLE9BQU8sR0FBZSxFQUFFLENBQUM7WUFDN0IsTUFBTSxjQUFjLEdBQUcsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzVELE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBQyxjQUFjLEVBQUMsRUFBRTtnQkFDeEQsSUFBSSxjQUFjLENBQUMsVUFBVSxDQUFDLDJCQUFlLENBQUMsRUFBRTtvQkFDNUMsTUFBTSxNQUFNLEdBQUcsY0FBYyxDQUFDLFNBQVMsQ0FBQywyQkFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNoRSxNQUFNLFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUN4RSxVQUFVLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO29CQUNuRSxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsVUFBVSxDQUFDLENBQUM7aUJBQy9CO3FCQUFNO29CQUNILE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2lCQUNsQztZQUNMLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDSixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN6RDthQUFNO1lBQ0osT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ2Q7SUFDTCxDQUFDO0lBSUQsS0FBSyxDQUFDLGVBQWUsQ0FBQyxRQUFnQjtRQUNsQyxNQUFNLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDNUIsU0FBUyxFQUFFLGlCQUFpQjtZQUM1QixRQUFRLEVBQUUsR0FBRywyQkFBZSxHQUFHLFFBQVEsRUFBRTtZQUN6QyxJQUFJLEVBQUUsTUFBTTtZQUNaLFFBQVEsRUFBRSxLQUFLO1NBQ2xCLEVBQUUsQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztRQUV2QixPQUFPLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBTyxDQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUN4RSxDQUFDO0NBQ0o7QUE5Q0c7SUFEQyx1QkFBVSxFQUFFOytDQVdaO0FBSUQ7SUFGQyxtQkFBUSxFQUFFO0lBQ1YsdUJBQVUsRUFBRTsyREFtQlo7QUFJRDtJQUZDLG1CQUFRLEVBQUU7SUFDVix1QkFBVSxFQUFFO29EQVVaO0FBaERMLHNDQWlEQyJ9