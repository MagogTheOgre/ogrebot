<?php
abstract class PNG_Creator {

	public abstract function get_thumb(string $svg_text, int $height): string ;
	
}
