/// <reference path="modules/projectMultibox.ts" />
/// <reference path="modules/documentFunctions.ts" />

projectMultibox();
const submitProcessUploads = querySelector<HTMLInputElement>(".submit-pu")!!;


const src = querySelector<HTMLInputElement>("#src")!!;
const trg = querySelector<HTMLInputElement>("#trg")!!;
const srcSpan = querySelector<HTMLAnchorElement>("#srcspan > a")!!;
const trgSpan = querySelector<HTMLAnchorElement>("#trgspan > a")!!;
const submitOv = querySelector<HTMLInputElement>("#submit_OV")!!;

function updateTarget() {
    trg.value = src.value;
}

if (srcSpan) {
    src.value = srcSpan.innerHTML;
    trg.value = trgSpan.innerHTML;
}
addEventListeners("keyup change", updateTarget, src)
trg.addEventListener("change", () => removeEventListeners("keyup change", updateTarget, src));

querySelector<HTMLInputElement>("#process_form")?.addEventListener("submit", () => {
    querySelector<HTMLSpanElement>(".upload_text")!!.style.display = "block";
    /* ensure submit button isn't pressed multiple times */
    submitProcessUploads.disabled = true;
});
const changeFiles = querySelector<HTMLInputElement>("#change_files");
changeFiles?.addEventListener("click", () => {
    [changeFiles, srcSpan, trgSpan].forEach(el => el.style.display = "none");
    [src, trg, submitOv].forEach(el => el.style.removeProperty("display"));
});
/* first valid click... update fields */
querySelector<HTMLInputElement>("#form")!!.addEventListener("submit", () =>
    querySelector<HTMLSpanElement>(".pleaseWait")!!.style.display = "block")

querySelector<HTMLIFrameElement>("#ident-frame")?.addEventListener("load", ({target}) => {
    (target as HTMLIFrameElement).contentWindow!!.postMessage({ watch: true }, location.origin);
    addEventListener("message", ({origin, data}) => {
        if (origin !== location.origin) {
            return;
        }

        const {cookie} = data;
        if (cookie) {
            submitProcessUploads.style.removeProperty("display");
        }
        querySelector<HTMLInputElement>("#ident-cookie")!!.value = cookie;
    });
});
