export interface Store<K, V> {
    get(key: K): V | undefined;
    set(key: K, value: V): any;
}

export function computeIfAbsent<K, V>(store: Store<K, V>, key: K, valueFactory: () => V) {
    var value = store.get(key);
    if (value === undefined) {
        value = valueFactory();
        store.set(key, value);
    }
    return value;
}

export function arrayFindOrDie<T>(array: T[], predicate: (value: T, index: number, obj: T[]) => boolean, thisArg?: any) {
    return array[arrayFindCallback(array, predicate)];
}

export function arrayFindCallback<T>(array: T[], predicate: (value: T, index: number, obj: T[]) => boolean, thisArg?: any): number {
    const index = array.findIndex(predicate);
    if (index < 0) {
        throw new Error(`Predicate failed on array: ${JSON.stringify(array)}`);
    }
    return index;
}

export function arrayFind<T>(array: T[], value: T): number {
    return arrayFindCallback(array, e => e === value);
}

export function arrayFindAll<T>(array: T[], ...values: T[]): number[] {
    return values.map(value => arrayFind(array, value));
}

/**
 * 
 * @param array 
 * @param values a sparsely populated array
 */
export function arrayFindAllMap<T extends string, U>(array: U[], values: {[x in T]: U}): {[x in number]: T} {
    const obj: {[x in number]: string} = {}; 
    for (const [key, dataKey] of Object.entries(values)) {
        obj[arrayFind(array, dataKey)] = key;
    }
    return <{[x in number]: T}>obj;
}

export function getAndDelete<T extends string|number, U>(object: {[x in T]: U}, key: T) {
    const value = object[key];
    delete object[key];
    return value;
}
export function mapEntries<T, U>(object: { [x in string]: T }, mapper: (key: string, value: T) => [string, U][]): { [x in string]: U } {
    return Object.fromEntries(Object.entries(object).flatMap(args => mapper(...args)));
}
export function mapValues<T, U>(object: { [x in string]: T }, mapper: (value: T, key: string) => U | undefined): { [x in string]: U } {
    return mapEntries(object, (key, value) => {
        const newValue = mapper(value, key);
        return newValue != null ? [[key, newValue]]: [];
    });
}
export function toArray<T>(generator: () => Generator<T>) : T[] {
    return [...generator()]
}
export function invert(object: Record<string, string>) {
    return Object.fromEntries(Object.entries(object).map(([key, val]) => [val, key] as const));
}