<?php
class Imagick_PNG_Creator extends PNG_Creator {

	private Imagick $imagick;
	
	public function __construct() {
		$this->imagick = new Imagick();
	}

	public function get_thumb(string $svg_text, int $height): string {
		$read_success = $this->imagick->readImageBlob($svg_text);
		if (!$read_success) {
			throw new CantOpenFileException(substr($svg_text, 0, 500));
		}
		$ratio = $this->imagick->getImageWidth() / $this->imagick->getImageHeight();
		$this->imagick->thumbnailImage((int)($height * $ratio), $height, true);
		$this->imagick->setImageFormat("png");
		$data = $this->imagick->getImageBlob();
		$this->imagick->clear();
		
		return $data;
	}
}
