<?php

interface LogFormatter {

	function doFormat(string $level, string $file, int $line, string $message): string;

}
