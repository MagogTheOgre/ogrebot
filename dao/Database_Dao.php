<?php

/**
 * 
 * @author magog
 *
 */
interface Database_Dao {    
    /**
     * 
     * @param string $environment
     * @param string[] $names
     * @param string[] $fields
     * @return array
     * @throws BaseException
     */
    public function read_metadata( string $environment, array $names, array $fields) : array ;
    
    /**
     * 
     * @param string $environment
     * @param int[] $tagIds
     * @param int $startTime
     * @param int $endTime
     * @return array
     * @throws BaseException
     */
    public function get_pages_with_tags( string $environment, array $tagIds, int $startTime,
        int $endTime): array;

    /**
     * @param string $environment
     * @param string[] $categories
     * @param int $limit
     * @return array(int, string, array(int, string))[]
     * @throws BaseException
     */
    public function get_pages_with_redirects_in_categories(string $environment, array $categories,
                                                           int $limit): array;

    /**
     * @param string $environment
     * @param int $namespace
     * @param string[] $titles
     * @param int $limit
     * @return array(int, string)[]
     * @throws BaseException
     */
    public function get_redirects(string $environment, int $namespace, array $titles,
                                  int $limit): array;
    /**
     * @param string $environment
     * @param int $namespace
     * @param string[] $titles
     * @param int $limit
     * @return array(int, string)[]
     * @throws BaseException
     */
    public function get_redirects_to(string $environment, int $namespace, array $titles,
                                  int $limit): array;

    /**
     * @param string $environment
     * @param int $namespace
     * @param string[] $pageNames
     * @return string[][]
     * @throws BaseException
     */
    public function get_visible_categories(string $environment, int $namespace, array $pageNames):
        array;
}