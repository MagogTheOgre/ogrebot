/// <reference path="../../../../js/node_modules/@types/jquery/index.d.ts" />
/// <reference path="../../../../js/node_modules/@types/jqueryui/index.d.ts" />
/// <reference path="./generateUniqueClass.ts" />
const tooltipContent = "tooltip-content";

class TooltipsFactory {
    constructor() {
        (async function () {
            await $.ready;
            $(".tooltip-div")
                .on("click", e => void e.preventDefault())
                .each((_, element) => {
                    const $element = $(element);
                    const id = $element.attr("id");
                    const tooltipClass = $element.data("tooltipClass");
                    var span = $("<span class='glyphicon glyphicon-question-sign tooltip-icon'/>");

                    const uniqueClass = generateUniqueClass("tooltip");
                    const uniqueClassSelector = `.${uniqueClass}`;

                    function keepOpen() {
                        $element.tooltip("open");
                    }

                    if (tooltipClass) {
                        span.addClass(tooltipClass);
                    }

                    span = span.prependTo(element);

                    $element.tooltip({
                        content() {
                            return $(".tooltip-text").filter((_, element) => $(element).data("for") === id).html();
                        },
                        close() {
                            if ($element.data("keepOpen")) {
                                keepOpen();
                            }
                        },
                        open() {
                            $(uniqueClassSelector).on("hover", keepOpen, () => void $element.tooltip("close"));
                        },
                        items: $element,
                        hide: 300,
                        tooltipClass: uniqueClass
                    }).data(tooltipContent, uniqueClassSelector);

                    $("*").on("click", event => {
                        var isKeepOpen = !!$(event.target).closest($element.add(uniqueClassSelector))[0];

                        $element.data("keepOpen", isKeepOpen);
                        if (isKeepOpen) {
                            keepOpen();
                        } else {
                            $element.tooltip("close");
                        }
                    });
                });
        })();
    }

    async close(jq:  JQuery<any>) {
        await $.ready;
        jq.data("keepOpen", 0).tooltip("close");
    }
}