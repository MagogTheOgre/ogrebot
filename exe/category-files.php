<?php
require_once __DIR__ . "/../base/bootstrap.php";
global $env;

/* parse command line variables */
$argv = $env->load_command_line_args();
$file_override = Array_Utils::find_command_line_arg($argv, "FILE");
$log = Array_Utils::find_command_line_arg($argv, "NOLOG") === null;
$startArg = $argv[1] ?? date('Ymd', time() - SECONDS_PER_DAY);
$endArg = $argv[2] ?? date('Ymd', strtotime($startArg));

//$startArg = "20210801000000";
//$endArg = "20210802000000";

//$file_override = ARTIFACTS_DIRECTORY . "/filestuff.txt";
//$env->get_wiki_interface()->set_live_edits(false);
$tree_logger = $log ? RollingCategoryTreeLogger::class : NoopCategoryTreeLogger::class;
$outputter = new Category_Files_Outputter(new Project_Data("commons.wikimedia"), $file_override, $tree_logger);
$iterator = new Wiki_Date_Iterator(
    new Wiki_Date($startArg, Wiki_Date_Rounding::ROUND_DOWN),
    new Wiki_Date($endArg, Wiki_Date_Rounding::ROUND_UP)
);
foreach ($iterator as [$start, $end]){
    $outputter->run($start, $end);
}

?>
