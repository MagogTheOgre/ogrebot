<?php

/**
 * 
 * @author magog
 *
 */
class Download_Util extends Remote_Util implements File_Downloader {
    
    /**
     * 
     * @var string[]
     */
    private $paths = [];
    
    
    /**
     *
     * @param string $url
     * @param string $path
     * @return void
     */
    private function add_path_impl(string $url, string $path): void {
        global $logger;
        if (isset($this->paths[$url]) && $this->paths[$url] !== $path) {
            $logger->warn("Different download paths set for url $url: $path vs. " . $this->paths[$url]);
        }
        $this->paths[$url] = $path;
    }
    
    /**
     * 
     * @param string $url
     * @param string $path
     * @return void
     */
    public function add_path(string $url, string $path): void {
        $this->add_path_impl($url, $path);
    }

	/**
	 * 
	 * {@inheritDoc}
	 * @see Remote_Util::execute()
	 * @throws CURLError, IllegalStateException
	 */
    public function execute(string $url): Remote_Response {
	    $path = $this->paths[$url];
	    if (!$path) {
	        throw new IllegalStateException("Download path not set for $url.");
	    }
	    $response = parent::execute($url);
	    $response->filename = $path;
	    Local_Io::file_put_contents_ensure($path, $response->response_text);
	    return $response;
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see File_Downloader::download()
	 */
	public function download(string $url, string $dest): void {
	    $this->add_path_impl($url, $dest);
        $this->execute($url);
    }

}