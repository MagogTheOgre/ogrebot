<?php

class Page_Text_Response {

	public bool $exists = false;

	public ?string $redirect_from = null;

	public ?string $redirect_to = null;

	public ?string $text = null;

    public ?string $normalized_from = null;

    public ?string $normalized_to = null;

}