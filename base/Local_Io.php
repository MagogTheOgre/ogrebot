<?php

class Local_Io {
    
    
    /**
     *
     * @param string|resource $directory
     * @param bool $include_directories
     *        	DEFAULT false
     * @return string[]
     * @throws CantOpenFileException If $directory is a string and the directory can't be opened
     * @throws IllegalArgumentException if $directory is a file, not a directory
     */
    static function get_all_files_in_directory(string $directory, bool $include_directories = false) {
        global $logger;
        
        $logger->debug("Opening files in directory $directory");
        
        $handle = opendir($directory);
        
        if ($handle === null) {
            throw new CantOpenFileException("Can't open directory $directory");
        }
        
        try {
            if (!is_dir($directory)) {
                throw new IllegalArgumentException("$directory is not a directory.");
            }
            
            $all_files = array();
            if (String_Utils::str_ends_with($directory, DIRECTORY_SEPARATOR)) {
                $directory = substr($directory, 0, strlen($directory) -1 );
            }
            while (($next = readdir($handle)) !== false) {
                $full_path = $directory . DIRECTORY_SEPARATOR . $next;
                if ($next === '.' || $next === '..') {
                    $logger->trace("Directory skipped: $next");
                    continue;
                }
                if (is_dir($full_path)) {
                    $logger->trace("Directory skipped: $next");
                    continue;
                }
                $logger->trace("File found: $next");
                $all_files[] = $include_directories ? $full_path : $next;
            }
        } finally {
            closedir($handle);
        }
        
        $logger->debug(count($all_files) . "  files found.");
        
        return $all_files;
    }
    
    
    /**
     *
     * @param string $command
     * @return int
     */
    static function ogrebotExec(string $command): int {
        global $logger;
        
        $logger->info("Running:\n$command");
        
        $output = []; //for PDT
        $return_var = 0; //for PDT
        exec($command, $output, $return_var);
        
        $logger->info(" ...done");
        
        $output = join(PHP_EOL, $output);
        if ($return_var !== 0) {
            Remote_Io::ogrebotMail("Command $command FAILED. Output: $output. Return status: $return_var.");
        } else {
            $logger->info("Success; output: $output");
        }
        
        return $return_var;
    }

    /**
     * http://bytes.com/groups/php/6770-how-read-stdin-using-php
     * @param int $length
     * @return string
     */
    public static function stdin_get($length = 255)
    {
        $fr = fopen("php://stdin", "r");
        $input = fgets($fr, $length);
        $input = rtrim($input);
        fclose($fr);
        return $input;
    }

    /**
     * Prints a message to standard error stream.
     * 
     * @param mixed $msg        	
     * @return void
     */
    public static function stderr_print($msg)
    {
        static $logfd;

        if (!is_resource($logfd)) {
            $logfd = fopen('php://stderr', 'w');
        }
        fwrite($logfd, $msg);
    }

    public static function logger_or_stderr(Level $level, mixed $message): void {
        global $logger;

        if (isset($logger)) {
            $logger->log($level, $message, 1);
        } else if ($level <= Level::WARN) {
            Local_Io::stderr_print("$level->name :: $message\n");
        }
    }

    /**
     *
     * @throws CantOpenFileException
     */
    public static function file_get_contents_ensure(string $filename, bool $use_include_path = false): string {
        $text = file_get_contents($filename, $use_include_path);

        if ($text === false) {
            throw new CantOpenFileException($filename);
        }
        return $text;
    }

    /**
     *
     * @param string $filename
     * @param mixed $data
     * @param int $flags
     * @return int the number of bytes returns by file_put_contents
     * @throws CantWriteToFileException
     */
    public static function file_put_contents_ensure(string $filename, $data, $flags = 0)
    {
        $bytes = file_put_contents($filename, $data, $flags);

        if ($bytes === false) {
            throw new CantWriteToFileException($filename);
        }

        return $bytes;
    }

    /**
     *
     * @param string $filename
     * @param string $mode
     * @return resource
     * @throws CantOpenFileException
     */
    public static function fopen_ensure(string $filename, string $mode) {
        $handle = fopen($filename, $mode);

        if ($handle === false) {
            throw new CantOpenFileException($filename);
        }

        return $handle;
    }

    /**
     *
     * @param string $filename
     * @param string $text
     * @return void
     * @throws CantOpenFileException
     * @throws CantWriteToFileException
     */
    public static function write_to_log($filename, $text)
    {
        global $logger, $validator;

        try {
            $validator->validate_arg($filename, "string");
            $validator->validate_arg($text, "string");
        } catch (AssertionFailureException $e) {
            $filename = "$filename";
            $text = "$text";
            Remote_Io::ogrebotMail($e);
        }

        if (strlen($text) === 0) {
            Remote_Io::ogrebotMail("Appending empty text to log ignored.");
            return;
        }

        $logger->debug("write_to_log(\"$filename\", \"$text\")");

        $log = fopen($filename, 'a');

        if (!$log) {
            throw new CantOpenFileException($filename);
        }

        if (!fwrite($log, $text)) {
            throw new CantWriteToFileException($filename);
        }
    }

    /**
     *
     * @param string $filename
     * @param bool $short_name
     * @return array
     * @throws ParseException
     */
    public static function load_property_file($filename, $short_name = true): array {
        if ($short_name) {
            $filename = BASE_DIRECTORY . "/properties/$filename.properties";
        }

        Local_Io::logger_or_stderr(Level::TRACE, "load_property_file_into_variable(\$var, $filename)");

        $out = parse_ini_file($filename);
        if ($out === false) {
            throw new ParseException("Can't parse $filename");
        }

        if ($filename === BASE_DIRECTORY . "/properties/secrets.properties") {
            $debug_data = array_fill_keys(array_keys($out), "[SCRUBBED]");
        } else {
            $debug_data = &$out;
        }

        self::log_property_data($debug_data);
        return $out;
    }

    /**
     *
     * @param string $prompt
     * @throws IllegalStateException
     */
    public static function read_password_from_stdin($prompt = "Enter password: ")
    {
        if (php_sapi_name() !== "cli") {
            throw new IllegalStateException("Can't read from stdin for web server request");
        }

        $unix = !preg_match('/^win/i', PHP_OS);

        if ($unix) {
            echo $prompt;
            try {
                $command = "/usr/bin/env bash -c 'stty -echo; read password && echo \"\$password\"'";
                $password = shell_exec($command);
                echo "\n";
            } catch (Exception $e) {
                // generic handler to account for stty problems after mistyped password
                Remote_Io::ogrebotMail($e);
                throw new IllegalStateException($e->getMessage());
            }
        } else {
            Local_Io::stderr_print("Warning: Windows; can't blank password input.\n");
            readline($prompt);
        }

        return String_Utils::mb_trim($password);
    }

    /**
     *
     * @param array $data
     * @return void
     */
    public static function log_property_data($data)
    {
        global $logger;

        if (isset($logger) && $logger->isTraceEnabled()) {
            $logger->trace(count($data) . " entries found.");
            $constantData = "Data:";
            foreach ($data as $key => $val) {
                if (!is_string($val)) {
                    $val = str_replace("\n", "\n\t", print_r($val, true));
                }
                $constantData .= "\n  $key => $val";
            }
            $logger->trace($constantData);
        }
    }
    
    /**
     * 
     * @param string $filename
     * @param mixed $default
     * @param callable | string $serializer
     * @return mixed
     */
    public static function deserialize(string $filename, $default = false, $serializer = "unserialize") {
        $file_contents = file_exists($filename) ? file_get_contents($filename) : false;
        return $file_contents ? $serializer($file_contents) : $default;
    }

    /**
     *
     * @param string $filename
     * @param mixed $default
     * @return mixed
     */
    public static function json_decode(string $filename, $default = false) {
        return self::deserialize($filename, $default, "json_decode");
    }
}

