<?php

/**
 * 
 * @author magog
 *
 */
interface File_Downloader {
    
    /**
     * 
     * @param string $url
     * @param string $dest
     * @return void
     */
    function download(string $url, string $dest): void;
}

