import { invert, mapEntries } from "../collectionUtils";

const COMBINED_COUNTIES : Record<string, string[]> = {
    "New York_NY": [
        "Bronx_NY",
        "Kings_NY",
        "Queens_NY",
        "Richmond_NY"
    ]
};
const STATE_ABBREVIATIONS: Readonly<Record<string, string>> = Object.freeze({
    "Alabama": "AL",
    "Alaska": "AK",
    "Arizona": "AZ",
    "Arkansas": "AR",
    "California": "CA",
    "Colorado": "CO",
    "Connecticut": "CT",
    "Delaware": "DE",
    "District of Columbia": "DC",
    "Florida": "FL",
    "Georgia": "GA",
    "Hawaii": "HI",
    "Idaho": "ID",
    "Illinois": "IL",
    "Indiana": "IN",
    "Iowa": "IA",
    "Kansas": "KS",
    "Kentucky": "KY",
    "Louisiana": "LA",
    "Maine": "ME",
    "Maryland": "MD",
    "Massachusetts": "MA",
    "Michigan": "MI",
    "Minnesota": "MN",
    "Mississippi": "MS",
    "Missouri": "MO",
    "Montana": "MT",
    "Nebraska": "NE",
    "Nevada": "NV",
    "New Hampshire": "NH",
    "New Jersey": "NJ",
    "New Mexico": "NM",
    "New York": "NY",
    "North Carolina": "NC",
    "North Dakota": "ND",
    "Ohio": "OH",
    "Oklahoma": "OK",
    "Oregon": "OR",
    "Pennsylvania": "PA",
    "Rhode Island": "RI",
    "South Carolina": "SC",
    "South Dakota": "SD",
    "Tennessee": "TN",
    "Texas": "TX",
    "Utah": "UT",
    "Vermont": "VT",
    "Virginia": "VA",
    "Washington": "WA",
    "West Virginia": "WV",
    "Wisconsin": "WI",
    "Wyoming": "WY"
});
export default class CovidConstants {

    static readonly COMBINED_COUNTIES = Object.freeze(COMBINED_COUNTIES);

    static readonly COMBINED_COUNTIES_INV = Object.freeze(mapEntries(COMBINED_COUNTIES, (parent: string, children: string[]) => 
        children.map(child => [child, parent])));

    static readonly STATE_ABBREVIATIONS: Readonly<Record<string, string>> = Object.freeze(STATE_ABBREVIATIONS);

    static readonly STATE_ABBREVIATIONS_INV: Readonly<Record<string, string>> = 
        Object.freeze(invert(STATE_ABBREVIATIONS));
}