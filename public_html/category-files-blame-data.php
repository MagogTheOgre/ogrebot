<?php
require_once __DIR__ . "/../base/bootstrap.php";

$args = Environment::get()->get_request_args();

$file = Category_Files_Log_Entry::get_json_file(+preg_replace("/\D/", "", $args['date']), $args['gallery']);

header('content-type: application/json; charset=utf-8');
header('Cache-Control: max-age=' . SECONDS_PER_DAY);

// stream the file
if ($file) {
    header('content-length: ' . filesize($file));
    fpassthru(fopen($file, 'rb'));
} else {
    header('content-length: 2');
    echo "[]";
}

