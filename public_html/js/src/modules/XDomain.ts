/// <reference path="../../../../js/node_modules/@types/jquery/index.d.ts" />
/// <reference path="./setIntervalImmediate.ts" />
/// <reference path="../types/JQuery.isReady.d.ts" />

class XDomain {
    
    private listeners = [] as ((data: any) => any)[];
    private contentWindow: Window;
    private origin: string;

    constructor({contentWindow}: HTMLIFrameElement, xdDomain: string) {

        this.contentWindow = contentWindow;

        this.origin = xdDomain;

        addEventListener("message", ({origin, data}) => {
            if (origin !== xdDomain) {
                return;
            }

            this.listeners.forEach(listener => void listener(data));
        });
    }

    addListener(listener: (data: any) => any) {
        this.listeners.push(listener);
    }

    postMessage(message: any) {
        this.contentWindow.postMessage(message, this.origin);
    }

    static promise() {
        return new Promise<XDomain>((resolve, reject) => {
            setIntervalImmediate(() => {
                const input = document.querySelector("input[data-xd-auto]") as HTMLInputElement;

                if (input) {
                    function stopListeningForPing() {
                        removeEventListener("message", checkedResolved);
                        if (failTimeout) {
                            clearTimeout(failTimeout);
                        }
                    }
                  
                    function checkedResolved ({origin, data}: MessageEvent) {
                        if (origin !== xdDomain) {
                            return;
                        }

                        if (data === "ping") {
                            resolve(new XDomain(iframe[0], xdDomain));
                            stopListeningForPing();
                        }
                    }
                    
                    const { xdDomain, xdPath, xdTimeout } = $(input).data() as
                        { xdDomain: string, xdPath: string, xdTimeout?: string };
                   
                    const iframe = $("<iframe/>")
                        .attr({ style: "display: none", src: `${xdDomain}/${xdPath}` })
                        .appendTo("body") as JQuery<HTMLIFrameElement>;

                    const failTimeout = xdTimeout && setTimeout(() => {
                        reject();
                        stopListeningForPing();
                    }, +xdTimeout);

                    addEventListener("message", checkedResolved);

                    return false;
                }

                const {isReady} = $;
                if (isReady) {
                    reject();
                }

                return !isReady;
            }, 100);
        });
    }
};