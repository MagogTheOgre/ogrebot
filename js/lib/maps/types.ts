export type Party = "r"|"d";
export type PartyOrTie = Party | "t";

export type UnPromisify<T> = T extends Promise<infer U> ? U : T;