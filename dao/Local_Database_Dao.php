<?php

/**
 * 
 * @author magog
 *
 */
class Local_Database_Dao implements Database_Dao {

    const MAX_QUERY = ComputedConstants::DATABASE_MAX_QUERY;

    const REMOTE_ENABLED = [
        "read_metadata",
        "get_pages_with_tags",
        "get_pages_with_redirects_in_categories",
        "get_redirects",
        "get_redirects_to",
        "get_visible_categories"
    ];


    /**
     * @var String_Replacer
     */
    private $space_to_underbar;

    /**
     * @var String_Replacer
     */
    private $underbar_to_space;

    public function __construct() {
        $this->space_to_underbar = new Deep_String_Replacer(" ", "_", true);
        $this->underbar_to_space = new Deep_String_Replacer("_", " ", true);
    }

    /**
     * 
     * {@inheritDoc}
     * @see Database_Dao::read_metadata()
     */
    public function read_metadata(string $environment, array $names, array $fields): array {
        $logger = Environment::get()->get_logger();

        $names = $this->space_to_underbar->replace($names, 1);
        $logger->debugWithArrays("read_metadata($environment, %s, %s)", $names, $fields);

        try {
            $connection = ConnectionManager::getConnection($environment);
            $statement = new FileStatement("read_metadata", $connection);
            $metadata = [];
            for ($i = 0; $i < count($names); $i += self::MAX_QUERY) {
                $image_names = array_slice($names, $i, min(count($names) - $i, self::MAX_QUERY));
                $statement->array_string($image_names);
                $all = $statement->execute(MYSQLI_NUM);
    
                foreach ($all as list ($name, $metadata_string)) {
                    if ($name) {
                        $name = $this->underbar_to_space->replace($name);
                        $parsed_metadata = $metadata_string ? unserialize($metadata_string)? : [] : [];
                        $metadata[$name] = array_filter($parsed_metadata, function (string $key) use (&$fields) {
                            return in_array($key, $fields);
                        }, ARRAY_FILTER_USE_KEY);
                    }
                }
                
            }
        } finally {
            ConnectionManager::close($statement, $connection);
        }

        return $metadata;
    }

    /**
     *
     * {@inheritDoc}
     * @see Database_Dao::get_pages_with_tags()
     */
    public function get_pages_with_tags(string $environment, array $tagIds, int $startTime, 
        int $endTime): array {
        global $logger;

        $logger->debugWithArrays("get_pages_with_tags($environment, [%s], $startTime, $endTime,)",
            $tagIds);

        try {
            $connection = ConnectionManager::getConnection($environment);
            $statement = new FileStatement("get_pages_with_tags", $connection);
            return $this->underbar_to_space->replace(
                $statement->array_int($tagIds)->int($startTime)->int($endTime)->execute(MYSQLI_NUM), 2);
        } finally {
            ConnectionManager::close($statement, $connection);
        }
    }

    /**
     * {@inheritDoc}
     * @see Database_Dao::get_pages_with_redirects_in_categories()
     */
    public function get_pages_with_redirects_in_categories(string $environment, array $categories, int $limit): array
    {
        $logger = Environment::get()->get_logger();
        $logger->debugWithArrays("get_pages_with_redirects_in_categories($environment, [%s], $limit)",
            $categories);

        $categories = $this->space_to_underbar->replace($categories, 1);
        $results = [];
        try {
            $connection = ConnectionManager::getConnection($environment);
            $pages = $this->get_pages_in_categories_with_connection($connection, $categories, $limit);

            $pages_by_namespace = Array_Utils::chunk($pages, [Array_Utils::class, "identity"]);
            foreach ($pages_by_namespace as $namespace => $titles) {
                /** @var int $namespace */
                /** @var string[] $titles */
                $redirects = $this->get_redirects_with_connection($connection, $namespace, $titles, $limit);
                foreach ($titles as $title) {
                    $results[] = [$namespace, $title, $redirects[$title] ?? []];
                }
            }
            return $this->underbar_to_space->replace($results, 4);
        } finally {
            ConnectionManager::close($connection);
        }
    }

    /**
     * {@inheritDoc}
     * @see Database_Dao::get_redirects()
     */
    public function get_redirects(string $environment,int $namespace, array $titles,
                                  int $limit): array {
        $logger = Environment::get()->get_logger();
        $logger->debugWithArrays("get_pages_with_redirects_in_categories($environment," .
            " $namespace, [%s], $limit)", $titles);

        $connection = ConnectionManager::getConnection($environment);
        try {
            $results = $this->get_redirects_with_connection($connection, $namespace, $titles, $limit);
            return $this->underbar_to_space->replace($results, 3);
        } finally {
            ConnectionManager::close($connection);
        }
    }


    /**
     * {@inheritDoc}
     * @see Database_Dao::get_redirects_to()
     */
    public function get_redirects_to(string $environment, int $namespace, array $titles,
                                     int $limit): array {
        $logger = Environment::get()->get_logger();
        $logger->debugWithArrays("get_redirects_to($environment, $namespace, [%s], $limit)", $titles);
        $titles = $this->space_to_underbar->replace($titles, 1);

        $connection = ConnectionManager::getConnection($environment);
        try {
            $statement = new FileStatement("get_redirects_to", $connection);
            $redirects = Array_Utils::chunk($statement->int($namespace)->array_string($titles)->limit($limit)->
                execute(MYSQLI_NUM), function(array $row) {
                return [$row[0], [$row[1], $row[2]]];
            });
            return $this->underbar_to_space->replace($redirects, 3);
        } finally {
            ConnectionManager::close($statement, $connection);
        }
    }

    public function get_visible_categories(string $environment, int $namespace, array $pageNames): array {
        $connection = ConnectionManager::getConnection($environment);
        try {
            $categories = $this->get_categories_with_connection($connection, "get_categories",
                $namespace, $pageNames);
            $hiddenCategories = $this->get_categories_with_connection($connection, "get_hidden_categories",
                $namespace, $pageNames);

            return Array_Utils::map_array_function_keys($pageNames,
                function (string $page) use (&$categories, &$hiddenCategories): array {
                    return [$page, array_values(array_diff($categories[$page] ?? [],
                        $hiddenCategories[$page] ?? []))];
                });
        } finally {
            ConnectionManager::close($connection);
        }
    }

    /**
     * @param mysqli $connection
     * @param int $namespace
     * @param array $page_names
     * @param int $limit
     * @return array(int, string)[]
     * @throws CantOpenFileException
     * @throws SQLException
     * @throws SQLLimitException
     */
    private function get_redirects_with_connection(mysqli $connection, int $namespace,
                                                   array $page_names, int $limit): array {
        $logger = Environment::get()->get_logger();
        $logger->debugWithArrays("get_redirects_with_connection($namespace, [%s], $limit)",
            $page_names);
        $page_names = $this->space_to_underbar->replace($page_names, 1);
        $statement = new FileStatement("get_redirects", $connection);
        try {
            return Array_Utils::chunk($statement->int($namespace)->array_string($page_names)->limit($limit)->
                execute(MYSQLI_NUM), function(array $row) {
                return [$row[2], [$row[0], $row[1]]];
            });
        } finally {
            $statement->close();
        }
    }

    /**
     * @param mysqli $connection
     * @param string[] $categories
     * @param int $limit
     * @return array(int, string, string)[]
     * @throws CantOpenFileException
     * @throws SQLException
     * @throws SQLLimitException
     */
    private function get_pages_in_categories_with_connection(mysqli $connection,
                                                array $categories, int $limit): array {
        $logger = Environment::get()->get_logger();
        $logger->debugWithArrays("get_pages_in_categories_with_namespace([%s], $limit)",
            $categories);
        $categories = str_replace(" ", "_", $categories);
        $statement = new FileStatement("get_pages_in_categories_with_namespace", $connection);
        try {
            return $statement->array_string($categories)->limit($limit)->execute(MYSQLI_NUM);
        } finally {
            $statement->close();
        }
    }

    /**
     * @param mysqli $connection
     * @param string $sqlFile
     * @param int $namespace
     * @param string[] $pageNames
     * @return string[][]
     * @throws CantOpenFileException
     * @throws SQLException
     * @throws SQLLimitException
     */
    private function get_categories_with_connection(mysqli $connection, string $sqlFile, int $namespace,
                                    array $pageNames): array {
        $logger = Environment::get()->get_logger();
        $logger->debugWithArrays("get_categories($sqlFile, $namespace, [%s])",
            $pageNames);
        $pageNames = $this->space_to_underbar->replace($pageNames, 1);
        $statement = new FileStatement($sqlFile, $connection);
        try {
            $results = $this->underbar_to_space->replace($statement->int($namespace)->array_string(
                $pageNames)->execute(MYSQLI_NUM), 3);
            return Array_Utils::chunk($results, [Array_Utils::class, "identity"]);
        } finally {
            $statement->close();
        }
    }
}