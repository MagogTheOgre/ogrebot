export interface ThreadPool {
    enqueue<T>(action: (() => Promise<T>)): Promise<T>;
    enqueueAll<T>(actions: (() => Promise<T>)[] | (() => Iterable<(() => Promise<T>)>)): Promise<T[]>;
}

//Windows freezes with processor-intensive multithreading
const DEFAULT_THREAD_POOL_SIZE = process.platform === "win32" ? 2 : 10

export class ThreadPoolImpl implements ThreadPool {

    private actions: (() => Promise<void>)[] = [];
    private _threadCount: number = 0;
    private shutDownHook? : () => void;
    private shutdown = false;

    constructor(private maxThreads: number = DEFAULT_THREAD_POOL_SIZE) { 
        process.once('SIGINT', () => {
            console.info("Shutdown signal received; shutting down gracefully");
            this.shutdown = true;
        });
    }

    enqueue<T>(action: (() => Promise<T>)) : Promise<T> {
        return this.enqueueAll([action]).then(res => res[0]);
    }

    enqueueAll<T>(actions: (() => Promise<T>)[] | (() => Iterable<(() => Promise<T>)>)) {
        return Promise.all((actions instanceof Function ? [...actions()] : actions).map(action =>
            new Promise<T>((resolve, reject) => {
                this.actions.push(() => {
                    return this.shutdown ? Promise.reject() : action().finally(() => void this._threadCount--).then(resolve,
                        reject).then(() => this.run());
                });
                this.run();
            })));
    }
    
    private run() {
        var action;
        while (this._threadCount < this.maxThreads && (action = this.actions.shift())) {
            this._threadCount++;
            action();
        }
        if (!this._threadCount && this.shutDownHook) {
            this.shutDownHook();
        }
    }
}

export class AllSettledThreadPoolImpl extends ThreadPoolImpl {

    enqueueAll<T>(actions: (() => Promise<T>)[] | (() => Iterable<(() => Promise<T>)>)) {
        return Promise.allSettled((actions instanceof Function ? [...actions()] : actions).
            map(action => super.enqueueAll([action]).then(res => res[0])))
            .then(results => results.flatMap(res => {
                if (res.status === "fulfilled") {
                    return [res.value];
                } else {
                    console.error(res.reason);
                    return [];
                }
            }));
    }
}