<?php
/**
 * 
 * @author magog
 *
 */
class Statement {
    
    /**
     * 
     * @var string
     */
    private $sql;
    
    /**
     * 
     * @var string
     */
    private $types = "";
    
    /**
     * 
     * @var string[]
     */
    private $replacements = [];
    
    /**
     *
     * @var string[]
     */
    private $arrays = [];
    
    /**
     * 
     * @var mysqli
     */
    private $connection;

    /**
     *
     * @var mysqli_stmt|null
     */
    private $statement;

    /**
     *
     * @var mysqli_result|null
     */
    private $result;

    /**
     * @var int
     */
    private $limit = -1;

    /**
     * Statement constructor.
     * @param string $sql
     * @param mysqli $connection
     */
    function __construct(string $sql, mysqli $connection) {
        $this->sql = $sql;
        $this->connection = $connection;
    }

    function close(): void {
        ConnectionManager::close($this->result, $this->statement);
    }
    
    function int(int $int): self {
        $this->types .= "d";
        $this->replacements[] = "$int";
        return $this;
    }
    
    function string(string $string): self {
        $this->types .= "s";
        $this->replacements[] = $string;
        return $this;
    }
    
    
    function array_int(array $ints): self {
        global $validator;
        $validator->validate_arg_array($ints, "integer");
        $this->arrays[] = join(",", $ints);
        return $this;
    }
    
    /**
     * 
     * @param string[] $strings
     */
    function array_string(array $strings): self {
        $this->arrays[] = join(",", array_map(function(string $string): string {
            return "'{$this->connection->escape_string($string)}'";
        }, $strings));
        return $this;
    }

    function limit(int $limit): self {
        $this->limit = $limit;
        return $this;
    }

    /**
     *
     * @param int|null $resultType
     * @return array[]
     * @throws SQLException
     * @throws SQLLimitException
     */
    function execute(int $resultType = null): array {
        $logger = Environment::get()->get_logger();

        $timer = new Timer();
        $count = 0;
        $preparedStatement = preg_replace_callback(
            "/\?\?/", 
            function () use (&$count) {
               return $this->arrays[$count++];
            },
            $this->sql);

        $logger->trace("SQL is $preparedStatement");

        if ($this->limit >= 0) {
            $preparedStatement = $preparedStatement . " LIMIT " . ($this->limit + 1);
        }

        $statement = $this->connection->prepare($preparedStatement);
        if ($this->types) {
            $statement->bind_param($this->types, ...$this->replacements);
        }
        
        if (!$statement->execute()) {
            throw new SQLException("Failed with $statement->error [$statement->errno]");
        }
        $this->statement = $statement;
        $this->result = $result = $statement->get_result();
        try {
            $result = $result->fetch_all($resultType);

            if ($this->limit >=0 && count($result) > $this->limit) {
                throw new SQLLimitException("Result count greater than $this->limit");
            }
            return $result;
        } finally {
            $this->types = "";
            $this->arrays = [];
            $this->replacements = [];
            $logger->info("DB statement took {$timer->get_diff()} ms");
        }
    }

}