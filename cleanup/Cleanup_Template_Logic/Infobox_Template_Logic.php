<?php
/**
 * A class for the actual information template
 * @author magog
 *
 */
class Infobox_Template_Logic extends Infobox_Style_Template_Logic {
	
	/**
	 *
	 * @var string[]
	 */
	private $names;
	
	/**
	 * 
	 */
	public function __construct() {
        $xml_template_type = XmlTemplateType::get_all_types()["Infobox"];
        $this->names = array_fill_keys(
            Array_Utils::flat_map($xml_template_type->get_xmlTemplates(),
                fn(XmlTemplate $xml_template) => $xml_template->get_aliases_and_name()), 1);
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see Whitespace_Preserve_Template_Logic::is_add_carriage_return_new_field()
	 */
	protected function is_add_carriage_return_new_field() {
		return true;
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see Template_Logic::is_eligible()
	 */
	public function is_eligible(Template $template) {
		return isset($this->names[String_Utils::ucfirst_utf8(String_Utils::mb_trim($template->getname()))]);
	}
}