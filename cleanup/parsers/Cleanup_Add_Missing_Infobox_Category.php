<?php
/**
 * 
 * @author magog
 *
 */
class Cleanup_Add_Missing_Infobox_Category implements Cleanup_Module {
	
	
	/**
	 *
	 * @var Template_Factory
	 */
    private $template_factory;

    public function __construct() {
        $this->template_factory = new New_Infobox_Template_Factory();
	}
	
	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see Cleanup_Module::cleanup()
	 */
	public function cleanup(Cleanup_Instance $ci) {
	    $logger = Environment::get()->get_logger();

        $text = $ci->get_text();
		$iterator = new Template_Iterator($text, $this->template_factory);
		if ($iterator->valid()) {
		    $template = String_Utils::mb_trim($iterator->current()->getname());
		    $logger->debug("Template of type $template found.");
        } else {
            //check category isn't already present
            $logger->info("No infobox template found. Adding media missing category.");
		    $ci->add_category("Media missing infobox template");
        }
	}
}