<?php

/**
 * Low level class to replace string recursively with bars
 */
interface String_Replacer
{
    /**
     * @param mixed $input
     * @param int $depth
     * @return mixed
     */
    function replace($input, int $depth = 0);
}