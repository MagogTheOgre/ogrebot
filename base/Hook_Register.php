<?php

/**
 * 
 * @author magog
 *
 */
class Hook_Register {
	

	/**
	 * 
	 * @var Hook[]
	 */
	private array $hooks = [];


	private function get_hook(string $name): Hook {
        return $this->hooks[$name] ??= new Hook();
	}

	/**
	 * @throws IllegalStateException
	 */
	public function add(string $name, callable $callable): void {
        $this->get_hook($name)->add($callable);
	}


	/**
	 *
	 * @throws IllegalStateException
	 */
	public function trigger(string $name): void {
        $this->get_hook($name)->trigger();
	} 
}