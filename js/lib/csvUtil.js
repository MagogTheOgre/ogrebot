"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.csvToObject = void 0;
const sync_1 = __importDefault(require("csv-parse/lib/sync"));
function csvToObject(fileContent) {
    const [fields, ...results] = sync_1.default(fileContent);
    return results.map(resultArray => Object.fromEntries(resultArray.map((value, index) => [fields[index], value])));
}
exports.csvToObject = csvToObject;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3N2VXRpbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNzdlV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsOERBQTBDO0FBRzFDLFNBQWdCLFdBQVcsQ0FBbUIsV0FBbUI7SUFDN0QsTUFBTSxDQUFDLE1BQU0sRUFBRSxHQUFHLE9BQU8sQ0FBQyxHQUFlLGNBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUMvRCxPQUFPLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBa0QsTUFBTSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxFQUFFLENBQ25JLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ2xDLENBQUM7QUFKRCxrQ0FJQyJ9