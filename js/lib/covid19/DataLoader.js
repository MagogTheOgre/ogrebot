"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const xml2js_1 = __importDefault(require("xml2js"));
const XmlCounty_1 = __importDefault(require("./County/XmlCounty"));
const cachable_1 = require("../decorators/cachable");
const io_1 = __importDefault(require("../io"));
const csvUtil_1 = require("../csvUtil");
const CovidUtils_1 = __importDefault(require("./CovidUtils"));
const Constants_1 = __importDefault(require("./Constants"));
const GIT_DIR = `${io_1.default.PROJECT_DIR}/../git`;
const text = io_1.default.readFile(`${io_1.default.PROJECT_DIR}/county-maps/USA counties.svg`);
class DataLoader {
    //private static globalStates = DataLoader.getStates().then(([states]) => states);
    static async getPopulations() {
        const fileContent = await io_1.default.readFile(`${GIT_DIR}/co-est2019-alldata.csv`);
        const populations = csvUtil_1.csvToObject(fileContent).flatMap(({ COUNTY, STATE, STNAME, CTYNAME, POPESTIMATE2019 }) => +COUNTY && CTYNAME && STNAME ? {
            fips: +STATE * 1000 + +COUNTY,
            county: CTYNAME,
            state: STNAME,
            population: +POPESTIMATE2019
        } : []);
        (function combineCounties() {
            for (const populationsI of populations) {
                const { county, state } = populationsI;
                const countyState = CovidUtils_1.default.getCountyString(county, state);
                const combinedCounties = Constants_1.default.COMBINED_COUNTIES[countyState];
                if (combinedCounties) {
                    var updates = false;
                    for (var j = 0; j < populations.length; j++) {
                        const populationsJ = populations[j];
                        const { county, state } = populationsJ;
                        const countyState = CovidUtils_1.default.getCountyString(county, state);
                        if (combinedCounties.includes(countyState)) {
                            populations.splice(j, 1);
                            j--;
                            updates = true;
                            for (const index of ["population"]) {
                                populationsI[index] += populationsJ[index];
                            }
                        }
                    }
                    if (updates) {
                        combineCounties();
                        return;
                    }
                }
            }
        }());
        return Object.fromEntries(populations.map(({ county, state, population }) => [CovidUtils_1.default.getCountyString(county, state), population]));
    }
    static async getStates() {
        const xml = await xml2js_1.default.parseStringPromise(await text);
        const build = () => new xml2js_1.default.Builder().buildObject(xml);
        const states = [...(function* getAllCounties(parent) {
                for (const g of parent.g || []) {
                    yield* getAllCounties(g);
                }
                for (const { $ } of parent.path || []) {
                    const { id } = $;
                    const [, stateName, countyName] = id && id.match(/^([A-Z]{2})_([A-Za-z_\.\-]+)$/) || [];
                    if (stateName) {
                        yield new XmlCounty_1.default(stateName, countyName.replace(/ /g, "_"), $);
                    }
                }
            })(xml.svg)];
        return [states, build];
    }
    static async getRawDataForDate(date) {
        const fileContent = io_1.default.readFileSync(`${GIT_DIR}/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/${date}.csv`);
        const all = csvUtil_1.csvToObject(fileContent).flatMap(({ Country_Region, Admin2, Confirmed, Deaths, FIPS, Recovered, Active, Province_State }) => Country_Region && (+FIPS % 1000) ? {
            state: Province_State,
            county: Admin2,
            confirmed: +Confirmed,
            deaths: +Deaths,
            // recovered: +Recovered,
            // active: +Active
        } : []);
        return Object.fromEntries(all.map(({ county, state, confirmed, deaths }) => [CovidUtils_1.default.getCountyString(county, state), { confirmed, deaths }]));
    }
}
__decorate([
    cachable_1.cachable()
], DataLoader, "getPopulations", null);
__decorate([
    cachable_1.cachable()
], DataLoader, "getRawDataForDate", null);
exports.default = DataLoader;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGF0YUxvYWRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkRhdGFMb2FkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFDQSxvREFBMkI7QUFDM0IsbUVBQTJDO0FBQzNDLHFEQUFrRDtBQUNsRCwrQ0FBdUI7QUFDdkIsd0NBQXlDO0FBQ3pDLDhEQUFzQztBQUN0Qyw0REFBeUM7QUFHekMsTUFBTSxPQUFPLEdBQUcsR0FBRyxZQUFFLENBQUMsV0FBVyxTQUFTLENBQUM7QUFFM0MsTUFBTSxJQUFJLEdBQUcsWUFBRSxDQUFDLFFBQVEsQ0FBQyxHQUFHLFlBQUUsQ0FBQyxXQUFXLCtCQUErQixDQUFDLENBQUM7QUFDM0UsTUFBcUIsVUFBVTtJQUMzQixrRkFBa0Y7SUFHbEYsTUFBTSxDQUFDLEtBQUssQ0FBQyxjQUFjO1FBQ3ZCLE1BQU0sV0FBVyxHQUFHLE1BQU0sWUFBRSxDQUFDLFFBQVEsQ0FBQyxHQUFHLE9BQU8seUJBQXlCLENBQUMsQ0FBQztRQUMzRSxNQUFNLFdBQVcsR0FBRyxxQkFBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxFQUFFLEVBQUUsQ0FDekcsQ0FBQyxNQUFNLElBQUksT0FBTyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDM0IsSUFBSSxFQUFFLENBQUMsS0FBSyxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU07WUFDN0IsTUFBTSxFQUFFLE9BQU87WUFDZixLQUFLLEVBQUUsTUFBTTtZQUNiLFVBQVUsRUFBRSxDQUFDLGVBQWU7U0FDL0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUNULENBQUM7UUFFRixDQUFDLFNBQVMsZUFBZTtZQUNyQixLQUFLLE1BQU0sWUFBWSxJQUFJLFdBQVcsRUFBRTtnQkFDcEMsTUFBTSxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUMsR0FBRyxZQUFZLENBQUM7Z0JBQ3JDLE1BQU0sV0FBVyxHQUFHLG9CQUFVLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFFOUQsTUFBTSxnQkFBZ0IsR0FBRyxtQkFBYyxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN2RSxJQUFJLGdCQUFnQixFQUFFO29CQUNsQixJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUM7b0JBQ3BCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO3dCQUN6QyxNQUFNLFlBQVksR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3BDLE1BQU0sRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLEdBQUcsWUFBWSxDQUFDO3dCQUN2QyxNQUFNLFdBQVcsR0FBRyxvQkFBVSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7d0JBQzlELElBQUksZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQUFFOzRCQUN4QyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzs0QkFDekIsQ0FBQyxFQUFFLENBQUM7NEJBQ0osT0FBTyxHQUFHLElBQUksQ0FBQzs0QkFDZixLQUFLLE1BQU0sS0FBSyxJQUFJLENBQUMsWUFBWSxDQUFVLEVBQUU7Z0NBQ3pDLFlBQVksQ0FBQyxLQUFLLENBQUMsSUFBSSxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7NkJBQzlDO3lCQUNKO3FCQUNKO29CQUNELElBQUksT0FBTyxFQUFFO3dCQUNULGVBQWUsRUFBRSxDQUFDO3dCQUNsQixPQUFPO3FCQUNWO2lCQUNKO2FBQ0o7UUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRUwsT0FBTyxNQUFNLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLEVBQUUsRUFBRSxDQUN4RSxDQUFDLG9CQUFVLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsRUFBRSxVQUFVLENBQVUsQ0FBQyxDQUFDLENBQUM7SUFDM0UsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFLLENBQUMsU0FBUztRQUNsQixNQUFNLEdBQUcsR0FBRyxNQUFNLGdCQUFLLENBQUMsa0JBQWtCLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztRQUN2RCxNQUFNLEtBQUssR0FBRyxHQUFHLEVBQUUsQ0FBQyxJQUFJLGdCQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pELE1BQU0sTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDLFFBQVMsQ0FBQyxDQUFBLGNBQWMsQ0FBQyxNQUFXO2dCQUNwRCxLQUFLLE1BQU0sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO29CQUM1QixLQUFNLENBQUMsQ0FBQSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQzVCO2dCQUVELEtBQUssTUFBTSxFQUFDLENBQUMsRUFBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFO29CQUNqQyxNQUFNLEVBQUMsRUFBRSxFQUFDLEdBQTJCLENBQUMsQ0FBQztvQkFDdkMsTUFBTSxDQUFDLEVBQUUsU0FBUyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLElBQUksRUFBRSxDQUFDO29CQUN4RixJQUFJLFNBQVMsRUFBRTt3QkFDWCxNQUFNLElBQUksbUJBQVMsQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7cUJBQ3BFO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFYixPQUFPLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBVSxDQUFDO0lBQ3BDLENBQUM7SUFHRCxNQUFNLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLElBQVk7UUFDdkMsTUFBTSxXQUFXLEdBQUcsWUFBRSxDQUFDLFlBQVksQ0FBQyxHQUFHLE9BQU8sNERBQTRELElBQUksTUFBTSxDQUFDLENBQUM7UUFDdEgsTUFBTSxHQUFHLEdBQUcscUJBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxjQUFjLEVBQUUsRUFBRSxFQUFFLENBQ3BJLGNBQWMsSUFBSSxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvQixLQUFLLEVBQUUsY0FBYztZQUNyQixNQUFNLEVBQUUsTUFBTTtZQUNkLFNBQVMsRUFBRSxDQUFDLFNBQVM7WUFDckIsTUFBTSxFQUFFLENBQUMsTUFBTTtZQUNmLHlCQUF5QjtZQUN6QixrQkFBa0I7U0FDckIsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUNULENBQUM7UUFFRixPQUErQixNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsQ0FDL0YsQ0FBQyxvQkFBVSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUUsRUFBQyxTQUFTLEVBQUUsTUFBTSxFQUFDLENBQVUsQ0FBQyxDQUFDLENBQUM7SUFDcEYsQ0FBQztDQUNKO0FBakZHO0lBREMsbUJBQVEsRUFBRTtzQ0EyQ1Y7QUF1QkQ7SUFEQyxtQkFBUSxFQUFFO3lDQWdCVjtBQXBGTCw2QkFxRkMifQ==