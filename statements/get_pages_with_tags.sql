SELECT
 	p.page_namespace,
 	p.page_title,
 	r.rev_timestamp,
 	a.actor_name
FROM
	revision r
INNER JOIN 
	page p
ON
	p.page_id = r.rev_page
INNER JOIN 
	change_tag t
ON 
	r.rev_id = t.ct_rev_id
INNER JOIN 
	actor a
ON 
	r.rev_actor = a.actor_id
WHERE
	t.ct_tag_id in (??)
AND 
	r.rev_timestamp BETWEEN ? AND ?
