<?php

/**
 * Class to bypass OpenSSL shortcomings for some sites
 * @author magog
 *
 */
class Wget_File_Downloader implements File_Downloader {
    
    /**
     * 
     * @var string[]
     */
    private $opts = [];
    
    /**
     * 
     * @param array $opts
     */
    public function __construct(array $opts = []) {
        $this->opts = $opts;
    }

    public function download(string $url, string $dest): void {
        $cmd = "wget " . escapeshellarg($url);
        $opts = array_replace([], $this->opts, ["output-document" => $dest]);
        foreach ($opts as $key => $value) {
            if (! preg_match("/^[a-z][a-z-]+$/", $key)) {
                throw new IllegalArgumentException("Invalid wget key: $key");
            }
            $cmd .= " --$key=" . escapeshellarg($value);
        }
        Local_Io::ogrebotExec($cmd);
    }

    
}

