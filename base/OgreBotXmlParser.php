<?php
class OgreBotXmlParser {

	/**
	 * @throws XMLParserException
	 */
	public static function xmlFileToStruct(string $filename, bool $properties_directory = true): array {
		global $logger;
		
		if ($properties_directory) {
			$filename = BASE_DIRECTORY . "/properties/$filename";
		}
		
		if (@$logger) {
			$logger->debug("xmlFileToStruct($filename)");
		}
		
		$text = Local_Io::file_get_contents_ensure($filename, true);
		
		$parser = xml_parser_create('');
		
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 1);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, $text, $xmlValues);
		xml_parser_free($parser);
		
		if (!$xmlValues) {
			throw new XMLParserException("Can't parse file: $filename");
		}
		
		return self::parse($xmlValues);
	}

	private static function parse(array $data, int &$ptr = 0, int $level = 0): array {
		$xmlData = [];
		do {
			$next = $data[$ptr++];
			if ($next['type'] === 'close') {
				if ($level == $next['level']) {
					// we're done; pass it up the chain
					return $xmlData;
				}
			}
			
			//for HHVM
			if ($next['type'] === 'cdata') {
				continue;
			}
			
			$element = [];
			
			if ($next['type'] === 'open') {
				$element['elements'] = self::parse($data, $ptr, $next['level']);
			} else if ($next['type'] !== 'complete') {
				throw new AssertionFailureException("Unrecognized element type: $next[type]");
			}
			
			if (isset($next['attributes'])) {
				$element['attributes'] = $next['attributes'];
			}
			
			if (isset($next['value'])) {
				$element['value'] = String_Utils::mb_trim($next['value']);
			}
			
			$tag = $next['tag'];
			if (!isset($xmlData[$tag])) {
				$xmlData[$tag] = [];
			}
			$xmlData[$tag][] = $element;
		} while ($ptr < count($data));
		
		return $xmlData;
	}
}
?>
