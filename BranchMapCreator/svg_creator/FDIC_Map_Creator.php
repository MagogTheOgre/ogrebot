<?php
class FDIC_Map_Creator extends Latitude_Longitude_Svg_Creator {

	const STEP = 10000;

	/**
	 *
	 * @var int[]
	 */
	private array $certNumbers;

	/**
	 *
	 * @param int[] $certNumbers
	 */
	public function __construct(array $certNumbers) {
		$this->certNumbers = $certNumbers;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Latitude_Longitude_Svg_Creator::get_latitude_longitudes()
	 */
	public function get_latitude_longitudes(): Latitude_Longitude_Svg_Internal_Get_Result {
        $validator = Environment::get()->get_validator();
        $logger = Environment::get()->get_logger();
        $base_url = Environment::prop("constants", "fdic_map_creator.json_url");

		$http_cache_reader = new Http_Cache_Reader();
		$http_cache_reader->set_cache_time(SECONDS_PER_DAY * 7);

        /**
         * @var $latitude_longitudes Latitude_Longitude[]
         */
        $latitude_longitudes = [];

        /**
         * @var $zips int[]
         */
        $zips = [];

        /**
         * @var $zips string[]
         */
        $warnings = [];
		foreach ($this->certNumbers as $certNumber) {

			$i = 0;
			do {
                $logger->info("Count $i");
                $url = String_Utils::replace_named_variables($base_url,
                    ["certNumber" => $certNumber, "step" => $i], false);

				$string = $http_cache_reader->get_and_store_url($url);
				$json_data = json_decode($string, true);

				$json_results = $json_data["data"] ?? [];
				foreach ($json_results as ["data" => $json_result]) {
                    try {
                        $servTypeCd = Array_Utils::array_key_or_exception($json_result, 'SERVTYPE');
                        $latitude = Array_Utils::array_key_or_exception($json_result, 'LATITUDE');
                        $longitude = Array_Utils::array_key_or_exception($json_result, 'LONGITUDE');
                        $zip = +Array_Utils::array_key_or_exception($json_result, 'ZIP');

                        $validator->validate_arg($latitude, "numeric");
                        $validator->validate_arg($longitude, "numeric");
                        $validator->validate_arg($servTypeCd, "numeric");

                        if (in_array($servTypeCd, [11, 12, 16, 22, 23], false)) {
                            $lat_long = new Latitude_Longitude();
                            if ($latitude && $longitude) {
                                $lat_long->latitude = $latitude;
                                $lat_long->longitude = $longitude;
                                $lat_long->count = 1;
                                $latitude_longitudes[] = $lat_long;
                            } else if ($zip) {
                                $zips[] = $zip;
                            } else {
                                $address = $json_result["ADDRESS"];
                                $city = $json_result["CITY"];
                                $state = $json_result["STATE"];
                                $branchId = $json_result["UNINUM"];
                                $warnings[] = "Can't find lat/long/zip for $address, $city, $state (#$branchId)";
                            }
                        }
                    } catch (ArrayIndexNotFoundException | AssertionFailureException $e) {
                        $logger->error($e);
                    }
                }
                $count = $json_data["totals"]["count"] ?? 0;
                $i += self::STEP;
			} while ($i < $count);
		}

        $zips_result = new class ($zips) extends Zip_Code_Branch_Map_Creator {

            /**
             * @var int[]
             */
            private array $zips;

            /**
             * @param int[] $zips
             */
            public function __construct(array $zips) {
                $this->zips = $zips;
            }

            /**
             * @returns never
             */
            public static function get_arg_key(){
                throw new IllegalStateException("Unimplemented");
            }

            /**
             * @returns never
             */
            public static function load_from_request_args(array $args) {
                throw new IllegalStateException("Unimplemented");
            }

            protected function get_zips(): array {
                return $this->zips;
            }
        };

        $latitude_longitude_result = $zips_result->get_latitude_longitudes();
        $latitude_longitude_result->latitude_longitudes = array_merge($latitude_longitude_result->latitude_longitudes,
            $latitude_longitudes);
        $latitude_longitude_result->warnings = array_merge($latitude_longitude_result->warnings, $warnings);

        return $latitude_longitude_result;
	}

	public static function parse_from_string(string $string): self {
		$certNumbers = array_map(fn(string $s): int => +$s, preg_split("/[\,\s]+/",
            String_Utils::mb_trim($string)));
		return new self($certNumbers);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Latitude_Longitude_Svg_Creator::get_arg_key()
	 */
	public static function get_arg_key(): string {
		return "fdic";
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Latitude_Longitude_Svg_Creator::load_from_request_args()
	 */
	public static function load_from_request_args(array $args): self {
		return self::parse_from_string($args['fdic']);
	}
}