"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CATEGORY_PREFIX = exports.MediawikiUsername = exports.MediawikiApi = void 0;
var MediawikiApi;
(function (MediawikiApi) {
    MediawikiApi["COMMONS"] = "https://commons.wikimedia.org/w/api.php";
    MediawikiApi["EN_WIKI"] = "https://en.wikipedia.org/w/api.php";
    MediawikiApi["WIKIDATA"] = "https://www.wikidata.org/w/api.php";
})(MediawikiApi = exports.MediawikiApi || (exports.MediawikiApi = {}));
var MediawikiUsername;
(function (MediawikiUsername) {
    MediawikiUsername["OGREBOT"] = "OgreBot";
    MediawikiUsername["OGREBOT_2"] = "OgreBot_2";
})(MediawikiUsername = exports.MediawikiUsername || (exports.MediawikiUsername = {}));
exports.CATEGORY_PREFIX = "Category:";
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWVkaWF3aWtpLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiTWVkaWF3aWtpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLElBQVksWUFJWDtBQUpELFdBQVksWUFBWTtJQUNwQixtRUFBbUQsQ0FBQTtJQUNuRCw4REFBOEMsQ0FBQTtJQUM5QywrREFBK0MsQ0FBQTtBQUNuRCxDQUFDLEVBSlcsWUFBWSxHQUFaLG9CQUFZLEtBQVosb0JBQVksUUFJdkI7QUFFRCxJQUFZLGlCQUdYO0FBSEQsV0FBWSxpQkFBaUI7SUFDekIsd0NBQW1CLENBQUE7SUFDbkIsNENBQXVCLENBQUE7QUFDM0IsQ0FBQyxFQUhXLGlCQUFpQixHQUFqQix5QkFBaUIsS0FBakIseUJBQWlCLFFBRzVCO0FBUVksUUFBQSxlQUFlLEdBQUcsV0FBVyxDQUFDIn0=