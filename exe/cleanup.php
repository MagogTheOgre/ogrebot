<?php

require_once __DIR__ . "/../base/bootstrap.php";

$env = Environment::get();
$argv = $env->load_command_line_args();
$logger = $env->get_logger();
$wiki_interface = $env->get_wiki_interface();
$database_dao = $env->get_database_dao();

$endTime = $argv[1] ?? date('Ymd235959', time() - SECONDS_PER_DAY * 2 - date("Z"));
$startTime = $argv[2] ?? date('Ymd000000', strtotime($endTime));
$importsOnly = Array_Utils::find_command_line_arg($argv, "imports-only") !== null;

$logger->debug("Querying recent changes (Step 1) - $startTime to $endTime");
try {
    $files = array_keys($wiki_interface->get_file_imports("commons.wikimedia", $startTime, 
        $endTime));
    
    $co2 = $wiki_interface->new_wiki( "OgreBot_2Commons" );
    if (!$importsOnly) {
        $files = array_merge($files, preg_replace("/^File:", "", array_keys(
            $wiki_interface->new_files($co2, $endTime, $startTime))));
    }
    
    $count = count($files);
    $logger->debug("$count files found. Querying page content (Step 2)");
    $pageCategories = $database_dao->get_visible_categories("commons.wikimedia", 6, $files);
    $pageTexts = $wiki_interface->get_text($co2, String_Utils::str_prepend($files, "File:"));
    $auto_editor = new Cleanup_Auto_Editor($co2, "new upload");
} catch (Exception $e) {
    Remote_Io::ogrebotMail($e);
    throw $e;
}

$logger->debug("Analyzing text and performing edits (Step 3)");
$timelapse=0;
$i=0;

foreach($files as $title) {
    $fullTitle = "File:$title";
    $text = $pageTexts[$fullTitle]?->text;
    //mediawiki bug or page deleted
    if (!$text) {
        $logger->error("$fullTitle doesn't exist. Was it moved/deleted during the query?");
        continue;
    }
    
    $now = time();
    $i++;
    if ($now - $timelapse > 4) {
        $logger->debug((((int)($i * 1000 / count($pageTexts))) / 10) . "% complete "
            . "($i processed)");
        $timelapse = time();
    }
    
    do {
        $retry = false;
        try {
            $auto_editor->process($fullTitle, $text, $pageCategories[$title]);
        } catch (EditConflictException $e) {
            $logger->warn($e);
            $retry = true;
        } catch (Exception $e) {
            Remote_Io::ogrebotMail($e);
        }
    } while ($retry);
}

$logger->debug("Cleanup complete.");

