<?php

/**
 * 
 * @author magog
 *
 */
abstract class AbstractCronRunner implements CronRunner {

	/**
	 * 
	 * @var CronDate
	 */
	protected $cronDate;
	
	/**
	 * 
	 * @var CronConfig
	 */
	protected $runConfig;
	
	/**
	 * 
	 * @var bool
	 */
	private $hasRun = false;

	/**
	 * 
	 * @return CronDate
	 */
	public function getCronDate() {
		return $this->cronDate;
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see CronRunner::getRunConfig()
	 */
	public function getRunConfig(): CronConfig {
		return $this->runConfig;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function getHasRun(){
		return $this->hasRun;
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see CronRunner::setCronDate()
	 */
	public function setCronDate(CronDate $cronDate): void {
		global $logger;
				
		$this->cronDate = $cronDate;
		$logger->trace($cronDate);
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see CronRunner::setRunConfig()
	 */
	public function setRunConfig(CronConfig $runConfig): void {
		global $logger;

		$this->runConfig = $runConfig;
		$logger->trace($runConfig);
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see CronRunner::run()
	 */
	public function run(): void {
		global $logger;
		
		$logger->debug($this);
		
		if ($this->hasRun) {
			throw new IllegalStateException("Already ran this cron job once.");
		}
		
		if ($this->runConfig === null) {
			throw new IllegalStateException("RunConfig not set.");
		}
		
		$this->setUpVariables();
		
		$command = $this->getExecCommand();
		
		Local_Io::ogrebotExec($command);
		
		$post = $this->runConfig->getPost();
		
		if ($post !== null) {
		    Local_Io::ogrebotExec($post);
		}
	}
	
	/**
	 * 
	 * @return string
	 */
	public function __toString() {
		$cronDateString = $this->cronDate === null? "[date not set]" : $this->cronDate->__toString();
		$configString = $this->runConfig === null? "[config not set]" : $this->runConfig->__toString();
		
		return "$cronDateString $configString";
	}

	/**
	 * @return void
	 */
	private function setUpVariables() {
		global $logger;
		
		$oldArgs = $this->runConfig->getArgs();
		$logger->trace("Old Args: $oldArgs");
        if ($this->cronDate !== null) {
            $newArgs = String_Utils::replace_named_variables($this->runConfig->getArgs(), [
                "year" => $this->cronDate->getYear(),
                "month" => $this->cronDate->getMonth(),
                "day" => $this->cronDate->getDay(),
                "hour" => $this->cronDate->getHour()
            ]);
        } else {
            $newArgs = $oldArgs;
        }
		
		$logger->trace("New Args: $newArgs");
		
		$this->runConfig->setArgs(String_Utils::mb_trim($newArgs));
		$this->runConfig->setDirectory(String_Utils::mb_trim($this->runConfig->getDirectory()));
		$this->runConfig->setCommand(String_Utils::mb_trim($this->runConfig->getCommand()));
	}
	
	/**
	 * @return string
	 */
	protected abstract function getExecCommand();

}