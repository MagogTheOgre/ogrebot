<?php
require_once __DIR__ . "/../base/bootstrap.php";

global $env;
$http_io = new Http_Io();
$http_io->ob_start();

$request_args = $env->get_request_args();

$action = match ($request_args['type'] ?? null) {
	"report" => "UpdateNewUploads.php",
	"uploads" => "UpdateUploadReport.php",
	default => die("Page cannot be loaded directly")
}
?>
<!DOCTYPE html>
<html>
<head>
<?php $http_io->transcludeScriptRemote('bootstrap.css.url'); ?>
</head>
<body>
	<form method="post" action="<?= $action ?>">
		<input type="hidden" name="project" value="<?= String_Utils::sanitize($request_args['project']) ?>" />
		<input type="hidden" name="start" value="<?= String_Utils::sanitize($request_args['start']) ?>" />
		<input type="submit" class="btn btn-primary" value="Click here to update" />	
	</form>
</body>
</html>
