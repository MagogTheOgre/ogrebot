"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.styleToString = exports.stringHash = exports.sortCaseInsensitive = exports.matchAll = void 0;
function* matchAllPolyfill(regexp, string) {
    let match;
    var regexpClone = new RegExp(regexp);
    while (match = regexpClone.exec(string)) {
        yield match;
    }
}
function mapStyleString(string) {
    if (string.includes("'")) {
        throw new Error(`Unsupported character in string ${string}`);
    }
    return string.replace(/^([\S\s]+ ^[\S\s]+)$/, "'$1'");
}
exports.matchAll = RegExp.prototype.matchAll ? (regexp, string) => regexp.matchAll(string) : matchAllPolyfill;
function sortCaseInsensitive(a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
}
exports.sortCaseInsensitive = sortCaseInsensitive;
function stringHash(input) {
    return String(input).split('').reduce((prevHash, currVal) => (((prevHash << 5) - prevHash) + currVal.charCodeAt(0)) | 0, 0);
}
exports.stringHash = stringHash;
function styleToString(style) {
    return style ? Object.entries(style).map(([key, val]) => `${key}: ${mapStyleString(val)}`).join("; ") : null;
}
exports.styleToString = styleToString;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RyaW5nVXRpbHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdHJpbmdVdGlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFPQSxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBRSxNQUFjLEVBQUUsTUFBYztJQUN0RCxJQUFJLEtBQUssQ0FBQztJQUNWLElBQUksV0FBVyxHQUFHLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3JDLE9BQU8sS0FBSyxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7UUFDckMsTUFBTSxLQUFLLENBQUM7S0FDZjtBQUNMLENBQUM7QUFFRCxTQUFTLGNBQWMsQ0FBQyxNQUFjO0lBQ2xDLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtRQUN0QixNQUFNLElBQUksS0FBSyxDQUFDLG1DQUFtQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0tBQ2hFO0lBQ0QsT0FBTyxNQUFNLENBQUMsT0FBTyxDQUFDLHNCQUFzQixFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQzFELENBQUM7QUFFWSxRQUFBLFFBQVEsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFjLEVBQUUsTUFBYyxFQUFFLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQztBQUVwSSxTQUFnQixtQkFBbUIsQ0FBQyxDQUFTLEVBQUUsQ0FBUztJQUNwRCxPQUFPLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7QUFDMUQsQ0FBQztBQUZELGtEQUVDO0FBRUQsU0FBZ0IsVUFBVSxDQUFDLEtBQVU7SUFDakMsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUMxRCxDQUFDLENBQUMsQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztBQUNuRSxDQUFDO0FBSEQsZ0NBR0M7QUFFRCxTQUFnQixhQUFhLENBQUMsS0FBb0M7SUFDOUQsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsR0FBRyxLQUFLLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7QUFDakgsQ0FBQztBQUZELHNDQUVDIn0=