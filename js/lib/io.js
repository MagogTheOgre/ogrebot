"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const concat_stream_1 = __importDefault(require("concat-stream"));
const yauzl_promise_1 = __importDefault(require("yauzl-promise"));
const fs_1 = __importDefault(require("fs"));
const stringUtils_1 = require("./stringUtils");
const os_1 = require("os");
const cachable_1 = require("./decorators/cachable");
const util_1 = require("util");
const child_process_1 = require("child_process");
const canonical_path_1 = require("canonical-path");
class Io {
    static readDir = fs_1.default.readdirSync.bind(fs_1.default);
    static EOL = os_1.EOL;
    static PROJECT_DIR = (0, canonical_path_1.normalize)(`${__dirname}/../..`);
    static TMP_DIR;
    static tempIndex = 0;
    /**
     * Get a path relative to the project directory
     * @param path
     */
    static path(path) {
        return (0, canonical_path_1.normalize)(`${this.PROJECT_DIR}/${path}`);
    }
    static getProperties(file) {
        const thisProperties = new Map();
        const contents = fs_1.default.readFileSync(`${this.PROJECT_DIR}/properties/${file}.properties`, { encoding: "utf8" });
        for (const [, key, val] of (0, stringUtils_1.matchAll)(/^\s*(.+?)\s*\=\s*"?(.+)"\s*?$/gm, contents)) {
            thisProperties.set(key, val);
        }
        return thisProperties;
    }
    static runCommandLowPriority(command, cwd) {
        return (0, util_1.promisify)(child_process_1.exec)(process.platform === "win32" ? `start /low /b ${command}`
            : `nice ${command}`, { cwd });
    }
    static getProperty(file, property) {
        return this.getProperties(file).get(property);
    }
    static writeFile(filename, data, options = {}) {
        return new Promise((resolve, reject) => {
            fs_1.default.writeFile(filename, data, options, function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data.toString());
                }
            });
        });
    }
    static readFile(filename) {
        return new Promise((resolve, reject) => {
            fs_1.default.readFile(filename, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data.toString());
                }
            });
        });
    }
    static readFileSync(filename) {
        return fs_1.default.readFileSync(filename, "utf8");
    }
    static readFileSyncSuppress(filename) {
        try {
            return fs_1.default.readFileSync(filename, "utf8");
        }
        catch (e) {
            console.warn(e);
            return null;
        }
    }
    static existsSync = fs_1.default.existsSync;
    static mkdirSync = fs_1.default.mkdirSync;
    static get tempDir() {
        if (!this.TMP_DIR) {
            const tempDir = this.path(`tmp/node-${process.pid}`);
            if (!this.existsSync(tempDir)) {
                this.mkdirSync(tempDir);
            }
            this.TMP_DIR = tempDir;
            console.log(`Temp directory : ${tempDir}`);
        }
        return this.TMP_DIR;
    }
    static get tempFile() {
        return `${this.tempDir}/temp-${this.tempIndex++}`;
    }
    static removeTempFiles() {
        if (this.TMP_DIR) {
            (fs_1.default.rmSync || (0, util_1.promisify)(fs_1.default.rmdir))(this.TMP_DIR, {
                recursive: true
            });
        }
    }
    static exec = (0, util_1.promisify)(child_process_1.exec);
    static async readFileFromZip(path, fileToRead) {
        const zipFile = await yauzl_promise_1.default.open(path);
        try {
            while (true) {
                const entry = await zipFile.readEntry();
                if (entry.fileName === fileToRead) {
                    const stream = await entry.openReadStream();
                    try {
                        return await new Promise(async (resolve, reject) => {
                            stream.pipe((0, concat_stream_1.default)(resolve));
                            stream.on("error", reject);
                        });
                    }
                    finally {
                        stream.destroy();
                    }
                }
            }
        }
        finally {
            await zipFile.close();
        }
    }
}
__decorate([
    (0, cachable_1.cachable)()
], Io, "getProperties", null);
exports.default = Io;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLGtFQUFvQztBQUNwQyxrRUFBa0M7QUFDbEMsNENBQW9CO0FBQ3BCLCtDQUF5QztBQUN6QywyQkFBeUI7QUFDekIsb0RBQWlEO0FBQ2pELCtCQUFpQztBQUNqQyxpREFBb0M7QUFDcEMsbURBQTJDO0FBRzNDLE1BQXFCLEVBQUU7SUFFWixNQUFNLENBQVUsT0FBTyxHQUFHLFlBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQUUsQ0FBQyxDQUFDO0lBRWxELE1BQU0sQ0FBVSxHQUFHLEdBQUcsUUFBRyxDQUFDO0lBRTFCLE1BQU0sQ0FBVSxXQUFXLEdBQUksSUFBQSwwQkFBUyxFQUFDLEdBQUcsU0FBUyxRQUFRLENBQUMsQ0FBQztJQUU5RCxNQUFNLENBQUMsT0FBTyxDQUFVO0lBRXhCLE1BQU0sQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO0lBRTdCOzs7T0FHRztJQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBWTtRQUMzQixPQUFPLElBQUEsMEJBQVMsRUFBQyxHQUFHLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQTtJQUNuRCxDQUFDO0lBR08sTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFZO1FBQ3JDLE1BQU0sY0FBYyxHQUFHLElBQUksR0FBRyxFQUFrQixDQUFDO1FBQ2pELE1BQU0sUUFBUSxHQUFHLFlBQUUsQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxlQUFlLElBQUksYUFBYSxFQUFFLEVBQUMsUUFBUSxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUM7UUFDMUcsS0FBSyxNQUFNLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLElBQUksSUFBQSxzQkFBUSxFQUFDLGlDQUFpQyxFQUFFLFFBQVEsQ0FBQyxFQUFFO1lBQzlFLGNBQWMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ2hDO1FBQ0QsT0FBTyxjQUFjLENBQUM7SUFFMUIsQ0FBQztJQUVNLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxPQUFlLEVBQUUsR0FBWTtRQUM3RCxPQUFPLElBQUEsZ0JBQVMsRUFBQyxvQkFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixPQUFPLEVBQUU7WUFDaEYsQ0FBQyxDQUFDLFFBQVEsT0FBTyxFQUFFLEVBQUUsRUFBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFTSxNQUFNLENBQUMsV0FBVyxDQUFDLElBQVksRUFBRSxRQUFnQjtRQUNwRCxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFTSxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQWdCLEVBQUUsSUFBcUMsRUFBRSxVQUErQixFQUFFO1FBQzlHLE9BQU8sSUFBSSxPQUFPLENBQVMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDM0MsWUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxVQUFTLEdBQWlDO2dCQUM1RSxJQUFJLEdBQUcsRUFBRTtvQkFDTCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ2Y7cUJBQU07b0JBQ0gsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO2lCQUM1QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFnQjtRQUNuQyxPQUFPLElBQUksT0FBTyxDQUFTLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQzNDLFlBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLFVBQVMsR0FBRyxFQUFFLElBQUk7Z0JBQ3BDLElBQUksR0FBRyxFQUFFO29CQUNMLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDZjtxQkFBTTtvQkFDSCxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7aUJBQzVCO1lBQ0wsQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDTSxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQWdCO1FBQ3ZDLE9BQU8sWUFBRSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUNNLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxRQUFnQjtRQUMvQyxJQUFJO1lBQ0EsT0FBTyxZQUFFLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztTQUM1QztRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoQixPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0wsQ0FBQztJQUVNLE1BQU0sQ0FBQyxVQUFVLEdBQUcsWUFBRSxDQUFDLFVBQVUsQ0FBQztJQUVsQyxNQUFNLENBQUMsU0FBUyxHQUFHLFlBQUUsQ0FBQyxTQUFTLENBQUM7SUFFaEMsTUFBTSxLQUFLLE9BQU87UUFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDM0I7WUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztZQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixPQUFPLEVBQUUsQ0FBQyxDQUFDO1NBQzlDO1FBQ0QsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFFTSxNQUFNLEtBQUssUUFBUTtRQUN0QixPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sU0FBUyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQztJQUN0RCxDQUFDO0lBRU0sTUFBTSxDQUFDLGVBQWU7UUFDekIsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2QsQ0FBQyxZQUFFLENBQUMsTUFBTSxJQUFJLElBQUEsZ0JBQVMsRUFBQyxZQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUM3QyxTQUFTLEVBQUUsSUFBSTthQUNsQixDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUEsZ0JBQVMsRUFBQyxvQkFBSSxDQUFDLENBQUM7SUFFOUIsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBWSxFQUFFLFVBQWtCO1FBQ2hFLE1BQU0sT0FBTyxHQUFHLE1BQU0sdUJBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdkMsSUFBSTtZQUNBLE9BQU8sSUFBSSxFQUFFO2dCQUNULE1BQU0sS0FBSyxHQUFHLE1BQU0sT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUN4QyxJQUFJLEtBQUssQ0FBQyxRQUFRLEtBQUssVUFBVSxFQUFFO29CQUUvQixNQUFNLE1BQU0sR0FBRyxNQUFNLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDNUMsSUFBSTt3QkFDQSxPQUFPLE1BQU0sSUFBSSxPQUFPLENBQVMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTs0QkFDdkQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFBLHVCQUFNLEVBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFDN0IsTUFBTSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBQy9CLENBQUMsQ0FBQyxDQUFDO3FCQUNOOzRCQUFTO3dCQUNOLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztxQkFDcEI7aUJBQ0o7YUFDSjtTQUNKO2dCQUFTO1lBQ04sTUFBTSxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDekI7SUFDTCxDQUFDOztBQTFHRDtJQURDLElBQUEsbUJBQVEsR0FBRTs2QkFTVjtBQTdCTCxxQkFnSUMifQ==