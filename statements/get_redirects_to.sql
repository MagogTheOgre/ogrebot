SELECT
    p.page_title,
    r.rd_namespace,
    r.rd_title
FROM
    page p
INNER JOIN
    redirect r
ON
    p.page_id = r.rd_from
WHERE
    p.page_namespace = ?
AND
    p.page_title in (??)
