<?php

/**
 *
 * @author magog
 *
 */
class SQLException extends BaseException {

    /**
     * SQLException constructor.
     * @param $message
     * @param Exception|null $chained
     */
    public function __construct($message, Exception $chained = null) {
        parent::__construct($message, $chained, true);
    }
}