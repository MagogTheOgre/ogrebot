"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.diffElectionResultColorer = exports.standardElectionResultColorer = void 0;
const allColorsUsed = new Set();
const electionColors = {
    d: {
        0: "#d3e7ff",
        1: "#d3e7ff",
        2: "#d3e7ff",
        3: "#d3e7ff",
        4: "#b9d7ff",
        5: "#86b6f2",
        6: "#4389e3",
        7: "#1666cb",
        8: "#0645b4",
        9: "#0645b4",
        10: "#0645b4"
    },
    r: {
        0: "#ffccd0",
        1: "#ffccd0",
        2: "#ffccd0",
        3: "#ffccd0",
        4: "#f2b3be",
        5: "#e27f90",
        6: "#cc2f4a",
        7: "#d40000",
        8: "#aa0000",
        9: "#800000",
        10: "#800000"
    },
    t: {
        0: "#888",
        1: "#888",
        2: "#888",
        3: "#888",
        4: "#888",
        5: "#888",
        6: "#888",
        7: "#888",
        8: "#888",
        9: "#888",
        10: "#888"
    },
    i: {
        0: "",
        1: "",
        2: "",
        3: "",
        4: "",
        5: "",
        6: "",
        7: "",
        8: "",
        9: "",
        10: ""
    }
};
const standardElectionResultColorer = ({ percent, party }) => {
    const roundedPercent = Math.floor(percent * 10);
    const color = electionColors[party][roundedPercent];
    if (!allColorsUsed.has(`${party}${roundedPercent}`)) {
        console.log(`${party}${roundedPercent}0: ${color}`);
        allColorsUsed.add(`${party}${roundedPercent}`);
    }
    return color;
};
exports.standardElectionResultColorer = standardElectionResultColorer;
const diffElectionResultColorer = ({ percent, party, complete }) => {
    if (complete) {
        if (party === "d") {
            if (percent >= .3) {
                return "#00a1c8";
            }
            if (percent >= .25) {
                return "#00bdec";
            }
            if (percent >= .2) {
                return "#09ceff";
            }
            if (percent >= .15) {
                return "#4bdbff";
            }
            if (percent >= .1) {
                return "#77e3ff";
            }
            if (percent >= .05) {
                return "#AAEEFF";
            }
            if (percent > .02) {
                return "#D5F6FF";
            }
        }
        else {
            if (percent >= .3) {
                return "#D40000";
            }
            if (percent >= .25) {
                return "#FF0000";
            }
            if (percent >= .2) {
                return "#FF2A2A";
            }
            if (percent >= .15) {
                return "#FF5555";
            }
            if (percent >= .1) {
                return "#FF8080";
            }
            if (percent >= .05) {
                return "#FFAAAA";
            }
            if (percent > .02) {
                return "#FFD5D5";
            }
        }
        return "#eee";
    }
    return "#bbbbbb";
};
exports.diffElectionResultColorer = diffElectionResultColorer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRWxlY3Rpb25SZXN1bHRzQ29sb3Jlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkVsZWN0aW9uUmVzdWx0c0NvbG9yZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBRUEsTUFBTSxhQUFhLEdBQUcsSUFBSSxHQUFHLEVBQVUsQ0FBQztBQUl4QyxNQUFNLGNBQWMsR0FBRztJQUNuQixDQUFDLEVBQUU7UUFDQyxDQUFDLEVBQUUsU0FBUztRQUNaLENBQUMsRUFBRSxTQUFTO1FBQ1osQ0FBQyxFQUFFLFNBQVM7UUFDWixDQUFDLEVBQUUsU0FBUztRQUNaLENBQUMsRUFBRSxTQUFTO1FBQ1osQ0FBQyxFQUFFLFNBQVM7UUFDWixDQUFDLEVBQUUsU0FBUztRQUNaLENBQUMsRUFBRSxTQUFTO1FBQ1osQ0FBQyxFQUFFLFNBQVM7UUFDWixDQUFDLEVBQUUsU0FBUztRQUNaLEVBQUUsRUFBRSxTQUFTO0tBQ1A7SUFDVixDQUFDLEVBQUU7UUFDQyxDQUFDLEVBQUUsU0FBUztRQUNaLENBQUMsRUFBRSxTQUFTO1FBQ1osQ0FBQyxFQUFFLFNBQVM7UUFDWixDQUFDLEVBQUUsU0FBUztRQUNaLENBQUMsRUFBRSxTQUFTO1FBQ1osQ0FBQyxFQUFFLFNBQVM7UUFDWixDQUFDLEVBQUUsU0FBUztRQUNaLENBQUMsRUFBRSxTQUFTO1FBQ1osQ0FBQyxFQUFFLFNBQVM7UUFDWixDQUFDLEVBQUUsU0FBUztRQUNaLEVBQUUsRUFBRSxTQUFTO0tBQ1A7SUFDVixDQUFDLEVBQUU7UUFDQyxDQUFDLEVBQUUsTUFBTTtRQUNULENBQUMsRUFBRSxNQUFNO1FBQ1QsQ0FBQyxFQUFFLE1BQU07UUFDVCxDQUFDLEVBQUUsTUFBTTtRQUNULENBQUMsRUFBRSxNQUFNO1FBQ1QsQ0FBQyxFQUFFLE1BQU07UUFDVCxDQUFDLEVBQUUsTUFBTTtRQUNULENBQUMsRUFBRSxNQUFNO1FBQ1QsQ0FBQyxFQUFFLE1BQU07UUFDVCxDQUFDLEVBQUUsTUFBTTtRQUNULEVBQUUsRUFBRSxNQUFNO0tBQ0o7SUFDVixDQUFDLEVBQUU7UUFDQyxDQUFDLEVBQUUsRUFBRTtRQUNMLENBQUMsRUFBRSxFQUFFO1FBQ0wsQ0FBQyxFQUFFLEVBQUU7UUFDTCxDQUFDLEVBQUUsRUFBRTtRQUNMLENBQUMsRUFBRSxFQUFFO1FBQ0wsQ0FBQyxFQUFFLEVBQUU7UUFDTCxDQUFDLEVBQUUsRUFBRTtRQUNMLENBQUMsRUFBRSxFQUFFO1FBQ0wsQ0FBQyxFQUFFLEVBQUU7UUFDTCxDQUFDLEVBQUUsRUFBRTtRQUNMLEVBQUUsRUFBRSxFQUFFO0tBQ0E7Q0FDSixDQUFDO0FBRUosTUFBTSw2QkFBNkIsR0FBRyxDQUFDLEVBQUMsT0FBTyxFQUFFLEtBQUssRUFBaUIsRUFBRSxFQUFFO0lBQzlFLE1BQU0sY0FBYyxHQUErQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQztJQUM1RixNQUFNLEtBQUssR0FBRyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUM7SUFFcEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLEdBQUcsY0FBYyxFQUFFLENBQUMsRUFBRTtRQUNqRCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxHQUFHLGNBQWMsTUFBTSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBQ3BELGFBQWEsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLEdBQUcsY0FBYyxFQUFFLENBQUMsQ0FBQztLQUNsRDtJQUNELE9BQU8sS0FBSyxDQUFDO0FBQ2pCLENBQUMsQ0FBQTtBQVRZLFFBQUEsNkJBQTZCLGlDQVN6QztBQUVNLE1BQU0seUJBQXlCLEdBQUcsQ0FBQyxFQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFpQixFQUFFLEVBQUU7SUFDcEYsSUFBSSxRQUFRLEVBQUU7UUFDVixJQUFJLEtBQUssS0FBSyxHQUFHLEVBQUU7WUFDZixJQUFJLE9BQU8sSUFBSSxFQUFFLEVBQUU7Z0JBQ2YsT0FBTyxTQUFTLENBQUM7YUFDcEI7WUFDRCxJQUFJLE9BQU8sSUFBSSxHQUFHLEVBQUU7Z0JBQ2hCLE9BQU8sU0FBUyxDQUFDO2FBQ3BCO1lBQ0QsSUFBSSxPQUFPLElBQUksRUFBRSxFQUFFO2dCQUNmLE9BQU8sU0FBUyxDQUFDO2FBQ3BCO1lBQ0QsSUFBSSxPQUFPLElBQUksR0FBRyxFQUFFO2dCQUNoQixPQUFPLFNBQVMsQ0FBQzthQUNwQjtZQUNELElBQUksT0FBTyxJQUFJLEVBQUUsRUFBRTtnQkFDZixPQUFPLFNBQVMsQ0FBQzthQUNwQjtZQUNELElBQUksT0FBTyxJQUFJLEdBQUcsRUFBRTtnQkFDaEIsT0FBTyxTQUFTLENBQUM7YUFDcEI7WUFDRCxJQUFJLE9BQU8sR0FBRyxHQUFHLEVBQUU7Z0JBQ2YsT0FBTyxTQUFTLENBQUM7YUFDcEI7U0FDSjthQUFNO1lBQ0gsSUFBSSxPQUFPLElBQUksRUFBRSxFQUFFO2dCQUNmLE9BQU8sU0FBUyxDQUFDO2FBQ3BCO1lBQ0QsSUFBSSxPQUFPLElBQUksR0FBRyxFQUFFO2dCQUNoQixPQUFPLFNBQVMsQ0FBQzthQUNwQjtZQUNELElBQUksT0FBTyxJQUFJLEVBQUUsRUFBRTtnQkFDZixPQUFPLFNBQVMsQ0FBQzthQUNwQjtZQUNELElBQUksT0FBTyxJQUFJLEdBQUcsRUFBRTtnQkFDaEIsT0FBTyxTQUFTLENBQUM7YUFDcEI7WUFDRCxJQUFJLE9BQU8sSUFBSSxFQUFFLEVBQUU7Z0JBQ2YsT0FBTyxTQUFTLENBQUM7YUFDcEI7WUFDRCxJQUFJLE9BQU8sSUFBSSxHQUFHLEVBQUU7Z0JBQ2hCLE9BQU8sU0FBUyxDQUFDO2FBQ3BCO1lBQ0QsSUFBSSxPQUFPLEdBQUcsR0FBRyxFQUFFO2dCQUNmLE9BQU8sU0FBUyxDQUFDO2FBQ3BCO1NBQ0o7UUFDRCxPQUFPLE1BQU0sQ0FBQztLQUNqQjtJQUNELE9BQU8sU0FBUyxDQUFDO0FBQ3JCLENBQUMsQ0FBQTtBQWxEWSxRQUFBLHlCQUF5Qiw2QkFrRHJDIn0=