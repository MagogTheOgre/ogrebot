"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ElectionIncompleteResult = exports.ElectionResultImpl = void 0;
class ElectionResultImpl {
    results;
    constructor(results) {
        this.results = results;
    }
    get party() {
        const { r, d } = this.results;
        return r > d ? "r" : d > r ? "d" : "t";
    }
    get percent() {
        const { party } = this;
        return party !== "t" ? this.results[party] : this.results.r;
    }
    get complete() {
        const { r, d } = this.results;
        return r !== 0 || d !== 0;
    }
}
exports.ElectionResultImpl = ElectionResultImpl;
class ElectionIncompleteResult {
    get party() {
        return "t";
    }
    get percent() {
        return 0;
    }
    get results() {
        return {
            r: 0,
            d: 0
        };
    }
    get complete() {
        return false;
    }
}
exports.ElectionIncompleteResult = ElectionIncompleteResult;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRWxlY3Rpb25SZXN1bHQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJFbGVjdGlvblJlc3VsdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFVQSxNQUFhLGtCQUFrQjtJQUNDO0lBQTVCLFlBQTRCLE9BQWtDO1FBQWxDLFlBQU8sR0FBUCxPQUFPLENBQTJCO0lBRTlELENBQUM7SUFDRCxJQUFJLEtBQUs7UUFDTCxNQUFNLEVBQUMsQ0FBQyxFQUFFLENBQUMsRUFBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBQzNDLENBQUM7SUFFRCxJQUFJLE9BQU87UUFDUCxNQUFNLEVBQUMsS0FBSyxFQUFDLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLE9BQU8sS0FBSyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUVELElBQUksUUFBUTtRQUNSLE1BQU0sRUFBQyxDQUFDLEVBQUUsQ0FBQyxFQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUM1QixPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM5QixDQUFDO0NBRUo7QUFuQkQsZ0RBbUJDO0FBRUQsTUFBYSx3QkFBd0I7SUFFakMsSUFBSSxLQUFLO1FBQ0wsT0FBTyxHQUFZLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQUksT0FBTztRQUNQLE9BQU8sQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVELElBQUksT0FBTztRQUNQLE9BQU87WUFDSCxDQUFDLEVBQUUsQ0FBQztZQUNKLENBQUMsRUFBRSxDQUFDO1NBQ1AsQ0FBQztJQUNOLENBQUM7SUFFRCxJQUFJLFFBQVE7UUFDUixPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0NBRUo7QUFyQkQsNERBcUJDIn0=